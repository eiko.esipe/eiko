package com.eiko;

import android.content.Context;

import com.eiko.geolocalization.Position;

import com.eiko.DataObject.DisplayableData;
import com.eiko.matrix.ARObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.eiko.database.Database.getAllObjectsAtDistanceByType;
import static com.eiko.matrix.ARObjectFactory.createARObject;

/**
 * Created by eikouser on 19/02/18.
 */

public class ARObjectProvider {

    /**
     * Get DisplayableData from database.
     * @param position : position where you want to get data
     * @param radius : radius in meters for disk of data
     * @param context : Current context of the activity
     * @return Map of String and List of DisplayableData.
     */
    public static Map<String,List<? extends DisplayableData>> getFromDatabase(Position position, int radius, Context context){
        return getAllObjectsAtDistanceByType(position,radius,context);
    }

    /**
     * Convert DisplayableData to ARObject
     * @param datas : Map of String and List of DisplayableData to be converted.
     * @param bearing : Angle between north and device.
     * @param devicePosition : GPS position of device.
     * @return Map of String and List of ARObject make with data
     */
    public static Map<String,List<ARObject>> dataToARObject(Map<String,List<? extends DisplayableData>> datas, double bearing, Position devicePosition){
        HashMap<String, List<ARObject>> mARObject = new HashMap<>();
        for(String type : datas.keySet()){
            mARObject.put(type,new ArrayList<>());
            for(DisplayableData data : datas.get(type)){
                mARObject.get(type).add(createARObject(devicePosition,bearing,data.getGeom().getLatitude(),data.getGeom().getLongitude(),data));
            }
        }
        return mARObject;
    }
}
