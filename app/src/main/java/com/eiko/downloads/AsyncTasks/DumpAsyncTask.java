package com.eiko.downloads.AsyncTasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.eiko.DataObject.DisplayableData;
import com.eiko.DataObject.Perimeter;
import com.eiko.ObserverObservable.Observable;
import com.eiko.ObserverObservable.Observer;
import com.eiko.PropertiesReader;
import com.eiko.database.Database;
import com.eiko.downloads.DataDownloadedParser;
import com.eiko.downloads.DataDownloader;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rinkui on 28/02/2018.
 */

public class DumpAsyncTask extends AsyncTask<Perimeter, Void, String> implements Observable {
    private PropertiesReader pr;
    private String address;
    private String suffix;
    private Context context;
    private List<Observer> observers = new ArrayList<>();

    /**
     * Connect to the AFF's server and send the request.
     * http://server.ip:server.port/eiko/[]&password
     * @param requestBody request body = []
     * @return
     * @throws IOException
     */
    public HttpURLConnection connect(String requestBody) throws IOException {
        String urlS = address+requestBody+suffix;
        URL url = new URL(urlS);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setConnectTimeout(5000);
        urlConnection.setRequestProperty("Content-Type", "text/html; charset=UTF-8");
        urlConnection.setDoInput(true);
        urlConnection.setRequestMethod("GET");
        urlConnection.connect();
        return urlConnection;
    }

    public HttpURLConnection connectDumpURL(String requestBody) throws IOException {
        URL url = new URL(requestBody);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setConnectTimeout(5000);
        urlConnection.setRequestProperty("Content-Type", "text/html; charset=UTF-8");
        urlConnection.setDoInput(true);
        urlConnection.setRequestMethod("GET");
        urlConnection.connect();
        return urlConnection;
    }

    /**
     * Read the server's answer.
     * @param urlConnection
     * @return JSON in the answer body or null if kind of bad request
     * @throws IOException
     */
    public String readResponse(HttpURLConnection urlConnection) throws IOException {
        int responseCode = urlConnection.getResponseCode();
        if (responseCode == 200) {
            Log.d("RESPONSE CODE", "200 OK - Dump");

            InputStream in = urlConnection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder result = new StringBuilder();
            String line;
            while((line = reader.readLine()) != null) {
                result.append(line);
            }

            return result.toString();
        } else if (responseCode == 400) {
            Log.d("RESPONSE CODE", "400 Bad Request");
        } else {
            Log.d("RESPONSE CODE", ""+responseCode);
        }
        urlConnection.disconnect();
        return null;
    }

    private void init(){
        DataDownloader dd = DataDownloader.getDataDownloader();
        pr = dd.getPr();
        suffix = dd.getSuffix();
        address = dd.getAddress();
        context = dd.getContext();
    }

    @Override
    protected String doInBackground(Perimeter... perimeters) {
        if(perimeters == null || perimeters.length == 0){
            return null;
        }
        init();
        Perimeter peri = perimeters[0];
        try {
            HttpURLConnection urlConnectionTiles = connect(pr.getProperty(PropertiesReader.SERVER_DUMP)+"?pt="+peri.getType()+"&pn="+peri.getQueryCode()+"&");
            String tiles = readResponse(urlConnectionTiles);
            JSONArray dumpArray = new JSONArray(tiles);

            String url = DataDownloadedParser.readDump(dumpArray);

            HttpURLConnection urlConnectionDumpURL = connectDumpURL(url);
            String data = readResponse(urlConnectionDumpURL);
            if(data == null){
                return null;
            }
            JSONObject dataJSON = new JSONObject(data);
            List<List<? extends DisplayableData>> result = DataDownloadedParser.readDumpFile(dataJSON.getJSONArray("data"));
            for(List<? extends DisplayableData> l : result){
                Database.addDataInPark(l, perimeters[0].getQueryCode(), context);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "error " + perimeters[0].getQueryCode();
        }
        return perimeters[0].getQueryCode();
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if(result == null){
            notifyObserver("ERROR");
            return;
        }
        notifyObserver(result);
    }

    @Override
    public void addObservator(Observer o) {
        observers.add(o);
    }

    @Override
    public void deleteObserver(Observer o) {
        observers.remove(o);
    }

    @Override
    public void notifyObserver(String message) {
        for(Observer o : observers){
            o.actualize(this, message);
        }
    }
}
