package com.eiko.downloads.AsyncTasks;

import android.os.AsyncTask;
import android.util.Log;

import com.eiko.DataObject.DisplayableData;
import com.eiko.DataObject.Perimeter;
import com.eiko.ObserverObservable.Observable;
import com.eiko.ObserverObservable.Observer;
import com.eiko.PropertiesReader;
import com.eiko.downloads.DataDownloadedParser;
import com.eiko.downloads.DataDownloader;
import com.eiko.geolocalization.Position;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rinkui on 15/02/2018.
 */

public class TilesDownloaderAsyncTask extends AsyncTask<Perimeter, Integer, String> implements Observable {
    private String status;
    private PropertiesReader pr;
    private String address;
    private String suffix;
    private Perimeter perimeter;
    private List<Observer> observers = new ArrayList<>();

    public TilesDownloaderAsyncTask(Perimeter perimeter){
        this.perimeter = perimeter;
    }

    /**
     * Connect to the AFF's server and send the request.
     * http://server.ip:server.port/eiko/[]&password
     * @param requestBody request body = []
     * @return
     * @throws IOException
     */
    public HttpURLConnection connect(String requestBody) throws IOException {
        String urlS = address+requestBody+suffix;
        URL url = new URL(urlS);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setConnectTimeout(5000);
        urlConnection.setRequestProperty("Content-Type", "text/html; charset=UTF-8");
        urlConnection.setDoInput(true);
        urlConnection.setRequestMethod("GET");
        urlConnection.connect();
        return urlConnection;
    }


    /**
     * Read the server's answer.
     * @param urlConnection
     * @return JSON in the answer body or null if kind of bad request
     * @throws IOException
     */
    public String readResponse(HttpURLConnection urlConnection) throws IOException {
        int responseCode = urlConnection.getResponseCode();
        status = String.valueOf(responseCode);
        if (responseCode == 200) {
            Log.d("RESPONSE CODE", "200 OK - Tiles");

            InputStream in = urlConnection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder result = new StringBuilder();
            String line;
            while((line = reader.readLine()) != null) {
                result.append(line);
            }
            return result.toString();
        } else if (responseCode == 400) {
            Log.d("RESPONSE CODE", "400 Bad Request - Tiles");
        } else {
            Log.d("RESPONSE CODE", ""+responseCode);
        }
        urlConnection.disconnect();
        return null;
    }

    private void init(){
        DataDownloader dd = DataDownloader.getDataDownloader();
        pr = dd.getPr();
        suffix = dd.getSuffix();
        address = dd.getAddress();
    }

    @Override
    protected String doInBackground(Perimeter... perimeters) {
        if(perimeters == null || perimeters.length == 0){
            return null;
        }
        init();
        Perimeter peri = perimeters[0];
        try {
            HttpURLConnection urlConnectionTiles = connect(pr.getProperty(PropertiesReader.SERVER_TILES)+"?pt="+peri.getType()+"&pn="+peri.getQueryCode()+"&");
            String tiles = readResponse(urlConnectionTiles);
            if(tiles == null){
                return null;
            }
            tiles = tiles.replace("\"{", "{");
            tiles = tiles.replace("}\"", "}");
            tiles = tiles.replace("\\\"","\"");
            JSONArray tileJSON = new JSONArray(tiles);

            List<List<Double>> tilesList = DataDownloadedParser.readTileJSON(tileJSON);
            if( tilesList != null ) {
                int i = 0;
                for (List<Double> list : tilesList) {
                    List<Double> center = DisplayableData.getCenteredPosition(list);
                    Position position = new Position(center.get(1), center.get(0));
                    MetaAndDataDownloaderAsyncTask async = DataDownloader.getDataDownloader().downloadsSectors(position, perimeter);

                    // si cette task est annulée, annuler les autres
                    if(isCancelled()) {
                        Log.e("CANCEL TASK", "Cancel Tiles DL");
                        async.cancel(true);
                        return null;
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onCancelled(String s) {
        super.onCancelled(s);
        notifyObserver("CANCELLED" + s);
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if(result == null){
            notifyObserver("ERROR");
            return;
        }
        notifyObserver("Downnload Finished " + status);
    }

    @Override
    public void addObservator(Observer o) {
        observers.add(o);
    }

    @Override
    public void deleteObserver(Observer o) {
        observers.remove(o);
    }

    @Override
    public void notifyObserver(String message) {
        for(Observer o: observers) {
            o.actualize(this, message);
        }
    }
}
