package com.eiko.downloads.AsyncTasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.eiko.DataObject.DisplayableData;
import com.eiko.DataObject.Perimeter;
import com.eiko.ObserverObservable.Observable;
import com.eiko.ObserverObservable.Observer;
import com.eiko.PropertiesReader;
import com.eiko.database.Database;
import com.eiko.downloads.DataDownloadedParser;
import com.eiko.downloads.DataDownloader;
import com.eiko.geolocalization.Position;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rinkui on 12/02/2018.
 *
 * This class is the bridge between AFB and the app
 * To call the asynctask :
 * DataDownloaderAsynTask dlat = new ();
 * dlat.execute(...);
 */

public class MetaAndDataDownloaderAsyncTask extends AsyncTask<String, Void, Void>{
    private Position position;
    private PropertiesReader pr;
    private String address;
    private Perimeter perimeter;
    private String suffix;

    public MetaAndDataDownloaderAsyncTask(Perimeter perimeter){
        this.perimeter = perimeter;
    }

    public MetaAndDataDownloaderAsyncTask(){
        this(null);
    }

    /**
     * Connect to the AFF's server and send the request.
     * http://server.ip:server.port/eiko/[]&password
     * @param requestBody request body = []
     * @return
     * @throws IOException
     */
    public HttpURLConnection connect(String requestBody) throws IOException {
        String urlS = address+requestBody+suffix;
        URL url = new URL(urlS);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setConnectTimeout(5000);
        urlConnection.setRequestProperty("Content-Type", "text/html; charset=UTF-8");
        urlConnection.setDoInput(true);
        urlConnection.setRequestMethod("GET");
        urlConnection.connect();
        return urlConnection;
    }


    /**
     * Read the server's answer.
     * @param urlConnection
     * @return JSON in the answer body or null if kind of bad request
     * @throws IOException
     */
    public String readResponse(HttpURLConnection urlConnection) throws IOException {
        int responseCode = urlConnection.getResponseCode();
        if (responseCode == 200) {
            Log.d("RESPONSE CODE", "200 OK - Data");

            InputStream in = urlConnection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder result = new StringBuilder();
            String line;
            while((line = reader.readLine()) != null) {
                result.append(line);
            }
            return result.toString();
        } else if (responseCode == 400) {
            Log.d("RESPONSE CODE", "400 Bad Request");
        } else {
            Log.d("RESPONSE CODE", ""+responseCode);
        }
        urlConnection.disconnect();
        return null;
    }

    private void init(){
        DataDownloader dd = DataDownloader.getDataDownloader();
        pr = dd.getPr();
        suffix = dd.getSuffix();
        address = dd.getAddress();
        position = dd.getPosition();
    }

    private List<String> getGeoDataAdress(String urlBody) throws JSONException, IOException {
        HttpURLConnection urlConnectionMetaData = connect(urlBody);
        String metaDataString =  readResponse(urlConnectionMetaData);
        if(metaDataString == null){
            return null;
        }
        JSONArray metaDataJSON = new JSONArray(metaDataString);
        return DataDownloadedParser.readMetadataJSON(metaDataJSON);
    }

    private List<? extends DisplayableData> getGeoDataObject(String s) throws JSONException, IOException {
        HttpURLConnection urlConnectionGeoData = connect(pr.getProperty(PropertiesReader.SERVER_GEODATA)+ s + "x=" + position.getLongitude() + "&y=" + position.getLatitude() + "&r=" + pr.getProperty(PropertiesReader.SERVER_REQUEST_RADIUS)+ "&");
        String geoDataString = readResponse(urlConnectionGeoData);
        if( geoDataString == null ){
            return null;
        }
        JSONArray geoDataJSON = new JSONArray(geoDataString);
        return DataDownloadedParser.readGeoDataJSON(geoDataJSON);
    }

    @Override
    protected Void doInBackground(String... urlBody) {
        if(urlBody.length == 0) {
            return null;
        }
        init();
        try {
            for(String s : urlBody){
                List<String> urls = getGeoDataAdress(s);
                if( urls == null ){
                    return null;
                }
                for(String str : urls){
                    if (isCancelled()) {
                        return null;
                    }
                    List<? extends DisplayableData> list = getGeoDataObject(str);
                    if ( list != null){
                        position = new Position(position.getLongitude(), position.getLatitude());
                        Context context = DataDownloader.getDataDownloader().getContext();
                        if( perimeter != null ){
                            Database.addDataInPark(list, perimeter.getQueryCode(),  context);
                        }
                        else {
                            Database.importObjectsToDatabase(list, context);
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
