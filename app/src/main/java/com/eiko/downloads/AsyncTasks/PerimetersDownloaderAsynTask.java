package com.eiko.downloads.AsyncTasks;

import android.os.AsyncTask;
import android.util.Log;

import com.eiko.DataObject.Perimeter;
import com.eiko.ObserverObservable.Observable;
import com.eiko.ObserverObservable.Observer;
import com.eiko.PropertiesReader;
import com.eiko.downloads.DataDownloadedParser;
import com.eiko.downloads.DataDownloader;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rinkui on 15/02/2018.
 */

public class PerimetersDownloaderAsynTask extends AsyncTask<Void, Void, String> implements Observable {
    private PropertiesReader pr;
    private String address;
    private String suffix;
    private List<Observer> observers = new ArrayList<>();

    /**
     * Connect to the AFF's server and send the request.
     * http://server.ip:server.port/eiko/[]&password
     * @param requestBody request body = []
     * @return
     * @throws IOException
     */
    public HttpURLConnection connect(String requestBody) throws IOException {
        String urlS = address+requestBody+suffix;
        URL url = new URL(urlS);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setConnectTimeout(5000);
        urlConnection.setRequestProperty("Content-Type", "text/html; charset=UTF-8");
        urlConnection.setDoInput(true);
        urlConnection.setRequestMethod("GET");
        urlConnection.connect();
        return urlConnection;
    }

    /**
     * Read the server's answer.
     * @param urlConnection
     * @return JSON in the answer body or null if kind of bad request
     * @throws IOException
     */
    public String readResponse(HttpURLConnection urlConnection) throws IOException {
        int responseCode = urlConnection.getResponseCode();
        if (responseCode == 200) {
            Log.d("RESPONSE CODE", "200 OK - Perimeter");

            InputStream in = urlConnection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder result = new StringBuilder();
            String line;
            while((line = reader.readLine()) != null) {
                result.append(line);
            }

            return result.toString();
        } else if (responseCode == 400) {
            Log.d("RESPONSE CODE", "400 Bad Request");
        } else {
            Log.d("RESPONSE CODE", ""+responseCode);
        }
        urlConnection.disconnect();
        return null;
    }

    private void init(){
        DataDownloader dd = DataDownloader.getDataDownloader();
        pr = dd.getPr();
        suffix = dd.getSuffix();
        address = dd.getAddress();
    }

    @Override
    protected String doInBackground(Void... voids) {
        init();
        try {
            HttpURLConnection urlConnectionTypePar = connect(pr.getProperty(PropertiesReader.SERVER_TYPEPERIMETERS)+"?");
            String typePerimeters = readResponse(urlConnectionTypePar);
            if( typePerimeters == null ){
                return null;
            }
            JSONArray typePerimetersJSON = new JSONArray(typePerimeters);
            List<String> perimeters = DataDownloadedParser.readTypePerimetersJSON(typePerimetersJSON);
            for(int i = 0; i < perimeters.size() ; i ++){
                String p = perimeters.get(i);
                HttpURLConnection urlConnectionPerimetres = connect(pr.getProperty(PropertiesReader.SERVER_NAMEPERIMETERS)+"/"+p+"?");
                String queryCodePer = readResponse(urlConnectionPerimetres);
                JSONArray queryCodeJSON = new JSONArray(queryCodePer);
                List<Perimeter> perimeters1 = DataDownloadedParser.readPerimetersNameJSON(queryCodeJSON.getJSONArray(0), p);
                if(perimeters1 == null){
                    continue;
                }
                DataDownloader.getDataDownloader().getPerimeterManager().setPerimeters(perimeters1);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if(result == null){
            notifyObserver("ERROR");
            return;
        }
        notifyObserver("DONE");
    }

    @Override
    public void addObservator(Observer o) {
        observers.add(o);
    }

    @Override
    public void deleteObserver(Observer o) {
        observers.remove(o);
    }

    @Override
    public void notifyObserver(String message) {
        for(Observer o : observers){
            o.actualize(this, message);
        }
    }
}
