package com.eiko.downloads.AsyncTasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.eiko.DataObject.Perimeter;
import com.eiko.DataObject.PerimeterManager;
import com.eiko.ObserverObservable.Observable;
import com.eiko.ObserverObservable.Observer;
import com.eiko.PropertiesReader;
import com.eiko.downloads.DataDownloadedParser;
import com.eiko.downloads.DataDownloader;
import com.eiko.geolocalization.Position;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rinkui on 06/03/2018.
 */

public class WhereAsyncTask extends AsyncTask<Position, Void, String> implements Observable {
    private PropertiesReader pr;
    private String address;
    private String suffix;
    private Context context;
    private List<Observer> observers = new ArrayList<>();

    /**
     * Connect to the AFF's server and send the request.
     * http://server.ip:server.port/eiko/[]&password
     * @param requestBody request body = []
     * @return
     * @throws IOException
     */
    public HttpURLConnection connect(String requestBody) throws IOException {
        String urlS = address+requestBody+suffix;
        URL url = new URL(urlS);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setConnectTimeout(5000);
        urlConnection.setRequestProperty("Content-Type", "text/html; charset=UTF-8");
        urlConnection.setDoInput(true);
        urlConnection.setRequestMethod("GET");
        urlConnection.connect();
        return urlConnection;
    }

    /**
     * Read the server's answer.
     * @param urlConnection
     * @return JSON in the answer body or null if kind of bad request
     * @throws IOException
     */
    public String readResponse(HttpURLConnection urlConnection) throws IOException {
        int responseCode = urlConnection.getResponseCode();
        if (responseCode == 200) {
            Log.d("RESPONSE CODE", "200 OK - Dump");

            InputStream in = urlConnection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder result = new StringBuilder();
            String line;
            while((line = reader.readLine()) != null) {
                result.append(line);
            }

            return result.toString();
        } else if (responseCode == 400) {
            Log.d("RESPONSE CODE", "400 Bad Request");
        } else {
            Log.d("RESPONSE CODE", ""+responseCode);
        }
        urlConnection.disconnect();
        return null;
    }

    private void init(){
        DataDownloader dd = DataDownloader.getDataDownloader();
        pr = dd.getPr();
        suffix = dd.getSuffix();
        address = dd.getAddress();
        context = dd.getContext();
    }

    @Override
    protected String doInBackground(Position... positions) {
        if(positions == null || positions.length == 0){
            return null;
        }
        init();
        Position pos = positions[0];
        try {
            HttpURLConnection urlConnectionWhere = connect(pr.getProperty(PropertiesReader.SERVER_WHERE)+"?x="+pos.getLongitude()+"&y="+pos.getLatitude()+"&");
            String respWhere = readResponse(urlConnectionWhere);
            if( respWhere == null ){
                return null;
            }
            JSONArray jsonArrayWhere = new JSONArray(respWhere);
            Perimeter currentPerimeter = DataDownloadedParser.readWhereJSON(jsonArrayWhere.getJSONArray(0));
            if(currentPerimeter == null){
                return null;
            }
            PerimeterManager.getPerimeterManager(context).setCurrentPerimeter(currentPerimeter);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return "Vous êtes dans le parc " + PerimeterManager.getPerimeterManager(context).getCurrentPerimeter().getQueryCode();
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if(result == null){
            notifyObserver("ERROR");
            return;
        }
        notifyObserver(result);
    }

    @Override
    public void addObservator(Observer o) {
        observers.add(o);
    }

    @Override
    public void deleteObserver(Observer o) {
        observers.remove(o);
    }

    @Override
    public void notifyObserver(String message) {
        for(Observer o : observers){
            o.actualize(this, message);
        }
    }
}
