package com.eiko.downloads;

import com.eiko.DataObject.DataFactory;
import com.eiko.DataObject.DisplayableData;
import com.eiko.DataObject.Perimeter;
import com.eiko.geolocalization.Position;
import com.eiko.geolocalization.PositionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rinkui on 13/02/2018.
 */

public class DataDownloadedParser {

    private static List<? extends DisplayableData> getDisplayableDataObject(JSONObject allData, JSONObject metadata) throws JSONException {
        JSONArray data = allData.getJSONArray("data");
        //On récupère le nom de l'objet à créer soit la class et le group dans metadata.
        String dataClass = (String) metadata.get("class");
        String dataGroup = ((String) metadata.get("group")).replace("eiko_", "");
        //On créer une liste d'objets DisplayableData à partir du nom de la classe et du groupe.
        return DataFactory.getDataFromJSON(dataGroup, dataClass, data);
    }

    /**
     * @param jsonArray
     * @return list of displayabledata from geodata JSON
     * @throws JSONException
     */
    public static List<? extends DisplayableData> readGeoDataJSON(JSONArray jsonArray){
        if(jsonArray == null){
            return null;
        }
        try {
            // Le JSON est encapsulé dans une liste (à 1 élément) il faut donc l'extraire
            JSONObject allData = jsonArray.getJSONObject(0);
            // Puis on récupère les metadata (paramètre envoyés par la requêtes) et les datas
            JSONObject metadata = allData.getJSONObject("metadata");
            return getDisplayableDataObject(allData, metadata);
        } catch (JSONException e) {
            System.err.println("JSON geodata mal formatté");
            return null;
        }
    }

    public static List<String> readTypePerimetersJSON(JSONArray jsonArray){
        if(jsonArray == null){
            return null;
        }
        ArrayList<String> result = new ArrayList<>();
        try {
            JSONArray typeArray = jsonArray.getJSONArray(0);
            for (int i = 0; i < typeArray.length(); i++) {
                JSONObject type = typeArray.getJSONObject(i);
                String typeS = type.getString("type_perimeter");
                if (!(typeS.equals("commune") || typeS.equals("departement"))) {
                    result.add(typeS);
                }
            }
            return result;
        } catch (JSONException e) {
            System.err.println("JSON type perimeters mal formatté");
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @param jsonArray
     * @return geodata url body from metadata JSON
     */
    public static List<String> readMetadataJSON(JSONArray jsonArray) {
        if(jsonArray == null){
            return null;
        }
        try {
            ArrayList<String> result = new ArrayList<>();
            JSONObject allData = jsonArray.getJSONObject(0);
            Double doubleS;
            try {
                doubleS = allData.getDouble("altitude");
            }catch (JSONException e) {
                doubleS = 0d;
            }
            PositionManager.getInstance().getLocation().setAltitude(doubleS);
            JSONArray classes = allData.getJSONArray("classes");
            for (int i = 0; i < classes.length(); i++) {
                // sous la forme /group/class?
                JSONObject object = classes.getJSONObject(i);
                String group = object.getString("gr").replace("eiko_", "");
                String classe = object.getString("class");
                result.add("/" + group + "/" + classe + "?");
            }
            return result;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static List<List<Double>> readTileJSON(JSONArray jsonArray) {
        if(jsonArray == null){
            return null;
        }
        List<List<Double>> result = new ArrayList<>();
        try {
            JSONArray tileArray = jsonArray.getJSONArray(0);
            for (int i = 0; i < tileArray.length(); i++) {
                JSONArray coordinate = tileArray.getJSONObject(i).getJSONObject("geom").getJSONArray("coordinates").getJSONArray(0);
                List<Double> square = new ArrayList<>();
                for (int j = 0; j < coordinate.length(); j++) {
                    JSONArray pos = coordinate.getJSONArray(j);
                    square.add(Double.valueOf(pos.getString(0)));
                    square.add(Double.valueOf(pos.getString(1)));

                }
                result.add(square);
            }
            return result;
        } catch (JSONException e) {
            System.err.println("JSON tile mal formatté");
            e.printStackTrace();
        }
        return null;
    }

    public static String readDump(JSONArray jsonArray){
        if(jsonArray == null){
            return null;
        }
        try {
            return jsonArray.getJSONObject(0).getString("fileURL");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static List<List<? extends DisplayableData>> readDumpFile(JSONArray jsonArray){
        List<List<? extends DisplayableData>> result = new ArrayList<>();
        for(int i = 0 ; i < jsonArray.length() ; i ++){
            try {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                result.add(getDisplayableDataObject(jsonObject, jsonObject));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static Perimeter readWhereJSON(JSONArray jsonArray){
        if(jsonArray == null){
            return null;
        }
        for(int i = 0; i < jsonArray.length() ; i ++){
            try {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String typePerimeter = jsonObject.getString("type_perimeter");
                if( !typePerimeter.equals("departement") && !typePerimeter.equals("commune") ){
                    String namePerimeter = jsonObject.getString("query_code");
                    return new Perimeter(typePerimeter, namePerimeter);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    /**
     * @param file
     * @return JSON array from the file
     * @throws IOException
     * @throws JSONException
     */
    public static JSONArray getJsonFile(File file) throws IOException, JSONException {
        if( file == null ){
            return null;
        }
        InputStream is = new FileInputStream(file);
        int size = is.available();

        byte[] buffer = new byte[size];

        is.read(buffer);

        is.close();

        String jsonRes = new String(buffer, "UTF-8");

        if (jsonRes.length() == 0) {
            System.err.println("Le fichier " + file.getName() + " est vide");
            return null;
        }

        try {
            return new JSONArray(jsonRes);
        } catch (JSONException e) {
            System.err.println("Le contenu du fichier " + file.getName() + " n'est pas du JSON");
            return null;
        }
    }

    public static List<Perimeter> readPerimetersNameJSON(JSONArray jsonArray, String perimeterType){
        if(jsonArray == null ||perimeterType == null){
            return null;
        }
        ArrayList<Perimeter> result = new ArrayList<>();
        for(int i = 0 ; i < jsonArray.length() ; i ++){
            try {
                JSONObject perimeterObject = jsonArray.getJSONObject(i);
                result.add(new Perimeter(perimeterType, perimeterObject.getString("query_code")));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static List<Perimeter> readPerimetersJSON(JSONArray jsonArray, String perimeterType) {
        if(jsonArray == null ||perimeterType == null){
            return null;
        }
        ArrayList<Perimeter> result = new ArrayList<>();
        try {
            JSONArray perimArray = perimArray = jsonArray.getJSONArray(0);
        for (int i = 0; i < perimArray.length(); i++) {
                JSONObject perim = perimArray.getJSONObject(i);
                String queryCode = perim.getString("query_code");
                JSONArray coordinate = perim.getJSONObject("geom").getJSONArray("coordinates").getJSONArray(0);
                List<List<Position>> shape = new ArrayList<>();
                List<Position> positions = new ArrayList<>();
                for (int j = 0; j < coordinate.length(); j++) {
                    JSONArray posList = coordinate.getJSONArray(j);
                    try {
                        for(int k = 0 ; k < posList.length()-1; k+=2) {
                            JSONArray pos = posList.getJSONArray(k);
                            Position position = new Position(Double.valueOf(pos.getString(1)), Double.valueOf(pos.getString(0)));
                            positions.add(position);
                        }
                        shape.add(positions);
                        positions = new ArrayList<>();
                    }catch (JSONException e){
                        Position position = new Position(Double.valueOf(posList.getString(1)), Double.valueOf(posList.getString(0)));
                        positions.add(position);
                        shape.add(positions);
                    }
                }
                result.add(new Perimeter(perimeterType, queryCode, shape));
            }
            return result;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
