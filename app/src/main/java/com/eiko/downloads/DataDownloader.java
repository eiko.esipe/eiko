package com.eiko.downloads;

import android.content.Context;
import android.os.Environment;

import com.eiko.DataObject.DisplayableData;
import com.eiko.DataObject.Perimeter;
import com.eiko.DataObject.PerimeterManager;
import com.eiko.ObserverObservable.Observer;
import com.eiko.PropertiesReader;
import com.eiko.database.Database;
import com.eiko.downloads.AsyncTasks.DumpAsyncTask;
import com.eiko.downloads.AsyncTasks.MetaAndDataDownloaderAsyncTask;
import com.eiko.downloads.AsyncTasks.PerimetersDownloaderAsynTask;
import com.eiko.geolocalization.Position;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by Rinkui on 06/02/2018.
 */

public class DataDownloader {
    private static Context context;
    private String address;
    private PropertiesReader pr;
    private Position position;
    private String suffix;
    private static DataDownloader dataDownloader;
    private PerimeterManager perimeterManager;

    private DataDownloader(){
        if(context != null) {
            perimeterManager = PerimeterManager.getPerimeterManager(context);
            pr = PropertiesReader.getProReader(context);
            suffix = pr.getProperty(PropertiesReader.SERVER_REQUEST_SUFFIX);
            address = "http://" + pr.getProperty(PropertiesReader.SERVER_IP) + ":" + pr.getProperty(PropertiesReader.SERVER_PORT) + "/" + pr.getProperty(PropertiesReader.SERVER_APPNAME) + "/";
        }
    }

    public static DataDownloader getDataDownloader(Context mContext){
        if( context == null && mContext != null ){
            context = mContext;
        }
        if(dataDownloader == null){
            dataDownloader = new DataDownloader();
        }
        return dataDownloader;
    }

    public static DataDownloader getDataDownloader(){
        return getDataDownloader(context);
    }

    public void downloadsPerimeters(){
        PerimetersDownloaderAsynTask perimetersDownloaderAsynTask = new PerimetersDownloaderAsynTask();
        perimetersDownloaderAsynTask.execute();
    }

    public void downloadsPerimeters(Observer o){
        PerimetersDownloaderAsynTask perimetersDownloaderAsynTask = new PerimetersDownloaderAsynTask();
        perimetersDownloaderAsynTask.addObservator(o);
        perimetersDownloaderAsynTask.execute();
    }

    public DumpAsyncTask downloadDataFromAPI(Perimeter perim){
        DumpAsyncTask dumpAsyncTask = new DumpAsyncTask();
        dumpAsyncTask.execute(perim);
        return dumpAsyncTask;
        //TilesDownloaderAsyncTask tilesDownloaderAsyncTask = new TilesDownloaderAsyncTask();
        //tilesDownloaderAsyncTask.execute(perim);

    }

    public MetaAndDataDownloaderAsyncTask downloadsSectors(Position position, Perimeter perimeter) {
        if(position == null){
            return null;
        }
        this.position = position;
        MetaAndDataDownloaderAsyncTask asyncTask = new MetaAndDataDownloaderAsyncTask(perimeter);
        asyncTask.execute(pr.getProperty(PropertiesReader.SERVER_METADATA)+"?x=" + position.getLongitude() + "&y=" + position.getLatitude() + "&r=" + pr.getProperty(PropertiesReader.SERVER_REQUEST_RADIUS)+"&");
        return asyncTask;
    }

    public void downloadsSectors(Position position) {
        downloadsSectors(position, null);
    }

    // a utiliser si on veut les fichiers des assets par exemple
    public List<? extends DisplayableData> downloadsSectorsFromFile(File file) {
        return getDisplayableDataFromFile(file);
    }

    // par défaut le dossier ou on ira chercher les fichiers est /dataSource
    public void downloadsSectorsFromDefaultFile() {
        File folder = createForlderIfNotExists();
        File[] listOfFiles = folder.listFiles();
        try {
            for (int i = 0; i < listOfFiles.length; i++) {
                if (listOfFiles[i].isFile()) {
                    List<? extends DisplayableData> list = getDisplayableDataFromFile(listOfFiles[i]);
                    if (list == null) {
                        System.out.println("Error while loading Json files");
                        continue;
                    }
                    Database.importObjectsToDatabase(list, context);
                }
                else if (listOfFiles[i].isDirectory()) {  }
            }
        }catch (NullPointerException e){
            System.out.println("Accept permissions");
            e.printStackTrace();
        }
    }

    private File createForlderIfNotExists() {
        File folder = new File(Environment.getExternalStorageDirectory(), "dataSource");
        boolean success = true;
        if (!folder.exists()) {
            success = folder.mkdirs();
        }
        if (success) {
            return folder;
        }
        return null;
    }

    private List<? extends DisplayableData> getDisplayableDataFromFile(File file) {
        try {
            // Récupère le fichier JSON sous forme de JSONArray
            JSONArray jsonObject = DataDownloadedParser.getJsonFile(file);
            if (jsonObject == null) {
                return null;
            }
            if (jsonObject.length() == 0) {
                System.err.println("Le JSON " + file.getName() + " est vide");
                return null;
            }
            // On lit le JSON
            return DataDownloadedParser.readGeoDataJSON(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Position getPosition() {
        return position;
    }

    public void setContext(Context context){
        this.context = context;
    }

    public Context getContext() {
        return context;
    }

    public PropertiesReader getPr() {
        return pr;
    }

    public String getSuffix() {
        return suffix;
    }

    public String getAddress() {
        return address;
    }

    public PerimeterManager getPerimeterManager() {
        return perimeterManager;
    }
}
