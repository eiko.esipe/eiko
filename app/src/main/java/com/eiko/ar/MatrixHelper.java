package com.eiko.ar;

import com.google.ar.core.Pose;


/**
 * Created by Erwan on 22/02/2018, for project eiko
 */

public class MatrixHelper {
    /**
     * Function to detect gimbal lock
     * @param p Pose object to get all the rotation angles
     * @return 0 if no gimbal lock, 1 otherwise
     */
    private static int getGimbalPole (Pose p) {
        final float t = p.qy() * p.qx() + p.qz() * p.qw();
        return t > 0.499f ? 1 : (t < -0.499f ? -1 : 0);
    }

    /**
     * Functions to extract Y angle
     * @param p Pose object to extract the rotation angle
     * @return Angle in degree
     */
    static float getY(Pose p) {
        float res = getGimbalPole(p) == 0 ? (float) Math.atan2(2f * (p.qy() * p.qw() + p.qx() * p.qz()), 1f - 2f * (p.qy() * p.qy() + p.qx() * p.qx())) : 0f;
        return (float) Math.toDegrees(res);
    }

    /*  UNUSED BUT WORKING
    /**
     * Functions to extract Z angle
     * @param p Pose object to extract the rotation angle
     * @return Angle in degree
     */
    /*public static float getZ (Pose p) {
        final int pole = getGimbalPole(p);
        float res = pole == 0 ? (float) Math.atan2(2f * (p.qw() * p.qz() + p.qy() * p.qx()), 1f - 2f * (p.qx() * p.qx() + p.qz() * p.qz())) : pole * 2f * (float) Math.atan2(p.qy(), p.qw());
        return (float) Math.toDegrees(res);
    }*/

    /*  UNUSED BUT WORKING
    /**
     * Functions to extract X angle
     * @param p Pose object to extract the rotation angle
     * @return Angle in degree
     *//*
    public static float getX (Pose p) {
        final int pole = getGimbalPole(p);
        double f1 = Math.asin((double) MathUtils.clamp(2f * (p.qw() * p.qx() - p.qz() * p.qy()), -1f, 1f));
        double f2 = pole * Math.PI * 0.5f;
        float res = pole == 0 ? (float) f1 : (float) f2;
        return (float) Math.toDegrees(res);
    }*/


}
