/*
 * Copyright 2017 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.eiko.ar;

import android.Manifest;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.eiko.ARObjectManager;
import com.eiko.ConnectionActivity;
import com.eiko.DataObject.PerimeterManager;
import com.eiko.DownloadPerimeterActivity;
import com.eiko.FormActivity;
import com.eiko.ObserverObservable.Observable;
import com.eiko.ObserverObservable.Observer;
import com.eiko.PropertiesReader;
import com.eiko.R;
import com.eiko.ar.rendering.BackgroundRenderer;
import com.eiko.ar.rendering.LayRenderer;
import com.eiko.ar.rendering.PlaneRenderer;
import com.eiko.ar.rendering.PointCloudRenderer;
import com.eiko.database.UserParamDB;
import com.eiko.downloads.AsyncTasks.PerimetersDownloaderAsynTask;
import com.eiko.downloads.DataDownloader;
import com.eiko.forms.JobSchedulerService;
import com.eiko.geolocalization.Position;
import com.eiko.geolocalization.PositionManager;
import com.eiko.matrix.ARObject;
import com.eiko.permission.Permission;
import com.eiko.settingsButtons.SettingsButtonsManagement;
import com.google.ar.core.Anchor;
import com.google.ar.core.Camera;
import com.google.ar.core.Config;
import com.google.ar.core.Frame;
import com.google.ar.core.HitResult;
import com.google.ar.core.Plane;
import com.google.ar.core.PointCloud;
import com.google.ar.core.Pose;
import com.google.ar.core.Session;
import com.google.ar.core.Trackable;
import com.google.ar.core.Trackable.TrackingState;
import com.google.ar.core.exceptions.UnavailableApkTooOldException;
import com.google.ar.core.exceptions.UnavailableArcoreNotInstalledException;
import com.google.ar.core.exceptions.UnavailableSdkTooOldException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ArrayBlockingQueue;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * This is a simple example that shows how to create an augmented reality (AR) application using the
 * ARCore API. The application will display any detected planes and will allow the user to tap on a
 * plane to place a 3d model of the Android robot.
 */
public class HelloArActivity extends AppCompatActivity implements GLSurfaceView.Renderer, SensorEventListener, Observer, LocationListener {
    private static final String TAG = HelloArActivity.class.getSimpleName();

    // Rendering. The Renderers are created here, and initialized when the GL surface is created.
    private GLSurfaceView mSurfaceView;

    private Session mSession;
    private GestureDetector mGestureDetector;
    private Snackbar mMessageSnackbar;
    private DisplayRotationHelper mDisplayRotationHelper;

    private final BackgroundRenderer mBackgroundRenderer = new BackgroundRenderer();
    private final PlaneRenderer mPlaneRenderer = new PlaneRenderer();
    private final PointCloudRenderer mPointCloud = new PointCloudRenderer();

    // Temporary matrix allocated here to reduce number of allocations for each frame.
    private final float[] mAnchorMatrix = new float[16];

    // Tap handling and UI.
    private final ArrayBlockingQueue<MotionEvent> mQueuedSingleTaps = new ArrayBlockingQueue<>(16);
    private final ArrayList<Anchor> mAnchors = new ArrayList<>();

    /**
     * For Orientation
     **/
    private RealOrientation orientation;
    private SensorManager mSensorManager;
    private PositionManager positionManager;
    private TextView textBearing;

    /**
     * To test anchorPosition
     **/
    private HashMap<Anchor, LayRenderer> objects = new HashMap<>();

    /**
     * To init only one time
     **/
    private boolean loaded = false;


    private ARObjectManager mARObject;
    LocationManager lm;
    private PropertiesReader propertyReader;
    private boolean anchorCreated;
    private boolean needUpdate = false;

    private Snackbar snackbar;
    private boolean drawSnackbar;
    private boolean dismissSnackbar;
    private String messageSnackbar;
    private boolean drawn=false;

    /**
     * For the user button
     **/
    private boolean downloadInit = false;

    // POUR ROTATION
    private Float lastAngle = null;
    private Float angle;


    @Override
    public void actualize(Observable o, String message) {
        if(o instanceof PerimeterManager){
            if(message.isEmpty()){
                if(snackbar != null){
                    dismissSnackbar = true;
                    messageSnackbar = null;
                }
            }
            else{
                messageSnackbar = message;
                drawSnackbar = true;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        LayoutInflater inflater = getLayoutInflater();
                        View calibrationLayout = inflater.inflate(R.layout.in_perimeter,null);
                        addContentView(calibrationLayout,
                                new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT));
                        calibrationLayout.setVisibility(View.GONE);
                        calibrationLayout.setVisibility(View.VISIBLE);
                    }
                });
            }
        }
        if( o instanceof PerimetersDownloaderAsynTask){
            Log.i("Eiko", "Les parcs ont fini d'être téléchargés");
            PerimeterManager pm = PerimeterManager.getPerimeterManager(this);
            pm.addObservator(this);
            pm.checkPosition();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addContentView(new SettingsButtonsManagement().getFinalLayout(getApplicationContext()),
                new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));

        setOnClickMenuButtons();


        // Calibration
        LayoutInflater inflater = getLayoutInflater();
        View calibrationLayout = inflater.inflate(R.layout.calibration,null);
        addContentView(calibrationLayout,
                new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT));
        calibrationLayout.setVisibility(View.GONE);
        textBearing = findViewById(R.id.textView_compass);

        findViewById(R.id.buttonMenuCompass).setOnClickListener((View v) -> {
            calibrationLayout.setVisibility(View.VISIBLE);
            findViewById(R.id.calibrationButton).setOnClickListener((View w ) ->{
                needUpdate = true;
                calibrationLayout.setVisibility(View.GONE);
            });
            findViewById(R.id.close_layout).setOnClickListener((View w ) ->{
                calibrationLayout.setVisibility(View.GONE);
            });
        });

        // Creation de la snackbar
        snackbar = Snackbar.make(
                HelloArActivity.this.findViewById(android.R.id.content),
                messageSnackbar, Snackbar.LENGTH_INDEFINITE);
        View snackBarView = snackbar.getView();
        TextView snackBarTxt = snackBarView.findViewById(android.support.design.R.id.snackbar_text);
        snackBarView.setBackgroundColor(getColor(R.color.blueGreen));
        snackBarTxt.setGravity(Gravity.CENTER_HORIZONTAL);

        /** If GPS is disabled, go to the parameter **/
        enableGPS();
        System.out.println("Start getPermission");

        /* JOB SERVICE STARTED **/
        // ///////////////////////////////////
        // Setup service that checks connectivity every 15 min and do a job (just print right now)
        setJobSchedulerService();


        /* PERMISSION TO GET THE CAMERA/GPS/RW **/
        Permission.getPermissions(this);
        PositionManager.initPositionManager(getApplicationContext());
        positionManager = PositionManager.getInstance();

        /* REDIRECT TO GPS ENABLER **/
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        assert manager != null;
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Intent i = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);

            startActivityForResult(i, 1);
        }

        /* REAL ORIENTATION INITIALIZE **/
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        orientation = new RealOrientation(mSensorManager);

        mSurfaceView = findViewById(R.id.surfaceview);
        mDisplayRotationHelper = new DisplayRotationHelper(/*context=*/ this);

        // Set up tap listener.
        mGestureDetector = new GestureDetector(this, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                onSingleTap(e);
                return true;
            }

            @Override
            public boolean onDown(MotionEvent e) {
                return true;
            }
        });

        mSurfaceView.setOnTouchListener((v, event) -> mGestureDetector.onTouchEvent(event));


        // Set up renderer.
        mSurfaceView.setPreserveEGLContextOnPause(true);
        mSurfaceView.setEGLContextClientVersion(2);
        mSurfaceView.setEGLConfigChooser(8, 8, 8, 8, 16, 0); // Alpha used for plane blending.
        mSurfaceView.setRenderer(this);
        mSurfaceView.setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);

        Exception exception = null;
        String message = null;
        try {
            mSession = new Session(/* context= */ this);
        } catch (UnavailableArcoreNotInstalledException e) {
            message = "Please install ARCore";
            exception = e;
        } catch (UnavailableApkTooOldException e) {
            message = "Please update ARCore";
            exception = e;
        } catch (UnavailableSdkTooOldException e) {
            message = "Please update this app";
            exception = e;
        } catch (Exception e) {
            message = "This device does not support AR";
            exception = e;
        }

        /*if (message != null) {
            showSnackbarMessage(message, true);
            return;
        }*/

        // Create default config and check if supported.
        Config config = new Config(mSession);
        if (!mSession.isSupported(config)) {
            showSnackbarMessage("This device does not support AR", true);
        }
        mSession.configure(config);
    }

    /**
     * Set onClickListener for the menu buttons
     */
    private void setOnClickMenuButtons() {
        // Download button starts DownloadPerimeterActivity
        findViewById(R.id.buttonMenuDownload).setOnClickListener(v -> {
            Intent intent = new Intent(this, DownloadPerimeterActivity.class);
            startActivity(intent);
        });
        // Login button starts ConnectionActivity
        findViewById(R.id.buttonMenuLogin).setOnClickListener(v -> {
            Intent intent = new Intent(this, ConnectionActivity.class);
            startActivity(intent);
        });
        // Form button starts FormActivity
        findViewById(R.id.buttonMenuForm).setOnClickListener(v -> {
            UserParamDB userParamDB = new UserParamDB(this);
            Intent intent = new Intent(getApplicationContext(), FormActivity.class);
            intent.putExtra("position", positionManager.getPosition());
            intent.putExtra("userMail", userParamDB.getUserMail());
            startActivity(intent);
        });
    }

    private void setJobSchedulerService() {
        ComponentName componentName = new ComponentName(this, JobSchedulerService.class);
        JobInfo jobInfo = new JobInfo.Builder(1, componentName)
                .setPeriodic(15 * 60 * 1000) // 15 min minimum
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                .build();
        JobScheduler jobScheduler = (JobScheduler) getSystemService(JOB_SCHEDULER_SERVICE);
        assert jobScheduler != null;
        int resultCode = jobScheduler.schedule(jobInfo);
        if (resultCode == JobScheduler.RESULT_FAILURE) {
            Log.d("Eiko+", "JobScheduler failed to be scheduled!");
        }
    }


    /**
     * Initialize every field needed for the AR after accepting the Permission.
     */
    private void initAll() {
        if (Permission.checkPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) && Permission.checkPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            DataDownloader dl = DataDownloader.getDataDownloader(this);
            dl.downloadsSectorsFromDefaultFile();
            launchTracking(getApplicationContext(), positionManager);
            DataDownloader.getDataDownloader(this).downloadsPerimeters(this);
            mARObject = new ARObjectManager(positionManager, this);
            loaded = true;
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        PropertiesReader.getProReader(getApplicationContext());
        propertyReader = PropertiesReader.getProReader();

        mSensorManager.registerListener(this, orientation.getSensorGravity(), SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this, orientation.getSensorMagnetic(), SensorManager.SENSOR_DELAY_NORMAL);
        // ARCore requires camera permissions to operate. If we did not yet obtain runtime
        // permission on Android M and above, now is a good time to ask the user for it.


        if (CameraPermissionHelper.hasCameraPermission(this)) {
            if (!loaded) {
                initAll();
            }
            initListener();

            if (mSession != null) {
                //showLoadingMessage();
                // Note that order matters - see the note in onPause(), the reverse applies here.
                mSession.resume();
            }
            mSurfaceView.onResume();
            mDisplayRotationHelper.onResume();
        } else {
            //CameraPermissionHelper.requestCameraPermission(this);
            Permission.getPermissions(this);
        }

        checkUserConnection();
    }

    /**
     * If the user is not logged, the login button is accessible
     * If the user is logged, the form button is accessible
     */
    private void checkUserConnection() {
        UserParamDB userParamDB = new UserParamDB(this);
        if (userParamDB.isUserLogged()) {
            findViewById(R.id.buttonMenuLogin).setVisibility(View.INVISIBLE);
            ImageButton buttonMenuForm = findViewById(R.id.buttonMenuForm);
            buttonMenuForm.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Allow to takeForDisplay a Position item
     * filled with a latitude and a longitude
     */
    public void initListener() {
        lm = (LocationManager) this.getSystemService(LOCATION_SERVICE);
        try {
            assert lm != null;
            if (lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                lm.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                        Long.valueOf(propertyReader.getProperty(PropertiesReader.AR_UPDATE_TIME)),
                        Long.valueOf(propertyReader.getProperty(PropertiesReader.AR_UPDATE_DISTANCE)),
                        this);
            } else {
                lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                        Long.valueOf(propertyReader.getProperty(PropertiesReader.AR_UPDATE_TIME)),
                        Long.valueOf(propertyReader.getProperty(PropertiesReader.AR_UPDATE_DISTANCE)),
                        this);
            }
        } catch (SecurityException se) {
            throw new SecurityException("Need gps and network permissions");
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        // Note that the order matters - GLSurfaceView is paused first so that it does not try
        // to query the session. If Session is paused before GLSurfaceView, GLSurfaceView may
        // still call mSession.update() and takeForDisplay a SessionPausedException.
        mDisplayRotationHelper.onPause();
        mSurfaceView.onPause();
        if (mSession != null) {
            mSession.pause();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mSensorManager.unregisterListener(this, orientation.getSensorMagnetic());
        mSensorManager.unregisterListener(this, orientation.getSensorGravity());
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            // Standard Android full-screen functionality.
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }

    private void onSingleTap(MotionEvent e) {
        // Queue tap if there is space. Tap is lost if queue is full.
        mQueuedSingleTaps.offer(e);
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        GLES20.glClearColor(0.1f, 0.1f, 0.1f, 1.0f);

        // Create the texture and pass it to ARCore session to be filled during update().
        mBackgroundRenderer.createOnGlThread(/*context=*/ this);
        if (mSession != null) {
            mSession.setCameraTextureName(mBackgroundRenderer.getTextureId());
        }

        /** TODO, these are for testing purpose **/
        // Prepare the other rendering objects.
        try {
            mPlaneRenderer.createOnGlThread(/*context=*/this, "trigrid.png");
        } catch (IOException e) {
            //Log.e(TAG, "Failed to read plane texture");
        }
        mPointCloud.createOnGlThread(/*context=*/this);
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        mDisplayRotationHelper.onSurfaceChanged(width, height);
        GLES20.glViewport(0, 0, width, height);
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        // Clear screen to notify driver it should not load any pixels from previous frame.
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);

        if (mSession == null) {
            return;
        }
        // Notify ARCore session that the view size changed so that the perspective matrix and
        // the video background can be properly adjusted.
        mDisplayRotationHelper.updateSessionIfNeeded(mSession);

        try {
            // Obtain the current frame from ARSession. When the configuration is set to
            // UpdateMode.BLOCKING (it is by default), this will throttle the rendering to the
            // camera framerate.
            Frame frame = mSession.update();
            Camera camera = frame.getCamera();
            if (lastAngle == null) {
                lastAngle = MatrixHelper.getY(camera.getDisplayOrientedPose().extractRotation());
                angle = lastAngle - 180;
            } else {
                Float newAngle = MatrixHelper.getY(camera.getDisplayOrientedPose().extractRotation());
                angle = newAngle - lastAngle;
            }
            // Handle taps. Handling only one tap per frame, as taps are usually low frequency
            // compared to frame rate.
            MotionEvent tap = mQueuedSingleTaps.poll();
            if (camera.getTrackingState() == TrackingState.TRACKING && needUpdate && loaded) {
                updateARObjectManager(mSession, camera.getPose());
                needUpdate = false;
                anchorCreated = true;
            }
            if (tap != null && camera.getTrackingState() == TrackingState.TRACKING) {
                for (HitResult hit : frame.hitTest(tap)) {
                    // Check if any plane was hit, and if it was hit inside the plane polygon
                    Trackable trackable = hit.getTrackable();
                    boolean hitOnTarget = false;
                    LayRenderer l;
                    if (trackable instanceof Plane && ((Plane) trackable).isPoseInPolygon(hit.getHitPose()) && !hitOnTarget) {
                        if (mAnchors.size() >= 5) {
                            mAnchors.get(0).detach();
                            mAnchors.remove(0);
                        }

                        Anchor anch = hit.createAnchor();
                        mAnchors.add(anch);
                        break;
                    }
                }
            }

            // Draw background.
            mBackgroundRenderer.draw(frame);

            // Get projection matrix.
            float[] projmtx = new float[16];
            camera.getProjectionMatrix(projmtx, 0, 0.1f, 100.0f);

            // Get camera matrix and draw.
            float[] viewmtx = new float[16];
            camera.getViewMatrix(viewmtx, 0);
            // Compute lighting from average intensity of the image.
            final float lightIntensity = frame.getLightEstimate().getPixelIntensity();

            // Visualize tracked points.
            PointCloud pointCloud = frame.acquirePointCloud();
            mPointCloud.update(pointCloud);
            mPointCloud.draw(viewmtx, projmtx);

            // Application is responsible for releasing the point cloud resources after
            // using it.
            pointCloud.release();

            // Check if we detected at least one plane. If so, hide the loading message.
            /*if (mMessageSnackbar != null) {
                for (Plane plane : mSession.getAllTrackables(Plane.class)) {
                    if (plane.getType() == com.google.ar.core.Plane.Type.HORIZONTAL_UPWARD_FACING
                            && plane.getTrackingState() == TrackingState.TRACKING) {
                        hideLoadingMessage();
                        break;
                    }
                }
            }*/

            // Visualize planes.
            mPlaneRenderer.drawPlanes(
                    mSession.getAllTrackables(Plane.class), camera.getDisplayOrientedPose(), projmtx);

            // Visualize anchors created by touch.
            float scaleFactor = 10.0f;
            LayRenderer lay;
            for (Anchor anchor : mAnchors) {
                if (anchor.getTrackingState() != TrackingState.TRACKING) {
                    continue;
                }
                if ((lay = this.objects.get(anchor)) != null) {
                    PlaceObjects(lay, projmtx, viewmtx, lightIntensity, scaleFactor, anchor);
                }
            }

            if (anchorCreated) {
                drawTheARObjects(scaleFactor, viewmtx, projmtx, lightIntensity);
            }

            if(dismissSnackbar){
                hideLoadingMessage();
            }

            if(drawSnackbar && !drawn){
                showParkMessage();
                drawSnackbar = false;
                drawn=true;
            }

        } catch (Throwable t) {
            // Avoid crashing the application due to unhandled exceptions.
            Log.e(TAG, "Exception on the OpenGL thread", t);
        }

    }

    /**
     * Method use to draw all the ARObject currently in the ARObject Manager
     *
     * @param scaleFactor    The scalefactor use to draw the ARObject (it's size)
     * @param viewmtx        The view matrix
     * @param projmtx        The projection matrix
     * @param lightIntensity The light intensity
     * @throws IOException When failing generating XML Layout
     */
    private void drawTheARObjects(float scaleFactor, float[] viewmtx, float[] projmtx, float lightIntensity) throws IOException {
        for (String type : mARObject.getDisplayableMap().keySet()) {
            for (ARObject arObject : mARObject.getDisplayableMap().get(type)) {
                Anchor anchor = arObject.getArPosition().getAnchor();
                if (anchor == null || anchor.getTrackingState() != TrackingState.TRACKING) {
                    continue;
                }
                anchor.getPose().toMatrix(mAnchorMatrix, 0);
                LayRenderer mLayRenderer = arObject.getLayRenderer();
                mLayRenderer.updateModelMatrix(mAnchorMatrix, scaleFactor, angle);
                mLayRenderer.draw(viewmtx, projmtx, lightIntensity);
            }
        }
    }

    /**
     * Function to place and update Ar Objects
     *
     * @param lay            Which layout to update
     * @param projmtx        Projection matrix
     * @param viewmtx        View matrix
     * @param lightIntensity Light intensity value
     * @param scaleFactor    Scale factor value
     * @param anchor         Anchor associated to the Ar Objects
     */
    private void PlaceObjects(LayRenderer lay, float[] projmtx, float[] viewmtx, float lightIntensity, float scaleFactor, Anchor anchor) {
        anchor.getPose().toMatrix(mAnchorMatrix, 0);
        lay.updateModelMatrix(mAnchorMatrix, scaleFactor, angle);
        lay.draw(viewmtx, projmtx, lightIntensity);

    }

    /**
     * Compare position between the pose of an object and the pose of a hit (tap on the screen)
     *
     * @param a       Pose of the object
     * @param hitPose Pose of the hit
     * @return True is the hit was on an object, false otherwise
     */
    private boolean comparePosition(Pose a, Pose hitPose) {
        float minX, maxX, minZ, maxZ;
        minX = a.tx() - 0.6f;
        maxX = a.tx() + 0.6f;
        minZ = a.tz() - 2.5f;
        maxZ = a.tz() + 0.3f;
        float x = hitPose.tx();
        //float y = hitPose.ty();
        float z = hitPose.tz();
        if (x > minX && x < maxX) {
            if (z > minZ && z < maxZ)
                return true;
        }
        return false;
    }

    private void showSnackbarMessage(String message, boolean finishOnDismiss) {
        mMessageSnackbar = Snackbar.make(
                HelloArActivity.this.findViewById(android.R.id.content),
                message, Snackbar.LENGTH_INDEFINITE);
        mMessageSnackbar.getView().setBackgroundColor(0xFF51A99C);
        if (finishOnDismiss) {
            mMessageSnackbar.setAction(
                    "Dismiss",
                    v -> mMessageSnackbar.dismiss());
            mMessageSnackbar.addCallback(
                    new BaseTransientBottomBar.BaseCallback<Snackbar>() {
                        @Override
                        public void onDismissed(Snackbar transientBottomBar, int event) {
                            super.onDismissed(transientBottomBar, event);
                            finish();
                        }
                    });
        }
        mMessageSnackbar.show();
    }

    private void showParkMessage(){
        runOnUiThread(()-> showSnackbarMessage(messageSnackbar,false));
    }

    /*private void showLoadingMessage() {
        runOnUiThread(() -> showSnackbarMessage("Searching for surfaces...", false));
    }*/

    private void hideLoadingMessage() {
        Log.e("Eiko","Dismissing the snackbar");
        drawn=false;
        runOnUiThread(() -> {
            if (mMessageSnackbar != null) {
                mMessageSnackbar.dismiss();
            }
            mMessageSnackbar = null;
        });
    }

    /**
     * This method is called when the sensors have an update about their state.
     * It is use to calibrate the compass and create a bridge in orientation between AR and the real world.
     *
     * @param event Event caught
     */
    @Override
    public void onSensorChanged(SensorEvent event) {
        if (Permission.checkPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) &&
                Permission.checkPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            orientation.onLocationChanged(positionManager.getPosition());
        }
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            orientation.updateGravity(event);
        } else if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
            orientation.updateMagneticField(event);
        }
        orientation.updateBearing();
        updateTextBearing();
    }

    private void updateTextBearing() {
        Double myDouble = new Double(orientation.getBearing());
        textBearing.setText(String.format ("%.0f", myDouble)+"°");
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public void launchTracking(Context context, PositionManager pm) {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
            Position initPosition = pm.getFirstPosition();
            pm.checkPosition();
        }
    }

    private void updateARObjectManager(Session session, Pose camPose) throws IOException {
        if (mARObject != null) {
            mARObject.update(session, orientation.getBearing(), camPose);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(),"Updating Objects",Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (!downloadInit) {
            DataDownloader.getDataDownloader(this).downloadsSectors(positionManager.getPosition());
            downloadInit = true;
        }
        needUpdate = true;
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
        Toast.makeText(this, "Position located", Toast.LENGTH_SHORT);
    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(this, "Check your GPS status", Toast.LENGTH_SHORT).show();
    }

    public void enableGPS() {
        final LocationManager manager = (LocationManager) getSystemService(LOCATION_SERVICE);

        assert manager != null;
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Intent i = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);

            startActivityForResult(i, 1);
        }
    }
}
