package com.eiko.ar;

import android.content.Context;
import android.hardware.GeomagneticField;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import com.eiko.geolocalization.Position;
import com.eiko.geolocalization.PositionManager;
import com.eiko.matrix.LowPassFilter;

/**
 * Created by Korauw on 13/02/2018
 * This class is use to create a Compass and gather every information needed to make a bridge
 * between the real world and the augmented reality.
 */

public class RealOrientation {
    private static final int LOCATION_MIN_TIME = 30 * 1000;
    private static final int LOCATION_MIN_DISTANCE = 10;
    private float[] gravity = new float[3];
    // magnetic data
    private float[] geomagnetic = new float[3];
    // Rotation data
    private float[] rotation = new float[9];
    // orientation (azimuth, pitch, roll)
    private float[] orientation = new float[3];
    // smoothed values
    private float[] smoothed = new float[3];
    // sensor manager
    // sensor gravity
    private Sensor sensorGravity;
    private Sensor sensorMagnetic;
    private PositionManager location;
    private GeomagneticField geomagneticField;
    private double bearing = 0;

    /***
     * Initialize the two sensor needed with the SensorManager from the activity.
     * @param sensorManager
     */
    public RealOrientation(SensorManager sensorManager){
        sensorGravity = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorMagnetic = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

        //sensorManager.registerListener(sensorGravity,SensorManager.SENSOR_DELAY_NORMAL);
    }

    /***
     * Method use to takeForDisplay the magnetic sensor
     * @return the magnetic sensor
     */
    public Sensor getSensorMagnetic(){
        return sensorMagnetic;
    }

    /***
     * Method use to takeForDisplay the graivity sensor
     * @return the gravity sensor
     */
    public Sensor getSensorGravity(){
        return sensorGravity;
    }

    /***
     * Update the gravity according to the Sensor Event received (must be a change about the gravity Sensor)
     * @param event The event about the gravity Sensor
     * @return True to notify a modification.
     */
    public boolean updateGravity(SensorEvent event){
        smoothed = LowPassFilter.filter(event.values, gravity);
        gravity[0] = smoothed[0];
        gravity[1] = smoothed[1];
        gravity[2] = smoothed[2];
        return true;
    }

    /***
     * Update the geomagnetic data according to the event passed in parameter. It must be a change of
     * the magnetic field sensor.
     * @param event The event returned by the magnetic Field Sensor
     * @return true to notify of a modification.
     */
    public boolean updateMagneticField(SensorEvent event){
        smoothed = LowPassFilter.filter(event.values, geomagnetic);
        geomagnetic[0] = smoothed[0];
        geomagnetic[1] = smoothed[1];
        geomagnetic[2] = smoothed[2];
        return true;
    }

    /***
     * Method call when the location has changed, use this with your main activity to update the
     * geomagneticField with the new location.
     * @param position
     */
    public void onLocationChanged(Position position) {
        geomagneticField = new GeomagneticField(
                (float) position.getLatitude(),
                (float) position.getLongitude(),
                (float) 0,
                System.currentTimeMillis()
        );
    }

    private void getRotationMatrix(){
        SensorManager.getRotationMatrix(rotation, null, gravity, geomagnetic);
    }

    private void getOrientation(){
        SensorManager.getOrientation(rotation, orientation);
    }

    /**
     * Update the bearing from the north and keep it in degrees
     */
    public void updateBearing(){
        getRotationMatrix();
        getOrientation();
        bearing = orientation[0];
        // convert from radians to degrees
        bearing = Math.toDegrees(bearing);

        // fix difference between true North and magnetical North
        if (geomagneticField != null) {
            bearing += geomagneticField.getDeclination();
        }

        // bearing must be in 0-360
        if (bearing < 0) {
            bearing += 360;
        }
    }


    public double getBearing() {
        return bearing;
    }
}
