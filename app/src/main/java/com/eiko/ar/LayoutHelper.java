package com.eiko.ar;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.eiko.DataObject.DisplayableData;
import com.eiko.R;
import com.eiko.ar.rendering.LayRenderer;
import java.io.IOException;


/**
 * Created by Erwan on 16/02/2018, for project eiko
 */

public class LayoutHelper {

    /**
     * Enum of groups : each enum is corresponding to a category of object we can see in the
     * application, for testing purpose
     */
    public enum ArTypes {
        HydrobiologiePar ("HydrobiologiePar"),
        Flore ("Flore"),
        Invertebrés ("Invertebrés"),
        Amphibiens ("Amphibiens"),
        MacroInvertebrés ("MacroInvertebrés"),
        Physicochimie ("Physicochimie"),
        HydrobiologieTax ("HydrobiologieTax");
        public String name;
        ArTypes(String str) {name=str;}
    }

    /**
     * Function to create a layout that will be rendered in the application
     * @param context Activity's context
     * @return The layout created with all the parameters
     */
    // TODO should probably be the final version
    public static LayRenderer createInfoCard(Context context, DisplayableData data) throws IOException {
        ScrollView scrollLay = new ScrollView(context);
        ScrollView.LayoutParams scrollParams = new ScrollView.LayoutParams(ScrollView.LayoutParams.MATCH_PARENT, ScrollView.LayoutParams.MATCH_PARENT);
        scrollLay.setLayoutParams(scrollParams);
        int idScroll = View.generateViewId();
        scrollLay.setId(idScroll);

        // RELATIVELAYOUT CHILD
        RelativeLayout lay = new RelativeLayout(context);
        RelativeLayout.LayoutParams layParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        lay.setLayoutParams(layParams);
        int idLay = View.generateViewId();
        lay.setId(idLay);

        // TITLE
        RelativeLayout.LayoutParams titleParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        titleParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        titleParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
        TextView title = new TextView(context);
        title.setText(data.getName());
        title.setTextSize(32.0f);
        title.setTextColor(Color.BLACK);
        int idTitle = View.generateViewId();
        title.setId(idTitle);
        title.setLayoutParams(titleParams);

        //-- PICTURE --
        RelativeLayout.LayoutParams pictureParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        pictureParams.addRule(RelativeLayout.BELOW, title.getId());
        pictureParams.addRule(RelativeLayout.ALIGN_PARENT_START, RelativeLayout.TRUE);
        ImageView picture = new ImageView(context);
        picture.setLayoutParams(pictureParams);
        picture.setImageResource(data.getImage());
        int idPicture = View.generateViewId();
        picture.setId(idPicture);
        picture.setMinimumHeight(300);
        picture.setMinimumWidth(300);

        //-- TEXT --
        RelativeLayout.LayoutParams textParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        textParams.addRule(RelativeLayout.BELOW, title.getId());
        textParams.addRule(RelativeLayout.END_OF, picture.getId());
        textParams.setMargins(10,20,0,20);
        TextView text = new TextView(context);
        text.setText("Date de prélèvement : " + data.getDate() + "\nStationName :\n" + data.getGeom());
        text.setTextSize(25.0f);
        text.setTextColor(Color.BLACK);
        int idText = View.generateViewId();
        text.setId(idText);
        text.setLayoutParams(textParams);

        //-- FINALIZATION --
        lay.setBackgroundColor(Color.WHITE);
        lay.setOnClickListener(new View.OnClickListener() {
            float h = -1;
            @Override
            public void onClick(View v) {
                if (h == -1)
                    h = v.getHeight();
                if (h > 1200) {
                    v.scrollBy(0, 400);
                    h-=400;
                } else {
                    v.setScrollY(0);
                    h = v.getHeight();
                }
            }
        });
        lay.addView(title);
        lay.addView(picture);
        lay.addView(text);
        scrollLay.addView(lay);
        LayRenderer mLayRenderer = new LayRenderer(context, scrollLay);
        mLayRenderer.createOnGlThread(context);
        mLayRenderer.setMaterialProperties(0.0f, 3.5f, 1.0f, 6.0f);
        return mLayRenderer;
    }
}
