/*
 * Copyright 2017 Google Inc. All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.eiko.ar.rendering;
import com.eiko.renderTextRender.GLText;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.Matrix;

import java.io.IOException;


/**
 * Renders an object loaded from an OBJ file in OpenGL.
 */
public class TextRenderer {
    /**
     * Blend mode.
     *
     */
    /*public enum BlendMode {
        /** Multiplies the destination color by the source alpha. */
        /*Shadow,
        /** Normal alpha blending. */
        /*Grid
    }*/
    //private BlendMode mBlendMode = null;

    // Temporary matrices allocated here to reduce number of allocations for each frame.
    private float[] mModelMatrix = new float[16];
    private float[] mViewProjectionMatrix = new float[16];

    // Set some default material properties to use for lighting.
    //private float mAmbient = 0.3f;
    //private float mDiffuse = 1.0f;
    //private float mSpecular = 1.0f;
    //private float mSpecularPower = 6.0f;

    private GLText glText;

    public TextRenderer() {
    }

    /**
     * Creates and initializes OpenGL resources needed for rendering the model.
     *
     * @param context Context for loading the shader and below-named model and texture assets.
     */
    public void createOnGlThread(Context context) throws IOException {
        Matrix.setIdentityM(mModelMatrix, 0);

        glText = new GLText(context.getAssets());
        glText.load("Roboto-Regular.ttf", 14, 2, 2);
        GLES20.glEnable(GLES20.GL_BLEND);
        GLES20.glBlendFunc(GLES20.GL_ONE, GLES20.GL_ONE_MINUS_SRC_ALPHA);
    }
    /*
    /**
     * Selects the blending mode for rendering.
     *
     * @param blendMode The blending mode.  Null indicates no blending (opaque rendering).
     */
    /*public void setBlendMode(BlendMode blendMode) {
        mBlendMode = blendMode;
    }*/

    /**
     * Updates the object model matrix and applies scaling.
     *
     * @param modelMatrix A 4x4 model-to-world transformation matrix, stored in column-major order.
     * @param scaleFactor A separate scaling factor to apply before the {@code modelMatrix}.
     * @see Matrix
     */
    public void updateModelMatrix(float[] modelMatrix, float scaleFactor) {
        float[] scaleMatrix = new float[16];
        Matrix.setIdentityM(scaleMatrix, 0);// si scale = 5.0
        scaleMatrix[0] = scaleFactor; // ( 5.0   0.0   0.0   0.0 )
        scaleMatrix[5] = scaleFactor; // ( 0.0   5.0   0.0   0.0 )
        scaleMatrix[10] = scaleFactor;// ( 0.0   0.0   5.0   0.0 )
                                      // ( 0.0   0.0   0.0   1.0 ) = homothétie
        Matrix.multiplyMM(mModelMatrix, 0, modelMatrix, 0, scaleMatrix, 0);
    }
    /*
    /**
     * Sets the surface characteristics of the rendered model.
     *
     * @param ambient  Intensity of non-directional surface illumination.
     * @param diffuse  Diffuse (matte) surface reflectivity.
     * @param specular  Specular (shiny) surface reflectivity.
     * @param specularPower  Surface shininess.  Larger values result in a smaller, sharper
     *     specular highlight.
     */
    /*public void setMaterialProperties(
            float ambient, float diffuse, float specular, float specularPower) {
        mAmbient = ambient;
        mDiffuse = diffuse;
        mSpecular = specular;
        mSpecularPower = specularPower;
    }*/

    /**
     * Draws the model.
     *
     * @param cameraView  A 4x4 view matrix, in column-major order.
     * @param cameraPerspective  A 4x4 projection matrix, in column-major order.
     * @see #updateModelMatrix(float[], float)
     * @see Matrix
     */
    public void draw(float[] cameraView, float[] cameraPerspective) {
        Matrix.multiplyMM(mViewProjectionMatrix, 0, cameraPerspective, 0, cameraView, 0);
        glText.begin( 1.0f, 1.0f, 1.0f, 0.0f, mViewProjectionMatrix );         // Begin Text Rendering (Set Color WHITE)
        glText.draw("POULET", 1.0f, 1.0f, 10.0f, 0f, mModelMatrix);
        glText.end();
    }

    /*private static void normalizeVec3(float[] v) {
        float reciprocalLength = 1.0f / (float) Math.sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
        v[0] *= reciprocalLength;
        v[1] *= reciprocalLength;
        v[2] *= reciprocalLength;
    }*/
}
