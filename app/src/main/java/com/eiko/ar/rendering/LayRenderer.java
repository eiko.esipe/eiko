package com.eiko.ar.rendering;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.hardware.display.DisplayManager;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ScrollView;

import java.io.IOException;

/**
 * Renders an object loaded from an OBJ file in OpenGL.
 */
public class LayRenderer extends ObjectRenderer {
    private ScrollView relLay;
    private final Context context;

    /**
     * Constructor
     * @param context Application's context
     * @param lay ScrollView object linked to this object
     */
    public LayRenderer(Context context, ScrollView lay) {
        this.context = context;
        relLay = lay;
    }

    /**
     * Function to perform a clic and update the display of the layout
     * @throws IOException If fail creating OpenGL object
     */
    public void updateLay() throws IOException {
        relLay.getChildAt(0).performClick();
        this.createOnGlThread(context);
    }

    /**
     * Function to read texture from a XML file
     * @param context Application's context
     * @return A Bitmap object
     */
    @Override
    Bitmap readTexture(Context context) throws IOException {
        final DisplayMetrics displayMetrics = new DisplayMetrics();
        final DisplayManager manager = context.getSystemService(DisplayManager.class);
        assert manager != null;
        manager.getDisplays()[0].getMetrics(displayMetrics);
        final int height = displayMetrics.heightPixels;
        final int width = displayMetrics.widthPixels;

        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(bitmap);

        int measuredWidth = View.MeasureSpec.makeMeasureSpec(width, View.MeasureSpec.EXACTLY);
        int measuredHeight = View.MeasureSpec.makeMeasureSpec(height, View.MeasureSpec.EXACTLY);

        relLay.measure(measuredWidth, measuredHeight);
        relLay.layout(0, 0, relLay.getMeasuredWidth(), relLay.getMeasuredHeight());
        relLay.draw(canvas);

        return bitmap;
    }

    public ScrollView getLayout() {return relLay;}
}
