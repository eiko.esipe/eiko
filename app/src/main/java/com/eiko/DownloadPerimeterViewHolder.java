package com.eiko;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eiko.DataObject.Perimeter;
import com.eiko.downloads.AsyncTasks.TilesDownloaderAsyncTask;
import com.eiko.downloads.DataDownloader;

/**
 * Created by axelheine on 24/02/2018.
 */

public class DownloadPerimeterViewHolder extends RecyclerView.ViewHolder {
    private PropertiesReader pr;
    private TilesDownloaderAsyncTask asyncTask;
    private DownloadPerimeterAdapter.Listener listenerInterface;
    private Perimeter perimeter;

    private RelativeLayout relativeLayout;
    private TextView textViewView;
    private ImageButton deleteButton;
    private ImageButton addButton;
    private ProgressBar progressSpinner;
    private Context context;

    public DownloadPerimeterViewHolder(View itemView, DownloadPerimeterAdapter.Listener listenerInterface, Context context) {
        super(itemView);

        DataDownloader dd = DataDownloader.getDataDownloader(context);
        pr = dd.getPr();

        this.context = context;

        this.listenerInterface = listenerInterface;

        this.relativeLayout = itemView.findViewById(R.id.cardLayout);
        this.textViewView = itemView.findViewById(R.id.list_item_string);

        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.setMarginEnd(5);
        lp.alignWithParent = true;
        lp.addRule(RelativeLayout.ALIGN_PARENT_END);

        this.deleteButton = new ImageButton(context);
        deleteButton.setBaselineAlignBottom(true);
        deleteButton.setLayoutParams(lp);
        deleteButton.setImageResource(R.drawable.ic_delete_black);
        deleteButton.setBackgroundColor(Color.parseColor("#00FF0000"));

        this.addButton = new ImageButton(context);
        addButton.setBaselineAlignBottom(true);
        addButton.setLayoutParams(lp);
        addButton.setImageResource(R.drawable.ic_download_black);
        addButton.setBackgroundColor(Color.parseColor("#00FF0000"));

        this.progressSpinner = new ProgressBar(context);
        progressSpinner.setIndeterminate(true);
        progressSpinner.setLayoutParams(lp);
    }

    public void bind(Perimeter perimeter, int position, boolean isDownloaded, boolean isDLRunning){
        this.perimeter = perimeter;
        textViewView.setText(perimeter.getQueryCode());

        relativeLayout.removeView(addButton);
        relativeLayout.removeView(deleteButton);
        relativeLayout.removeView(progressSpinner);
        if (!isDLRunning) {
            if (isDownloaded) {
                relativeLayout.addView(deleteButton);
            } else {
                relativeLayout.addView(addButton);
            }
        } else {
            relativeLayout.addView(progressSpinner);
        }

        addButton.setOnClickListener((view) -> {
            listenerInterface.runAsyncTask(perimeter);
            listenerInterface.add(perimeter.getQueryCode());
            listenerInterface.updateEvent(position);
        });

        deleteButton.setOnClickListener((view) -> {
            listenerInterface.delete(perimeter.getQueryCode());
            listenerInterface.updateEvent(position);
        });
    }
}
