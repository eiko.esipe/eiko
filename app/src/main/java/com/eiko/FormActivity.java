package com.eiko;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.eiko.DataObject.Station;
import com.eiko.database.FormJsonDB;
import com.eiko.database.StationDB;
import com.eiko.forms.Form;
import com.eiko.forms.FormServer;
import com.eiko.forms.PictureManagement;
import com.eiko.geolocalization.Position;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Classe Form lié à l'interface graphique de création d'objet Form.
 * latitude et longitude : coordonnées
 * LocationManager et LocationListener : Permet la localisation de l'appareil
 * hasCoord : boolean pour savoir si à la création du formulaire, les coordonnées ont été récupéré
 * gpsInfo : TextView correspondant aux coordonnées GPS
 */
public class FormActivity extends Activity {

    private Position position;
    private String userMail;

    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 1;
    private static final int REQUEST_IMAGE_CAPTURE = 2;
    private static final String NO_STATION_MSG = "Pas de station proche";
    private boolean PICTURE_OK = false;
    private boolean COMMENT_OK = false;

    private TextView gpsInfo;
    private ImageView picture;
    private EditText commentBox;
    private Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            position = (Position) extras.get("position");
            userMail = (String) extras.get("userMail");
        }

        spinner = findViewById(R.id.Form_Station_Value);
        commentBox = findViewById(R.id.Form_Commentary_Value);
        picture = findViewById(R.id.Form_Image_Value);
        gpsInfo = findViewById(R.id.Form_GPS_Value);

        initOnClickButtons();
        resetActivity();

        commentBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                COMMENT_OK = s.length() != 0;
                enableSendingButton();
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });
    }

    private void enableSendingButton() {
        Button b = findViewById(R.id.Form_Button_Send);
        if(COMMENT_OK && PICTURE_OK) {
            b.setEnabled(true);
            b.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.formButtonEnabled));
        } else {
            b.setEnabled(false);
            b.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.formButtonDisabled));
        }
    }

    /**
     * Init FormActivity buttons
     */
    private void initOnClickButtons() {
        findViewById(R.id.Form_Button_Modify).setOnClickListener(v -> dispatchTakePictureIntent());
        findViewById(R.id.Form_Button_Reset).setOnClickListener(v -> resetActivity());
        findViewById(R.id.Form_Button_Send).setOnClickListener(v -> {
            saveFormInDB();
            finish();
        });
    }

    /**
     * Run the camera to take a picture
     */
    private void dispatchTakePictureIntent() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},
                    MY_PERMISSIONS_REQUEST_CAMERA);
        } else {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    /**
     * Reset the form activity elements
     */
    private void resetActivity() {
        setGPSLocation();
        commentBox.setText("");
        picture.setImageResource(R.mipmap.dummy_picture);
        picture.setTag(PictureManagement.DUMMY_PICTURE);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, getSurroundingStations());
        spinner.setAdapter(dataAdapter);
        Button b = findViewById(R.id.Form_Button_Send);
        b.setEnabled(false);
        b.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.formButtonDisabled));
    }

    /**
     * Save the current form in the DB
     */
    private void saveFormInDB() {
        try {
            FormServer formServer = new FormServer(FormActivity.super.getApplicationContext());
            FormJsonDB formDB = new FormJsonDB(FormActivity.super.getApplicationContext());
            Form generatedForm = getForm(formServer.getPassword());
            Log.d("Eiko+", "Saving form: " + generatedForm.toJsonString());
            formDB.addJSON(generatedForm.toJsonString());
            Toast.makeText(FormActivity.super.getApplicationContext(), "Formulaire sauvegardé", Toast.LENGTH_SHORT).show();
        } catch (IOException | JSONException e) {
            Toast.makeText(FormActivity.super.getApplicationContext(), "Erreur sauvegarde formulaire", Toast.LENGTH_SHORT).show();
            Log.d("Eiko+", "Fail saving form");
            e.printStackTrace();
        }
    }

    /**
     * Generate a Form from UI elements
     * @param formServerPassword Form server password necessary to create a Form
     * @return Form object created
     */
    private Form getForm(String formServerPassword) {
        String imageString = PictureManagement.getImageString(picture);
        String comment = commentBox.getText().toString();
        String stationSpinner = spinner.getSelectedItem().toString();
        int startIndexOfDistance = stationSpinner.lastIndexOf(" ("); // station_name (distance m)
        String station = stationSpinner;
        if (startIndexOfDistance != -1) {
            station = stationSpinner.substring(0, startIndexOfDistance);
        }
        return Form.generateFormFromUI(userMail, formServerPassword,
                comment, imageString, position, station);
    }

    /**
     * Display GPS location
     */
    private void setGPSLocation() {
        String gpsString = String.format(getResources().getString(R.string.gps_message), position.getLatitude(), position.getLongitude());
        gpsInfo.setText(gpsString);
    }

    /**
     * Get at least the 3 closest stations
     * @return List of station names
     */
    private List<String> getSurroundingStations() {
        int renderDistance = 100;
        StationDB stationDB = new StationDB(this);
        List<Station> stationsList = new ArrayList<>();
        while (stationsList.size() < 3 && renderDistance < 10000) {
            stationsList = stationDB.getAllStationsAtDistance(position, renderDistance);
            renderDistance = renderDistance * 2;
        }
        if (stationsList.size() == 0) {
            ArrayList<String> noStationList = new ArrayList<>();
            noStationList.add(NO_STATION_MSG);
            return noStationList;
        }
        return stationsList.stream().map((s) -> s.getName() + " (" + (int)position.getDistanceTo(s.getPosition()) + "m)").collect(Collectors.toList());
    }

    /**
     * Fonction pour gérer le retour d'utilisation d'une application, ici l'appareil photo
     * @param requestCode Code correspondant à la demande (ici REQUEST_IMAGE_CAPTURE)
     * @param resultCode Code du résultat
     * @param data Intent contenant la donnée résultat de l'activité appelé
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            picture.setImageBitmap(imageBitmap);
            picture.setTag(PictureManagement.REAL_PICTURE);
            PICTURE_OK = true;
            enableSendingButton();
        }
    }
}
