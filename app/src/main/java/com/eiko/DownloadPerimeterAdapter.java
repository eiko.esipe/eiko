package com.eiko;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.eiko.DataObject.Perimeter;
import com.eiko.ObserverObservable.Observable;
import com.eiko.ObserverObservable.Observer;
import com.eiko.database.Database;
import com.eiko.downloads.AsyncTasks.DumpAsyncTask;
import com.eiko.downloads.DataDownloader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

/**
 * Created by axelheine on 24/02/2018.
 */
class DownloadPerimeterAdapter extends RecyclerView.Adapter<DownloadPerimeterViewHolder> implements Observer {
    private View emptyView;
    private View mainLayout;
    private List<Perimeter> perimeterList = new ArrayList<>();
    private List<Perimeter> showList = new ArrayList<>();
    private List<Perimeter> dlRunningList = new ArrayList<>();
    private Map<String, DumpAsyncTask> runningDownloads;
    private List<String> downloadedParks;
    private DataDownloader dataDownloader;
    private Listener listenerInterface;
    private Context context;
    private DownloadPerimeterAdapter instance;

    public DownloadPerimeterAdapter(View emptyView, View mainLayout, List<Perimeter> list, List<String> downloadedParks, DataDownloader dataDownloader, Context context) {
        this.instance = this;
        this.emptyView = emptyView;
        this.mainLayout = mainLayout;
        this.dataDownloader = dataDownloader;

        if (Objects.nonNull(list)) {
            setData(list);
        }
        this.downloadedParks = downloadedParks;
        this.context = context;

        this.listenerInterface = new DownloadPerimeterAdapter.Listener() {
            @Override
            public void updateEvent() { notifyDataSetChanged();
            }

            @Override
            public void updateEvent(int position) {
                notifyItemChanged(position);
            }

            @Override
            public void add(String parkName) {
                downloadedParks.add(parkName);
                notifyDataSetChanged();
            }

            @Override
            public void delete(String parkName) {
                downloadedParks.remove(parkName);
                Database.deleteAllInPark(parkName, context);
                Snackbar.make(mainLayout, "Parc supprimé : " + parkName, Snackbar.LENGTH_SHORT).show();
                notifyDataSetChanged();
            }

            @Override
            public void runAsyncTask(Perimeter p) {
                DumpAsyncTask task = dataDownloader.downloadDataFromAPI(p);
                task.addObservator(instance);
                dlRunningList.add(p);
                notifyDataSetChanged();
            }
        };
    }

    @Override
    public DownloadPerimeterViewHolder onCreateViewHolder(ViewGroup viewGroup, int itemType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_download_item ,viewGroup,false);
        return new DownloadPerimeterViewHolder(view, listenerInterface, context);
    }

    @Override
    public void onBindViewHolder(DownloadPerimeterViewHolder myViewHolder, int position) {
        Perimeter perimeter = showList.get(position);

        boolean isDL = false;
        if (downloadedParks.contains(perimeter.getQueryCode())) {
            isDL = true;
        }

        boolean isDlRunning = false;
        if (dlRunningList.contains(perimeter)) {
            isDlRunning = true;
        }

        myViewHolder.setIsRecyclable(false);
        myViewHolder.bind(perimeter, position, isDL, isDlRunning);
    }

    @Override
    public int getItemCount() {
        return showList.size();
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        showList.clear();
        if(emptyView != null) {
            emptyView.setVisibility(View.GONE);
        }

        if (charText.length() == 0) {
            showList.addAll(perimeterList);
        }
        else
        {
            for (Perimeter p : perimeterList)
            {
                if (p.getQueryCode().toLowerCase(Locale.getDefault()).contains(charText))
                {
                    showList.add(p);
                }
            }
        }

        if (showList.isEmpty()) {
            emptyView.setVisibility(View.VISIBLE);
        }

        notifyDataSetChanged();
    }

    public void setData(List<Perimeter> perimeterList) {
        this.perimeterList = perimeterList;
        Collections.sort(perimeterList);
        this.showList = new ArrayList<>();
        showList.addAll(perimeterList);
    }

    @Override
    public void actualize(Observable o, String message) {
        if (o instanceof DumpAsyncTask) {
            boolean error = false;
            Perimeter perimeter = null;
            if (message.toLowerCase().contains("error")) {
                message = message.substring(6);
                error = true;
            }

            for (Perimeter p: dlRunningList) {
                if(p.getQueryCode().equals(message)) {
                    perimeter = p;
                }
            }

            if (perimeter == null) {
                return;
            }

            if (error) {
                Snackbar.make(mainLayout, "Erreur lors du télécharhement du parc : " + perimeter.getQueryCode(), Snackbar.LENGTH_SHORT).show();
                downloadedParks.remove(perimeter.getQueryCode());
                Database.deleteAllInPark(message, context);
            } else {
                Snackbar.make(mainLayout, "Parc téléchargé : " + perimeter.getQueryCode(), Snackbar.LENGTH_SHORT).show();
            }

            dlRunningList.remove(perimeter);

            notifyDataSetChanged();
        }
    }

    interface Listener {
        /**
         * Function to call to update list when an event is throwed
         */
        void updateEvent();

        /**
         * Used to update one item at one precise position
         * @param position position of the item
         */
        void updateEvent(int position);

        /**
         * Add a parkname in the list, then update the adapter
         * @param parkName Park name to add
         */
        void add(String parkName);

        /**
         * Delete a parkname in the list, then update the adapter
         * @param parkName Park name to delete
         */
        void delete(String parkName);

        /**
         * Run a new task to download a perimeter.
         * @param p perimeter to download
         */
        void runAsyncTask(Perimeter p);
    }
}



