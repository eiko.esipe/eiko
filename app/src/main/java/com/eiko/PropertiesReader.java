package com.eiko;

import android.content.Context;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Properties;

/**
 * Created by Rinkui on 12/02/2018.
 *
 * /!\ Files have to be in assets ! /!\
 */

public class PropertiesReader {
    private static HashMap<String, String> propertiesMap = new HashMap<>();
    private static final String extension = ".properties";
    private static Context context;
    public static final String SERVER_IP = "SERVER.IP";
    public static final String SERVER_METADATA = "SERVER.METADATA";
    public static final String SERVER_GEODATA = "SERVER.GEODATA";
    public static final String SERVER_PORT = "SERVER.PORT";
    public static final String SERVER_APPNAME = "SERVER.APPNAME";
    public static final String SERVER_REQUEST_SUFFIX = "SERVER.REQUEST.SUFFIX";
    public static final String POSITION_UPDATE_TIME = "POSITION.UPDATE.TIME";
    public static final String POSITION_UPDATE_DISTANCE = "POSITION.UPDATE.DISTANCE";
    public static final String SERVER_REQUEST_RADIUS = "SERVER.REQUEST.RADIUS";
    public static final String SERVER_TYPEPERIMETERS = "SERVER.TYPEPERIMETERS";
    public static final String SERVER_NAMEPERIMETERS = "SERVER.NAMEPERIMETERS";
    public static final String SERVER_WHERE = "SERVER.WHERE";
    public static final String SERVER_DUMP = "SERVER.DUMP";
    public static final String SERVER_PERIMETERS = "SERVER.PERIMETERS";
    public static final String SERVER_TILES = "SERVER.TILES";
    public static final String PERIMETER_UPDATE_TIME = "PERIMETER.UPDATE.TIME";
    public static final String PERIMETER_UPDATE_DISTANCE = "PERIMETER.UPDATE.DISTANCE";
    public static final String AR_UPDATE_DISTANCE = "AR.UPDATE.DISTANCE";
    public static final String AR_UPDATE_TIME = "AR.UPDATE.TIME";
    public static final String ASYNCTASK_404 = "ASYNCTASK.404";
    public static final String ASYNCTASK_400 = "ASYNCTASK.400";
    public static final String ASYNCTASK_FINISHED = "ASYNCTASK.FINISHED";
    public static final String ASYNCTASK_IN_PROGRESS = "ASYNCTASK.PERIMETER.IN.PROGRESS";

    private static PropertiesReader proReader;

    private PropertiesReader() {
        propertiesMap = new HashMap<>();
    }

    public static PropertiesReader getProReader(Context context) {
        if(proReader == null){
            if(context == null){
                System.err.println("Le contexte ne doit pas être null");
                return null;
            }
            proReader = new PropertiesReader();
            PropertiesReader.context = context;
            readFile("server", context);
            readFile("positionManager", context);
            readFile("perimeterManager", context);
            readFile("arupdate",context);
            readFile("asyntask",context);
        }
        return proReader;
    }

    public static PropertiesReader getProReader(){
        return getProReader(context);
    }

    public static void readFile(String fileName, Context context){
        try {
            InputStream iss = context.getAssets().open(fileName + extension);
            Properties properties = new Properties();
            properties.load(iss);
            iss.close();

            Enumeration enuKeys = properties.keys();
            while (enuKeys.hasMoreElements()) {
                String key = (String) enuKeys.nextElement();
                String value = properties.getProperty(key);
                propertiesMap.put(key, value);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void resetPropertiesReader(){
        context = null;
        propertiesMap = null;
        proReader = null;
    }

    public String getProperty(String property){
        return propertiesMap.get(property);
    }
}

