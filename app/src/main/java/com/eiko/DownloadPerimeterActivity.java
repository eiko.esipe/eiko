package com.eiko;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.eiko.DataObject.Perimeter;
import com.eiko.DataObject.PerimeterManager;
import com.eiko.ObserverObservable.Observable;
import com.eiko.ObserverObservable.Observer;
import com.eiko.database.Database;
import com.eiko.downloads.AsyncTasks.PerimetersDownloaderAsynTask;
import com.eiko.downloads.DataDownloader;

import java.util.List;

public class DownloadPerimeterActivity extends AppCompatActivity implements Observer{
    private PerimeterManager pm;

    private ProgressBar progressBar;
    private RelativeLayout relativeLayout;

    private RecyclerView recyclerView;
    private DownloadPerimeterAdapter perimeterAdapter;

    private List<Perimeter> perimeters;
    private List<String> savedParks;

    private DataDownloader dataDownloader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_download_perimeter);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);

        progressBar = findViewById(R.id.download_progress_spinner);

        relativeLayout = findViewById(R.id.download_main_layout);

        this.dataDownloader = DataDownloader.getDataDownloader(getApplicationContext());

        pm = PerimeterManager.getPerimeterManager(this);
        perimeters = pm.getPerimeters();
        if (!perimeters.isEmpty()) {
            relativeLayout.removeView(progressBar);
        } else {
            DataDownloader.getDataDownloader(getApplicationContext()).downloadsPerimeters(this);
        }

        savedParks = Database.getSavedParks(this);

        recyclerView = findViewById(R.id.parkList);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        View emptyView = findViewById(R.id.empty_view);

        this.perimeterAdapter = new DownloadPerimeterAdapter(emptyView, relativeLayout, perimeters, savedParks, dataDownloader, this);

        recyclerView.setAdapter(perimeterAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        perimeterAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_download_menu, menu);
        MenuItem mSearch = menu.findItem(R.id.action_search);

        SearchView mSearchView = (SearchView) mSearch.getActionView();

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                perimeterAdapter.filter(newText);
                return true;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void actualize(Observable o, String message) {
        if(o instanceof PerimetersDownloaderAsynTask) {
            relativeLayout.removeView(progressBar);
            perimeters = pm.getPerimeters();
            perimeterAdapter.setData(perimeters);
            perimeterAdapter.notifyDataSetChanged();
        }
    }
}
