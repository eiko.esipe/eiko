package com.eiko;

import android.content.Context;
import android.util.Log;
import android.location.Location;

import com.eiko.DataObject.DisplayableData;
import com.eiko.ar.LayoutHelper;
import com.eiko.database.Database;
import com.eiko.database.UserParamDB;
import com.eiko.geolocalization.Position;
import com.eiko.geolocalization.PositionManager;
import com.eiko.matrix.ARObject;
import com.google.ar.core.Pose;
import com.google.ar.core.Session;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.eiko.ARObjectProvider.dataToARObject;
import static com.eiko.ARObjectProvider.getFromDatabase;

/**
 * Created by eikouser on 15/02/18.
 */

public class ARObjectManager {

    private Map<String,List<ARObject>> cacheMap;
    private Map<String,List<ARObject>> displayableMap;
    private PositionManager pm;
    private Context context;
    private int distance;
    private boolean filterPhysicochimie;
    private boolean filterBiologie;
    private boolean filterFlore;
    private boolean filterHydrobiologie;
    private Position dlAreaCenter;

    public ARObjectManager(PositionManager pm, Context context) {
        this.cacheMap = new HashMap<>();
        this.displayableMap = new HashMap<>();
        this.displayableMap.put("physicochimie", new ArrayList<>());
        this.displayableMap.put("biologie", new ArrayList<>());
        this.displayableMap.put("flore", new ArrayList<>());
        this.displayableMap.put("hydrobiologie", new ArrayList<>());
        this.pm = pm;
        this.context = context;
        this.dlAreaCenter = pm.getPosition();
    }

    public Map<String, List<ARObject>> getDisplayableMap() {
        return displayableMap;
    }

    /**
     * Clear the content of cacheMap and put the parameter instead.
     * @param arMap : new map in cacheMap.
     */
    private void setCacheMap(Map<String,List<ARObject>> arMap) {
        this.cacheMap.clear();
        this.cacheMap = new HashMap<>(arMap);
    }

    /**
     * Add an ARObject in the displayableMap or replace it if already present.
     * @param group : Class of the ARObject.
     * @param aro : ARObject to add in the displayableMap.
     */
    private void addARObjectInDisplayableMap(String group, ARObject aro) {
        int index;
        switch(group){
            case "physicochimie" :
                index = displayableMap.get("physicochimie").indexOf(aro);
                if(index == -1)
                    displayableMap.get("physicochimie").add(aro);
                else
                    displayableMap.get("physicochimie").set(index, aro);
                break;

            case "biologie" :
                index = displayableMap.get("biologie").indexOf(aro);
                if(index == -1)
                    displayableMap.get("biologie").add(aro);
                else
                    displayableMap.get("biologie").set(index, aro);
                break;

            case "flore" :
                index = displayableMap.get("flore").indexOf(aro);
                if(index == -1)
                    displayableMap.get("flore").add(aro);
                else
                    displayableMap.get("flore").set(index, aro);
                break;

            case "hydrobiologie" :
                index = displayableMap.get("hydrobiologie").indexOf(aro);
                if(index == -1)
                    displayableMap.get("hydrobiologie").add(aro);
                else
                    displayableMap.get("hydrobiologie").set(index, aro);
                break;
        }
    }

    /**
     * @Deprecated not used (maybe one day)
     * Remove the ARObject from displayableMap.
     * @param group : Class of the ARObject.
     * @param aro : ARObject to remove in the displayableMap.
     */
    private void removeARObjectInDisplayableMap(String group, ARObject aro){
        switch(group){
            case "physicochimie" :
                displayableMap.get("physicochimie").remove(aro);
                break;

            case "biologie" :
                displayableMap.get("biologie").remove(aro);
                break;

            case "flore" :
                displayableMap.get("flore").remove(aro);
                break;

            case "hydrobiologie" :
                displayableMap.get("hydrobiologie").remove(aro);
                break;
        }
    }

    /**
     * Clear all lists in displayableMap.
     */
    private void clearDisplayableMap(){
        for (List<ARObject> list: displayableMap.values()) {
            list.clear();
        }
    }

    /**
     * Take ARObject in cacheMap from the given class and put them to displayableMap if they are before the given distance.
     * @param classe : Class to filter.
     * @param distance : Distance max.
     */
    private void cacheMapForDisplayableMap(String classe, int distance){
        List<ARObject> arList = cacheMap.get(classe);
        if(arList != null) {
            for (ARObject aro : arList) {
                if (aro.getDistance() <= distance)
                    addARObjectInDisplayableMap(classe, aro);
            }
        }
    }

    /**
     * Get user's preferences from database.
     */
    private void updateUserPreferences(){
        UserParamDB upd = new UserParamDB(context);
        this.distance = upd.getRenderDistance();
        this.filterPhysicochimie = upd.getPhysicochimie();
        this.filterBiologie = upd.getBiologie();
        this.filterFlore = upd.getFlore();
        this.filterHydrobiologie = upd.getHydrobiologie();
    }

    /**
     * Update displayableMap according to user's preferences.
     */
    private void updateDisplayableMap(){
        updateUserPreferences();
        if(this.filterPhysicochimie) {
            cacheMapForDisplayableMap("physicochimie", this.distance);
        }
        else {
            displayableMap.get("physicochimie").clear();
        }

        if(this.filterBiologie) {
            cacheMapForDisplayableMap("biologie", this.distance);
        }
        else {
            displayableMap.get("biologie").clear();
        }

        if(this.filterFlore) {
            cacheMapForDisplayableMap("flore", this.distance);
        }
        else {
            displayableMap.get("flore").clear();
        }

        if(this.filterHydrobiologie) {
            cacheMapForDisplayableMap("hydrobiologie", this.distance);
        }
        else {
            displayableMap.get("hydrobiologie").clear();
        }
    }


    /**
     * @param session
     * @param bearing
     * @param userPosition
     *
     * This method is used to launch a new download from the DB.
     * Then, it will clear and fill again the DisplayableMap.
     */
    private void updateAR(Session session, double bearing, Position userPosition, Pose camPose, Context context) throws IOException {
        int currentDistance = distance;
        if(distance<=0){
            currentDistance = 50;
        }
        Map<String, List<? extends DisplayableData>> fromDatabase = Database.getAllObjectsAtDistanceByType(userPosition, currentDistance, context);
        printMap(fromDatabase);
        Map<String, List<ARObject>> allARObjects = dataToARObject(fromDatabase, bearing, userPosition);
        ARObjectManager.createAnchorObjectForAR(session, allARObjects, camPose, context);

        setCacheMap(allARObjects);
        clearDisplayableMap();
        updateDisplayableMap();
    }

    public void printMap(Map<String, List<? extends DisplayableData>> objsFromDB){
        for (String type : objsFromDB.keySet()){
            for(DisplayableData data : objsFromDB.get(type)){
            }
        }
    }


    /**
     * @param session
     * @param bearing
     *
     * This method will be called tu update the list of ARObjects to display.
     * It will calculate the distance between the user and the download position, and
     * regarding the value, update the displayableMap or the cacheMap (with a new download).
     */
    public void update(Session session, double bearing, Pose camPose) throws IOException {
        Position userPosition = pm.getPosition();
        float result[] = new float[5];

        Location.distanceBetween(userPosition.getLatitude(),
                userPosition.getLongitude(),
                dlAreaCenter.getLatitude(),
                dlAreaCenter.getLongitude(),
                result);

        if(result[0] < (float) distance * 1.5) {
            clearDisplayableMap();
            updateDisplayableMap();
        }
        else if(result[0] >= (float) distance * 1.5) {
            dlAreaCenter = userPosition;
            updateAR(session, bearing, userPosition, camPose, context);
        }
    }



    /**
     * Create the anchor relative to the current session.
     * @param session The ARCore session
     * @param allARObjects The map containing every ARObject created from the DB.
     * @return
     */
    public static Map<String, List<ARObject>> createAnchorObjectForAR(Session session, Map<String, List<ARObject>> allARObjects,Pose camPose, Context context) throws IOException {
        for(String type : allARObjects.keySet()){
            for(ARObject data : allARObjects.get(type)){
                data.setLayRenderer(LayoutHelper.createInfoCard(context, data.getData()));
                data.setARObjectAnchor(session, camPose);
            }
        }
        return allARObjects;
    }

    public static void adjustArPosition(ARObject reference, ARObject objectToAdjust) {
        // Verify Delta-X
        if(reference.getX() - objectToAdjust.getX() > 0 && reference.getX() - objectToAdjust.getX() <= 1 && Math.abs(reference.getZ() - objectToAdjust.getZ()) <= 1) {
            objectToAdjust.setX(objectToAdjust.getX() - 1);
        }
        if(reference.getX() - objectToAdjust.getX() >= -1 && reference.getX() - objectToAdjust.getX() <= 0 && Math.abs(reference.getZ() - objectToAdjust.getZ()) <= 1) {
            objectToAdjust.setX(objectToAdjust.getX() + 1);
        }
        // Verify Delta-Z
        if(reference.getZ() - objectToAdjust.getZ() > 0 && reference.getZ() - objectToAdjust.getZ() <= 1 && Math.abs(reference.getX() - objectToAdjust.getX()) <= 1) {
            objectToAdjust.setZ(objectToAdjust.getZ() - 1);
        }
        if(reference.getZ() - objectToAdjust.getZ() >= -1 && reference.getZ() - objectToAdjust.getZ() <= 0 && Math.abs(reference.getX() - objectToAdjust.getX()) <= 1) {
            objectToAdjust.setZ(objectToAdjust.getZ() + 1);
        }
    }
}
