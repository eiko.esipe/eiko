package com.eiko.renderer;

import android.content.Context;
import android.opengl.GLSurfaceView;

/**
 * Created by Erwan on 09/02/2018.
 */

public class EikoSurfaceView extends GLSurfaceView {

    private EikoRenderer mRenderer;
    //private TexampleRenderer r;

    public EikoSurfaceView(Context context) {
        super(context);
        // Create an OpenGL ES 2.0 context.
        //setEGLContextClientVersion(2);

        // Set the Renderer for drawing on the GLSurfaceView
        mRenderer = new EikoRenderer();
        //r = new TexampleRenderer(context);
        //setRenderer(r);

        // Render the view only when there is a change in the drawing data
        setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
    }
}
