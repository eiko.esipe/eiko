package com.eiko.renderer;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class RenderViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EikoSurfaceView view = new EikoSurfaceView(this);
        setContentView(view);
    }
}
