package com.eiko.permission;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;

import com.eiko.downloads.DataDownloader;

import static android.support.v4.app.ActivityCompat.requestPermissions;

/**
 * Created by Rinkui on 09/02/2018.
 * <p>
 * /!\ Pour chaque permission, choisir un requestCode /!\
 * <p>
 * write : 2
 * read : 3
 * Fine location : 4
 * Coarse location : 5
 * All : 6
 */

public class Permission {

    public static void getPermissions(Activity activity) {
        requestPermissions(activity, new String[] { Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA},
                6);
    }

    public static boolean checkPermission(Activity activity, String permissionType){
        return ContextCompat.checkSelfPermission(activity,permissionType) == PackageManager.PERMISSION_GRANTED;
    }

    /*public static boolean checkLocationPermission(Activity){

    }*/


    /**
     * This method seems deprecated.
     * @param requestCode ah
     * @param permissions ah
     * @param grantResults ah
     * @param activity ah
     * @param context merci michel
     */
    public static void onRequestPermission(int requestCode, String[] permissions, int[] grantResults, Activity activity, Context context) {
        switch (requestCode) {
            case 2:
                System.out.println("External storage write");
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    System.out.println("Permission: " + permissions[0] + "was " + grantResults[0]);
                    //resume tasks needing this permission
                    DataDownloader.getDataDownloader().downloadsSectorsFromDefaultFile();
                } else {
                    // progress dismiss()
                }
                break;
            case 3:
                System.out.println("External storage read");
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    System.out.println("Permission: " + permissions[0] + "was " + grantResults[0]);
                    //resume tasks needing this permission
                    //WriteExternalStoragePermission.getWriteExternalStoragePermission().isWriteStoragePermissionGranted(activity, context);
                } else {
                    //progress.dismiss();
                }
                break;
            default:
                break;
        }
    }
}
