package com.eiko.authentification;

import com.eiko.util.EmailVerification;

/**
 * Manage the authentication of a public user to a technician
 */
public class Connection {
    /**
     * Try to connect a user
     * The authentication with the password is not done and needs a remote server
     * @param userMail Mail used to login
     * @param password Password of the account
     * @return User object if connected, null if not
     */
    public String connect(String userMail, String password) {
        if (EmailVerification.isValidEmail(userMail)) {
            return userMail;
        }
        return null;
    }
}
