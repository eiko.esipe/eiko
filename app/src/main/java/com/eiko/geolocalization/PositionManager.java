package com.eiko.geolocalization;

import android.Manifest;
import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.eiko.PropertiesReader;
import com.eiko.downloads.DataDownloader;


/**
 * Created by yslh on 05/02/18.
 * <p>
 * This class will allow the application to
 * save the datas (latitude / longitude) collected by the
 * smartphone GPS.
 * In the end, it allows to takeForDisplay a new object Position.
 */

public class PositionManager extends Service {
    /**
     * GPS settings
     */

    private final long MIN_DISTANCE_FOR_UPDATE = 3;         // Meters
    private final long MIN_TIME_FOR_UPDATE = 3 * 1000;     // Milliseconds

    /**
     * Device position
     */
    private Context context;
    private Location location;
    private Location lastDownloadLocation;
    private LocationManager lm;
    private boolean gpsStatus;
    private PropertiesReader propertyReader;

    private static PositionManager pm;


/* *************** */
/* *************** */

    /** Private constructor */
    private PositionManager(Context cont) {
        this.context = cont;
        this.location = new Location("Service Provider");
        this.gpsStatus = false;
        this.propertyReader = PropertiesReader.getProReader(cont);
    }

    public static void initPositionManager(Context cont) {
        if(pm == null) {
            pm = new PositionManager(cont);
        }
    }

    public static PositionManager getInstance() {
        if(pm == null) {
            System.out.println("PositionManager not initialized...");
            return null;
        }
        return pm;
    }

/* *************** */
/* *************** */

    private final LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location currentLocation) {
            // Detect new sector
            if(lastDownloadLocation == null) {
                lastDownloadLocation = currentLocation;
            }

            float result[] = new float[5];
            Location.distanceBetween(lastDownloadLocation.getLatitude(),
                    lastDownloadLocation.getLongitude(),
                    currentLocation.getLatitude(),
                    currentLocation.getLongitude(), result);

            if(result[0] >= 2000 && result[0] <= 2500) {
                //PropertiesReader.getProReader().getProperty(PropertiesReader.SERVER_REQUEST_DIAMETER);
                lastDownloadLocation = currentLocation;
                DataDownloader.getDataDownloader()
                        .downloadsSectors(new Position(lastDownloadLocation.getLatitude(), lastDownloadLocation.getLongitude()));
            }

            // Update the position via "location"
            location = currentLocation;
            /*Toast.makeText(context,
                    "-Auto-\nLatitude: " + location.getLatitude() + "\nLongitude: " + location.getLongitude(),
                    Toast.LENGTH_SHORT).show();*/

            // TODO Integrer DownloadManager pour gerer le changement de secteur

        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            /*String newStatus = "";
            switch (status) {
                case LocationProvider.OUT_OF_SERVICE:
                    newStatus = "OUT_OF_SERVICE";
                    break;
                case LocationProvider.TEMPORARILY_UNAVAILABLE:
                    newStatus = "TEMPORARILY_UNAVAILABLE";
                    break;
                case LocationProvider.AVAILABLE:
                    newStatus = "AVAILABLE";
                    break;
            }
            String msg = "Provider " + provider + " " + newStatus;
            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();*/
        }

        @Override
        public void onProviderEnabled(String provider) {
            String msg = "Provider " + provider + " enabled";
            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onProviderDisabled(String provider) {
            if(!lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                isLocated();
                if(!gpsStatus) {
                    location.setLongitude(0);
                    location.setLatitude(0);
                    Toast.makeText(context, "No GPS position detected yet...\nLat/Long = 0/0", Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(context, "GPS not available...\nKeeping last position", Toast.LENGTH_SHORT).show();
                }
            }
        }
    };


    /**
     * Allow to takeForDisplay a Position item
     * filled with a latitude and a longitude
     */
    public void checkPosition() {
        lm = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        try {
            assert lm != null;
            if (lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                lm.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                        Long.valueOf(propertyReader.getProperty(PropertiesReader.POSITION_UPDATE_TIME)),
                        Long.valueOf(propertyReader.getProperty(PropertiesReader.POSITION_UPDATE_DISTANCE)),
                        locationListener);
            } else {
                lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                        Long.valueOf(propertyReader.getProperty(PropertiesReader.POSITION_UPDATE_TIME)),
                        Long.valueOf(propertyReader.getProperty(PropertiesReader.POSITION_UPDATE_DISTANCE)),
                        locationListener);
            }
        } catch (SecurityException se) {
            throw new SecurityException("Need gps and network permissions");
        }
    }

    public Position getFirstPosition() {
        lm = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        assert lm != null;
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            location = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if (location == null) {
                return new Position(0, 0);
            }
            return changeLocationToPosition();
        } else if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (location == null) {
                return new Position(0, 0);
            }
            return changeLocationToPosition();
        }
        return null;
    }

    public boolean isLocated() {
        if (location.getLongitude() == 0 && location.getLatitude() == 0) {
            return false;
        }
        return true;
    }

    public boolean getGpsStatus() {
        return gpsStatus;
    }

    public Location getLocation() { return location; }

    public Position getPosition() { return changeLocationToPosition(); }

    /**
     * Convert a Location item in a Position item
     */
    public Position changeLocationToPosition() {
        if (location==null){
            return new Position(0,0);
        }
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();

        return (new Position(latitude, longitude));
    }

  /*  @Override
    public void onLocationChanged(Location location) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }*/

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }
}