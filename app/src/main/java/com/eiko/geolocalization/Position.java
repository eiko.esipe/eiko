package com.eiko.geolocalization;

import java.io.Serializable;
import java.security.AlgorithmConstraints;

import android.location.Location;


/**
 * Created by yslh on 05/02/18.
 *
 * This class will contain the GPS position of
 * the device.
 * Each position will be represented by an
 * object "Position".
 */

public class Position implements Serializable {
    private final double latitude;
    private final double longitude;
    private final double altitude;

    public Position(double latitude, double longitude, double altitude) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
    }

    public Position(double latitude, double longitude) {
        this(latitude, longitude, 0);
    }

    /**
     * Allows to takeForDisplay the values of latitude
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * Allows to takeForDisplay the values of longitude
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     * Allows to takeForDisplay the values of altitude
     */
    public double getAltitude() {
        return altitude;
    }

    /**
     * Return distance in meters between this position from anoter position
     * @param p2 Anoter Position object
     * @return distance between the two Position objects in meters
     */
    public float getDistanceTo(Position p2) {
        Location l1 = new Location("");
        l1.setLatitude(latitude);
        l1.setLongitude(longitude);

        Location l2 = new Location("");
        l2.setLatitude(p2.getLatitude());
        l2.setLongitude(p2.getLongitude());

        return l1.distanceTo(l2);
    }

    @Override
    public String toString() {
        return "x : " + longitude +
                "\ny : " + latitude;
    }
}