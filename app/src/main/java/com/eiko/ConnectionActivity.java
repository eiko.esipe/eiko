package com.eiko;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.eiko.authentification.Connection;
import com.eiko.database.UserParamDB;

public class ConnectionActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connection);

        EditText emailBox = findViewById(R.id.email_value);
        EditText passwordBox = findViewById(R.id.password_value);
        Button connectionButton = findViewById(R.id.connection_button);
        connectionButton.setOnClickListener((v) -> {
            String email = emailBox.getText().toString();
            String password = passwordBox.getText().toString();
            String userMail = new Connection().connect(email, password);
            if (userMail != null) {
                UserParamDB userParamDB = new UserParamDB(getApplicationContext());
                userParamDB.setUserMail(userMail);
                setResult(Activity.RESULT_OK, new Intent());
                finish();
            } else {
                findViewById(R.id.error_message).setVisibility(View.VISIBLE);
            }
        });
    }
}
