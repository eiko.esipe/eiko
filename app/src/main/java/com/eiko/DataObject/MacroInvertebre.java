package com.eiko.DataObject;

import android.support.annotation.NonNull;
import android.util.Log;

import com.eiko.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by eikouser on 06/02/18.
 */

public class MacroInvertebre extends Biologie {
    private static final int image = R.mipmap.macroinvertebre;

    public MacroInvertebre(Map<String, String> map) {
        super(map);
    }

    /**
     * Parse a JSON and return a list of macro invertebrate.
     * @param json JSONArray to be parsed.
     * @return List of all macro invertebrates in the JSON.
     */
    public static List<MacroInvertebre> fromJSON(JSONArray json) {
        List<MacroInvertebre> list = new ArrayList<>();
        JSONObject jsondata;
        for(int index = 0; index< json.length(); index++){
            try {
                Map<String, String> data;
                jsondata = json.getJSONObject(index);

                //Récupération des données communes
                data = commonData(jsondata);
                if(data == null)
                    continue;

                //Récupération des coordonnées
                JSONObject geom =jsondata.getJSONObject("geom");
                ArrayList<Double> positions = DisplayableData.getCoordinates(geom);

                List<Double> centeredPositions = DisplayableData.getCenteredPosition(positions);
                //Ajout des coordonnées centrées
                data.put("longitude",centeredPositions.get(0).toString());
                data.put("latitude",centeredPositions.get(1).toString());

                JSONArray taxons = jsondata.getJSONArray("occurences_taxons");
                //Récupération des occurences
                ArrayList<JSONObject> analyses = getOccurences(taxons);

                //Pour chaque occurences_taxon
                for (int i=0; i < analyses.size(); i++) {
                    //Récupération de toutes les analyses pour une espèce
                    ArrayList<JSONObject> taxon_analyses = getAllTaxonAnalysis(analyses.get(i));
                    if(taxon_analyses == null)
                        continue;

                    //Comparaison des dates des analyses d'un taxon et récupération de l'analyse la plus récente
                    JSONObject last_analyse = getLastAnalysis(taxon_analyses);

                    //Création d'une map avec données communes et celles du taxon
                    Map<String, String> taxon_map = getTaxonData(data, last_analyse);
                    //Récupération de l'espèce/taxon et ajout dans la map
                    taxon_map.put("taxon",analyses.get(i).getString("taxon"));

                    //Création et ajout de l'objet dans la liste
                    list.add(new MacroInvertebre(taxon_map));
                }

            } catch (JSONException e) {
                Log.e("Eiko","Error while parsing macroinvertebrate json");
            }
        }
        return list;
    }

    /**
     * Get common data to all macro invertebrate species in a JSON.
     * @param json JSONObject to be parsed.
     * @return Map with the String of name and value of data.
     */
    private static Map<String, String> commonData(JSONObject json) {
        Map<String, String> data = new HashMap<>();
        //Récupération des data
        try {
            data.put("station_code",json.getString("station_code"));
            data.put("altitude",Double.toString(json.optDouble("altitude")));
        } catch (JSONException e) {
            Log.e("Eiko","Error while getting macroinvertebrate's common data");
            return null;
        }
        return data;
    }

    /**
     * Get all taxon and analysis.
     * @param taxons JSONArray to be parsed.
     * @return List of JSONObject with taxon name and all analysis.
     */
    @NonNull
    private static ArrayList<JSONObject> getOccurences(JSONArray taxons) {

        ArrayList<JSONObject> occurences = new ArrayList<>();
        //Récupération des occurences
        try {
            for(int i =0; i < taxons.length(); i++ ){
                occurences.add(taxons.getJSONObject(i));
            }
        } catch (JSONException e) {
            Log.e("Eiko","Error while getting all macroinvertebrate's occurrences");
        }

        return occurences;
    }

    /**
     * Get all analysis of one taxon.
     * @param analyses JSONObject with taxon name and all analysis.
     * @return List of JSONObject (analysis).
     */
    @NonNull
    private static ArrayList<JSONObject> getAllTaxonAnalysis(JSONObject analyses) {
        //Récupération de toutes les analyses pour une espèce
        ArrayList<JSONObject> taxon_analyses = new ArrayList<>();
        int nombre_analyse;
        try {
            nombre_analyse = analyses.getJSONArray("analyses").length();
            for (int i = 0; i < nombre_analyse; i++) {
                taxon_analyses.add(analyses.getJSONArray("analyses").getJSONObject(i));
            }
        } catch (JSONException e) {
            Log.e("Eiko","Error while getting all macroinvertebrate's analyzes");
            return null;
        }

        return taxon_analyses;
    }

    /**
     * Get the most recent analysis for one taxon.
     * @param taxon_analyses List of JSONObject with all analysis.
     * @return JSONObject of the last analysis.
     */
    private static JSONObject getLastAnalysis(ArrayList<JSONObject> taxon_analyses) {
        int index = 0;
        try {
            String date_recente = taxon_analyses.get(0).getString("date_de_sortie");
            String date;

            SimpleDateFormat sdf = new SimpleDateFormat("y-M-d");
            for(int i = 0; i < taxon_analyses.size(); i++){
                date = taxon_analyses.get(i).getString("date_de_sortie");
                //Comparaison des dates
                if(sdf.parse(date).after(sdf.parse(date_recente))){
                    date_recente = date;
                    index = i;
                }
            }
        } catch (JSONException e) {
            Log.e("Eiko","Unparsable date while getting macroinvertebrate's last analysis");
        } catch (ParseException e) {
            Log.e("Eiko","Unparsable date (MacroInvertebre.getLastAnalysis)");
        }
        return taxon_analyses.get(index);
    }

    /**
     * Get data from an analysis.
     * @param data Map with common data.
     * @param last_analyse JSONObject of the most recent analysis.
     * @return Map with common data, data of the analysis and taxon.
     */
    @NonNull
    private static Map<String, String> getTaxonData(Map<String, String> data, JSONObject last_analyse) {
        Map<String, String> taxon_map = data;
        try {
            //Ajout des données dans la map
            taxon_map.put("total_ind", Integer.toString(last_analyse.getInt("total_ind")));
            taxon_map.put("date_de_sortie", last_analyse.getString("date_de_sortie"));

        } catch (JSONException e) {
            Log.e("Eiko","Error while getting macroinvertebrate's taxon data");
        }
        return taxon_map;
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public int getImage(){
        return image;
    }
}
