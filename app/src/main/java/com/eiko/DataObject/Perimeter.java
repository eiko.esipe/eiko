package com.eiko.DataObject;

import android.support.annotation.NonNull;

import com.eiko.geolocalization.Position;

import java.text.Collator;
import java.util.List;
import java.util.Locale;

/**
 * Created by Rinkui on 15/02/2018.
 */

public class Perimeter implements Comparable{
    private String type;
    private String queryCode;
    private List<List<Position>> boundary;

    public Perimeter(String type, String queryCode, List<List<Position>> boundary){
        this.queryCode = queryCode;
        this.type = type;
        this.boundary = boundary;
    }

    public Perimeter(String type, String queryCode){
        this(type, queryCode, null);
    }

    public String getQueryCode() {
        return queryCode;
    }

    public String getType() {
        return type;
    }

    public List<List<Position>> getBoundary() {
        return boundary;
    }

    @Override
    public String toString() {
        return queryCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Perimeter perimeter = (Perimeter) o;

        if (type != null ? !type.equals(perimeter.type) : perimeter.type != null) return false;
        return queryCode != null ? queryCode.equals(perimeter.queryCode) : perimeter.queryCode == null;
    }

    @Override
    public int hashCode() {
        int result = type != null ? type.hashCode() : 0;
        result = 31 * result + (queryCode != null ? queryCode.hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(@NonNull Object o) {
        Collator c = Collator.getInstance(Locale.FRENCH);
        return c.compare(queryCode, ((Perimeter) o).getQueryCode());
    }
}
