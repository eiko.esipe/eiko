package com.eiko.DataObject;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by eikouser on 14/02/18.
 */

public class HydrobiologieTax extends Hydrobiologie{

    private final String taxon;
    private final String taxons_denormalises;
    private final String taxons_code;
    private final String nature_station;

    public HydrobiologieTax(Map<String, String> map) {
        super(map);
        this.taxon = map.get("taxon");
        this.taxons_denormalises = map.get("taxons_denormalises");
        this.taxons_code = map.get("taxons_code");
        this.nature_station = map.get("naturestation");
    }

    /**
     * Parse a JSON and return a list of hydrobiology taxon.
     * @param json JSONArray to be parsed.
     * @return List of all hydrobiology taxons in the JSON.
     */
    public static List<HydrobiologieTax> fromJSON(JSONArray json) {
        List<HydrobiologieTax> list = new ArrayList<>();
        JSONObject jsondata;
        try {
            for(int index = 0; index< json.length(); index++){
                Map<String, String> data;
                jsondata = json.getJSONObject(index);

                //Récupération des données communes
                data = commonData(jsondata);
                if(data == null)
                    continue;

                //Récupération des coordonnées
                JSONObject geom =jsondata.getJSONObject("geom");
                ArrayList<Double> positions = DisplayableData.getCoordinates(geom);

                List<Double> centeredPositions = DisplayableData.getCenteredPosition(positions);
                //Ajout des coordonnées centrées
                data.put("longitude",centeredPositions.get(0).toString());
                data.put("latitude",centeredPositions.get(1).toString());

                JSONArray analyses = jsondata.getJSONArray("analyses");
                //Comparaison des dates des analyses d'un taxon et récupération de l'analyse la plus récente
                JSONObject last_analyse = Hydrobiologie.getLastAnalysis(analyses);

                //Création d'une map avec données communes et celles du taxon
                Map<String, String> taxon_map = Hydrobiologie.getTaxonData(data, last_analyse);

                //Création et ajout de l'objet dans la liste
                list.add(new HydrobiologieTax(taxon_map));
            }
        } catch (JSONException e) {
            Log.e("Eiko","Error while parsing hydrobiologyTax json");
        }

        return list;
    }

    /**
     * Get common data to all hydrobiology taxon species in a JSON.
     * @param json JSONObject to be parsed.
     * @return Map with the String of name and value of data.
     */
    private static Map<String, String> commonData(JSONObject json) {
        Map<String, String> data = new HashMap<>();
        //Récupération des data
        try {
            data.put("taxons_denormalises",json.getString("taxons_denormalises"));
            data.put("altitude",Double.toString(json.optDouble("altitude")));
            data.put("taxon",json.getString("taxon"));
            data.put("taxon_code",json.getString("taxon_code"));
            data.put("station",json.getString("station"));
            data.put("nomstation",json.getString("nomstation"));
            data.put("naturestation", json.getString("naturestation"));
        } catch (JSONException e) {
            Log.e("Eiko","Error while getting hydrobiologyTax's common data");
            return null;
        }

        return data;
    }

    public String getName(){
        return taxon;
    }

    public String getTaxon() {
        return taxon;
    }

    public String getTaxons_denormalises() {
        return taxons_denormalises;
    }

    public String getTaxons_code() {
        return taxons_code;
    }

    public String getNature_station() {
        return nature_station;
    }

    @Override
    public String toString() {
        return taxon;
    }
}
