package com.eiko.DataObject;

import android.support.annotation.NonNull;
import android.util.Log;

import com.eiko.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by eikouser on 06/02/18.
 */

public class Invertebre extends Biologie {

    private final String duree_obs;
    private final String heure_depart;
    private final String temperature;
    private final String type_echantillon;
    private final String males;
    private final String nebulosite;
    private final String femelles;
    private final String observateur;
    private final String suivi_machine;
    private final String temps_obs;
    private static final int image = R.mipmap.invertebre;

    public Invertebre(Map<String, String> map) {
        super(map);
        this.duree_obs = map.get("duree_obs");
        this.heure_depart = map.get("heure_depart");
        this.temperature = map.get("temperature");
        this.type_echantillon = map.get("type_echantillon");
        this.males = map.get("males");
        this.nebulosite = map.get("nebulosite");
        this.femelles = map.get("femelles");
        this.observateur = map.get("observateur");
        this.suivi_machine = map.get("suivi_machine");
        this.temps_obs = map.get("temps_obs");
    }

    /**
     * Parse a JSON and return a list of invertebrate.
     * @param json JSONArray to be parsed.
     * @return List of all invertebrates in the JSON.
     */
    public static List<Invertebre> fromJSON(JSONArray json) {
        List<Invertebre> list = new ArrayList<>();
        JSONObject jsondata;

        for(int index = 0; index< json.length(); index++) {
            try {
                Map<String, String> data;
                jsondata = json.getJSONObject(index);

                //Récupération des données communes
                data = commonData(jsondata);
                if(data == null)
                    continue;

                //Récupération des coordonnées
                JSONObject geom = jsondata.getJSONObject("geom");

                ArrayList<Double> positions = DisplayableData.getCoordinates(geom);
                List<Double> centeredPositions = DisplayableData.getCenteredPosition(positions);
                //Ajout des coordonnées centrées
                data.put("longitude",centeredPositions.get(0).toString());
                data.put("latitude",centeredPositions.get(1).toString());

                JSONArray taxons = jsondata.getJSONArray("occurences_taxons");
                //Récupération des occurences
                ArrayList<JSONObject> analyses = getOccurences(taxons);

                //Pour chaque occurences_taxon
                for (int i = 0; i < analyses.size(); i++) {
                    //Récupération de toutes les analyses pour une espèce
                    ArrayList<JSONObject> taxon_analyses = getAllTaxonAnalysis(analyses.get(i));
                    if(taxon_analyses == null)
                        continue;

                    //Comparaison des dates des analyses d'un taxon et récupération de l'analyse la plus récente
                    JSONObject last_analyse = getLastAnalysis(taxon_analyses);

                    //Création d'une map avec données communes et celles du taxon
                    Map<String, String> taxon_map = getTaxonData(data, last_analyse);
                    //Récupération de l'espèce/taxon et ajout dans la map
                    taxon_map.put("taxon", analyses.get(i).getString("taxon"));

                    //Création et ajout de l'objet dans la liste
                    list.add(new Invertebre(taxon_map));
                }
            } catch (JSONException e) {
                Log.e("Eiko","Error while parsing invertebrate json");
            }
        }

        return list;
    }

    /**
     * Get common data to all invertebrate species in a JSON.
     * @param json JSONObject to be parsed.
     * @return Map with the String of name and value of data.
     */
    private static Map<String, String> commonData(JSONObject json) {
        Map<String, String> data = new HashMap<>();
        //Récupération des data
        try {
            data.put("station_code",json.getString("station_code"));
            data.put("altitude",Double.toString(json.optDouble("altitude")));
            data.put("station_code",json.getString("station_code"));
        } catch (JSONException e) {
            Log.e("Eiko","Error while getting invertebrate's common data");
            return null;
        }
        return data;
    }

    /**
     * Get all taxon and analysis.
     * @param taxons JSONArray to be parsed.
     * @return List of JSONObject with taxon name and all analysis.
     */
    @NonNull
    private static ArrayList<JSONObject> getOccurences(JSONArray taxons) {

        ArrayList<JSONObject> occurences = new ArrayList<>();
        //Récupération des occurences
        try {
            for(int i =0; i < taxons.length(); i++ ){
                occurences.add(taxons.getJSONObject(i));
            }
        } catch (JSONException e) {
            Log.e("Eiko","Error while getting all invertebrate's occurrences");
        }

        return occurences;
    }

    /**
     * Get all analysis of one taxon.
     * @param analyses JSONObject with taxon name and all analysis.
     * @return List of JSONObject (analysis).
     */
    @NonNull
    private static ArrayList<JSONObject> getAllTaxonAnalysis(JSONObject analyses) {
        //Récupération de toutes les analyses pour une espèce
        ArrayList<JSONObject> taxon_analyses = new ArrayList<>();
        int nombre_analyse;
        try {
            nombre_analyse = analyses.getJSONArray("analyses").length();
            for(int i = 0; i < nombre_analyse; i++){
                taxon_analyses.add(analyses.getJSONArray("analyses").getJSONObject(i));
            }
        } catch (JSONException e) {
            Log.e("Eiko","Error while getting all invertebrate's analyzes");
            return null;
        }

        return taxon_analyses;
    }

    /**
     * Get the most recent analysis for one taxon.
     * @param taxon_analyses List of JSONObject with all analysis.
     * @return JSONObject of the last analysis.
     */
    private static JSONObject getLastAnalysis(ArrayList<JSONObject> taxon_analyses) {
        int index = 0;
        try {
            String date_recente = taxon_analyses.get(0).getString("date_de_sortie");
            String date;

            SimpleDateFormat sdf = new SimpleDateFormat("y-M-d");
            for(int i = 0; i < taxon_analyses.size(); i++){
                date = taxon_analyses.get(i).getString("date_de_sortie");
                //Comparaison des dates
                if(sdf.parse(date).after(sdf.parse(date_recente))){
                    date_recente = date;
                    index = i;
                }
            }
        } catch (JSONException e) {
            Log.e("Eiko","Unparsable date while getting invertebrate's last analysis");
        } catch (ParseException e) {
            Log.e("Eiko","Unparsable date (Invertebre.getLastAnalysis)");
        }
        return taxon_analyses.get(index);
    }

    /**
     * Get data from an analysis.
     * @param data Map with common data.
     * @param last_analyse JSONObject of the most recent analysis.
     * @return Map with common data, data of the analysis and taxon.
     */
    @NonNull
    private static Map<String, String> getTaxonData(Map<String, String> data, JSONObject last_analyse) {
        Map<String, String> taxon_map = data;
        try {
            //Ajout des données dans la map
            taxon_map.put("duree_obs", Integer.toString(last_analyse.getInt("duree_obs")));
            taxon_map.put("heure_depart", last_analyse.getString("heure_depart"));
            taxon_map.put("temperature",Integer.toString(last_analyse.getInt("temperature")));
            taxon_map.put("type_echantillon", last_analyse.getString("type_echantillon"));
            taxon_map.put("males", last_analyse.getString("males"));
            taxon_map.put("nebulosite", Integer.toString(last_analyse.getInt("nebulosite")));
            taxon_map.put("date_de_sortie", last_analyse.getString("date_de_sortie"));
            taxon_map.put("femelles", last_analyse.optString("femelles"));
            taxon_map.put("observateur", last_analyse.getString("observateur"));
            taxon_map.put("suivi_metre", Integer.toString(last_analyse.getInt("suivi_metre")));
            taxon_map.put("temps_obs", last_analyse.getString("temps_obs"));
            taxon_map.put("total_ind", last_analyse.getString("total_ind"));
        } catch (JSONException e) {
            Log.e("Eiko","Error while getting invertebrate's taxon data");
        }
        return taxon_map;
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public int getImage() {
        return image;
    }
}
