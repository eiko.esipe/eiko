package com.eiko.DataObject;

import android.util.Log;

import com.eiko.R;
import com.eiko.geolocalization.Position;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import static com.eiko.util.DateFormat.dateToString;

/**
 * Created by Rinkui on 08/02/2018.
 */

public abstract class Hydrobiologie implements DisplayableData{

    private final String station;
    private final Position geom;
    private final String nom_station;
    private final String resultat;
    private final Date dtprodresultatbiologique;
    private final String preleveur;
    private final String code_operation;
    private static final int image = R.mipmap.hydrobiologie;

    public Hydrobiologie(Map<String, String> map) {
        this.station = map.get("station");
        this.geom = initGeom(map.get("latitude"), map.get("longitude"), map.get("altitude"));
        this.nom_station = map.get("nomstation");
        this.resultat = map.get("resultat");
        this.dtprodresultatbiologique = initDate(map.get("dtprodresultatbiologique"));
        this.preleveur = map.get("preleveur");
        this.code_operation = map.get("code_operation");
    }

    private Date initDate(String date){
        SimpleDateFormat sdf = new SimpleDateFormat("y-M-d");
        try {
            return sdf.parse(date);
        } catch (ParseException e) {
            Log.e("Eiko","Erreur parsing date (Hydrobiologie.initDate)");
            return null;
        }
    }

    private Position initGeom(String latitude, String longitude, String altitude){
        double position;
        if("NaN".equals(altitude))
            return new Position(Double.parseDouble(latitude), Double.parseDouble(longitude), 0);
        try {
            position = Double.parseDouble(altitude);
        } catch (NumberFormatException | NullPointerException e) {
            position = 0;
        }
        return new Position(Double.parseDouble(latitude), Double.parseDouble(longitude), position);
    }

    /**
     * Get the most recent analysis.
     * @param analyses JSONArray with all analysis.
     * @return JSONObject of the last analysis.
     */
    public static JSONObject getLastAnalysis(JSONArray analyses) throws JSONException {
        int index = 0;
        try {
            String date_recente = analyses.getJSONObject(0).getString("datedebutoperationprelbio");
            String date;

            SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-DD");
            for(int i = 0; i < analyses.length(); i++){
                date = analyses.getJSONObject(i).getString("datedebutoperationprelbio");
                //Comparaison des dates
                if(sdf.parse(date).after(sdf.parse(date_recente))){
                    date_recente = date;
                    index = i;
                }
            }
        } catch (JSONException e) {
            Log.e("Eiko","Unparsable date while getting hydrobiology's last analysis");
        } catch (ParseException e) {
            Log.e("Eiko","Unparsable date (Hydrobiologie.getLastAnalysis)");
        }
        return analyses.getJSONObject(index);
    }

    /**
     * Get data from an analysis.
     * @param data Map with common data.
     * @param last_analyse JSONObject of the most recent analysis.
     * @return Map with common data, data of the analysis and taxon.
     */
    public static Map<String, String> getTaxonData(Map<String, String> data, JSONObject last_analyse) {
        Map<String, String> taxon_map = data;
        try {
            //Ajout des données dans la map
            taxon_map.put("preleveur", last_analyse.getString("preleveur"));
            taxon_map.put("code_operation", last_analyse.getString("code_operation"));
            JSONObject resultat = last_analyse.getJSONArray("operation").getJSONObject(0).getJSONArray("resultat").getJSONObject(0);
            taxon_map.put("resultat", resultat.getString("resultat"));
            taxon_map.put("dtprodresultatbiologique", resultat.getString("dtprodresultatbiologique"));

        } catch (JSONException e) {
            Log.e("Eiko","Error while getting hydrobiology's taxon data");
        }
        return taxon_map;
    }

    public String getResultat() {
        return resultat;
    }

    public String getCode_operation() {
        return code_operation;
    }

    public String getStation() {
        return station;
    }

    public Position getGeom() {
        return geom;
    }

    public String getDate(){
        return dateToString(dtprodresultatbiologique,"d-M-y");
    }

    public String getNom_station() {
        return nom_station;
    }

    public Date getDtprodresultatbiologique() {
        return dtprodresultatbiologique;
    }

    public String getPreleveur() {
        return preleveur;
    }


    public String toString() {
        return getResultat()+" "+getCode_operation();
    }

    @Override
    public int getImage(){
        return image;
    }
}
