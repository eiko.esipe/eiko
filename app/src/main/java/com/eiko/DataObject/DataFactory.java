package com.eiko.DataObject;

import org.json.JSONArray;

import java.util.HashMap;
import java.util.List;
import java.util.function.Function;

/**
 * Created by Rinkui on 06/02/2018.
 *
 * A partir d'un JSONObject créer la liste des objets associés
 */

public class DataFactory {
    private static HashMap<String, Function<JSONArray, List<? extends DisplayableData>>> fromJSONtoDisplayableData;

    static{
        // Initialisation de la map qui permet d'associer à un nom d'objet l'objet qui y correspond
        fromJSONtoDisplayableData = new HashMap<>();
        fromJSONtoDisplayableData.put("invertebres", Invertebre::fromJSON);
        fromJSONtoDisplayableData.put("amphibiens", Amphibien::fromJSON);
        fromJSONtoDisplayableData.put("macroinvertebres", MacroInvertebre::fromJSON);
        fromJSONtoDisplayableData.put("physicochimie", Physicochimie::fromJSON);
        fromJSONtoDisplayableData.put("flore", Flore::fromJSON);
        fromJSONtoDisplayableData.put("tax", HydrobiologieTax::fromJSON);
        fromJSONtoDisplayableData.put("par", HydrobiologiePar::fromJSON);
    }

    public static List<? extends DisplayableData> getDataFromJSON(String dataGroup, String dataClass, JSONArray jsonObject){
        Function<JSONArray, List<? extends DisplayableData>> list;
        if( dataGroup.equals("biologie") ) {
            list = fromJSONtoDisplayableData.get(dataClass);
        }
        else if (dataGroup.equals("hydrobiologie") ){
            String sub = dataClass.substring(0,3);
            list = fromJSONtoDisplayableData.get(sub);
        }
        else {
            list = fromJSONtoDisplayableData.get(dataGroup);
        }
        if(list != null)
            return list.apply(jsonObject);
        return null;
    }
}
