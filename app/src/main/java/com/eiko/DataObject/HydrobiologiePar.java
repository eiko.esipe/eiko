package com.eiko.DataObject;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by eikouser on 14/02/18.
 */

public class HydrobiologiePar extends Hydrobiologie {

    private final String parametre;
    private final String parametre_code;

    public HydrobiologiePar(Map<String, String> map) {
        super(map);
        this.parametre = map.get("parametre");
        this.parametre_code = map.get("parametre_code");
    }


    /**
     * Parse a JSON and return a list of hydrology parameter.
     * @param json JSONArray to be parsed.
     * @return List of all hydrology parameters in the JSON.
     */
    public static List<HydrobiologiePar> fromJSON(JSONArray json) {
        List<HydrobiologiePar> list = new ArrayList<>();
        JSONObject jsondata;
        try {
            for(int index = 0; index< json.length(); index++){
                Map<String, String> data;
                jsondata = json.getJSONObject(index);

                //Récupération des données communes
                data = commonData(jsondata);
                if(data == null)
                    continue;

                //Récupération des coordonnées
                JSONObject geom =jsondata.getJSONObject("geom");
                ArrayList<Double> positions = DisplayableData.getCoordinates(geom);

                List<Double> centeredPositions = DisplayableData.getCenteredPosition(positions);
                //Ajout des coordonnées centrées
                data.put("longitude",centeredPositions.get(0).toString());
                data.put("latitude",centeredPositions.get(1).toString());

                JSONArray analyses = jsondata.getJSONArray("analyses");
                //Comparaison des dates des analyses d'un taxon et récupération de l'analyse la plus récente
                JSONObject last_analyse = Hydrobiologie.getLastAnalysis(analyses);

                //Création d'une map avec données communes et celles du taxon
                Map<String, String> taxon_map = Hydrobiologie.getTaxonData(data, last_analyse);

                //Création et ajout de l'objet dans la liste
                list.add(new HydrobiologiePar(taxon_map));
            }
        } catch (JSONException e) {
            Log.e("Eiko","Error while parsing hydrobiologyPar json");
        }

        return list;
    }

    /**
     * Get common data to all hydrobiology parameter species in a JSON.
     * @param json JSONObject to be parsed.
     * @return Map with the String of name and value of data.
     */
    private static Map<String, String> commonData(JSONObject json) {
        Map<String, String> data = new HashMap<>();
        //Récupération des data
        try {
            data.put("parametre",json.getString("parametre"));
            data.put("parametre_code",json.getString("parametre_code"));
            data.put("altitude",Double.toString(json.optDouble("altitude")));
            data.put("station",json.getString("station"));
            data.put("nomstation",json.getString("nomstation"));
        } catch (JSONException e) {
            Log.e("Eiko","Error while getting hydrobiologyPar's common data");
            return null;
        }
        return data;
    }
    public String getName(){
        return parametre;
    }

    public String getParametre() {
        return parametre;
    }

    public String getParametre_code() {
        return parametre_code;
    }

    @Override
    public String toString() {
        return parametre;
    }

}
