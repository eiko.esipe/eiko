package com.eiko.DataObject;

import android.util.Log;

import com.eiko.R;
import com.eiko.geolocalization.Position;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.eiko.util.DateFormat.dateToString;

/**
 * Created by Rinkui on 08/02/2018.
 */

public class Flore implements DisplayableData {

    private final String observation_rmq;
    private List<String> names_sp = new ArrayList<>();
    private final String cd_jdd;
    private final String metadonnees_acteur_nom;
    private List<String> names_ssp = new ArrayList<>();
    private final String releve_acteur_cd;
    private final String nom_ent_ref;
    private final String cd_spp;
    private final Date date_fin;
    private final Position geom;
    private final String releve_rmq;
    private final String cd_sp;
    private final String metadonnees_acteur_cd;
    private final String cd_releve;
    private final Date date_debut;
    private final String releve_acteur_nom;
    private static final int image = R.mipmap.flore;

    public Flore(Map<String, String> map, Map<String, List<String>> especes) {
        this.observation_rmq = map.get("observation_rmq");
        this.names_sp= especes.get("names_sp");
        this.cd_jdd = map.get("cd_jdd");
        this.metadonnees_acteur_nom = map.get("metadonnees_acteur_nom");
        this.names_ssp= especes.get("names_ssp");
        this.releve_acteur_cd = map.get("releve_acteur_cd");
        this.nom_ent_ref = map.get("nom_ent_ref");
        this.cd_spp = map.get("cd_spp");
        this.date_fin = initDate(map.get("date_fin"));
        this.geom = initGeom(map.get("latitude"), map.get("longitude"), map.get("altitude"));
        this.releve_rmq = map.get("releve_rmq");
        this.cd_sp = map.get("cd_sp");
        this.metadonnees_acteur_cd = map.get("metadonnees_acteur_cd");
        this.cd_releve = map.get("cd_releve");
        this.date_debut = initDate(map.get("date_debut"));
        this.releve_acteur_nom = map.get("releve_acteur_nom");
    }

    private Date initDate(String date){
        SimpleDateFormat sdf = new SimpleDateFormat("y-M-d");
        try {
            return sdf.parse(date);
        } catch (ParseException e) {
            Log.e("Eiko","Erreur parsing date (Flore.initDate)");
            return null;
        }
    }

    private Position initGeom(String latitude, String longitude, String altitude){
        if (altitude == null)
            return new Position(Double.parseDouble(latitude), Double.parseDouble(longitude), 0);
        double position;

        if("NaN".equals(altitude))
            return new Position(Double.parseDouble(latitude), Double.parseDouble(longitude), 0);
        try {
            position = Double.parseDouble(altitude);
        } catch (NumberFormatException | NullPointerException e) {
            position = 0;
        }
        return new Position(Double.parseDouble(latitude), Double.parseDouble(longitude), position);
    }

    /**
     * Parse a JSON and return a list of floras.
     * @param json JSONArray to be parsed.
     * @return List of all floras in the JSON.
     */
    public static List<Flore> fromJSON(JSONArray json) {
        List<Flore> list = new ArrayList<>();
        JSONObject jsondata;
        for(int index = 0; index< json.length(); index++){
            try {
                Map<String, String> data;
                Map<String, List<String>> species;
                jsondata = json.getJSONObject(index);

                //Récupération des données
                data = commonData(jsondata);
                if(data == null)
                    continue;

                //Récupération des espèces
                species = getAllSpecies(jsondata);

                //Récupération des coordonnées
                JSONObject geom =jsondata.getJSONObject("geom");
                ArrayList<Double> positions = DisplayableData.getCoordinates(geom);

                List<Double> centeredPositions = DisplayableData.getCenteredPosition(positions);
                //Ajout des coordonnées centrées
                data.put("longitude",centeredPositions.get(0).toString());
                data.put("latitude",centeredPositions.get(1).toString());

                //Création et ajout de l'objet dans la liste
                list.add(new Flore(data, species));

            } catch (JSONException e) {
                Log.e("Eiko","Error while parsing flora json");
            }
        }
        return list;
    }

    /**
     * Get common data to all floras species in a JSON.
     * @param json JSONObject to be parsed.
     * @return Map with the String of name and value of data.
     */
    private static Map<String, String> commonData(JSONObject json) {
        Map<String, String> data = new HashMap<>();
        //Récupération des data
        try {
            data.put("observation_rmq",json.getString("observation_rmq"));
            data.put("cd_jdd",json.getString("cd_jdd"));
            data.put("metadonnees_acteur_nom",json.getString("metadonnees_acteur_nom"));
            data.put("altitude",Double.toString(json.optDouble("altitude")));
            data.put("releve_acteur_cd",json.getString("releve_acteur_cd"));
            data.put("nom_ent_ref",json.getString("nom_ent_ref"));
            data.put("cd_ssp",Double.toString(json.getDouble("cd_ssp")));
            data.put("date_fin",json.getString("date_fin"));
            data.put("releve_rmq",json.getString("releve_rmq"));
            data.put("cd_sp",Double.toString(json.getDouble("cd_sp")));
            data.put("metadonnees_acteur_cd",json.getString("metadonnees_acteur_cd"));
            data.put("cd_releve",json.getString("cd_releve"));
            data.put("date_debut",json.getString("date_debut"));
            data.put("releve_acteur_nom",json.getString("releve_acteur_nom"));
        } catch (JSONException e) {
            Log.e("Eiko","Error while getting flora's common data");
            return null;
        }
        return data;
    }

    /**
     * Get all species and subspecies.
     * @param jsondata JSONObject
     * @return Map with lists of species and subspecies.
     */
    private static Map<String,List<String>> getAllSpecies(JSONObject jsondata) {
        Map<String, List<String>> map = new HashMap<>();

        try {
            //Récupération des espèces
            JSONArray sp = jsondata.getJSONArray("names_sp");
            ArrayList<String> sp_list = new ArrayList<>();
            for(int i = 0; i <sp.length(); i++){
                sp_list.add(sp.getString(i));
            }

            //Récupération des sous-espèces
            JSONArray ssp = jsondata.getJSONArray("names_ssp");
            ArrayList<String> ssp_list = new ArrayList<>();
            for(int i = 0; i <ssp.length(); i++){
                ssp_list.add(ssp.getString(i));
            }

            //Ajout des listes dans la map
            map.put("names_sp", sp_list);
            map.put("names_ssp", ssp_list);

        } catch (JSONException e) {
            Log.e("Eiko","Error while getting all flora's species");
        }
        return map;
    }

    public String getCd_jdol() {
        return cd_jdd;
    }

    public String getCd_releve() {
        return cd_releve;
    }

    public String getCd_spp() {
        return cd_spp;
    }

    public Date getDate_debut() {
        return date_debut;
    }

    public Date getDate_fin() {
        return date_fin;
    }

    public Position getGeom() {
        return geom;
    }

    public String getName(){
        return names_sp.get(0);
    }

    public String getDate(){
        return  dateToString(date_fin,"d-M-y");
    }

    public String getReleve_acteur_nom() {
        return releve_acteur_nom;
    }

    public String getReleve_acteur_cd() {
        return releve_acteur_cd;
    }

    public String getMetadonnees_acteur_nom() {
        return metadonnees_acteur_nom;
    }

    public String getReleve_rmq() {
        return releve_rmq;
    }

    public String getObservation_rmq() {
        return observation_rmq;
    }

    public List<String> getNames_sp() {
        return names_sp;
    }

    public String getNom_ent_ref() {
        return nom_ent_ref;
    }

    public String getCd_sp() {
        return cd_sp;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(observation_rmq);sb.append("\n");
        for (String str : names_sp) {
            sb.append(str).append(", ");
        }
        sb.append("\n");
        sb.append(date_fin);sb.append("\n");
        sb.append(geom.toString());sb.append("\n");
        sb.append(date_debut.toString());sb.append("\n");

        return sb.toString();
    }

    @Override
    public int getImage() {
        return image;
    }
}
