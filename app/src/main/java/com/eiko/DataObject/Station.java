package com.eiko.DataObject;

import com.eiko.geolocalization.Position;

import java.util.Map;

/**
 * Created by axelheine on 14/02/2018.
 */

public class Station {
    private final Position position;
    private final String station_nom;

    public Station(Map<String, String> map) {
        this.position = initPosition(map.get("latitude"), map.get("longitude"));
        this.station_nom = map.get("station_nom");
    }

    private Position initPosition(String lat, String lng) {
        return new Position(Double.parseDouble(lat), Double.parseDouble(lng));
    }

    public Position getPosition() {
        return position;
    }

    public String getName() {
        return station_nom;
    }
}
