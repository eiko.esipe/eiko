package com.eiko.DataObject;

import android.util.Log;

import com.eiko.geolocalization.Position;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import static com.eiko.util.DateFormat.dateToString;

/**
 * Created by eikouser on 06/02/18.
 */

public abstract class Biologie implements DisplayableData{

    private final String station_code; //code de la station
    private final Position geom; //coordonnées de la station
    private String taxon = null; //taxon
    private final Date date_de_sortie;
    private int total_individu = 1; //total_ind

    public Biologie(Map<String, String> map) {
        this.station_code = map.get("station_code");
        this.geom = initGeom(map.get("latitude"), map.get("longitude"), map.get("altitude"));
        this.taxon = map.get("taxon");
        this.date_de_sortie = initDateDeSortie(map.get("date_de_sortie"));
        this.total_individu = initTotalIndividu(map.get("total_ind"));
    }

    private Date initDateDeSortie(String date){
        SimpleDateFormat sdf = new SimpleDateFormat("d-M-y");
        try {
            return sdf.parse(date);
        } catch (ParseException e) {
            Log.e("Eiko","Erreur parsing date");
            return null;
        }
    }

    private int initTotalIndividu(String total){
        if (total==null || total.equals("null")){
            return 0;
        }

        return Integer.parseInt(total);
    }

    private Position initGeom(String latitude, String longitude, String altitude){
        double position;
        if("NaN".equals(altitude))
            return new Position(Double.parseDouble(latitude), Double.parseDouble(longitude), 0);
        try {
            position = Double.parseDouble(altitude);
        } catch (NumberFormatException | NullPointerException e) {
            position = 0;
        }
        return new Position(Double.parseDouble(latitude), Double.parseDouble(longitude), position);
    }


    public Position getGeom() {
        return geom;
    }

    public String getName(){
        return taxon;
    }

    public String getDate(){
        return dateToString(date_de_sortie,"d-M-y");
    }

    public String getStationCode() {
        return station_code;
    }

    public String getTaxon() {
        return taxon;
    }

    public Date getDateDeSortie() {
        return date_de_sortie;
    }

    public int getTotalIndividu() {
        return total_individu;
    }

    public String toString() {
        return getTaxon();
    }
}
