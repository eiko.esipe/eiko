package com.eiko.DataObject;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import com.eiko.ObserverObservable.Observable;
import com.eiko.ObserverObservable.Observer;
import com.eiko.PropertiesReader;
import com.eiko.downloads.AsyncTasks.WhereAsyncTask;
import com.eiko.geolocalization.Position;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by Rinkui on 20/02/2018.
 */

public class PerimeterManager implements Observable, Observer {
    private static PerimeterManager perimeterManager;
    private List<Perimeter> perimeters;
    private Perimeter currentPerimeter;
    private LocationManager lm;
    private PropertiesReader propertyReader;
    private Context context;
    private List<Observer> observers;

    private PerimeterManager(Context context){
        this.context = context;
        propertyReader = PropertiesReader.getProReader(context);
        perimeters = new ArrayList<>();
        observers = new ArrayList<>();
    }

    public static PerimeterManager getPerimeterManager(Context context){
        if(perimeterManager == null){
            perimeterManager = new PerimeterManager(context);
        }
        return perimeterManager;
    }

    public void setPerimeters(List<Perimeter> perimeters){
        for(Perimeter p : perimeters){
            if(!this.perimeters.contains(p)){
                this.perimeters.add(p);
            }
        }
    }

    private final LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location currentLocation) {
            WhereAsyncTask whereAsyncTask = new WhereAsyncTask();
            whereAsyncTask.addObservator(PerimeterManager.getPerimeterManager(context));
            whereAsyncTask.execute(new Position(currentLocation.getLatitude(), currentLocation.getLongitude()));
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {}

        @Override
        public void onProviderEnabled(String provider) {}

        @Override
        public void onProviderDisabled(String provider) {}
    };

    public void checkPosition() {
        lm = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        try {
            assert lm != null;
            if (lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                lm.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                        Long.valueOf(propertyReader.getProperty(PropertiesReader.PERIMETER_UPDATE_TIME)),
                        Long.valueOf(propertyReader.getProperty(PropertiesReader.PERIMETER_UPDATE_DISTANCE)),
                        locationListener);
            } else {
                lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                        Long.valueOf(propertyReader.getProperty(PropertiesReader.PERIMETER_UPDATE_TIME)),
                        Long.valueOf(propertyReader.getProperty(PropertiesReader.PERIMETER_UPDATE_DISTANCE)),
                        locationListener);
            }
        } catch (SecurityException se) {
            throw new SecurityException("Need gps and network permissions");
        }
    }

    public boolean positionInsidePerimeter(Position position, List<Position> surface){
        if(position == null || surface == null){
            return false;
        }
        boolean result = false;
        for (int i = 0, j = surface.size() - 1; i < surface.size(); j = i++) {
            if ((surface.get(i).getLatitude() > position.getLatitude()) != (surface.get(j).getLatitude() > position.getLatitude()) &&
                    (position.getLongitude() < (surface.get(j).getLongitude() - surface.get(i).getLongitude()) * (position.getLatitude() - surface.get(i).getLatitude()) / (surface.get(j).getLatitude()-surface.get(i).getLatitude()) + surface.get(i).getLongitude())) {
                result = !result;
            }
        }
        return result;

    }

    @Override
    public void addObservator(Observer o) {
        observers.add(o);
    }

    @Override
    public void deleteObserver(Observer o) {
        observers.remove(o);
    }

    @Override
    public void notifyObserver(String message) {
        for(Observer o : observers){
            o.actualize(this, message);
        }
    }

    public List<Perimeter> getPerimeters() {
        return perimeters;
    }

    public List<String> getStringPerimeters() {
        List<String> stringPerim = new ArrayList<>();
        for(Perimeter p : perimeters){
            stringPerim.add(p.getQueryCode());
        }
        return stringPerim;
    }

    public void setCurrentPerimeter(Perimeter currentPerimeter) {
        this.currentPerimeter = currentPerimeter;
    }

    @Override
    public void actualize(Observable o, String message) {
        if(o instanceof WhereAsyncTask){
            if(message.equals("ERROR")){
                Log.e("Eiko", "Erreur lors de la requête à l'API Where");
            }
            else {
                Log.i("Eiko", "Message envoyé par l'API Where : " + message);
                notifyObserver(message);
            }
        }
    }

    public Perimeter getCurrentPerimeter() {
        return currentPerimeter;
    }
}
