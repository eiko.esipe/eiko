package com.eiko.DataObject;

import android.util.Log;

import com.eiko.R;
import com.eiko.geolocalization.Position;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.eiko.util.DateFormat.dateToString;

/**
 * Created by eikouser on 06/02/18.
 */

public class Physicochimie implements DisplayableData{

    private final String parametre;
    private final Position station_geom; //coordonnées de la station
    //private final Position prelevement_geom; //coordonnées du prélévement
    private final String lsana;
    private final String ldana;
    private final String rsana;
    private final String laboratoire;
    private final String code_remarque;
    private final String methode;
    private final String producteur;
    private final String lqana;
    private final Date dateprel; //date de prélèvement
    private final String station_nom;
    private static final int image = R.mipmap.physicochimie;

    public Physicochimie(Map<String, String> map) {
        this.parametre = map.get("parametre");
        this.station_geom = initGeom(map.get("latitude"), map.get("longitude"), map.get("altitude"));
        //this.prelevement_geom = initGeom(map.get("latitude_prel"), map.get("longitude_prel"));
        this.lsana = map.get("lsana");
        this.ldana = map.get("ldana");
        this.rsana = map.get("rsana");
        this.laboratoire = map.get("laboratoire");
        this.code_remarque = map.get("code_remarque");
        this.methode = map.get("methode");
        this.producteur = map.get("producteur");
        this.lqana = map.get("lqana");
        this.dateprel = initDateDePrel(map.get("dateprel"));
        this.station_nom = map.get("station_nom");
    }

    private Date initDateDePrel(String date){
        SimpleDateFormat sdf = new SimpleDateFormat("y-M-d");
        try {
            return sdf.parse(date);
        } catch (ParseException e) {
            Log.e("Eiko","Erreur parsing date (Physicochime.initDateDePrel)");
            return null;
        }
    }

    private Position initGeom(String latitude, String longitude, String altitude){
        double position;
        if("NaN".equals(altitude))
            return new Position(Double.parseDouble(latitude), Double.parseDouble(longitude), 0);
        try {
            position = Double.parseDouble(altitude);
        } catch (NumberFormatException | NullPointerException e) {
            position = 0;
        }
        return new Position(Double.parseDouble(latitude), Double.parseDouble(longitude), position);
    }

    /**
     * Parse a JSON and return a list of physical chemistry.
     * @param json JSONArray to be parsed.
     * @return List of all physical chemistry in the JSON.
     */
    public static List<Physicochimie> fromJSON(JSONArray json) {
        List<Physicochimie> list = new ArrayList<>();
        JSONObject jsondata;

        for(int index = 0; index< json.length(); index++){
            try {
                Map<String, String> data;
                jsondata = json.getJSONObject(index);

                //Récupération des données communes
                data = commonData(jsondata);
                if(data == null)
                    continue;

                //Récupération des coordonnées
                JSONObject geom =jsondata.getJSONObject("geom");
                ArrayList<Double> positions = DisplayableData.getCoordinates(geom);

                //Récupération des coordonnées
                JSONObject station_geom =jsondata.getJSONObject("station_geom");
                ArrayList<Double> station_positions = DisplayableData.getCoordinates(station_geom);

                List<Double> centeredPositions = DisplayableData.getCenteredPosition(positions);
                //Ajout des coordonnées centrées
                data.put("longitude",centeredPositions.get(0).toString());
                data.put("latitude",centeredPositions.get(1).toString());

                List<Double> centeredStationPositions = DisplayableData.getCenteredPosition(positions);
                //Ajout des coordonnées centrées
                data.put("longitude_prel",centeredStationPositions.get(0).toString());
                data.put("latitude_prel",centeredStationPositions.get(1).toString());

                JSONArray analyses = jsondata.getJSONArray("analyses");
                for (int i = 0; i < analyses.length(); i++) {
                    //Comparaison des dates des analyses d'un taxon et récupération de l'analyse la plus récente
                    JSONObject last_analyse = getLastAnalysis(analyses.getJSONObject(i).getJSONArray("resultats"));

                    //Création d'une map avec données communes et celles de l'analyse
                    Map<String, String> analyse_map = getAnalyseData(data, last_analyse);

                    analyse_map.put("fraction_analysee", analyses.getJSONObject(i).getJSONObject("type_analyse").getString("fraction_analysee"));

                    //Création et ajout de l'objet dans la liste
                    list.add(new Physicochimie(analyse_map));
                }

            } catch (JSONException e) {
                Log.e("Eiko","Error while parsing physicalchemistry json");
            }
        }

        return list;
    }

    /**
     * Get common data to all physical chemistry species in a JSON.
     * @param json JSONObject to be parsed.
     * @return Map with the String of name and value of data.
     */
    private static Map<String, String> commonData(JSONObject json) {
        Map<String, String> data = new HashMap<>();
        //Récupération des data
        try {
            data.put("parametre",json.getString("parametre"));
            data.put("station_nom",json.getString("station_nom"));
            data.put("station_code",json.getString("station_code"));

        } catch (JSONException e) {
            Log.e("Eiko","Error while getting physicalchemistry's common data");
            return null;
        }
        return data;
    }

    /**
     * Get the most recent analysis for one taxon.
     * @param analyses JSONArray with all analysis.
     * @return JSONObject of the last analysis.
     */
    private static JSONObject getLastAnalysis(JSONArray analyses) throws JSONException {
        int index = 0;
        try {
            String date_recente = analyses.getJSONObject(0).getString("dateprel");
            String date;

            SimpleDateFormat sdf = new SimpleDateFormat("y-M-d");
            for(int i = 0; i < analyses.length(); i++){
                date = analyses.getJSONObject(i).getString("dateprel");
                //Comparaison des dates
                if(sdf.parse(date).after(sdf.parse(date_recente))){
                    date_recente = date;
                    index = i;
                }
            }
        } catch (JSONException e) {
            Log.e("Eiko","Unparsable date while getting physicalchemistry's last analysis");
        } catch (ParseException e) {
            Log.e("Eiko","Unparsable date (Physicochime.getLastAnalysis)");
        }
        return analyses.getJSONObject(index);
    }

    /**
     * Get data from an analysis.
     * @param data Map with common data.
     * @param last_analyse JSONObject of the most recent analysis.
     * @return Map with common data, data of the analysis and taxon.
     */
    private static Map<String, String> getAnalyseData(Map<String, String> data, JSONObject last_analyse) {
        Map<String, String> analyse_map = data;
        try {
            //Ajout des données dans la map
            analyse_map.put("lsana", last_analyse.getString("lsana"));
            analyse_map.put("ldana", last_analyse.getString("ldana"));
            analyse_map.put("rsana", last_analyse.getString("rsana"));
            analyse_map.put("laboratoire", last_analyse.getString("laboratoire"));
            analyse_map.put("code_remarque", last_analyse.getString("code_remarque"));
            analyse_map.put("methode", last_analyse.getString("methode"));
            analyse_map.put("producteur", last_analyse.getString("producteur"));
            analyse_map.put("lqana", last_analyse.getString("lqana"));
            analyse_map.put("dateprel", last_analyse.getString("dateprel"));

        } catch (JSONException e) {
            Log.e("Eiko","Error while getting data from physicalchemistry's analysis");
        }
        return analyse_map;
    }

    public String getParametre() {
        return parametre;
    }

    public Position getStation_geom() {
        return station_geom;
    }

    public String getLsana() {
        return lsana;
    }

    public String getLdana() {
        return ldana;
    }

    public String getRsana() {
        return rsana;
    }

    public String getLaboratoire() {
        return laboratoire;
    }

    public String getCode_remarque() {
        return code_remarque;
    }

    public String getMethode() {
        return methode;
    }

    public String getProducteur() {
        return producteur;
    }

    public String getLqana() {
        return lqana;
    }

    public Date getDateprel() {
        return dateprel;
    }



    public String getStation_nom() {
        return station_nom;
    }

    @Override
    public String toString() {
        return "Physicochimie{" +
                "parametre='" + parametre + '\'' +
                ", station_geom=" + station_geom +
                //", prelevement_geom=" + prelevement_geom +
                ", lsana='" + lsana + '\'' +
                ", ldana='" + ldana + '\'' +
                ", rsana='" + rsana + '\'' +
                ", laboratoire='" + laboratoire + '\'' +
                ", code_remarque='" + code_remarque + '\'' +
                ", methode='" + methode + '\'' +
                ", producteur='" + producteur + '\'' +
                ", lqana='" + lqana + '\'' +
                ", dateprel=" + dateprel +
                ", station_nom='" + station_nom + '\'' +
                '}';
    }

    public Position getGeom() {
        return station_geom;
    }

    public String getName(){
        return parametre;
    }

    public String getDate(){
        return  dateToString(dateprel,"d-M-y");
    }

    @Override
    public int getImage() {
        return image;
    }
}
