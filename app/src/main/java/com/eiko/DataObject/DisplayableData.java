package com.eiko.DataObject;

import android.util.Log;

import com.eiko.geolocalization.Position;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public interface DisplayableData {

    Position getGeom();
    String getName();
    String getDate();

    /**
     * Get all coordinates in a JSONObject
     * @param geom : JSONObject with coordinates to be parse
     * @return ArrayList of all coordinates
     */
    static ArrayList<Double> getCoordinates(JSONObject geom) {
        ArrayList<Double> positions = new ArrayList<>();
        try {
            JSONArray coordinates;
            JSONArray pos;
            //Récupération des coordonnées dans une liste

            switch(geom.getString("type")){
                case "Point" :

                    coordinates = geom.getJSONArray("coordinates");
                    for (int i = 0; i < coordinates.length(); i++) {
                        positions.add(coordinates.getDouble(i));
                    }
                    break;

                case "MultiLineString" :
                    coordinates = geom.getJSONArray("coordinates").getJSONArray(0);
                    for (int i = 0; i < coordinates.length(); i++) {
                        pos = coordinates.getJSONArray(i);
                        for (int j=0; j < pos.length(); j++){
                            positions.add(pos.getDouble(j));
                        }
                    }
                    break;

                case "MultiPoint" :
                    coordinates = geom.getJSONArray("coordinates");
                    for (int i = 0; i < coordinates.length(); i++) {
                        pos = coordinates.getJSONArray(i);
                        for (int j=0; j < pos.length(); j++){
                            positions.add(pos.getDouble(j));
                        }
                    }
                    break;

                case "Polygon" :
                    coordinates = geom.getJSONArray("coordinates").getJSONArray(0);
                    for (int i = 0; i < coordinates.length(); i++) {
                        pos = coordinates.getJSONArray(i);
                        for (int j=0; j < pos.length(); j++){
                            positions.add(pos.getDouble(j));
                        }
                    }
                    break;

                case "MultiPolygon" :
                    JSONArray tab1, tab2, tab3;

                    coordinates = geom.getJSONArray("coordinates");
                    for (int i = 0; i < coordinates.length(); i++) {
                        tab1 = coordinates.getJSONArray(i);
                        for (int j = 0; j < tab1.length(); j++){
                            tab2 = tab1.getJSONArray(j);
                            for (int k = 0; k < tab2.length(); k++){
                                tab3 = tab2.getJSONArray(k);
                                for (int l = 0; l < tab3.length(); l++) {
                                    positions.add(tab3.getDouble(l));
                                }
                            }
                        }
                    }
                    break;
            }
        } catch (JSONException e) {
            Log.e("Eiko","Error while getting coordinates");
        }
        return positions;
    }

    /**
     * Calculate the average coordinates of a list of coordinates
     * @param list : all the coordinates
     * @return Average latitude and longitude
     */
    static List<Double> getCenteredPosition(List<Double> list){
        List<Double> centerCoords = new ArrayList<>();
        double latSum = 0;
        double lngSum = 0;

        for(int i = 0; i < list.size() / 2; i++) {
            double lat = list.get(i * 2);
            double lng = list.get(i * 2 + 1);
            latSum += lat;
            lngSum += lng;
        }

        centerCoords.add(latSum / (list.size() / 2) );
        centerCoords.add(lngSum / (list.size() / 2) );

        return centerCoords;
    }

    int getImage();
}
