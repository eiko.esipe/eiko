package com.eiko.ObserverObservable;

/**
 * Created by Rinkui on 21/02/2018.
 */

public interface Observable {
    public void addObservator(Observer o);
    public void deleteObserver(Observer o);
    public void notifyObserver(String message);
}
