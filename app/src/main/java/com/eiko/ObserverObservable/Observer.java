package com.eiko.ObserverObservable;

/**
 * Created by Rinkui on 21/02/2018.
 */

public interface Observer {
    public void actualize(Observable o, String message);
}
