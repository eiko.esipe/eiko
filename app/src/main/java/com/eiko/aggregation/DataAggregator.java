package com.eiko.aggregation;

import com.eiko.DataObject.Biologie;
import com.eiko.DataObject.DisplayableData;
import com.eiko.DataObject.Flore;
import com.eiko.DataObject.Hydrobiologie;
import com.eiko.DataObject.Physicochimie;

import java.util.ArrayList;
import java.util.List;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.T;

/**
 * Created by yslh on 07/02/18.
 */

public class DataAggregator {
    private final int limit;
    private List<Biologie> biologyList;
    private List<Flore> floraList;
    private List<Hydrobiologie> hydrobiologyList;
    private List<Physicochimie> chemistryList;

    /* ****************** */
    /* ****************** */

    public DataAggregator(int displayLimit) {
        this.limit = displayLimit;
        this.biologyList = new ArrayList<>();
        this.floraList = new ArrayList<>();
        this.hydrobiologyList = new ArrayList<>();
        this.chemistryList = new ArrayList<>();
    }

    /* ****************** */
    /* ****************** */

    public List<Biologie> getBiologyList() {
        if(biologyList.isEmpty()){
            return null;
        }
        return biologyList;
    }

    public List<Flore> getFloraList() {
       if(floraList.isEmpty()) {
           return null;
       }
       return floraList;
    }

    public List<Hydrobiologie> getHydrobiologyList() {
        if(hydrobiologyList.isEmpty()) {
            return null;
        }
        return hydrobiologyList;
    }

    public List<Physicochimie> getChemistryList() {
        if(chemistryList.isEmpty()) {
            return null;
        }
        return chemistryList;
    }

    /* ****************** */
    /* ****************** */

    private boolean testAggregation(List<DisplayableData> dataList) {
        if(dataList.size() > limit) {
            return true;
        }
        return false;
    }

    public boolean makeAggregation(List<DisplayableData> dataList) {
        // If limit not reached, we do not aggregate
        if(!testAggregation(dataList)) {
            return false;
        }

        // Aggregation in single lists of each data type
        for(DisplayableData e : dataList) {
            switch (e.getClass().getName())
            {
                case ("Biologie"):
                    biologyList.add((Biologie) e);
                    dataList.remove(e);
                    break;

                case ("Flore"):
                    floraList.add((Flore) e);
                    dataList.remove(e);
                    break;

                case ("Hydrobiologie"):
                    hydrobiologyList.add((Hydrobiologie) e);
                    dataList.remove(e);
                    break;

                case ("Physicochimie"):
                    chemistryList.add((Physicochimie) e);
                    dataList.remove(e);
                    break;
            }
        }
        return true;
    }
}