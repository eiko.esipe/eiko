package com.eiko;

import android.Manifest;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.eiko.DataObject.DisplayableData;
import com.eiko.DataObject.PerimeterManager;
import com.eiko.ObserverObservable.Observable;
import com.eiko.ObserverObservable.Observer;
import com.eiko.ar.RealOrientation;
import com.eiko.database.Database;
import com.eiko.database.UserParamDB;
import com.eiko.downloads.AsyncTasks.PerimetersDownloaderAsynTask;
import com.eiko.downloads.DataDownloader;
import com.eiko.forms.JobSchedulerService;
import com.eiko.geolocalization.Position;
import com.eiko.geolocalization.PositionManager;
import com.eiko.matrix.ARObject;
import com.eiko.matrix.ARObjectFactory;
import com.eiko.permission.Permission;
import com.eiko.settingsButtons.SettingsButtonsManagement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.eiko.matrix.ARObjectFactory.createARObject;

//import com.eiko.database.HydrobiologieDB;
//import com.eiko.database.HydrobiologieTaxDB;

/**
 * Created by eikouser on 08/02/18.
 */

public class DevActivity extends AppCompatActivity implements SensorEventListener, LocationListener, Observer {
    /**
     * For Orientation
     **/
    private RealOrientation orientation;
    private SensorManager mSensorManager;
    private PositionManager positionManager;
    private Position currentPosition;
    private TextView textBearing;
    private FrameLayout.LayoutParams textBearingParam = new FrameLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

    /** For Position **/
    private TextView positionView;
    private FrameLayout.LayoutParams positionViewParam = new FrameLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

    /** TESTING ARPOSITION IN AR **/
    private List<ARObject> arDatas = new ArrayList<>();
    private List<Position> posDatas = new ArrayList<>();
    private TextView arObjectsText;
    private FrameLayout.LayoutParams arObjectsTextParam = new FrameLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

    /** Custom layout for dev activity **/
    private TextView objectVisualiser;
    private FrameLayout.LayoutParams objectVisualiserParam = new FrameLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

    /** Custom field to visualize data without AR **/
    private Map<String,List<ARObject>> mARObject;


    /** ONE TIME INIT, AVOID TO LOAD MULTIPLE TIME **/
    private boolean loaded=false;

    /** For further tests **/
    private Map<String, List<? extends DisplayableData>> objsFromDB;

    private Snackbar snackbar;


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        //Permission.onRequestPermission(requestCode, permissions, grantResults, this, getApplicationContext());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dev_main);
        addContentView(new SettingsButtonsManagement().getFinalLayout(getApplicationContext()),
                new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));

        // Ask for GPS activation
        enableGPS(getApplicationContext());

        // Ask for multiple permissions
        Permission.getPermissions(this);

        setupJobSchedulerService();
        setOnClickMenuButtons();

        // Calibration's layout
        LayoutInflater inflater = getLayoutInflater();
        View calibrationLayout = inflater.inflate(R.layout.calibration,null);
        addContentView(calibrationLayout,
                new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT));
        calibrationLayout.setVisibility(View.GONE);
        textBearing = findViewById(R.id.textView_compass);

        findViewById(R.id.buttonMenuCompass).setOnClickListener((View v) -> {
            calibrationLayout.setVisibility(View.VISIBLE);
            findViewById(R.id.calibrationButton).setOnClickListener((View w ) ->{
                calibrationLayout.setVisibility(View.GONE);
            });
            findViewById(R.id.close_layout).setOnClickListener((View w ) ->{
                calibrationLayout.setVisibility(View.GONE);
            });
        });

        /** FOR ORIENTATION **/
        /** REAL ORIENTATION INITIALIZE **/
        PositionManager.initPositionManager(this);
        positionManager = PositionManager.getInstance();
        mSensorManager = (SensorManager) getSystemService(this.SENSOR_SERVICE);
        orientation = new RealOrientation(mSensorManager);
        mSensorManager.registerListener(this, orientation.getSensorGravity(), SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this, orientation.getSensorMagnetic(), SensorManager.SENSOR_DELAY_NORMAL);

        /** TODO to show the position, use to create test data **/
        positionView = new TextView(this);
        positionView.setText("This is for location");
        positionView.setTextColor(Color.RED);
        positionView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
        positionViewParam.gravity = Gravity.CENTER;
        positionViewParam.gravity = Gravity.BOTTOM;
        //addContentView(positionView, positionViewParam);


        /** TODO Text to show bearing in the middle of the screen **/
        /*textBearing = new TextView(this);
        textBearing.setText("This is for the bearing");
        textBearing.setTextColor(Color.RED);
        textBearing.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        textBearingParam.gravity = Gravity.RIGHT;
        textBearingParam.gravity = Gravity.BOTTOM;
        addContentView(textBearing,textBearingParam);*/

        /** This method is use to show the objects in AR **/
        objectVisualiser();

        Button testDBButton = new Button(this);
        testDBButton.setText("testData");
        //this.addContentView(testDBButton,
         //       new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT));

        testDBButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                objsFromDB = Database.getAllObjectsAtDistanceByType(new Position(48.83942628,2.58853119),1000000,getApplicationContext());
                printMap(objsFromDB);
            }
        });


        arObjectsText = new TextView(this);
        arObjectsText.setText("This is for the ARObjects localisation");
        arObjectsText.setTextColor(Color.GREEN);
        arObjectsText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
        arObjectsTextParam.gravity = Gravity.CENTER;
        //addContentView(arObjectsText,arObjectsTextParam);

        findViewById(R.id.download).setOnClickListener(v -> {
            updateObjectManager(100);
            showObjectsOnText();
            //List<Perimeter> perimeters = DataDownloader.getDataDownloader().downloadsPerimeters();

            //Pour tester et qu'on soit dans une parc (sur Paris)
            /*List<Perimeter> perimeters = new ArrayList<>();

            List<Position> perim2 = new ArrayList<>();
            perim2.add(new Position(48.8402181450014, 2.59488402316992));
            perim2.add(new Position(48.8393402034649, 2.5947601723059));
            perim2.add(new Position( 48.8384950795223, 2.59437923468367));
            perim2.add(new Position(48.8377152484208, 2.59375586089478));
            perim2.add(new Position(48.8370306749946, 2.59291401453438));
            perim2.add(new Position(48.8364676626876, 2.59188605012371));
            perim2.add(new Position(48.8360478433854, 2.59071146931275));
            perim2.add(new Position(48.8357873467776, 2.58943540322667));
            perim2.add(new Position(48.8356961810713, 2.58810687922195));
            perim2.add(new Position(48.8357778487659, 2.58677693848916));
            perim2.add(new Position(48.8360292121983, 2.58549667658386));
            perim2.add(new Position(48.8364406140031, 2.58431528188022));
            perim2.add(new Position(48.8369962478755, 2.58327814702033));
            perim2.add(new Position(48.837674765446, 2.58242512567161));
            perim2.add(new Position(48.838450096027, 2.58178900140602));
            perim2.add(new Position(48.8392924478312, 2.58139422747501));
            perim2.add(new Position(48.8401694522845, 2.58125598596622));
            perim2.add(new Position( 48.8410474075471, 2.58137960266798));
            perim2.add(new Position(48.8418925735108, 2.58176034038614));
            perim2.add(new Position(48.8426724685312, 2.58238357896272));
            perim2.add(new Position(48.8433571180428, 2.58322537538744));
            perim2.add(new Position(48.8439202070186, 2.58425338274128));
            perim2.add(new Position(48.8443400919009, 2.58542809283659));
            perim2.add(new Position(48.8446006330159, 2.58670435486582));
            perim2.add(new Position(48.8446918153808, 2.58803311164191));
            perim2.add(new Position(48.8446101339602, 2.58936328653696));
            perim2.add(new Position(48.8443587285066, 2.59064374834634));
            perim2.add(new Position( 48.843947262783, 2.59182527826229));
            perim2.add(new Position(48.8433915528251, 2.59286246305786));
            perim2.add(new Position(48.8427129585858, 2.59371544146339));
            perim2.add(new Position(48.8419375624248, 2.59435143644463));
            perim2.add(new Position(48.8410951661134, 2.5947460144325));
            perim2.add(new Position(48.840218145001, 2.59488402316992));

            List<List<Position>> listPerim2 = new ArrayList<>();
            listPerim2.add(perim2);
            perimeters.add(new Perimeter("perim2", "perim2", listPerim2));

            PerimeterManager pm = PerimeterManager.getPerimeterManager(getApplicationContext());
            pm.setPerimeters(perimeters);
            pm.addObservator(this);
            pm.checkPosition();
            DataDownloader.getDataDownloader(this).downloadDataFromAPI(perimeters.get(0));*/
        });
    }

    private void objectVisualiser() {
        objectVisualiser = findViewById(R.id.objectsList);
        objectVisualiser.setText("This field is created to see the objects");
        objectVisualiser.setTextColor(Color.GREEN);
        objectVisualiser.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
    }

    /**
     * Setup service that checks connectivity every 15 min and do a job
     */
    private void setupJobSchedulerService() {
        ComponentName componentName = new ComponentName(this, JobSchedulerService.class);
        JobInfo jobInfo = new JobInfo.Builder(1, componentName)
                .setPeriodic(15 * 60 * 1000) // 15 min minimum
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                .build();
        JobScheduler jobScheduler = (JobScheduler)getSystemService(JOB_SCHEDULER_SERVICE);
        int resultCode = jobScheduler.schedule(jobInfo);
        if (resultCode == JobScheduler.RESULT_FAILURE) {
            Log.d("JobScheduler", "Job not scheduled!");
        }
    }

    /**
     * Set onClickListener for the menu buttons
     */
    private void setOnClickMenuButtons() {
        // Download button starts DownloadPerimeterActivity
        findViewById(R.id.buttonMenuDownload).setOnClickListener(v -> {
            Intent intent = new Intent(this, DownloadPerimeterActivity.class);
            startActivity(intent);
        });
        // Login button starts ConnectionActivity
        findViewById(R.id.buttonMenuLogin).setOnClickListener(v -> {
            Intent intent = new Intent(this, ConnectionActivity.class);
            startActivity(intent);
        });
        // Form button starts FormActivity
        findViewById(R.id.buttonMenuForm).setOnClickListener(v -> {
            UserParamDB userParamDB = new UserParamDB(this);
            Intent intent = new Intent(getApplicationContext(), FormActivity.class);
            intent.putExtra("position", positionManager.getPosition());
            intent.putExtra("userMail", userParamDB.getUserMail());
            startActivity(intent);
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, orientation.getSensorGravity(), SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this, orientation.getSensorMagnetic(), SensorManager.SENSOR_DELAY_NORMAL);

        if(!loaded) {
            initAll();
        }

        checkUserConnection();
    }

    /**
     * If the user is not logged, the login button is accessible
     * If the user is logged, the form button is accessible
     */
    private void checkUserConnection() {
        UserParamDB userParamDB = new UserParamDB(this);
        if (userParamDB.isUserLogged()) {
            findViewById(R.id.buttonMenuLogin).setVisibility(View.INVISIBLE);
            ImageButton buttonMenuForm = findViewById(R.id.buttonMenuForm);
            buttonMenuForm.setVisibility(View.VISIBLE);
        }
    }

    private void initAll(){
        if(Permission.checkPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)&&Permission.checkPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            mARObject = new HashMap<>();
            DataDownloader dl = DataDownloader.getDataDownloader(this);
            dl.downloadsSectorsFromDefaultFile();
            launchTracking(getApplicationContext(), positionManager);
            PropertiesReader.getProReader(getApplicationContext());
            mSensorManager.registerListener(this, orientation.getSensorGravity(), SensorManager.SENSOR_DELAY_NORMAL);
            mSensorManager.registerListener(this, orientation.getSensorMagnetic(), SensorManager.SENSOR_DELAY_NORMAL);
            Log.e("myTest", "Position at init : " + positionManager.getPosition());

            dl.downloadsPerimeters(this);

            dl.downloadsSectors(new Position(48.839195,2.588587));

            loaded = true;
        }
    }

    public static void printMap(Map<String, List<? extends DisplayableData>> objsFromDB){
        Log.e("ARTest", "Entering printMap");
        for (String type : objsFromDB.keySet()){
            for(DisplayableData data : objsFromDB.get(type)){
                Log.e("ARTest", data.toString());
            }
        }
    }

    /** TODO FOR TEST ON DATAS **/
    private void initDatas(){
        //Data qui représente respectivement ABCD sur le schéma. (Coord prise avec le tel de Pierre)
        Log.e("myTest", positionManager.getPosition().toString());
        posDatas.add(new Position(48.83942628,2.58853119));
        posDatas.add(new Position(48.83948255,2.58857951));
        posDatas.add(new Position(48.83943364,2.58862743));
        posDatas.add(new Position(48.83951583,2.58859802));
        posDatas.add(new Position(48.840777, 2.588393));
        posDatas.add(new Position(48.838220, 2.585382));
        posDatas.add(new Position(48.837979, 2.582214));
        posDatas.add(new Position(48.842999, 2.581943));

        //Init ARObject
        for (Position pos : posDatas){
            //L'angle correspond à la salle 2b11/13, du tableau des post-it à la fenêtre.
            //arDatas.add(Azimuth.createARObject(new Position(48.83942628,2.58853119),90,pos.getLatitude(),pos.getLongitude()));
            arDatas.add(ARObjectFactory.createARObject(new Position(48.83942628,2.58853119),90,pos.getLatitude(),pos.getLongitude()));
        }
    }

    public void launchTracking(Context context, PositionManager pm) {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {

            positionManager.checkPosition();

            // TEST CREATION AROBJECT
           /* double angleNorthToDevice = 0;        //orientation.getBearing() * (-1);
            Position initPosition = new Position(48.839295, 2.588548);
            ARObject ARItem = ARObjectFactory.createARObject(initPosition, angleNorthToDevice, 48.842979, 2.581964);
            if(ARItem == null) {
                    Toast.makeText(getApplicationContext(),
                        "-ARobject-\n No coordinates to create an ARobject...",
                        Toast.LENGTH_LONG).show();
            }
            else {
            Toast.makeText(getApplicationContext(),
                    "-ARobject-\nx: " + ARItem.getX() + "\nz: " + ARItem.getZ()+ "\nDistance: " + ARItem.getDistance() + " meters",
                    Toast.LENGTH_LONG).show();
            }*/

            // TEST ADJUSTEMENT AROBJECT
            /*ARObject o1 = new ARObject(1, 2);
            o1.setX(2);
            o1.setZ(2);
            ARObject o2 = new ARObject(3, 4);
            o2.setX(2.5);
            o2.setZ(2);
            ARObject o3 = new ARObject(5,6);
            o3.setX(3.5);
            o3.setZ(1);
            ARObject o4 = new ARObject(7,8);
            o4.setX(2);
            o4.setZ(3);
            ARObject o5 = new ARObject(9,10);
            o5.setX(0.5);
            o5.setZ(0.5);

            ARObjectManager.adjustArPosition(o1, o2);
            ARObjectManager.adjustArPosition(o2, o3);
            ARObjectManager.adjustArPosition(o1, o4);
            ARObjectManager.adjustArPosition(o1, o5);

            System.out.println("- Test Adjustment -\n" + o1.getX() + " / " + o1.getZ() + "\n"
                    + o2.getX() + " / " + o2.getZ() + "\n"
                    + o3.getX() + " / " + o3.getZ() + "\n"
                    + o4.getX() + " / " + o4.getZ() + "\n"
                    + o5.getX() + " / " + o5.getZ());*/

        }
    }

    public void enableGPS(Context context) {
        final LocationManager manager = (LocationManager) getSystemService(context.LOCATION_SERVICE);

        if(!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Intent i = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);

            startActivityForResult(i, 1);
        }
    }

    public void updateObjectManager(int distance){
        Map<String,List<? extends DisplayableData>> mData = Database.getAllObjectsAtDistanceByType(positionManager.getPosition(),distance,getApplicationContext());
        Log.e("myTest", "Updating Map");
        for(String type : mData.keySet()) {
            Log.e("myTest", "Number of " + type + "=" + mData.get(type).size());
            mARObject.put(type, new ArrayList<>());
            for (DisplayableData data : mData.get(type)) {
                mARObject.get(type).add(createARObject(positionManager.getPosition(), orientation.getBearing(), data.getGeom().getLatitude(), data.getGeom().getLongitude(), data));
            }
        }
    }

    public void showObjectsOnText(){
        StringBuilder sb = new StringBuilder();
        for(String type : mARObject.keySet()){
            for(ARObject object : mARObject.get(type)){
                sb.append(type).append(" : ").append(object.getData().getName()).append("\n x=").append(round(object.getX(),1)).append("// y=").append(round(object.getY(),1))
                        .append("// z=").append(round(object.getZ(),1)).append("\n");
            }
        }
        objectVisualiser.setText(sb.toString());
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    @Override
    protected void onStop() {
        super.onStop();
        mSensorManager.unregisterListener(this, orientation.getSensorMagnetic());
        mSensorManager.unregisterListener(this, orientation.getSensorGravity());
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (Permission.checkPermission(this,Manifest.permission.ACCESS_COARSE_LOCATION) &&
                Permission.checkPermission(this,Manifest.permission.ACCESS_FINE_LOCATION)) {
            currentPosition = positionManager.getPosition();
            if (positionManager.getGpsStatus()) {
                orientation.onLocationChanged(positionManager.getPosition());
            }
        }
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            orientation.updateGravity(event);
        } else if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
            orientation.updateMagneticField(event);
        }
        orientation.updateBearing();
        updateTextBearing();
    }

    private void updateTextBearing() {
        Double myDouble = new Double(orientation.getBearing());
        textBearing.setText(String.format ("%.0f", myDouble)+"°");
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {}

    @Override
    public void onLocationChanged(Location location) {}

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {}

    @Override
    public void onProviderEnabled(String provider) {}

    @Override
    public void onProviderDisabled(String provider) {}

    @Override
    public void actualize(Observable o, String message) {
        if(o instanceof PerimeterManager){
            Log.i("Eiko", "Actualize " + message);
            if(message.isEmpty()){
                if(snackbar != null){
                    snackbar.dismiss();
                }
            }
            else{
                snackbar = Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_INDEFINITE );
                View snackBarView = snackbar.getView();
                TextView snackBarTxt = snackBarView.findViewById(android.support.design.R.id.snackbar_text);
                snackBarView.setBackgroundColor(getColor(R.color.blueGreen));
                snackBarTxt.setGravity(Gravity.CENTER_HORIZONTAL);
                snackbar.show();
            }
        }
        if( o instanceof PerimetersDownloaderAsynTask){
            Log.i("Eiko", "Les perimetres ont fini de telecharger");
            PerimeterManager pm = PerimeterManager.getPerimeterManager(this);
            pm.addObservator(this);
            pm.checkPosition();
        }
    }
}