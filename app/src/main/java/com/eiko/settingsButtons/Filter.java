package com.eiko.settingsButtons;

import android.widget.ImageButton;

/**
 * Represent a filter in the AR activity
 */
public interface Filter {
    /**
     * Toggle the filter
     * @return New value, true if activated
     */
    boolean toggleFilter();

    /**
     * Getter for the ImageButton concerned
     * @return ImageButton associated with the filter
     */
    ImageButton getButton();
}
