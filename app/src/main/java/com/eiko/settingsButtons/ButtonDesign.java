package com.eiko.settingsButtons;

import com.eiko.R;

/**
 * Regroup several constants
 */
public interface ButtonDesign {
    int MARGIN = 2;
    int ICON_ACTIVATED = R.drawable.menu_btn_activated;
    int ICON_DEACTIVATED = R.drawable.menu_btn_deactivated;
}
