package com.eiko.settingsButtons;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.eiko.R;

/**
 * Manage the settings buttons on the AR activity
 */
public class SettingsButtonsManagement {
    private Context context;

    /**
     * Programmatically create the layout for the settings buttons
     * @param mContext Context of the activity
     * @return Layout of the buttons
     */
    public RelativeLayout getFinalLayout(Context mContext) {
        context = mContext;

        RelativeLayout baseLayout = new RelativeLayout(context);
        RelativeLayout.LayoutParams relativeParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        baseLayout.setLayoutParams(relativeParams);

        ImageButton buttonMenuDownload = createMenuButton(R.id.buttonMenuDownload, R.drawable.icon_download, 0);
        ImageButton buttonMenuCompass = createMenuButton(R.id.buttonMenuCompass, R.drawable.icon_compass, R.id.buttonMenuDownload);
        ImageButton buttonMenuDistance = createMenuButton(R.id.buttonMenuDistance, R.drawable.icon_distance, R.id.buttonMenuCompass);
        ImageButton buttonMenuFilter = createMenuButton(R.id.buttonMenuFilter, R.drawable.icon_filter, R.id.buttonMenuDistance);
        ImageButton buttonMenuLogin = createMenuButton(R.id.buttonMenuLogin, R.drawable.icon_user, R.id.layoutMenuFilter);
        ImageButton buttonMenuForm = createMenuButton(R.id.buttonMenuForm, R.drawable.icon_form, R.id.layoutMenuFilter);
        buttonMenuForm.setVisibility(View.INVISIBLE);

        FiltersGraphic filtersGraphic = new FiltersGraphic(context);
        LinearLayout layoutMenuFilter = filtersGraphic.createFiltersLayout(buttonMenuFilter);

        baseLayout.addView(buttonMenuDownload);
        baseLayout.addView(buttonMenuCompass);
        baseLayout.addView(buttonMenuDistance);
        baseLayout.addView(layoutMenuFilter);
        baseLayout.addView(buttonMenuLogin);
        baseLayout.addView(buttonMenuForm);

        return baseLayout;
    }

    /**
     * Create a menu button
     * @param myId Id of the new button
     * @param myIcon Icon of the new button
     * @param belowId Id of the element above the new button, 0 is not considered
     * @return Button newly created
     */
    private ImageButton createMenuButton(int myId, int myIcon, int belowId) {
        ImageButton button = new ImageButton(context);
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(ButtonDesign.MARGIN, ButtonDesign.MARGIN, ButtonDesign.MARGIN, ButtonDesign.MARGIN);
        lp.addRule(RelativeLayout.ALIGN_PARENT_END);
        if (belowId != 0) {
            lp.addRule(RelativeLayout.BELOW, belowId);
        }
        button.setLayoutParams(lp);
        button.setId(myId);
        button.setImageDrawable(ContextCompat.getDrawable(context, myIcon));
        button.setBackgroundResource(ButtonDesign.ICON_ACTIVATED);
        return button;
    }
}
