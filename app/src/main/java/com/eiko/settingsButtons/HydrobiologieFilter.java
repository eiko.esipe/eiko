package com.eiko.settingsButtons;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.eiko.R;
import com.eiko.database.UserParamDB;

public class HydrobiologieFilter implements Filter, ButtonDesign {
    private Context context;
    private ImageButton imageButton;

    HydrobiologieFilter(Context mContext) {
        context = mContext;
        imageButton = new ImageButton(context);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(MARGIN, MARGIN, MARGIN, MARGIN);
        imageButton.setLayoutParams(lp);
        imageButton.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.icon_fish));
        imageButton.setBackgroundResource(ICON_ACTIVATED);
    }

    @Override
    public boolean toggleFilter() {
        UserParamDB userParamDB = new UserParamDB(context);
        boolean newValue = userParamDB.toggleHydrobiologie();
        if (newValue) {
            imageButton.setBackgroundResource(ICON_ACTIVATED);
        } else {
            imageButton.setBackgroundResource(ICON_DEACTIVATED);
        }
        return newValue;
    }

    @Override
    public ImageButton getButton() {
        return imageButton;
    }

}
