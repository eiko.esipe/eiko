package com.eiko.settingsButtons;

import android.content.Context;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.eiko.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Manage the layouts for the filters
 */
class FiltersGraphic {
    private boolean isButtonVisible = false;
    private LinearLayout filtersLayout;
    private Context context;

    FiltersGraphic(Context mContext) {
        context = mContext;
    }

    /**
     * Create the layout for filters. Contains the filter menu button and all filters
     * @param buttonMenuFilter Menu button to show filters
     * @return Layout to use as a menu button
     */
    LinearLayout createFiltersLayout(ImageButton buttonMenuFilter) {
        buttonMenuFilter.setOnClickListener(v -> flipButtonVisibility());

        LinearLayout baseLayout = createBaseLayout();
        filtersLayout = createFiltersLayout();

        List<Filter> filtersList = createFiltersList();
        for (Filter f : filtersList) {
            ImageButton filterButton = f.getButton();
            filterButton.setOnClickListener(v -> f.toggleFilter());
            filtersLayout.addView(filterButton);
        }

        baseLayout.addView(filtersLayout);
        baseLayout.addView(buttonMenuFilter);
        return baseLayout;
    }

    /**
     * Change the visibility of the layout for filters
     */
    private void flipButtonVisibility() {
        if (isButtonVisible) {
            filtersLayout.setVisibility(LinearLayout.GONE);
        } else {
            filtersLayout.setVisibility(LinearLayout.VISIBLE);
        }
        isButtonVisible = !isButtonVisible;
    }

    /**
     * Create the layout that contains the filter menu button and all the filters
     * @return FiltersGraphic layout, containing the menu button
     */
    private LinearLayout createBaseLayout() {
        LinearLayout layoutMenuFilter = new LinearLayout(context);
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        lp.addRule(RelativeLayout.BELOW, R.id.buttonMenuDistance);
        lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        layoutMenuFilter.setLayoutParams(lp);
        layoutMenuFilter.setOrientation(LinearLayout.HORIZONTAL);
        layoutMenuFilter.setId(R.id.layoutMenuFilter);
        return layoutMenuFilter;
    }

    /**
     * Create the layout for the filters
     * @return Linear layout, invisible by default
     */
    private LinearLayout createFiltersLayout() {
        LinearLayout filtersLayout = new LinearLayout(context);
        filtersLayout.setOrientation(LinearLayout.HORIZONTAL);
        filtersLayout.setVisibility(View.INVISIBLE);
        return filtersLayout;
    }

    /**
     * Create a list of filters
     * @return List of Filter objects
     */
    private List<Filter> createFiltersList() {
        ArrayList<Filter> list = new ArrayList<>();
        list.add(new FloreFilter(context));
        list.add(new BiologieFilter(context));
        list.add(new HydrobiologieFilter(context));
        list.add(new PhysicochimieFilter(context));
        return list;
    }
}
