package com.eiko.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.eiko.DataObject.Amphibien;
import com.eiko.DataObject.Biologie;
import com.eiko.DataObject.DisplayableData;
import com.eiko.DataObject.Flore;
import com.eiko.DataObject.Hydrobiologie;
import com.eiko.DataObject.HydrobiologiePar;
import com.eiko.DataObject.HydrobiologieTax;
import com.eiko.DataObject.Invertebre;
import com.eiko.DataObject.MacroInvertebre;
import com.eiko.DataObject.Physicochimie;
import com.eiko.database.contracts.AmphibienContract;
import com.eiko.database.contracts.FloreContract;
import com.eiko.database.contracts.FormJsonContract;
import com.eiko.database.contracts.HydrobiologieParContract;
import com.eiko.database.contracts.HydrobiologieTaxContract;
import com.eiko.database.contracts.InvertebreContract;
import com.eiko.database.contracts.MacroinvertebreContract;
import com.eiko.database.contracts.ParkContract;
import com.eiko.database.contracts.PhysicochimieContract;
import com.eiko.database.contracts.PositionContract;
import com.eiko.database.contracts.StationContract;
import com.eiko.database.contracts.UserParamContract;
import com.eiko.geolocalization.Position;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by axelheine on 06/02/2018.
 */

public class Database extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "test_db";
    private static final int DATABASE_VERSION = 24;
    private final Context context;

    private static Database instance = null;

    private Database(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    /**
     * Return a new SQLiteDatabase if no one exists, return the instance otherwise
     * @param context Context of view
     * @return Return the instance of writabel SQLiteDatabase
     */
    public static Database getInstance(Context context) {
        if (Database.instance == null) {
            Database.instance = new Database(context);
        }
        return Database.instance;
    }

    // Method called during the database configuration
    @Override
    public void onConfigure(SQLiteDatabase sqLiteDatabase) {
        // Activate foreign key constraint on database
        sqLiteDatabase.setForeignKeyConstraintsEnabled(true);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(FormJsonContract.CREATE_FORM_JSON_TABLE);
        sqLiteDatabase.execSQL(UserParamContract.CREATE_USER_PARAM_TABLE);

        sqLiteDatabase.execSQL(PositionContract.CREATE_POSITION_TABLE);
        sqLiteDatabase.execSQL(StationContract.CREATE_STATION_TABLE);

        sqLiteDatabase.execSQL(HydrobiologieParContract.CREATE_HYDROBIOLOGIEPAR_TABLE);
        sqLiteDatabase.execSQL(HydrobiologieTaxContract.CREATE_HYDROBIOLOGIETAX_TABLE);
        sqLiteDatabase.execSQL(MacroinvertebreContract.CREATE_MACROINVERTEBRE_TABLE);
        sqLiteDatabase.execSQL(InvertebreContract.CREATE_INVERTEBRE_TABLE);
        sqLiteDatabase.execSQL(AmphibienContract.CREATE_AMPHIBIEN_TABLE);
        sqLiteDatabase.execSQL(PhysicochimieContract.CREATE_PHYSICOCHIMIE_TABLE);
        sqLiteDatabase.execSQL(FloreContract.CREATE_FLORE_TABLE);

        sqLiteDatabase.execSQL(ParkContract.CREATE_PARK_TABLE);
    }

    //On détruit la table quand il y a une montee de version pour retrouver des id a 0.
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(FormJsonContract.DELETE_FORM_JSON_TABLE);
        sqLiteDatabase.execSQL(UserParamContract.DELETE_USER_PARAM_TABLE);

        sqLiteDatabase.execSQL(PositionContract.DELETE_POSITION_TABLE);
        sqLiteDatabase.execSQL(StationContract.DELETE_STATION_TABLE);

        sqLiteDatabase.execSQL(HydrobiologieParContract.DELETE_HYDROBIOLOGIEPAR_TABLE);
        sqLiteDatabase.execSQL(HydrobiologieTaxContract.DELETE_HYDROBIOLOGIETAX_TABLE);
        sqLiteDatabase.execSQL(MacroinvertebreContract.DELETE_MACROINVERTEBRE_TABLE);
        sqLiteDatabase.execSQL(InvertebreContract.DELETE_INVERTEBRE_TABLE);
        sqLiteDatabase.execSQL(AmphibienContract.DELETE_AMPHIBIEN_TABLE);
        sqLiteDatabase.execSQL(PhysicochimieContract.DELETE_PHYSICOCHIMIE_TABLE);
        sqLiteDatabase.execSQL(FloreContract.DELETE_FLORE_TABLE);

        sqLiteDatabase.execSQL(ParkContract.DELETE_PARK_TABLE);

        onCreate(sqLiteDatabase);
    }

    /**
     * returns all objects between the renderDistance and the ringDistance by group type (biologie, hydrobiologie, flore, physicochimie)
     * @param searchPosition center of search circle (user position in most cases)
     * @param renderDistance render distance (specified in user_parameters?)
     * @param ringDistance size of the empty zone before getting data
     * @param context context of the application
     * @return all objects in the render distance
     * @throws IllegalArgumentException if the render distance is smaller than the ring distance
     */
    public static Map<String, List<? extends DisplayableData>> getAllObjectsAtDistanceByType(Position searchPosition, int renderDistance, int ringDistance, Context context) throws IllegalArgumentException {
        if(renderDistance < ringDistance) {
            throw new IllegalArgumentException("ring cannot be larger than render distance");
        }

        Map<String, List<? extends DisplayableData>> objectsByType = new HashMap<>();

        AmphibienDB amphibienDB = new AmphibienDB(context);
        InvertebreDB invertebreDB = new InvertebreDB(context);
        MacroinvertebreDB macroinvertebreDB = new MacroinvertebreDB(context);
        FloreDB floreDB = new FloreDB(context);
        HydrobiologieParDB hydrobiologieParDB = new HydrobiologieParDB(context);
        HydrobiologieTaxDB hydrobiologieTaxDB = new HydrobiologieTaxDB(context);
        PhysicochimieDB physicochimieDB = new PhysicochimieDB(context);

        List<Biologie> biolist = new ArrayList<>();
        biolist.addAll(amphibienDB.getAllAmphibiensAtDistance(searchPosition, renderDistance, ringDistance));
        biolist.addAll(invertebreDB.getAllInvertebreAtDistance(searchPosition, renderDistance, ringDistance));
        biolist.addAll(macroinvertebreDB.getAllMacroInvertebreAtDistance(searchPosition, renderDistance, ringDistance));

        List<Hydrobiologie> hydrobioList = new ArrayList<>();
        hydrobioList.addAll(hydrobiologieParDB.getAllHydrobiologieAtDistance(searchPosition, renderDistance, ringDistance));
        hydrobioList.addAll(hydrobiologieTaxDB.getAllHydrobiologieAtDistance(searchPosition, renderDistance, ringDistance));

        objectsByType.put("biologie", biolist);

        objectsByType.put("flore", floreDB.getAllFloreAtDistance(searchPosition, renderDistance, ringDistance));

        objectsByType.put("hydrobiologie", hydrobioList);

        objectsByType.put("physicochimie", physicochimieDB.getAllPhysicochimieAtDistance(searchPosition, renderDistance, ringDistance));

        //amphibienDB.close();
        //invertebreDB.close();
        //macroinvertebreDB.close();
        //floreDB.close();
        //hydrobiologieParDB.close();
        //hydrobiologieTaxDB.close();
        //physicochimieDB.close();

        return objectsByType;
    }

    /**
     * returns all objects in the specified render distance by group type (biologie, hydrobiologie, flore, physicochimie)
     * @param searchPosition center of search circle (user position in most cases)
     * @param renderDistance render distance (specified in user_parameters?)
     * @param context context of the application
     * @return all objects in the render distance
     */
    public static Map<String, List<? extends DisplayableData>> getAllObjectsAtDistanceByType(Position searchPosition, int renderDistance, Context context) {
        return getAllObjectsAtDistanceByType(searchPosition, renderDistance, 0, context);
    }

    /**
     * import all objects in list
     * @param list list of objects extending displayable data to import
     */
    public static void importObjectsToDatabase(List<? extends DisplayableData> list, Context context) {
        AmphibienDB amphibienDB = new AmphibienDB(context);
        InvertebreDB invertebreDB = new InvertebreDB(context);
        MacroinvertebreDB macroinvertebreDB = new MacroinvertebreDB(context);
        FloreDB floreDB = new FloreDB(context);
        HydrobiologieParDB hydrobiologieParDB = new HydrobiologieParDB(context);
        HydrobiologieTaxDB hydrobiologieTaxDB = new HydrobiologieTaxDB(context);
        PhysicochimieDB physicochimieDB = new PhysicochimieDB(context);

        for(DisplayableData object: list) {
            switch (object.getClass().toString()) {
                case "class com.eiko.DataObject.Amphibien":
                    Amphibien amphibien = (Amphibien) object;
                    long id = amphibienDB.insert(amphibien);
                    break;

                case "class com.eiko.DataObject.Invertebre":
                    Invertebre invertebre = (Invertebre) object;
                    invertebreDB.insert(invertebre);
                    break;

                case "class com.eiko.DataObject.MacroInvertebre":
                    MacroInvertebre macroInvertebre = (MacroInvertebre) object;
                    macroinvertebreDB.insert(macroInvertebre);
                    break;

                case "class com.eiko.DataObject.Flore":
                    Flore flore = (Flore) object;
                    floreDB.insert(flore);
                    break;

                case "class com.eiko.DataObject.HydrobiologiePar":
                    HydrobiologiePar hydrobiologiePar = (HydrobiologiePar) object;
                    hydrobiologieParDB.insert(hydrobiologiePar);
                    break;

                case "class com.eiko.DataObject.HydrobiologieTax":
                    HydrobiologieTax hydrobiologieTax = (HydrobiologieTax) object;
                    hydrobiologieTaxDB.insert(hydrobiologieTax);
                    break;

                case "class com.eiko.DataObject.Physicochimie":
                    Physicochimie physicochimie = (Physicochimie) object;
                    physicochimieDB.insert(physicochimie);
                    break;

                default:
                    System.out.println("ECHEC : " + object.getClass().toString());
            }
        }

        //amphibienDB.close();
        //invertebreDB.close();
        //macroinvertebreDB.close();
        //floreDB.close();
        //hydrobiologieParDB.close();
        //hydrobiologieTaxDB.close();
        //physicochimieDB.close();
    }

    /**
     * Delete data form all table only if the park KF is empty
     * @param context current context of application
     */
    public static void deleteData(Context context) {
        SQLiteDatabase db = Database.getInstance(context).getWritableDatabase();

        db.execSQL("DELETE FROM " + AmphibienContract.AmphibienEntry.TABLE_AMPHIBIEN + " WHERE park_fk LIKE '';");
        db.execSQL("DELETE FROM " + FloreContract.FloreEntry.TABLE_FLORE + " WHERE park_fk LIKE '';");
        db.execSQL("DELETE FROM " + HydrobiologieParContract.HydrobiologieParEntry.TABLE_HYDROBIOLOGIE_PAR + " WHERE park_fk LIKE '';");
        db.execSQL("DELETE FROM " + HydrobiologieTaxContract.HydrobiologieTaxEntry.TABLE_HYDROBIOLOGIE_TAX + " WHERE park_fk LIKE '';");
        db.execSQL("DELETE FROM " + InvertebreContract.InvertebreEntry.TABLE_INVERTEBRE + " WHERE park_fk LIKE '';");
        db.execSQL("DELETE FROM " + MacroinvertebreContract.MacroinvertebreEntry.TABLE_MACTOINVERTEBRE + " WHERE park_fk LIKE '';");
        db.execSQL("DELETE FROM " + PhysicochimieContract.PhysicochimieEntry.TABLE_PHYSICOCHIMIE + " WHERE park_fk LIKE '';");
        db.execSQL("DELETE FROM " + StationContract.StationEntry.TABLE_STATION + " WHERE park_fk LIKE '';");
        db.execSQL("DELETE FROM " + PositionContract.PositionEntry.TABLE_POSITION + " WHERE park_fk LIKE '';");
    }

    /**
     * Add data with parkName key to save a park into database
     * @param list all DisplayableDatas to save
     * @param parkName park name to save
     * @param context current context of application
     */
    public static void addDataInPark(List<? extends DisplayableData> list, String parkName, Context context) {
        AmphibienDB amphibienDB = new AmphibienDB(context);
        InvertebreDB invertebreDB = new InvertebreDB(context);
        MacroinvertebreDB macroinvertebreDB = new MacroinvertebreDB(context);
        FloreDB floreDB = new FloreDB(context);
        HydrobiologieParDB hydrobiologieParDB = new HydrobiologieParDB(context);
        HydrobiologieTaxDB hydrobiologieTaxDB = new HydrobiologieTaxDB(context);
        PhysicochimieDB physicochimieDB = new PhysicochimieDB(context);

        parkName = parkName.replaceAll("'", "''");

        for(DisplayableData object: list) {
            switch (object.getClass().toString()) {
                case "class com.eiko.DataObject.Amphibien":
                    Amphibien amphibien = (Amphibien) object;
                    amphibienDB.insert(amphibien, parkName);
                    break;

                case "class com.eiko.DataObject.Invertebre":
                    Invertebre invertebre = (Invertebre) object;
                    invertebreDB.insert(invertebre, parkName);
                    break;

                case "class com.eiko.DataObject.MacroInvertebre":
                    MacroInvertebre macroInvertebre = (MacroInvertebre) object;
                    macroinvertebreDB.insert(macroInvertebre, parkName);
                    break;

                case "class com.eiko.DataObject.Flore":
                    Flore flore = (Flore) object;
                    floreDB.insert(flore, parkName);
                    break;

                case "class com.eiko.DataObject.HydrobiologiePar":
                    HydrobiologiePar hydrobiologiePar = (HydrobiologiePar) object;
                    hydrobiologieParDB.insert(hydrobiologiePar, parkName);
                    break;

                case "class com.eiko.DataObject.HydrobiologieTax":
                    HydrobiologieTax hydrobiologieTax = (HydrobiologieTax) object;
                    hydrobiologieTaxDB.insert(hydrobiologieTax, parkName);
                    break;

                case "class com.eiko.DataObject.Physicochimie":
                    Physicochimie physicochimie = (Physicochimie) object;
                    physicochimieDB.insert(physicochimie, parkName);
                    break;

                default:
                    System.out.println("ECHEC : " + object.getClass().toString());
            }
        }

        addSavedPark(parkName, Database.getInstance(context).getWritableDatabase());

        //amphibienDB.close();
        //invertebreDB.close();
        //macroinvertebreDB.close();
        //floreDB.close();
        //hydrobiologieParDB.close();
        //hydrobiologieTaxDB.close();
        //physicochimieDB.close();
    }

    /**
     * Delete all rows with park name int FK column
     * @param parkName park name to delete
     * @param context current context of application
     */
    public static void deleteAllInPark(String parkName, Context context) {
        SQLiteDatabase db = Database.getInstance(context).getWritableDatabase();

        parkName = parkName.replaceAll("'", "''");

        db.execSQL("DELETE FROM " + AmphibienContract.AmphibienEntry.TABLE_AMPHIBIEN + " WHERE park_fk LIKE '%" + parkName + "%';");
        db.execSQL("DELETE FROM " + FloreContract.FloreEntry.TABLE_FLORE + " WHERE park_fk LIKE '%" + parkName + "%';");
        db.execSQL("DELETE FROM " + HydrobiologieParContract.HydrobiologieParEntry.TABLE_HYDROBIOLOGIE_PAR + " WHERE park_fk LIKE '%" + parkName + "%';");
        db.execSQL("DELETE FROM " + HydrobiologieTaxContract.HydrobiologieTaxEntry.TABLE_HYDROBIOLOGIE_TAX + " WHERE park_fk LIKE '%" + parkName + "%';");
        db.execSQL("DELETE FROM " + InvertebreContract.InvertebreEntry.TABLE_INVERTEBRE + " WHERE park_fk LIKE '%" + parkName + "%';");
        db.execSQL("DELETE FROM " + MacroinvertebreContract.MacroinvertebreEntry.TABLE_MACTOINVERTEBRE + " WHERE park_fk LIKE '%" + parkName + "%';");
        db.execSQL("DELETE FROM " + PhysicochimieContract.PhysicochimieEntry.TABLE_PHYSICOCHIMIE + " WHERE park_fk LIKE '%" + parkName + "%';");
        db.execSQL("DELETE FROM " + StationContract.StationEntry.TABLE_STATION + " WHERE park_fk LIKE '%" + parkName + "%';");
        db.execSQL("DELETE FROM " + PositionContract.PositionEntry.TABLE_POSITION + " WHERE park_fk LIKE '%" + parkName + "%';");

        deleteSavedPark(parkName, db);

      //  db.close();
    }

    /**
     * Private method to save a park name
     * @param parkName park name to save
     * @param db SQLite database instance
     */
    private static void addSavedPark(String parkName, SQLiteDatabase db) {
        Cursor c = db.rawQuery("SELECT * FROM " + ParkContract.ParkEntry.TABLE_PARK + " WHERE " + ParkContract.ParkEntry.KEY_NAME + " LIKE '%" + parkName +"'", null);

        if (c.getCount() > 0) {
            c.close();
            return;
        }

        ContentValues cv = new ContentValues();
        cv.put(ParkContract.ParkEntry.KEY_NAME, parkName);
        db.insert(ParkContract.ParkEntry.TABLE_PARK, null, cv);
    }

    /**
     * Private method to delete a park previously saved
     * @param parkName park name to delete
     * @param db SQLite database instance
     */
    private static void deleteSavedPark(String parkName, SQLiteDatabase db) {
        db.delete(ParkContract.ParkEntry.TABLE_PARK, ParkContract.ParkEntry.KEY_NAME + " = ?", new String[] {parkName});
    }

    /**
     * Return all parks name stored in database
     * @param context current context of application
     * @return List of string containing all parks name saved
     */
    public static List<String> getSavedParks(Context context) {
        List<String> list = new ArrayList<>();
        SQLiteDatabase db = Database.getInstance(context).getWritableDatabase();

        Cursor c = db.rawQuery("SELECT * FROM " + ParkContract.ParkEntry.TABLE_PARK, null);

        if (c.getCount() <= 0) {
            return list;
        }

        c.moveToFirst();

        do {
            list.add(c.getString(c.getColumnIndex(ParkContract.ParkEntry.KEY_NAME)));
        } while (c.moveToNext());

        c.close();

        return list;
    }
}
