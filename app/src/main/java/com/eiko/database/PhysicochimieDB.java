package com.eiko.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.eiko.DataObject.Physicochimie;
import com.eiko.geolocalization.Position;
import com.eiko.database.contracts.PhysicochimieContract;
import com.eiko.database.contracts.PhysicochimieContract.PhysicochimieEntry;
import com.eiko.database.contracts.PositionContract;
import com.eiko.database.contracts.StationContract;
import com.eiko.util.DateFormat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created by axelheine on 12/02/2018.
 */

public class PhysicochimieDB {
    private Database dbInstance;
    private SQLiteDatabase db;
    private Context context;

    public PhysicochimieDB(Context context) {
        this.context = Objects.requireNonNull(context);
        this.dbInstance = Database.getInstance(context);
        this.db = dbInstance.getWritableDatabase();
    }

    public void close() {
        db.close();
    }

    /**
     * Insert new entry of Physicochimie object in database.
     *
     * @param physicochimie physicochimie object to insert in db
     * @return the id of inserted row in database
     */
    public long insert(Physicochimie physicochimie) {
        return insert(physicochimie, "");
    }

    /**
     * Insert new entry of Physicochimie object in database.
     *
     * @param physicochimie physicochimie object to insert in db
     * @param parkName park name to save
     * @return the id of inserted row in database
     */
    public long insert(Physicochimie physicochimie, String parkName) {
        StationDB stationDB = new StationDB(context);
        PositionDB positionDB = new PositionDB(context);
        long stationID = stationDB.insert(physicochimie.getStation_geom(), physicochimie.getStation_nom());
        //long positionID = positionDB.insert(physicochimie.getPrelevement_geom());

        long physicochimieID;

        // On essaye d'abord de select les donnees pour voir si l'element n'est pas deja insere
        Cursor c = db.query(
                PhysicochimieEntry.TABLE_PHYSICOCHIMIE,
                new String[]{PhysicochimieEntry.KEY_ID},
                PhysicochimieEntry.KEY_PARAMETRE + " = ? AND " + PhysicochimieEntry.KEY_RSANA + " = ? AND " + PhysicochimieEntry.KEY_DATE_PREL + " = ? AND " + PhysicochimieEntry.KEY_PARK_FK + " = ?",
                new String[]{physicochimie.getParametre(), physicochimie.getRsana(), DateFormat.dateToString(physicochimie.getDateprel(), "y-M-d"), parkName},
                null, null, null
        );

        // si il existe deja les donnees, on recupere l'id
        if (c.moveToFirst() && c.getCount() > 0) {
            physicochimieID = c.getLong(c.getColumnIndex(PhysicochimieEntry.KEY_ID));
        } else {
            ContentValues values = new ContentValues();

            values.put(PhysicochimieEntry.KEY_PARAMETRE, physicochimie.getParametre());
            values.put(PhysicochimieEntry.KEY_RSANA, physicochimie.getRsana());
            values.put(PhysicochimieEntry.KEY_DATE_PREL, DateFormat.dateToString(physicochimie.getDateprel(), "y-M-d"));
            values.put(PhysicochimieEntry.KEY_LSANA, physicochimie.getLsana());
            values.put(PhysicochimieEntry.KEY_LDANA, physicochimie.getLdana());
            values.put(PhysicochimieEntry.KEY_LQANA, physicochimie.getLdana());
            values.put(PhysicochimieEntry.KEY_LABORATOIRE, physicochimie.getLaboratoire());
            values.put(PhysicochimieEntry.KEY_METHODE, physicochimie.getMethode());
            values.put(PhysicochimieEntry.KEY_CODE_RMQ, physicochimie.getCode_remarque());
            values.put(PhysicochimieEntry.KEY_PRODUCTEUR, physicochimie.getProducteur());
            values.put(PhysicochimieEntry.KEY_STATION_FK, stationID);
            values.put(PhysicochimieEntry.KEY_PARK_FK, parkName);

            physicochimieID = db.insert(PhysicochimieEntry.TABLE_PHYSICOCHIMIE, null, values);
        }
        c.close();
        return physicochimieID;
    }

    /**
     * Select one object from his ID
     * @param id requested ID
     * @return object corresponding to the ID
     */
    public Physicochimie select(long id) {
        String selectFromId = "SELECT * FROM " + PhysicochimieEntry.TABLE_PHYSICOCHIMIE + " AS phys, "
                + StationContract.StationEntry.TABLE_STATION + " AS station, "
                + PositionContract.PositionEntry.TABLE_POSITION + " AS station_pos, "
                + PositionContract.PositionEntry.TABLE_POSITION + " AS prel_pos "
                + " WHERE phys." + PhysicochimieEntry.KEY_STATION_FK + " = station." + StationContract.StationEntry.KEY_ID
                + " AND station." + StationContract.StationEntry.KEY_POSITION_FK + " = station_pos." + PositionContract.PositionEntry.KEY_POSITION_ID
               // + " AND phys." + PhysicochimieEntry.KEY_POSITION_FK + " = prel_pos." + PositionContract.PositionEntry.KEY_POSITION_ID
                + " AND phys." + PhysicochimieEntry.KEY_ID + " = " + String.valueOf(id);

        Cursor cursor = db.rawQuery(selectFromId, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            return createPhysicochimieFromCursor(cursor);
        }

        cursor.close();
        return null;
    }

    /**
     * Select all physicochimie between renderDistance and ringDistance from the point in parameter
     * @param searchPosition position of search
     * @param renderDistance search distance
     * @param ringDistance size of the empty zone before getting data
     * @return return a list of physicochimie objects at requested distance
     * @throws IllegalArgumentException if the render distance is smaller than the ring distance
     */
    public List<Physicochimie> getAllPhysicochimieAtDistance(Position searchPosition, int renderDistance, int ringDistance) throws IllegalArgumentException {
        if(renderDistance < ringDistance) {
            throw new IllegalArgumentException("ring cannot be larger than render distance");
        }

        // list for store and return matching objects
        List<Physicochimie> physicochimieList = new ArrayList<>();

        // Select All Query
        Cursor cursor = db.rawQuery(PhysicochimieContract.SELECT_PHYSICOCHIMIE_STATION_POSITION, null);

        //if cursor is empty, return an empty list
        if(cursor.getCount() == 0) {
            return physicochimieList;
        }

        // looping through all rows and adding to list if they are in render distance
        if (cursor.moveToFirst()) {
            do {
                double objectLongitude =  Double.parseDouble(cursor.getString(cursor.getColumnIndex(PositionContract.PositionEntry.KEY_POSITION_LON)));
                double objectLatitude =  Double.parseDouble(cursor.getString(cursor.getColumnIndex(PositionContract.PositionEntry.KEY_POSITION_LAT)));

                // create position of the db object and calculates distance from user position
                Position np = new Position(objectLatitude, objectLongitude);
                float dstation = searchPosition.getDistanceTo(np);

                // if object is too far, continue
                if (dstation > renderDistance || dstation < ringDistance) {
                    continue;
                }

                // Adding to list
                physicochimieList.add(createPhysicochimieFromCursor(cursor));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return physicochimieList;
    }

    /**
     * Select all physicochimie at a certain distance from the point in parameter
     * @param searchPosition position of search
     * @param renderDistance search distance
     * @return return a list of physicochimie objects at requested distance
     */
    public List<Physicochimie> getAllPhysicochimieAtDistance(Position searchPosition, int renderDistance) {
        return getAllPhysicochimieAtDistance(searchPosition, renderDistance, 0);
    }

    private Physicochimie createPhysicochimieFromCursor(Cursor cursor) {
        Map<String, String> paramsMap = new HashMap<>();
        paramsMap.put("parametre",      cursor.getString(cursor.getColumnIndex(PhysicochimieEntry.KEY_PARAMETRE)));
        paramsMap.put("rsana",          cursor.getString(cursor.getColumnIndex(PhysicochimieEntry.KEY_RSANA)));
        paramsMap.put("lsana",          cursor.getString(cursor.getColumnIndex(PhysicochimieEntry.KEY_LSANA)));
        paramsMap.put("ldana",          cursor.getString(cursor.getColumnIndex(PhysicochimieEntry.KEY_LDANA)));
        paramsMap.put("lqana",          cursor.getString(cursor.getColumnIndex(PhysicochimieEntry.KEY_LQANA)));
        paramsMap.put("laboratoire",    cursor.getString(cursor.getColumnIndex(PhysicochimieEntry.KEY_LABORATOIRE)));
        paramsMap.put("methode",        cursor.getString(cursor.getColumnIndex(PhysicochimieEntry.KEY_METHODE)));
        paramsMap.put("dateprel",       cursor.getString(cursor.getColumnIndex(PhysicochimieEntry.KEY_DATE_PREL)));
        paramsMap.put("producteur",     cursor.getString(cursor.getColumnIndex(PhysicochimieEntry.KEY_PRODUCTEUR)));
        paramsMap.put("code_remarque",  cursor.getString(cursor.getColumnIndex(PhysicochimieEntry.KEY_CODE_RMQ)));
        paramsMap.put("station_nom",    cursor.getString(cursor.getColumnIndex(StationContract.StationEntry.KEY_NAME)));
        paramsMap.put("longitude",      cursor.getString(cursor.getColumnIndex(PositionContract.PositionEntry.KEY_POSITION_LON)));
        paramsMap.put("latitude",       cursor.getString(cursor.getColumnIndex(PositionContract.PositionEntry.KEY_POSITION_LAT)));
        //paramsMap.put("longitude_prel", cursor.getString(20));
        //paramsMap.put("latitude_prel",  cursor.getString(21));

        return new Physicochimie(paramsMap);
    }
}
