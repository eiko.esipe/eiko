package com.eiko.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.eiko.DataObject.Invertebre;
import com.eiko.geolocalization.Position;
import com.eiko.database.contracts.InvertebreContract;
import com.eiko.database.contracts.InvertebreContract.InvertebreEntry;
import com.eiko.database.contracts.PositionContract;
import com.eiko.database.contracts.StationContract;
import com.eiko.util.DateFormat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created by axelheine on 09/02/2018.
 */

public class InvertebreDB {
    private Database dbInstance;
    private SQLiteDatabase db;
    private Context context;

    public InvertebreDB(Context context) {
        this.context = Objects.requireNonNull(context);
        this.dbInstance = Database.getInstance(context);
        this.db = dbInstance.getWritableDatabase();
    }

    public void close() {
        db.close();
    }

    /**
     * Insert new entry of Invertebre class in database.
     *
     * @param invertebre invertebre object to insert in db
     * @return the id of inserted row in database
     */
    public long insert(Invertebre invertebre) {
        return insert(invertebre, "");
    }

    /**
     * Insert new entry of Invertebre class in database.
     *
     * @param invertebre invertebre object to insert in db
     * @param parkName park name to save
     * @return the id of inserted row in database
     */
    public long insert(Invertebre invertebre, String parkName) {
        StationDB stationDB = new StationDB(context);
        long stationID = stationDB.insert(invertebre.getGeom(), invertebre.getStationCode());
        long invertebreID;

        // On essaye d'abord de select les donnees pour voir si l'element n'est pas deja insere
        Cursor c = db.query(
                InvertebreEntry.TABLE_INVERTEBRE,
                new String[] {StationContract.StationEntry.KEY_ID},
                InvertebreEntry.KEY_TAXON + " = ? AND " + InvertebreEntry.KEY_DATE_SORTIE + " = ? AND " + InvertebreEntry.KEY_TOTAL_IND + " = ? AND " /*+ InvertebreEntry.KEY_STATION_FK + " = ? AND "*/ + InvertebreEntry.KEY_PARK_FK + " = ?",
                new String[] {invertebre.getTaxon(), DateFormat.dateToString(invertebre.getDateDeSortie(), "d-M-y"), String.valueOf(invertebre.getTotalIndividu()), /*String.valueOf(stationID),*/ parkName},
                null, null, null
        );

        // si il existe deja les donnees, on recupere l'id
        if (c.moveToFirst() && c.getCount() > 0) {
            invertebreID = c.getLong(c.getColumnIndex(PositionContract.PositionEntry.KEY_POSITION_ID));
        } else {
            ContentValues values = new ContentValues();
            values.put(InvertebreEntry.KEY_TAXON, invertebre.getTaxon());
            values.put(InvertebreEntry.KEY_DATE_SORTIE, DateFormat.dateToString(invertebre.getDateDeSortie(), "d-M-y"));
            values.put(InvertebreEntry.KEY_TOTAL_IND, invertebre.getTotalIndividu());
            values.put(InvertebreEntry.KEY_STATION_FK, stationID);
            values.put(InvertebreEntry.KEY_PARK_FK, parkName);

            invertebreID = db.insert(InvertebreEntry.TABLE_INVERTEBRE, null, values);
        }
        c.close();
        return invertebreID;
    }

    /**
     * Select one object from his ID
     * @param id requested ID
     * @return object corresponding to the ID
     */
    public Invertebre select(long id) {
        String selectFromId = "SELECT * FROM " + InvertebreEntry.TABLE_INVERTEBRE + ", "
                + StationContract.StationEntry.TABLE_STATION + ", "
                + PositionContract.PositionEntry.TABLE_POSITION
                + " WHERE " + InvertebreEntry.TABLE_INVERTEBRE + "." + InvertebreEntry.KEY_STATION_FK
                + " = " + StationContract.StationEntry.TABLE_STATION + "." + StationContract.StationEntry.KEY_ID
                + " AND " + StationContract.StationEntry.TABLE_STATION + "." + StationContract.StationEntry.KEY_POSITION_FK
                + " = " + PositionContract.PositionEntry.TABLE_POSITION + "." + PositionContract.PositionEntry.KEY_POSITION_ID
                + " AND " + InvertebreEntry.TABLE_INVERTEBRE + "." + InvertebreEntry.KEY_ID + " = " + String.valueOf(id);

        Cursor cursor = db.rawQuery(selectFromId, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            return createInvertebreFromCursor(cursor);
        }

        cursor.close();
        return null;
    }

    /**
     * Select all invertebre between renderDistance and ringDistance from the point in parameter
     * @param searchPosition position of search
     * @param renderDistance search distance
     * @param ringDistance size of the empty zone before getting data
     * @return return a list of invertebre objects at requested distance
     * @throws IllegalArgumentException if the render distance is smaller than the ring distance
     */
    public List<Invertebre> getAllInvertebreAtDistance(Position searchPosition, int renderDistance, int ringDistance) throws IllegalArgumentException {
        if(renderDistance < ringDistance) {
            throw new IllegalArgumentException("ring cannot be larger than render distance");
        }

        //Liste pour le retour des objets
        List<Invertebre> amphibList = new ArrayList<>();

        // Select All Query
        Cursor cursor = db.rawQuery(InvertebreContract.SELECT_INVERTEBRE_STATION_POSITION, null);

        //Si le curseur est vide, pas de résultat
        if(cursor.getCount() == 0) {
            return amphibList;
        }

        // looping through all rows and adding to list if they are in render distance
        if (cursor.moveToFirst()) {
            do {
                double objectLongitude =  Double.parseDouble(cursor.getString(cursor.getColumnIndex(PositionContract.PositionEntry.KEY_POSITION_LON)));
                double objectLatitude =  Double.parseDouble(cursor.getString(cursor.getColumnIndex(PositionContract.PositionEntry.KEY_POSITION_LAT)));

                // create position of the db object and calculates distance from user position
                Position np = new Position(objectLatitude, objectLongitude);
                float d = searchPosition.getDistanceTo(np);

                // if object is too far, continue
                if (d > renderDistance || d < ringDistance) {
                    continue;
                }

                // Adding to list
                amphibList.add(createInvertebreFromCursor(cursor));
            } while (cursor.moveToNext());
        }

        cursor.close();
        return amphibList;
    }

    /**
     * Select all invertebres at a certain distance from the point in parameter
     * @param searchPosition position of search
     * @param renderDistance search distance
     * @return return a list of invertebre objects at requested distance
     */
    public List<Invertebre> getAllInvertebreAtDistance(Position searchPosition, int renderDistance) {
        return getAllInvertebreAtDistance(searchPosition, renderDistance, 0);
    }

    private Invertebre createInvertebreFromCursor(Cursor cursor) {
        Map<String, String> paramsMap = new HashMap<>();
        paramsMap.put("taxon",          cursor.getString(cursor.getColumnIndex(InvertebreEntry.KEY_TAXON)));
        paramsMap.put("date_de_sortie", cursor.getString(cursor.getColumnIndex(InvertebreEntry.KEY_DATE_SORTIE)));
        paramsMap.put("total_ind",      cursor.getString(cursor.getColumnIndex(InvertebreEntry.KEY_TOTAL_IND)));
        paramsMap.put("station_code",   cursor.getString(cursor.getColumnIndex(StationContract.StationEntry.KEY_NAME)));
        paramsMap.put("longitude",      cursor.getString(cursor.getColumnIndex(PositionContract.PositionEntry.KEY_POSITION_LON)));
        paramsMap.put("latitude",       cursor.getString(cursor.getColumnIndex(PositionContract.PositionEntry.KEY_POSITION_LAT)));

        return new Invertebre(paramsMap);
    }
}
