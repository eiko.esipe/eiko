package com.eiko.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.eiko.database.contracts.UserParamContract;
import com.eiko.util.EmailVerification;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

/**
 * Created by axelheine on 12/02/2018.
 */

public class UserParamDB {
    private Database dbInstance;
    private SQLiteDatabase db;
    private Context context;

    public UserParamDB(Context context) {
        this.context = Objects.requireNonNull(context);
        this.dbInstance = Database.getInstance(context);
        this.db = dbInstance.getWritableDatabase();
        insertIfEmpty();
    }

    public void close() {
        db.close();
    }

    /**
     * Private method to insert one user if the database is empty
     */
    private void insertIfEmpty() {
        Cursor c = db.rawQuery("SELECT * FROM " + UserParamContract.UserParamEntry.TABLE_USER + " WHERE " + UserParamContract.UserParamEntry.KEY_ID + " = 1", null);

        if(c.getCount() == 0) {
            ContentValues cv = new ContentValues();
            cv.put(UserParamContract.UserParamEntry.KEY_HYDROBIOLOGIE, 1);
            cv.put(UserParamContract.UserParamEntry.KEY_BIOLOGIE, 1);
            cv.put(UserParamContract.UserParamEntry.KEY_FLORE, 1);
            cv.put(UserParamContract.UserParamEntry.KEY_PHYSICOCHIMIE, 1);
            cv.put(UserParamContract.UserParamEntry.KEY_RENDER_DISTANCE, 50);
            db.insert(UserParamContract.UserParamEntry.TABLE_USER, null, cv);
        }
    }

    /**
     * Private method to toggle any filter
     * @param field Filter field to toggle
     * @return the new value of the filter
     */
    private boolean toggleFilter(String field) {
        Cursor c = db.rawQuery("SELECT * FROM " + UserParamContract.UserParamEntry.TABLE_USER + " WHERE " + UserParamContract.UserParamEntry.KEY_ID + " = 1", null);

        int res = 1;
        if(c.getCount() > 0) {
            c.moveToFirst();
            int currentValue = c.getInt(c.getColumnIndex(field));

            res = (currentValue == 0 ? 1 : 0);

            ContentValues cv = new ContentValues();
            cv.put(field, res);
            db.update(UserParamContract.UserParamEntry.TABLE_USER, cv, "ID = 1", new String[]{});
        }
        c.close();

        return res == 1;
    }

    /**
     * Private method used to get value of any filter
     * @param field field of filter to get
     * @return boolean value of filter
     */
    private boolean getFilterValue(String field) {
        Cursor c = db.rawQuery("SELECT * FROM " + UserParamContract.UserParamEntry.TABLE_USER + " WHERE " + UserParamContract.UserParamEntry.KEY_ID + " = 1", null);

        int res = 1;
        if(c.getCount() > 0) {
            c.moveToFirst();
            res = c.getInt(c.getColumnIndex(field));
        }
        c.close();

        return res == 1;
    }

    /**
     * Toggle Hydrobiology filter (1 if 0 and 0 if 1), insert in database and return new value
     * @return the new filter value
     */
    public boolean toggleHydrobiologie() {
        return toggleFilter(UserParamContract.UserParamEntry.KEY_HYDROBIOLOGIE);
    }

    /**
     * Toggle Flore filter (1 if 0 and 0 if 1), insert in database and return new value
     * @return the new filter value
     */
    public boolean toggleFlore() {
        return toggleFilter(UserParamContract.UserParamEntry.KEY_FLORE);
    }

    /**
     * Toggle Biologie filter (1 if 0 and 0 if 1), insert in database and return new value
     * @return the new filter value
     */
    public boolean toggleBiologie() {
        return toggleFilter(UserParamContract.UserParamEntry.KEY_BIOLOGIE);
    }

    /**
     * Toggle Physicochimie filter (1 if 0 and 0 if 1), insert in database and return new value
     * @return the new filter value
     */
    public boolean togglePhysicochimie() {
        return toggleFilter(UserParamContract.UserParamEntry.KEY_PHYSICOCHIMIE);
    }

    /**
     * Return the current value of the hydrobiology filter
     * @return current value of filter
     */
    public boolean getHydrobiologie() {
        return getFilterValue(UserParamContract.UserParamEntry.KEY_HYDROBIOLOGIE);
    }

    /**
     * Return the current value of the flore filter
     * @return current value of filter
     */
    public boolean getFlore() {
        return getFilterValue(UserParamContract.UserParamEntry.KEY_FLORE);
    }

    /**
     * Return the current value of the biologie filter
     * @return current value of filter
     */
    public boolean getBiologie() {
        return getFilterValue(UserParamContract.UserParamEntry.KEY_BIOLOGIE);
    }

    /**
     * Return the current value of the physiochimie filter
     * @return current value of filter
     */
    public boolean getPhysicochimie() {
        return getFilterValue(UserParamContract.UserParamEntry.KEY_PHYSICOCHIMIE);
    }

    /**
     * Set the user mail into the database
     * @param mail mail to save
     */
    public void setUserMail(@NotNull String mail) {
        if (!EmailVerification.isValidEmail(mail)) {
            return;
        }
        ContentValues cv = new ContentValues();
        cv.put(UserParamContract.UserParamEntry.KEY_MAIL, mail);
        db.update(UserParamContract.UserParamEntry.TABLE_USER, cv, "ID = ?", new String[] {"1"});
    }

    /**
     * return true if user is logged (ie. if an email address is set), false otherwise
     * @return boolean indicates if user is logged or not
     */
    public boolean isUserLogged() {
        Cursor c = db.rawQuery("SELECT " + UserParamContract.UserParamEntry.KEY_MAIL
                + " FROM " + UserParamContract.UserParamEntry.TABLE_USER
                + " WHERE " + UserParamContract.UserParamEntry.KEY_ID + " = 1 "
                        + "AND " + UserParamContract.UserParamEntry.KEY_MAIL + " NOT NULL;",
                null);
        if (c.getCount() > 0) {
            return true;
        }
        return false;
    }

    /**
     * Method to get the user mail used to connection. WARN: Returns null if no mail set.
     * @return the user email as a string
     */
    public String getUserMail() {
        Cursor c = db.rawQuery("SELECT " + UserParamContract.UserParamEntry.KEY_MAIL
                        + " FROM " + UserParamContract.UserParamEntry.TABLE_USER
                        + " WHERE " + UserParamContract.UserParamEntry.KEY_ID + " = 1 "
                        + "AND " + UserParamContract.UserParamEntry.KEY_MAIL + " NOT NULL;",
                null);
        if (c.getCount() > 0) {
            c.moveToFirst();
            return c.getString(c.getColumnIndex(UserParamContract.UserParamEntry.KEY_MAIL));
        }
        return null;
    }

    /**
     * Delete user email (set to null in database). Use it when user disconnect from application
     */
    public void deleteUserMail() {
        ContentValues cv = new ContentValues();
        cv.putNull(UserParamContract.UserParamEntry.KEY_MAIL);
        db.update(UserParamContract.UserParamEntry.TABLE_USER, cv, "ID = ?", new String[] {"1"});
    }

    /**
     * Set the render distance to user parameters table (default 50m)
     * @param distance distance to set
     */
    public void setRenderDistance(int distance) {
        ContentValues cv = new ContentValues();
        cv.put(UserParamContract.UserParamEntry.KEY_RENDER_DISTANCE, distance);
        db.update(UserParamContract.UserParamEntry.TABLE_USER, cv, "ID = ?", new String[] {"1"});
    }

    /**
     * return the render distance previously setted (default 50m)
     * @return
     */
    public int getRenderDistance() {
        int renderDistance = 50;
        Cursor c = db.rawQuery("SELECT " + UserParamContract.UserParamEntry.KEY_RENDER_DISTANCE
                        + " FROM " + UserParamContract.UserParamEntry.TABLE_USER
                        + " WHERE " + UserParamContract.UserParamEntry.KEY_ID + " = 1 ",
                null);
        if (c.getCount() > 0) {
            c.moveToFirst();
            renderDistance = c.getInt(c.getColumnIndex(UserParamContract.UserParamEntry.KEY_RENDER_DISTANCE));
        }
        c.close();
        return renderDistance;
    }
}
