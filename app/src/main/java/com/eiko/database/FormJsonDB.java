package com.eiko.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.eiko.database.contracts.FormJsonContract.FormJsonEntry;

import java.util.Objects;

/**
 * Created by axelheine on 09/02/2018.
 * Object passing from json form to database and other way.
 * You have to call the close() method after using it.
 */

public class FormJsonDB {
    private Database dbInstance;
    private SQLiteDatabase db;
    private Context context;

    /**
     * Creates FormJsonDB object wich contains a database instance
     * @param context current context of application
     */
    public FormJsonDB(Context context) {
        this.context = Objects.requireNonNull(context);
        this.dbInstance = Database.getInstance(context);
        this.db = dbInstance.getWritableDatabase();
    }

    /**
     * Close the database instance
     */
    public void close() {
        db.close();
    }

    /**
     * Add JSON from Form to TABLE_FORM
     * @param json JSON to save into the database
     * @return id of the new entry
     */
    public long addJSON(String json) {
        ContentValues values = new ContentValues();
        values.put(FormJsonEntry.KEY_JSON, json);

        // Inserting Row
        return db.insert(FormJsonEntry.TABLE_FORM_JSON, null, values);
    }

    /**
     * Return the first occurence of Json saved into TABLE_FORM
     * @return String object representing JSON Form
     */
    public String getFirstJSON() {
        String queryString = "SELECT * FROM " + FormJsonEntry.TABLE_FORM_JSON;
        String json = null;

        Cursor cursor = db.rawQuery(queryString, null);
        if(cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            json = cursor.getString(cursor.getColumnIndex(FormJsonEntry.KEY_JSON));
        }


        cursor.close();
        return json;
    }

    /**
     * Delete first JSON entry from TABLE_JSON
     */
    public void deleteFirstJSON() {
        String queryString = "SELECT * FROM " + FormJsonEntry.TABLE_FORM_JSON;

        Cursor cursor = db.rawQuery(queryString, null);
        if (cursor == null || cursor.getCount() == 0) {
            return;
        }
        cursor.moveToFirst();
        int id = cursor.getInt(cursor.getColumnIndex(FormJsonEntry.KEY_ID));
        cursor.close();
        db.delete(FormJsonEntry.TABLE_FORM_JSON, FormJsonEntry.KEY_ID + " = ?", new String[] { String.valueOf(id) } );
    }
}
