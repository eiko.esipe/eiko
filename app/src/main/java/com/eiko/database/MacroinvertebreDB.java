package com.eiko.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.eiko.DataObject.MacroInvertebre;
import com.eiko.geolocalization.Position;
import com.eiko.database.contracts.MacroinvertebreContract;
import com.eiko.database.contracts.MacroinvertebreContract.MacroinvertebreEntry;
import com.eiko.database.contracts.PositionContract;
import com.eiko.database.contracts.StationContract;
import com.eiko.util.DateFormat;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created by axelheine on 12/02/2018.
 */

public class MacroinvertebreDB {
    private Database dbInstance;
    private SQLiteDatabase db;
    private Context context;

    public MacroinvertebreDB(Context context) {
        this.context = Objects.requireNonNull(context);
        this.dbInstance = Database.getInstance(context);
        this.db = dbInstance.getWritableDatabase();
    }

    public void close() {
        db.close();
    }

    /**
     * Insert new entry of MacroInvertebre class in database.
     *
     * @param macroInvertebre MacroInvertebre object to insert in db
     * @return the id of inserted row in database
     */
    public long insert(MacroInvertebre macroInvertebre) {
        return insert(macroInvertebre, "");
    }

    /**
     * Insert new entry of MacroInvertebre class in database with park name.
     *
     * @param macroInvertebre MacroInvertebre object to insert in db
     * @param parkName park name to save
     * @return the id of inserted row in database
     */
    public long insert(MacroInvertebre macroInvertebre, String parkName) {
        StationDB stationDB = new StationDB(context);
        long stationID = stationDB.insert(macroInvertebre.getGeom(), macroInvertebre.getStationCode());
        long macroInvertebreID;

        // On essaye d'abord de select les donnees pour voir si l'element n'est pas deja insere
        Cursor c = db.query(
                MacroinvertebreEntry.TABLE_MACTOINVERTEBRE,
                new String[]{MacroinvertebreEntry.KEY_ID},
                MacroinvertebreEntry.KEY_TAXON + " = ? AND " + MacroinvertebreEntry.KEY_DATE_SORTIE + " = ? AND " + MacroinvertebreEntry.KEY_TOTAL_IND + " = ? AND " + MacroinvertebreEntry.KEY_STATION_FK + " = ? AND " + MacroinvertebreEntry.KEY_PARK_FK + " = ?",
                new String[]{macroInvertebre.getTaxon(), DateFormat.dateToString(macroInvertebre.getDateDeSortie(), "d-M-y"), String.valueOf(macroInvertebre.getTotalIndividu()), String.valueOf(stationID), parkName},
                null, null, null
        );

        // si il existe deja les donnees, on recupere l'id
        if (c.moveToFirst() && c.getCount() > 0) {
            macroInvertebreID = c.getLong(c.getColumnIndex(MacroinvertebreEntry.KEY_ID));
        } else {
            ContentValues values = new ContentValues();
            values.put(MacroinvertebreEntry.KEY_TAXON, macroInvertebre.getTaxon());
            values.put(MacroinvertebreEntry.KEY_DATE_SORTIE, DateFormat.dateToString(macroInvertebre.getDateDeSortie(), "d-M-y"));
            values.put(MacroinvertebreEntry.KEY_TOTAL_IND, macroInvertebre.getTotalIndividu());
            values.put(MacroinvertebreEntry.KEY_STATION_FK, stationID);
            values.put(MacroinvertebreEntry.KEY_PARK_FK, parkName);

            macroInvertebreID = db.insert(MacroinvertebreEntry.TABLE_MACTOINVERTEBRE, null, values);
        }
        c.close();
        return macroInvertebreID;
    }

    /**
     * Select one object from his ID
     * @param id requested ID
     * @return object corresponding to the ID
     */
    public MacroInvertebre select(long id) {
        String selectFromId = "SELECT * FROM " + MacroinvertebreEntry.TABLE_MACTOINVERTEBRE + ", "
                + StationContract.StationEntry.TABLE_STATION + ", "
                + PositionContract.PositionEntry.TABLE_POSITION
                + " WHERE " + MacroinvertebreEntry.TABLE_MACTOINVERTEBRE + "." + MacroinvertebreEntry.KEY_STATION_FK
                + " = " + StationContract.StationEntry.TABLE_STATION + "." + StationContract.StationEntry.KEY_ID
                + " AND " + StationContract.StationEntry.TABLE_STATION + "." + StationContract.StationEntry.KEY_POSITION_FK
                + " = " + PositionContract.PositionEntry.TABLE_POSITION + "." + PositionContract.PositionEntry.KEY_POSITION_ID
                + " AND " + MacroinvertebreEntry.TABLE_MACTOINVERTEBRE + "." + MacroinvertebreEntry.KEY_ID + " = " + String.valueOf(id);

        Cursor cursor = db.rawQuery(selectFromId, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            return createMacroInvertebreFromCursor(cursor);
        }

        cursor.close();
        return null;
    }

    /**
     * Select all macroinvertebre between renderDistance and ringDistance from the point in parameter
     * @param searchPosition position of search
     * @param renderDistance search distance
     * @param ringDistance size of the empty zone before getting data
     * @return return a list of macroinvertebre objects at requested distance
     * @throws IllegalArgumentException if the render distance is smaller than the ring distance
     */
    public List<MacroInvertebre> getAllMacroInvertebreAtDistance(Position searchPosition, int renderDistance, int ringDistance) throws IllegalArgumentException {
        if(renderDistance < ringDistance) {
            throw new IllegalArgumentException("ring cannot be larger than render distance");
        }

        //Liste pour le retour des objets
        List<MacroInvertebre> macroList = new ArrayList<>();

        // Select All Query
        Cursor cursor = db.rawQuery(MacroinvertebreContract.SELECT_MACROINVERTEBRE_STATION_POSITION, null);

        //Si le curseur est vide, pas de résultat
        if(cursor.getCount() == 0) {
            return macroList;
        }

        // looping through all rows and adding to list if they are in render distance
        if (cursor.moveToFirst()) {
            do {
                double objectLongitude =  Double.parseDouble(cursor.getString(cursor.getColumnIndex(PositionContract.PositionEntry.KEY_POSITION_LON)));
                double objectLatitude =  Double.parseDouble(cursor.getString(cursor.getColumnIndex(PositionContract.PositionEntry.KEY_POSITION_LAT)));

                // create position of the db object and calculates distance from user position
                Position np = new Position(objectLatitude, objectLongitude);
                float d = searchPosition.getDistanceTo(np);

                // if object is too far, continue
                if (d > renderDistance || d < ringDistance) {
                    continue;
                }

                // Adding to list
                macroList.add(createMacroInvertebreFromCursor(cursor));
            } while (cursor.moveToNext());
        }

        cursor.close();
        return macroList;
    }

    /**
     * Select all macroinvertebres at a certain distance from the point in parameter
     * @param searchPosition position of search
     * @param renderDistance search distance
     * @return return a list of macroinvertebre objects at requested distance
     */
    public List<MacroInvertebre> getAllMacroInvertebreAtDistance(Position searchPosition, int renderDistance) {
        return getAllMacroInvertebreAtDistance(searchPosition, renderDistance, 0);
    }


    private MacroInvertebre createMacroInvertebreFromCursor(Cursor cursor) {
        Map<String, String> paramsMap = new HashMap<>();
        paramsMap.put("taxon",          cursor.getString(cursor.getColumnIndex(MacroinvertebreEntry.KEY_TAXON)));
        paramsMap.put("date_de_sortie", cursor.getString(cursor.getColumnIndex(MacroinvertebreEntry.KEY_DATE_SORTIE)));
        paramsMap.put("total_ind",      cursor.getString(cursor.getColumnIndex(MacroinvertebreEntry.KEY_TOTAL_IND)));
        paramsMap.put("station_code",   cursor.getString(cursor.getColumnIndex(StationContract.StationEntry.KEY_NAME)));
        paramsMap.put("longitude",      cursor.getString(cursor.getColumnIndex(PositionContract.PositionEntry.KEY_POSITION_LON)));
        paramsMap.put("latitude",       cursor.getString(cursor.getColumnIndex(PositionContract.PositionEntry.KEY_POSITION_LAT)));

        return new MacroInvertebre(paramsMap);
    }
}
