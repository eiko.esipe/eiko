package com.eiko.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.eiko.geolocalization.Position;
import com.eiko.database.contracts.PositionContract.PositionEntry;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


/**
 * Created by axelheine on 08/02/2018.
 */

public class PositionDB {
    private Database dbInstance;
    private SQLiteDatabase db;

    public PositionDB(Context context) {
        Objects.requireNonNull(context);
        this.dbInstance = Database.getInstance(context);
        this.db = dbInstance.getWritableDatabase();
    }

    /**
     * Close the SQLiteDatabase instance
     */
    public void close() {
        db.close();
    }

    /**
     * Insert new entry of Position class in database.
     *
     * @param position position object to insert in db
     * @return the id of inserted row in database
     */
    public long insert(Position position) {
        return insert(position, "");
    }

    /**
     * Insert new entry of Position class in database.
     *
     * @param position position object to insert in db
     * @param parkName park name to save
     * @return the id of inserted row in database
     */
    public long insert(Position position, String parkName) {
        long positionID;
        Cursor c = db.query(
                PositionEntry.TABLE_POSITION,
                new String[] {PositionEntry.KEY_POSITION_ID},
                PositionEntry.KEY_POSITION_LAT + " = ? AND " + PositionEntry.KEY_POSITION_LON + " = ? AND " + PositionEntry.KEY_ALTITUDE + " = ? AND " + PositionEntry.KEY_PARK_FK + " = ?",
                new String[] {String.valueOf(position.getLatitude()), String.valueOf(position.getLongitude()), String.valueOf(position.getAltitude()), parkName},
                null, null, null
        );

        if (c.getCount() > 0) {
            c.moveToFirst();
            positionID = c.getLong(c.getColumnIndex(PositionEntry.KEY_POSITION_ID));
        } else {
            ContentValues values = new ContentValues();
            values.put(PositionEntry.KEY_POSITION_LAT, position.getLatitude());
            values.put(PositionEntry.KEY_POSITION_LON, position.getLongitude());
            values.put(PositionEntry.KEY_ALTITUDE, position.getAltitude());
            values.put(PositionEntry.KEY_PARK_FK, parkName);

            positionID = db.insert(PositionEntry.TABLE_POSITION, null, values);
        }

        c.close();
        return positionID;
    }

    /**
     * Select one object from his ID
     * @param id requested ID
     * @return object corresponding to the ID
     */
    public Position select(long id) {
        Position position = null;
        Cursor cursor = db.query(PositionEntry.TABLE_POSITION,
                new String[] { PositionEntry.KEY_POSITION_ID, PositionEntry.KEY_POSITION_LAT, PositionEntry.KEY_POSITION_LON, PositionEntry.KEY_ALTITUDE },
                PositionEntry.KEY_POSITION_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);

        if (cursor.getCount() != 0) {
            cursor.moveToFirst();
            position = createPositionFromCursor(cursor);
        }

        cursor.close();
        return position;
    }

    public List<Position> selectAll() {
        List<Position> positionList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT * FROM " + PositionEntry.TABLE_POSITION ;

        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                // Adding position to list
                positionList.add(createPositionFromCursor(cursor));
            } while (cursor.moveToNext());
        }

        cursor.close();
        // return position to list
        return positionList;
    }

    private Position createPositionFromCursor(Cursor cursor) {
        return new Position(
                cursor.getDouble(cursor.getColumnIndex(PositionEntry.KEY_POSITION_LAT)),
                cursor.getDouble(cursor.getColumnIndex(PositionEntry.KEY_POSITION_LON)),
                cursor.getDouble(cursor.getColumnIndex(PositionEntry.KEY_ALTITUDE))
        );
    }
}
