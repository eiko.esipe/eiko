package com.eiko.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.eiko.DataObject.HydrobiologiePar;
import com.eiko.geolocalization.Position;
import com.eiko.database.contracts.HydrobiologieParContract;
import com.eiko.database.contracts.HydrobiologieParContract.HydrobiologieParEntry;
import com.eiko.database.contracts.PositionContract;
import com.eiko.database.contracts.StationContract;
import com.eiko.util.DateFormat;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Created by axelheine on 12/02/2018.
 */

public class HydrobiologieParDB {
    private Database dbInstance;
    private SQLiteDatabase db;
    private Context context;

    public HydrobiologieParDB(Context context) {
        this.context = Objects.requireNonNull(context);
        this.dbInstance = Database.getInstance(context);
        this.db = dbInstance.getWritableDatabase();
    }

    public void close() {
        db.close();
    }

    /**
     * Insert new entry of Hydrobiologie object in database.
     *
     * @param hydrobiologiePar hydrobiologiePar object to insert in db
     * @return the id of inserted row in database
     */
    public long insert(HydrobiologiePar hydrobiologiePar) {
        return insert(hydrobiologiePar, "");
    }

    /**
     * Insert new entry of Hydrobiologie object in database.
     *
     * @param hydrobiologiePar hydrobiologiePar object to insert in db
     * @param parkName park name to save
     * @return the id of inserted row in database
     */
    public long insert(HydrobiologiePar hydrobiologiePar, String parkName) {
        StationDB stationDB = new StationDB(context);
        long stationID = stationDB.insert(hydrobiologiePar.getGeom(), hydrobiologiePar.getNom_station());
        long hydrobiologieID;

        // On essaye d'abord de select les donnees pour voir si l'element n'est pas deja insere
        Cursor c = db.query(
                HydrobiologieParEntry.TABLE_HYDROBIOLOGIE_PAR,
                new String[] {HydrobiologieParEntry.KEY_ID},
                HydrobiologieParEntry.KEY_PARAMETRE + " = ? AND " + HydrobiologieParEntry.KEY_RESULTAT + " = ? AND " + HydrobiologieParEntry.KEY_DATE_PROD_RESULTAT + " = ? AND " + HydrobiologieParEntry.KEY_PRELEVEUR + " = ? AND " + HydrobiologieParEntry.KEY_STATION_FK + " = ? AND " + HydrobiologieParEntry.KEY_PARK_FK + " = ? ",
                new String[] {hydrobiologiePar.getParametre(), hydrobiologiePar.getResultat(), DateFormat.dateToString(hydrobiologiePar.getDtprodresultatbiologique(), "y-M-d"), hydrobiologiePar.getPreleveur(), String.valueOf(stationID), parkName},
                null, null, null
        );

        // si il existe deja les donnees, on recupere l'id
        if (c.moveToFirst() && c.getCount() > 0) {
            hydrobiologieID = c.getLong(c.getColumnIndex(HydrobiologieParEntry.KEY_ID));
        } else {
            ContentValues values = new ContentValues();
            values.put(HydrobiologieParEntry.KEY_PARAMETRE, hydrobiologiePar.getParametre());
            values.put(HydrobiologieParEntry.KEY_RESULTAT, hydrobiologiePar.getResultat());
            values.put(HydrobiologieParEntry.KEY_DATE_PROD_RESULTAT, DateFormat.dateToString(hydrobiologiePar.getDtprodresultatbiologique(), "y-M-d"));
            values.put(HydrobiologieParEntry.KEY_PRELEVEUR, hydrobiologiePar.getPreleveur());
            values.put(HydrobiologieParEntry.KEY_STATION_FK, stationID);
            values.put(HydrobiologieParEntry.KEY_PARK_FK, parkName);

            hydrobiologieID = db.insert(HydrobiologieParEntry.TABLE_HYDROBIOLOGIE_PAR, null, values);
        }
        c.close();
        return hydrobiologieID;
    }

    /**
     * Select one object from his ID
     * @param id requested ID
     * @return object corresponding to the ID
     */
    public HydrobiologiePar select(long id) {
        String selectFromId = "SELECT * FROM " + HydrobiologieParEntry.TABLE_HYDROBIOLOGIE_PAR + ", "
                + StationContract.StationEntry.TABLE_STATION + ", "
                + PositionContract.PositionEntry.TABLE_POSITION
                + " WHERE " + HydrobiologieParEntry.TABLE_HYDROBIOLOGIE_PAR + "." + HydrobiologieParEntry.KEY_STATION_FK
                + " = " + StationContract.StationEntry.TABLE_STATION + "." + StationContract.StationEntry.KEY_ID
                + " AND " + StationContract.StationEntry.TABLE_STATION + "." + StationContract.StationEntry.KEY_POSITION_FK
                + " = " + PositionContract.PositionEntry.TABLE_POSITION + "." + PositionContract.PositionEntry.KEY_POSITION_ID
                + " AND " + HydrobiologieParEntry.TABLE_HYDROBIOLOGIE_PAR + "." + HydrobiologieParEntry.KEY_ID + " = " + String.valueOf(id);

        Cursor cursor = db.rawQuery(selectFromId, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            return createHydrobilogieTaxFromCursor(cursor);
        }

        cursor.close();
        return null;
    }

    /**
     * Select all hydrobiologiePar between renderDistance and ringDistance from the point in parameter
     * @param searchPosition position of search
     * @param renderDistance search distance
     * @param ringDistance size of the empty zone before getting data
     * @return return a list of hydrobiologiePar objects at requested distance
     * @throws IllegalArgumentException if the render distance is smaller than the ring distance
     */
    public List<HydrobiologiePar> getAllHydrobiologieAtDistance(Position searchPosition, int renderDistance, int ringDistance) throws IllegalArgumentException {
        if(renderDistance < ringDistance) {
            throw new IllegalArgumentException("ring cannot be larger than render distance");
        }

        // list for store and return matching objects
        List<HydrobiologiePar> macroList = new ArrayList<>();

        // Select All Query
        Cursor cursor = db.rawQuery(HydrobiologieParContract.SELECT_HYDROBIOLOGIEPAR_STATION_POSITION, null);

        //if cursor is empty, return an empty list
        if(cursor.getCount() == 0) {
            return macroList;
        }

        // looping through all rows and adding to list if they are in render distance
        if (cursor.moveToFirst()) {
            do {
                double objectLongitude =  Double.parseDouble(cursor.getString(cursor.getColumnIndex(PositionContract.PositionEntry.KEY_POSITION_LON)));
                double objectLatitude =  Double.parseDouble(cursor.getString(cursor.getColumnIndex(PositionContract.PositionEntry.KEY_POSITION_LAT)));

                // create position of the db object and calculates distance from user position
                Position np = new Position(objectLatitude, objectLongitude);
                float d = searchPosition.getDistanceTo(np);

                // if object is too far, continue
                if (d > renderDistance || d < ringDistance) {
                    continue;
                }

                // Adding to list
                macroList.add(createHydrobilogieTaxFromCursor(cursor));
            } while (cursor.moveToNext());
        }

        cursor.close();
        return macroList;
    }

    /**
     * Select all hydrobiologiePar at a certain distance from the point in parameter
     * @param searchPosition position of search
     * @param renderDistance search distance
     * @return return a list of hydrobiologiePar objects at requested distance
     */
    public List<HydrobiologiePar> getAllHydrobiologieAtDistance(Position searchPosition, int renderDistance) {
        return getAllHydrobiologieAtDistance(searchPosition, renderDistance, 0);
    }

    public HydrobiologiePar createHydrobilogieTaxFromCursor(Cursor cursor) {
        Map<String, String> paramsMap = new HashMap<>();
        paramsMap.put("parametre",                  cursor.getString(cursor.getColumnIndex(HydrobiologieParEntry.KEY_PARAMETRE)));
        paramsMap.put("resultat",                   cursor.getString(cursor.getColumnIndex(HydrobiologieParEntry.KEY_RESULTAT)));
        paramsMap.put("dtprodresultatbiologique",   cursor.getString(cursor.getColumnIndex(HydrobiologieParEntry.KEY_DATE_PROD_RESULTAT)));
        paramsMap.put("preleveur",                  cursor.getString(cursor.getColumnIndex(HydrobiologieParEntry.KEY_PRELEVEUR)));
        paramsMap.put("nom_station",                cursor.getString(cursor.getColumnIndex(StationContract.StationEntry.KEY_NAME)));
        paramsMap.put("longitude",                  cursor.getString(cursor.getColumnIndex(PositionContract.PositionEntry.KEY_POSITION_LON)));
        paramsMap.put("latitude",                   cursor.getString(cursor.getColumnIndex(PositionContract.PositionEntry.KEY_POSITION_LAT)));

        return new HydrobiologiePar(paramsMap);
    }
}
