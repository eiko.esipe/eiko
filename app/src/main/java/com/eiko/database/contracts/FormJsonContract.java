package com.eiko.database.contracts;

import android.provider.BaseColumns;

/**
 * Created by axelheine on 08/02/2018.
 */

public class FormJsonContract {
    private FormJsonContract() {}

    public static class FormJsonEntry implements BaseColumns {
        public static final String TABLE_FORM_JSON = "form_json";
        public static final String KEY_ID = "ID";
        public static final String KEY_JSON = "json";
    }

    public final static String CREATE_FORM_JSON_TABLE = "CREATE TABLE " + FormJsonEntry.TABLE_FORM_JSON + " ("
            + FormJsonEntry.KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + FormJsonEntry.KEY_JSON + " TEXT NOT NULL"
            + ");";

    public final static String DELETE_FORM_JSON_TABLE = "DROP TABLE IF EXISTS " + FormJsonEntry.TABLE_FORM_JSON + ";";
}
