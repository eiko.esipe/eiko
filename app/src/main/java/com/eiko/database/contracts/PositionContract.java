package com.eiko.database.contracts;

import android.provider.BaseColumns;

import com.eiko.geolocalization.Position;

/**
 * Created by axelheine on 08/02/2018.
 */

public final class PositionContract {
    private PositionContract() {}

    public static class PositionEntry implements BaseColumns {
        public static final String TABLE_POSITION = "position";
        public static final String KEY_POSITION_ID = "ID";
        public static final String KEY_POSITION_LON = "longitude";
        public static final String KEY_POSITION_LAT = "latitude";
        public static final String KEY_ALTITUDE = "altitude";
        public static final String KEY_PARK_FK = "park_fk";
    }

    public final static String CREATE_POSITION_TABLE = "CREATE TABLE " + PositionEntry.TABLE_POSITION + " ("
            + PositionEntry.KEY_POSITION_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + PositionEntry.KEY_POSITION_LON + " TEXT NOT NULL, "
            + PositionEntry.KEY_POSITION_LAT + " TEXT NOT NULL, "
            + PositionEntry.KEY_ALTITUDE + " TEXT NOT NULL, "
            + PositionEntry.KEY_PARK_FK + " TEXT"
            + ");";

    public final static String DELETE_POSITION_TABLE = "DROP TABLE IF EXISTS " + PositionEntry.TABLE_POSITION + ";";
}

