package com.eiko.database.contracts;

import android.provider.BaseColumns;

/**
 * Created by axelheine on 08/02/2018.
 */

public class UserParamContract {
    private UserParamContract() {}

    public static class UserParamEntry implements BaseColumns {
        public static final String TABLE_USER = "user";
        public static final String KEY_ID = "ID";
        public static final String KEY_MAIL = "mail";
        public static final String KEY_RENDER_DISTANCE = "render_distance";
        public static final String KEY_HYDROBIOLOGIE = "hydrobiologie";
        public static final String KEY_FLORE = "flore";
        public static final String KEY_BIOLOGIE = "biologie";
        public static final String KEY_PHYSICOCHIMIE = "physicochimie";
    }

    public final static String CREATE_USER_PARAM_TABLE = "CREATE TABLE " + UserParamEntry.TABLE_USER + " ("
            + UserParamEntry.KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + UserParamEntry.KEY_MAIL + " TEXT, "
            + UserParamEntry.KEY_RENDER_DISTANCE + " INTEGER NOT NULL, "
            + UserParamEntry.KEY_HYDROBIOLOGIE + " INTEGER NOT NULL, "
            + UserParamEntry.KEY_FLORE + " INTEGER NOT NULL, "
            + UserParamEntry.KEY_BIOLOGIE + " INTEGER NOT NULL, "
            + UserParamEntry.KEY_PHYSICOCHIMIE + " INTEGER NOT NULL"
            + ");";

    public final static String DELETE_USER_PARAM_TABLE = "DROP TABLE IF EXISTS " + UserParamEntry.TABLE_USER + ";";

}
