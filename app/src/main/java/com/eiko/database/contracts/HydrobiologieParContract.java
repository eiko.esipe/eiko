package com.eiko.database.contracts;

import android.provider.BaseColumns;

/**
 * Created by axelheine on 14/02/2018.
 */

public class HydrobiologieParContract {
    private HydrobiologieParContract() {}

    public static class HydrobiologieParEntry implements BaseColumns {
        public static final String TABLE_HYDROBIOLOGIE_PAR = "hydrobiologie_par";
        public static final String KEY_ID = "ID";
        public static final String KEY_PARAMETRE = "parametre";
        public static final String KEY_RESULTAT = "resultat";
        public static final String KEY_DATE_PROD_RESULTAT = "date_prod_resultat";
        public static final String KEY_PRELEVEUR = "preleveur";
        public static final String KEY_STATION_FK = "station_fk";
        public static final String KEY_PARK_FK = "park_fk";
    }

    public final static String CREATE_HYDROBIOLOGIEPAR_TABLE = "CREATE TABLE " + HydrobiologieParEntry.TABLE_HYDROBIOLOGIE_PAR + " ("
            + HydrobiologieParEntry.KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + HydrobiologieParEntry.KEY_PARAMETRE + " TEXT NOT NULL, "
            + HydrobiologieParEntry.KEY_RESULTAT + " TEXT NOT NULL, "
            + HydrobiologieParEntry.KEY_DATE_PROD_RESULTAT + " TEXT NOT NULL, "
            + HydrobiologieParEntry.KEY_PRELEVEUR + " TEXT NOT NULL, "
            + HydrobiologieParEntry.KEY_STATION_FK + " INTEGER NOT NULL, "
            + HydrobiologieParEntry.KEY_PARK_FK + " TEXT"
            + ");";

    public final static  String SELECT_HYDROBIOLOGIEPAR_STATION_POSITION = "SELECT * FROM " + HydrobiologieParEntry.TABLE_HYDROBIOLOGIE_PAR + ", "
            + StationContract.StationEntry.TABLE_STATION + ", "
            + PositionContract.PositionEntry.TABLE_POSITION
            + " WHERE " + HydrobiologieParEntry.TABLE_HYDROBIOLOGIE_PAR + "." + HydrobiologieParEntry.KEY_STATION_FK
            + " = " + StationContract.StationEntry.TABLE_STATION + "." + StationContract.StationEntry.KEY_ID
            + " AND " + StationContract.StationEntry.TABLE_STATION + "." + StationContract.StationEntry.KEY_POSITION_FK
            + " = " + PositionContract.PositionEntry.TABLE_POSITION + "." + PositionContract.PositionEntry.KEY_POSITION_ID
            + ";";

    public final static String DELETE_HYDROBIOLOGIEPAR_TABLE = "DROP TABLE IF EXISTS " + HydrobiologieParEntry.TABLE_HYDROBIOLOGIE_PAR + ";";
}
