package com.eiko.database.contracts;

import android.provider.BaseColumns;

/**
 * Created by axelheine on 09/02/2018.
 */

public final class InvertebreContract {
    private InvertebreContract() {}

    public static class InvertebreEntry implements BaseColumns {
        public static final String TABLE_INVERTEBRE = "invertebre";
        public static final String KEY_ID = "ID";
        public static final String KEY_TAXON = "taxon";
        public static final String KEY_DATE_SORTIE = "date_sortie";
        public static final String KEY_TOTAL_IND = "total_ind";
        public static final String KEY_STATION_FK = "station_fk";
        public static final String KEY_PARK_FK = "park_fk";
    }

    public final static String CREATE_INVERTEBRE_TABLE = "CREATE TABLE " + InvertebreEntry.TABLE_INVERTEBRE + " ("
            + InvertebreEntry.KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + InvertebreEntry.KEY_TAXON + " TEXT NOT NULL, "
            + InvertebreEntry.KEY_DATE_SORTIE + " TEXT NOT NULL, "
            + InvertebreEntry.KEY_TOTAL_IND + " INTEGER NOT NULL, "
            + InvertebreEntry.KEY_STATION_FK + " INTEGER NOT NULL, "
            + InvertebreEntry.KEY_PARK_FK + " TEXT"
            + ");";

    public final static String SELECT_INVERTEBRE_STATION_POSITION = "SELECT * FROM " + InvertebreEntry.TABLE_INVERTEBRE + ", "
            + StationContract.StationEntry.TABLE_STATION + ", "
            + PositionContract.PositionEntry.TABLE_POSITION
            + " WHERE " + InvertebreEntry.TABLE_INVERTEBRE + "." + InvertebreEntry.KEY_STATION_FK
            + " = " + StationContract.StationEntry.TABLE_STATION + "." + StationContract.StationEntry.KEY_ID
            + " AND " + StationContract.StationEntry.TABLE_STATION + "." + StationContract.StationEntry.KEY_POSITION_FK
            + " = " + PositionContract.PositionEntry.TABLE_POSITION + "." + PositionContract.PositionEntry.KEY_POSITION_ID
            + ";";

    public final static String DELETE_INVERTEBRE_TABLE = "DROP TABLE IF EXISTS " + InvertebreEntry.TABLE_INVERTEBRE + ";";
}
