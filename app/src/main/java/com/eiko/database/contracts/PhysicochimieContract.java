package com.eiko.database.contracts;

import android.provider.BaseColumns;

import com.eiko.DataObject.Physicochimie;

/**
 * Created by axelheine on 08/02/2018.
 */

public final class PhysicochimieContract {
    private PhysicochimieContract() {}

    public static class PhysicochimieEntry implements BaseColumns {
        public static final String TABLE_PHYSICOCHIMIE = "physicochimie";
        public static final String KEY_ID = "ID";
        public static final String KEY_PARAMETRE = "parmetre";
        public static final String KEY_LSANA = "lsana";
        public static final String KEY_LDANA = "ldana";
        public static final String KEY_RSANA = "rsana";
        public static final String KEY_LABORATOIRE = "laboratoire";
        public static final String KEY_DATE_PREL = "date_prel";
        public static final String KEY_CODE_RMQ = "code_remarque";
        public static final String KEY_METHODE = "methode";
        public static final String KEY_PRODUCTEUR = "producteur";
        public static final String KEY_LQANA = "lqana";
        public static final String KEY_STATION_FK = "station_fk";
        public static final String KEY_PARK_FK = "park_fk";
    }

    public final static String CREATE_PHYSICOCHIMIE_TABLE = "CREATE TABLE " + PhysicochimieEntry.TABLE_PHYSICOCHIMIE + " ("
            + PhysicochimieEntry.KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + PhysicochimieEntry.KEY_PARAMETRE + " TEXT NOT NULL, "
            + PhysicochimieEntry.KEY_LSANA + " TEXT NOT NULL, "
            + PhysicochimieEntry.KEY_LDANA + " TEXT NOT NULL, "
            + PhysicochimieEntry.KEY_RSANA + " TEXT NOT NULL, "
            + PhysicochimieEntry.KEY_LABORATOIRE + " TEXT NOT NULL, "
            + PhysicochimieEntry.KEY_DATE_PREL + " TEXT NOT NULL, "
            + PhysicochimieEntry.KEY_CODE_RMQ + " TEXT NOT NULL, "
            + PhysicochimieEntry.KEY_METHODE + " TEXT NOT NULL, "
            + PhysicochimieEntry.KEY_PRODUCTEUR + " TEXT NOT NULL, "
            + PhysicochimieEntry.KEY_LQANA + " TEXT NOT NULL, "
            + PhysicochimieEntry.KEY_STATION_FK + " INTEGER NOT NULL, "
            + PhysicochimieEntry.KEY_PARK_FK + " TEXT"
            + ");";

    public final static String SELECT_PHYSICOCHIMIE_STATION_POSITION = "SELECT * FROM " + PhysicochimieEntry.TABLE_PHYSICOCHIMIE + ", "
            + StationContract.StationEntry.TABLE_STATION + ", "
            + PositionContract.PositionEntry.TABLE_POSITION
            + " WHERE " + PhysicochimieEntry.TABLE_PHYSICOCHIMIE + "." + PhysicochimieEntry.KEY_STATION_FK
            + " = " + StationContract.StationEntry.TABLE_STATION + "." + StationContract.StationEntry.KEY_ID
            + " AND " + StationContract.StationEntry.TABLE_STATION + "." + StationContract.StationEntry.KEY_POSITION_FK
            + " = " + PositionContract.PositionEntry.TABLE_POSITION + "." + PositionContract.PositionEntry.KEY_POSITION_ID
            + ";";

    public final static String DELETE_PHYSICOCHIMIE_TABLE = "DROP TABLE IF EXISTS " + PhysicochimieEntry.TABLE_PHYSICOCHIMIE + ";";
}
