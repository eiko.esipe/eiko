package com.eiko.database.contracts;

import android.provider.BaseColumns;

/**
 * Created by axelheine on 08/02/2018.
 */

public final class FloreContract {
    private FloreContract() {}

    public static class FloreEntry implements BaseColumns {
        public static final String TABLE_FLORE = "flore";
        public static final String KEY_ID = "ID";
        public static final String KEY_NAME_SP = "name_sp";
        public static final String KEY_DATE_DEBUT = "date_debut";
        public static final String KEY_DATE_FIN = "date_fin";
        public static final String KEY_OBSERVATION_RMQ = "observation_rmq";
        public static final String KEY_POSITION_FK = "position_fk";
        public static final String KEY_PARK_FK = "park_fk";
    }

    public final static String CREATE_FLORE_TABLE = "CREATE TABLE " + FloreEntry.TABLE_FLORE + " ("
            + FloreEntry.KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + FloreEntry.KEY_NAME_SP + " TEXT NOT NULL, "
            + FloreEntry.KEY_DATE_DEBUT + " TEXT NOT NULL, "
            + FloreEntry.KEY_DATE_FIN + " TEXT NOT NULL, "
            + FloreEntry.KEY_OBSERVATION_RMQ + " TEXT NOT NULL, "
            + FloreEntry.KEY_POSITION_FK + " INTEGER NOT NULL, "
            + FloreEntry.KEY_PARK_FK + " TEXT"
            + ");";

    public final static String SELECT_FLORE_STATION_POSITION = "SELECT * FROM " + FloreEntry.TABLE_FLORE + ", "
            + PositionContract.PositionEntry.TABLE_POSITION
            + " WHERE " + FloreEntry.TABLE_FLORE + "." + FloreEntry.KEY_POSITION_FK
            + " = " + PositionContract.PositionEntry.TABLE_POSITION + "." + PositionContract.PositionEntry.KEY_POSITION_ID
            + ";";

    public final static String DELETE_FLORE_TABLE = "DROP TABLE IF EXISTS " + FloreEntry.TABLE_FLORE + ";";
}
