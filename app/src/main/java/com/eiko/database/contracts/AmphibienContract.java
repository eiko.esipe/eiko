package com.eiko.database.contracts;

import android.provider.BaseColumns;

/**
 * Created by axelheine on 09/02/2018.
 */

public final class AmphibienContract {
    private AmphibienContract() {}

    public static class AmphibienEntry implements BaseColumns {
        public static final String TABLE_AMPHIBIEN = "amphibien";
        public static final String KEY_ID = "ID";
        public static final String KEY_TAXON = "taxon";
        public static final String KEY_DATE_SORTIE = "date_sortie";
        public static final String KEY_TOTAL_IND = "total_ind";
        public static final String KEY_STATION_FK = "station_fk";
        public static final String KEY_PARK_FK = "park_fk";
    }

    public final static String CREATE_AMPHIBIEN_TABLE = "CREATE TABLE " + AmphibienEntry.TABLE_AMPHIBIEN + " ("
            + AmphibienEntry.KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + AmphibienEntry.KEY_TAXON + " TEXT NOT NULL, "
            + AmphibienEntry.KEY_DATE_SORTIE + " TEXT NOT NULL, "
            + AmphibienEntry.KEY_TOTAL_IND + " INTEGER NOT NULL, "
            + AmphibienEntry.KEY_STATION_FK + " INTEGER NOT NULL, "
            + AmphibienEntry.KEY_PARK_FK + " TEXT"
            + ");";

    public final static String SELECT_AMPHIBIEN_STATION_POSITION = "SELECT * FROM " + AmphibienEntry.TABLE_AMPHIBIEN + ", "
            + StationContract.StationEntry.TABLE_STATION + ", "
            + PositionContract.PositionEntry.TABLE_POSITION
                + " WHERE " + AmphibienEntry.TABLE_AMPHIBIEN + "." + AmphibienEntry.KEY_STATION_FK
                + " = " + StationContract.StationEntry.TABLE_STATION + "." + StationContract.StationEntry.KEY_ID
                + " AND " + StationContract.StationEntry.TABLE_STATION + "." + StationContract.StationEntry.KEY_POSITION_FK
                + " = " + PositionContract.PositionEntry.TABLE_POSITION + "." + PositionContract.PositionEntry.KEY_POSITION_ID
                + ";";

    public final static String DELETE_AMPHIBIEN_TABLE = "DROP TABLE IF EXISTS " + AmphibienEntry.TABLE_AMPHIBIEN + ";";
}
