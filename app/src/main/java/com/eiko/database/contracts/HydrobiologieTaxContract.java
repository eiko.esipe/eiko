package com.eiko.database.contracts;

import android.provider.BaseColumns;

/**
 * Created by axelheine on 08/02/2018.
 */

public final class HydrobiologieTaxContract {
    private HydrobiologieTaxContract() {}

    public static class HydrobiologieTaxEntry implements BaseColumns {
        public static final String TABLE_HYDROBIOLOGIE_TAX = "hydrobiologie_tax";
        public static final String KEY_ID = "ID";
        public static final String KEY_TAXON = "taxon";
        public static final String KEY_RESULTAT = "resultat";
        public static final String KEY_DATE_PROD_RESULTAT = "date_prod_resultat";
        public static final String KEY_PRELEVEUR = "preleveur";
        public static final String KEY_STATION_FK = "station_fk";
        public static final String KEY_PARK_FK = "park_fk";
    }

    public final static String CREATE_HYDROBIOLOGIETAX_TABLE = "CREATE TABLE " + HydrobiologieTaxEntry.TABLE_HYDROBIOLOGIE_TAX + " ("
            + HydrobiologieTaxEntry.KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + HydrobiologieTaxEntry.KEY_TAXON + " TEXT NOT NULL, "
            + HydrobiologieTaxEntry.KEY_RESULTAT + " TEXT NOT NULL, "
            + HydrobiologieTaxEntry.KEY_DATE_PROD_RESULTAT + " TEXT NOT NULL, "
            + HydrobiologieTaxEntry.KEY_PRELEVEUR + " TEXT NOT NULL, "
            + HydrobiologieTaxEntry.KEY_STATION_FK + " INTEGER NOT NULL, "
            + HydrobiologieTaxEntry.KEY_PARK_FK + " TEXT"
            + ");";

    public final static  String SELECT_HYDROBIOLOGIETAX_STATION_POSITION = "SELECT * FROM " + HydrobiologieTaxEntry.TABLE_HYDROBIOLOGIE_TAX + ", "
            + StationContract.StationEntry.TABLE_STATION + ", "
            + PositionContract.PositionEntry.TABLE_POSITION
            + " WHERE " + HydrobiologieTaxEntry.TABLE_HYDROBIOLOGIE_TAX + "." + HydrobiologieTaxEntry.KEY_STATION_FK
            + " = " + StationContract.StationEntry.TABLE_STATION + "." + StationContract.StationEntry.KEY_ID
            + " AND " + StationContract.StationEntry.TABLE_STATION + "." + StationContract.StationEntry.KEY_POSITION_FK
            + " = " + PositionContract.PositionEntry.TABLE_POSITION + "." + PositionContract.PositionEntry.KEY_POSITION_ID
            + ";";

    public final static String DELETE_HYDROBIOLOGIETAX_TABLE = "DROP TABLE IF EXISTS " + HydrobiologieTaxEntry.TABLE_HYDROBIOLOGIE_TAX + ";";
}
