package com.eiko.database.contracts;

import android.provider.BaseColumns;

/**
 * Created by axelheine on 21/02/2018.
 */

public final class ParkContract {
    private ParkContract() {}

    public static class ParkEntry implements BaseColumns {
        public static final String TABLE_PARK = "park";
        public static final String KEY_ID = "ID";
        public static final String KEY_NAME = "name";
    }

    public final static String CREATE_PARK_TABLE = "CREATE TABLE " + ParkEntry.TABLE_PARK + " ("
            + ParkEntry.KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + ParkEntry.KEY_NAME + " TEXT NOT NULL"
            + ");";

    public final static String DELETE_PARK_TABLE = "DROP TABLE IF EXISTS " + ParkEntry.TABLE_PARK + ";";
}
