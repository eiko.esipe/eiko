package com.eiko.database.contracts;

import android.provider.BaseColumns;

/**
 * Created by axelheine on 08/02/2018.
 */

public final class StationContract {
    private StationContract() {}

    public static class StationEntry implements BaseColumns {
        public static final String TABLE_STATION = "station";
        public static final String KEY_ID = "ID";
        public static final String KEY_NAME = "name";
        public static final String KEY_POSITION_FK = "position_fk";
        public static final String KEY_PARK_FK = "park_fk";
    }

    public final static String CREATE_STATION_TABLE = "CREATE TABLE " + StationEntry.TABLE_STATION + " ("
            + StationEntry.KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + StationEntry.KEY_NAME + " TEXT NOT NULL, "
            + StationEntry.KEY_POSITION_FK + " INTEGER NOT NULL, "
            + StationEntry.KEY_PARK_FK + " TEXT"
            + ");";

    public final static String SELECT_STATION_POSITION = "SELECT * FROM " + StationEntry.TABLE_STATION + ", "
            + PositionContract.PositionEntry.TABLE_POSITION
            + " WHERE " + StationEntry.TABLE_STATION + "." + StationEntry.KEY_POSITION_FK
            + " = " + PositionContract.PositionEntry.TABLE_POSITION + "." + PositionContract.PositionEntry.KEY_POSITION_ID
            + ";";


    public final static String DELETE_STATION_TABLE = "DROP TABLE IF EXISTS " + StationEntry.TABLE_STATION + ";";
}
