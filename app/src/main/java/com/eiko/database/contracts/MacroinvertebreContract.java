package com.eiko.database.contracts;

import android.provider.BaseColumns;

/**
 * Created by axelheine on 09/02/2018.
 */

public final class MacroinvertebreContract {
    private MacroinvertebreContract() {}

    public static class MacroinvertebreEntry implements BaseColumns {
        public static final String TABLE_MACTOINVERTEBRE = "macroinvertebre";
        public static final String KEY_ID = "ID";
        public static final String KEY_TAXON = "taxon";
        public static final String KEY_DATE_SORTIE = "date_sortie";
        public static final String KEY_TOTAL_IND = "total_ind";
        public static final String KEY_STATION_FK = "station_fk";
        public static final String KEY_PARK_FK = "park_fk";
    }

    public final static String CREATE_MACROINVERTEBRE_TABLE = "CREATE TABLE " + MacroinvertebreEntry.TABLE_MACTOINVERTEBRE + " ("
            + MacroinvertebreEntry.KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + MacroinvertebreEntry.KEY_TAXON + " TEXT NOT NULL, "
            + MacroinvertebreEntry.KEY_DATE_SORTIE + " TEXT NOT NULL, "
            + MacroinvertebreEntry.KEY_TOTAL_IND + " INTEGER NOT NULL, "
            + MacroinvertebreEntry.KEY_STATION_FK + " INTEGER NOT NULL, "
            + MacroinvertebreEntry.KEY_PARK_FK + " TEXT"
            + ");";

    public final static String SELECT_MACROINVERTEBRE_STATION_POSITION = "SELECT * FROM " + MacroinvertebreEntry.TABLE_MACTOINVERTEBRE + ", "
            + StationContract.StationEntry.TABLE_STATION + ", "
            + PositionContract.PositionEntry.TABLE_POSITION
            + " WHERE " + MacroinvertebreEntry.TABLE_MACTOINVERTEBRE + "." + MacroinvertebreEntry.KEY_STATION_FK
            + " = " + StationContract.StationEntry.TABLE_STATION + "." + StationContract.StationEntry.KEY_ID
            + " AND " + StationContract.StationEntry.TABLE_STATION + "." + StationContract.StationEntry.KEY_POSITION_FK
            + " = " + PositionContract.PositionEntry.TABLE_POSITION + "." + PositionContract.PositionEntry.KEY_POSITION_ID
            + ";";

    public final static String DELETE_MACROINVERTEBRE_TABLE = "DROP TABLE IF EXISTS " + MacroinvertebreEntry.TABLE_MACTOINVERTEBRE + ";";
}
