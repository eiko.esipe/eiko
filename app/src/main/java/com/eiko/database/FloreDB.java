package com.eiko.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.eiko.DataObject.Flore;
import com.eiko.geolocalization.Position;
import com.eiko.database.contracts.FloreContract;
import com.eiko.database.contracts.FloreContract.FloreEntry;
import com.eiko.database.contracts.PositionContract;
import com.eiko.util.DateFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created by axelheine on 12/02/2018.
 */

public class FloreDB {
    private Database dbInstance;
    private SQLiteDatabase db;
    private Context context;

    public FloreDB(Context context) {
        this.context = Objects.requireNonNull(context);
        this.dbInstance = Database.getInstance(context);
        this.db = dbInstance.getWritableDatabase();
    }

    public void close() {
        db.close();
    }

    /**
     * Insert new entry of Flore object in database.
     *
     * @param flore amphibien object to insert in db
     * @return the id of inserted row in database
     */
    public long insert(Flore flore) {
        return insert(flore, "");
    }

    /**
     * Insert new entry of Flore object in database.
     *
     * @param flore amphibien object to insert in db
     * @param parkName park name to save
     * @return the id of inserted row in database
     */
    public long insert(Flore flore, String parkName) {
        PositionDB positionDB = new PositionDB(context);
        long positionID = positionDB.insert(flore.getGeom());
        long floreID;

        String formattedNames = formatNames(flore.getNames_sp());

        // On essaye d'abord de select les donnees pour voir si l'element n'est pas deja insere
        Cursor c = db.query(
                FloreEntry.TABLE_FLORE,
                new String[] {FloreEntry.KEY_ID},
                FloreEntry.KEY_NAME_SP + " = ? AND " + FloreEntry.KEY_DATE_DEBUT + " = ? AND " + FloreEntry.KEY_DATE_FIN + " = ? AND " + FloreEntry.KEY_OBSERVATION_RMQ + " = ? AND " + FloreEntry.KEY_POSITION_FK + " = ? AND " + FloreEntry.KEY_PARK_FK + " = ?",
                new String[] {formattedNames, DateFormat.dateToString(flore.getDate_debut(), "y-M-d"), DateFormat.dateToString(flore.getDate_fin(), "y-M-d"), flore.getObservation_rmq(), String.valueOf(positionID), parkName},
                null, null, null
        );

        // si il existe deja les donnees, on recupere l'id
        if (c.moveToFirst() && c.getCount() > 0) {
            floreID = c.getLong(c.getColumnIndex(FloreEntry.KEY_ID));
        } else {
            ContentValues values = new ContentValues();
            values.put(FloreEntry.KEY_NAME_SP, formattedNames);
            values.put(FloreEntry.KEY_DATE_DEBUT, DateFormat.dateToString(flore.getDate_debut(), "y-M-d"));
            values.put(FloreEntry.KEY_DATE_FIN, DateFormat.dateToString(flore.getDate_fin(), "y-M-d"));
            values.put(FloreEntry.KEY_OBSERVATION_RMQ, flore.getObservation_rmq());
            values.put(FloreEntry.KEY_POSITION_FK, positionID);
            values.put(FloreEntry.KEY_PARK_FK, parkName);

            floreID = db.insert(FloreEntry.TABLE_FLORE, null, values);
        }
        c.close();
        return floreID;
    }

    /**
     * Select one object from his ID
     * @param id requested ID
     * @return object corresponding to the ID
     */
    public Flore select(long id) {
        String selectFromId = "SELECT * FROM " + FloreEntry.TABLE_FLORE + ", "
                + PositionContract.PositionEntry.TABLE_POSITION
                + " WHERE " + FloreEntry.TABLE_FLORE + "." + FloreEntry.KEY_POSITION_FK
                + " = " + PositionContract.PositionEntry.TABLE_POSITION + "." + PositionContract.PositionEntry.KEY_POSITION_ID
                + " AND " + FloreEntry.TABLE_FLORE + "." + FloreEntry.KEY_ID + " = " + String.valueOf(id);

        Cursor cursor = db.rawQuery(selectFromId, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            return createFloreFromCursor(cursor);
        }

        cursor.close();
        return null;
    }

    /**
     * Select all flore between renderDistance and ringDistance from the point in parameter
     * @param searchPosition position of search
     * @param renderDistance search distance
     * @param ringDistance size of the empty zone before getting data
     * @return return a list of flore objects at requested distance
     * @throws IllegalArgumentException if the render distance is smaller than the ring distance
     */
    public List<Flore> getAllFloreAtDistance(Position searchPosition, int renderDistance, int ringDistance) throws IllegalArgumentException {
        if(renderDistance < ringDistance) {
            throw new IllegalArgumentException("ring cannot be larger than render distance");
        }

        //Liste pour le retour des objets
        List<Flore> macroList = new ArrayList<>();

        // Select All Query
        Cursor cursor = db.rawQuery(FloreContract.SELECT_FLORE_STATION_POSITION, null);

        //Si le curseur est vide, pas de résultat
        if(cursor.getCount() == 0) {
            cursor.close();
            return macroList;
        }

        // loop through all rows and adding to list if they are in render distance
        if (cursor.moveToFirst()) {
            do {
                double objectLongitude =  Double.parseDouble(cursor.getString(cursor.getColumnIndex(PositionContract.PositionEntry.KEY_POSITION_LON)));
                double objectLatitude =  Double.parseDouble(cursor.getString(cursor.getColumnIndex(PositionContract.PositionEntry.KEY_POSITION_LAT)));

                // create position of the db object and calculates distance from user position
                Position np = new Position(objectLatitude, objectLongitude);
                float d = searchPosition.getDistanceTo(np);

                // if object is too far, continue
                if (d > renderDistance || d < ringDistance) {
                    continue;
                }

                // Adding to list
                macroList.add(createFloreFromCursor(cursor));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return macroList;
    }

    /**
     * Select all Flore at a certain distance from the point in parameter
     * @param searchPosition position of search
     * @param renderDistance search distance
     * @return return a list of Flore objects at requested distance
     */
    public List<Flore> getAllFloreAtDistance(Position searchPosition, int renderDistance) {
        return getAllFloreAtDistance(searchPosition, renderDistance, 0);
    }

    /**
     * Creates a Flore object from the content of a cursor
     * @param cursor SQLiteDatabase cursor containing datas of an HydrobiologieTax object
     * @return the created object
     */
    private Flore createFloreFromCursor(Cursor cursor) {
        Map<String, String> paramsMap = new HashMap<>();
        Map<String, List<String>> paramsNameMap = new HashMap<>();
        paramsMap.put("date_debut",     cursor.getString(cursor.getColumnIndex(FloreEntry.KEY_DATE_DEBUT)));
        paramsMap.put("date_fin",       cursor.getString(cursor.getColumnIndex(FloreEntry.KEY_DATE_FIN)));
        paramsMap.put("observation_rmq",cursor.getString(cursor.getColumnIndex(FloreEntry.KEY_OBSERVATION_RMQ)));
        paramsMap.put("longitude",      cursor.getString(cursor.getColumnIndex(PositionContract.PositionEntry.KEY_POSITION_LON)));
        paramsMap.put("latitude",       cursor.getString(cursor.getColumnIndex(PositionContract.PositionEntry.KEY_POSITION_LAT)));

        // createlist of names from formatted field content ("name", "name2"...)
        String names = cursor.getString(cursor.getColumnIndex(FloreEntry.KEY_NAME_SP));
        List<String> listNames = new ArrayList<>(Arrays.asList(names.substring(1, names.length() - 1).split("\", \""))); // cut at ", "

        paramsNameMap.put("names_sp", listNames);

        return new Flore(paramsMap, paramsNameMap);
    }

    /**
     * Create string format from list of names
     * @param names_sp list of flore especes names
     * @return string formatted as "name1", "name2"...
     */
    private String formatNames(List<String> names_sp) {
        StringBuilder sb = new StringBuilder("\"");
        for(String name: names_sp) {
            sb.append(name);
            sb.append("\", \"");
        }
        String str = sb.substring(0, sb.length() - 3);
        return str;
    }
}
