package com.eiko.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.eiko.DataObject.Amphibien;
import com.eiko.geolocalization.Position;
import com.eiko.database.contracts.AmphibienContract;
import com.eiko.database.contracts.AmphibienContract.AmphibienEntry;
import com.eiko.database.contracts.PositionContract;
import com.eiko.database.contracts.PositionContract.PositionEntry;
import com.eiko.database.contracts.StationContract;
import com.eiko.database.contracts.StationContract.StationEntry;
import com.eiko.util.DateFormat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created by axelheine on 09/02/2018.
 */

public class AmphibienDB {
    private Database dbInstance;
    private SQLiteDatabase db;
    private Context context;

    public AmphibienDB(Context context) {
        this.context = Objects.requireNonNull(context);
        this.dbInstance = Database.getInstance(context);
        this.db = dbInstance.getWritableDatabase();

    }

    //public void close() {
    //    db.close();
    //}

    /**
     * Insert new entry of Amphibien class in database.
     *
     * @param amphibien amphibien object to insert in db
     * @return the id of inserted row in database
     */
    public long insert(Amphibien amphibien) {
        return insert(amphibien, "");
    }

    /**
     * Insert amphibian in database with park info
     * @param amphibien amphibien object to insert in db
     * @param parkName park name to save
     * @return the id of inserted row in database
     */
    public long insert(Amphibien amphibien, String parkName) {
        StationDB stationDB = new StationDB(context);
        long stationID = stationDB.insert(amphibien.getGeom(), amphibien.getStationCode());
        long amphibienID;

        // On essaye d'abord de select les donnees pour voir si l'element n'est pas deja insere
        Cursor c = db.query(
                AmphibienEntry.TABLE_AMPHIBIEN,
                new String[] {AmphibienEntry.KEY_ID},
                AmphibienEntry.KEY_TAXON + " = ? AND " + AmphibienEntry.KEY_DATE_SORTIE + " = ? AND " + AmphibienEntry.KEY_TOTAL_IND + " = ? AND " + AmphibienEntry.KEY_STATION_FK + " = ? AND " + AmphibienEntry.KEY_PARK_FK + " = ?",
                new String[] {amphibien.getTaxon(), DateFormat.dateToString(amphibien.getDateDeSortie(), "d-M-y"), String.valueOf(amphibien.getTotalIndividu()), String.valueOf(stationID), parkName},
                null, null, null
        );

        // si il existe deja les donnees, on recupere l'id
        if (c.moveToFirst() && c.getCount() > 0) {
            amphibienID = c.getLong(c.getColumnIndex(AmphibienEntry.KEY_ID));
        } else {
            ContentValues values = new ContentValues();
            values.put(AmphibienEntry.KEY_TAXON, amphibien.getTaxon());
            values.put(AmphibienEntry.KEY_DATE_SORTIE, DateFormat.dateToString(amphibien.getDateDeSortie(), "d-M-y"));
            values.put(AmphibienEntry.KEY_TOTAL_IND, amphibien.getTotalIndividu());
            values.put(AmphibienEntry.KEY_STATION_FK, stationID);
            values.put(AmphibienEntry.KEY_PARK_FK, parkName);

            amphibienID = db.insert(AmphibienEntry.TABLE_AMPHIBIEN, null, values);
        }
        c.close();
        return amphibienID;
    }

    /**
     * Select an amphibian in database from his id. Returns an amphibian object.
     * @param id ID of the amphibian to select.
     * @return Amphibian coresponding to the passed ID if exists, null otherwise.
     */
    public Amphibien select(long id) {
        String selectFromId = "SELECT * FROM " + AmphibienEntry.TABLE_AMPHIBIEN + ", "
                + StationContract.StationEntry.TABLE_STATION + ", "
                + PositionContract.PositionEntry.TABLE_POSITION
                + " WHERE " + AmphibienEntry.TABLE_AMPHIBIEN + "." + AmphibienEntry.KEY_STATION_FK
                + " = " + StationContract.StationEntry.TABLE_STATION + "." + StationContract.StationEntry.KEY_ID
                + " AND " + StationContract.StationEntry.TABLE_STATION + "." + StationContract.StationEntry.KEY_POSITION_FK
                + " = " + PositionContract.PositionEntry.TABLE_POSITION + "." + PositionContract.PositionEntry.KEY_POSITION_ID
                + " AND " + AmphibienEntry.TABLE_AMPHIBIEN + "." + AmphibienEntry.KEY_ID + " = " + String.valueOf(id);

        Cursor cursor = db.rawQuery(selectFromId, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            return createAmphibienFromCursor(cursor);
        }
        cursor.close();
        return null;
    }

    /**
     * Select all amphibians between renderDistance and ringDistance from the point in parameter
     * @param searchPosition position of search
     * @param renderDistance search distance
     * @param ringDistance size of the empty zone before getting data
     * @return return a list of amphibian objects at requested distance
     * @throws IllegalArgumentException if the render distance is smaller than the ring distance
     */
    public List<Amphibien> getAllAmphibiensAtDistance(Position searchPosition, int renderDistance, int ringDistance) throws IllegalArgumentException {
        if(renderDistance < ringDistance) {
            throw new IllegalArgumentException("ring cannot be larger than render distance");
        }

        //Liste pour le retour des objets
        List<Amphibien> amphibList = new ArrayList<>();

        // Select All Query
        Cursor cursor = db.rawQuery(AmphibienContract.SELECT_AMPHIBIEN_STATION_POSITION, null);

        //Si le curseur est vide, pas de résultat
        if(cursor.getCount() == 0) {
            cursor.close();
            return amphibList;
        }

        // looping through all rows and adding to list if they are in render distance
        if (cursor.moveToFirst()) {
            do {
                double objectLongitude =  Double.parseDouble(cursor.getString(cursor.getColumnIndex(PositionEntry.KEY_POSITION_LON)));
                double objectLatitude =  Double.parseDouble(cursor.getString(cursor.getColumnIndex(PositionEntry.KEY_POSITION_LAT)));

                // if object is too far, continue
                Position np = new Position(objectLatitude, objectLongitude);
                float d = searchPosition.getDistanceTo(np);
                if (d > renderDistance || d < ringDistance) {
                    continue;
                }

                // Adding to list
                amphibList.add(createAmphibienFromCursor(cursor));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return amphibList;
    }

    /**
     * Select all amphibians at a certain distance from the point in parameter
     * @param searchPosition position of search
     * @param renderDistance search distance
     * @return return a list of amphibian objects at requested distance
     */
    public List<Amphibien> getAllAmphibiensAtDistance(Position searchPosition, int renderDistance) {
        return getAllAmphibiensAtDistance(searchPosition, renderDistance, 0);
    }

    /**
     * Creates an Amphibien object from the given Cursor.
     * @param cursor database result Cursor (given by an select request)
     * @return The amphibian parsed in the cursor.
     */
    private Amphibien createAmphibienFromCursor(Cursor cursor) {
        Map<String, String> paramsMap = new HashMap<>();
        paramsMap.put("taxon",          cursor.getString(cursor.getColumnIndex(AmphibienEntry.KEY_TAXON)));
        paramsMap.put("date_de_sortie", cursor.getString(cursor.getColumnIndex(AmphibienEntry.KEY_DATE_SORTIE)));
        paramsMap.put("total_ind",      cursor.getString(cursor.getColumnIndex(AmphibienEntry.KEY_TOTAL_IND)));
        paramsMap.put("station_code",   cursor.getString(cursor.getColumnIndex(StationEntry.KEY_NAME)));
        paramsMap.put("longitude",      cursor.getString(cursor.getColumnIndex(PositionEntry.KEY_POSITION_LON)));
        paramsMap.put("latitude",       cursor.getString(cursor.getColumnIndex(PositionEntry.KEY_POSITION_LAT)));
        paramsMap.put("parc",           cursor.getString(cursor.getColumnIndex(AmphibienEntry.KEY_PARK_FK)));

        return new Amphibien(paramsMap);
    }
}
