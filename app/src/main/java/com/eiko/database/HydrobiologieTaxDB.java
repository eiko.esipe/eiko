package com.eiko.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.eiko.DataObject.HydrobiologieTax;
import com.eiko.geolocalization.Position;
import com.eiko.database.contracts.HydrobiologieTaxContract;
import com.eiko.database.contracts.HydrobiologieTaxContract.HydrobiologieTaxEntry;
import com.eiko.database.contracts.PositionContract;
import com.eiko.database.contracts.StationContract;
import com.eiko.util.DateFormat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created by axelheine on 12/02/2018.
 */

public class HydrobiologieTaxDB {
    private Database dbInstance;
    private SQLiteDatabase db;
    private Context context;

    public HydrobiologieTaxDB(Context context) {
        this.context = Objects.requireNonNull(context);
        this.dbInstance = Database.getInstance(context);
        this.db = dbInstance.getWritableDatabase();
    }

    public void close() {
        db.close();
    }

    /**
     * Insert new entry of Hydrobiologie object in database.
     *
     * @param hydrobiologieTax hydrobiologieTax object to insert in db
     * @return the id of inserted row in database
     */
    public long insert(HydrobiologieTax hydrobiologieTax) {
        return insert(hydrobiologieTax, "");
    }

    /**
     * Insert new entry of Hydrobiologie object in database.
     *
     * @param hydrobiologieTax hydrobiologieTax object to insert in db
     * @param parkName park name to save
     * @return the id of inserted row in database
     */
    public long insert(HydrobiologieTax hydrobiologieTax, String parkName) {
        StationDB stationDB = new StationDB(context);
        long stationID = stationDB.insert(hydrobiologieTax.getGeom(), hydrobiologieTax.getNom_station());
        long hydrobiologieID;

        // On essaye d'abord de select les donnees pour voir si l'element n'est pas deja insere
        Cursor c = db.query(
                HydrobiologieTaxEntry.TABLE_HYDROBIOLOGIE_TAX,
                new String[] {HydrobiologieTaxEntry.KEY_ID},
                HydrobiologieTaxEntry.KEY_TAXON + " = ? AND " + HydrobiologieTaxEntry.KEY_RESULTAT + " = ? AND " + HydrobiologieTaxEntry.KEY_DATE_PROD_RESULTAT + " = ? AND " + HydrobiologieTaxEntry.KEY_PRELEVEUR + " = ? AND " + HydrobiologieTaxEntry.KEY_STATION_FK + " = ? AND " + HydrobiologieTaxEntry.KEY_PARK_FK + " = ?",
                new String[] {hydrobiologieTax.getTaxon(), hydrobiologieTax.getResultat(), DateFormat.dateToString(hydrobiologieTax.getDtprodresultatbiologique(), "y-M-d"), hydrobiologieTax.getPreleveur(), String.valueOf(stationID), parkName},
                null, null, null
        );

        // si il existe deja les donnees, on recupere l'id
        if (c.moveToFirst() && c.getCount() > 0) {
            hydrobiologieID = c.getLong(c.getColumnIndex(HydrobiologieTaxEntry.KEY_ID));
        } else {
            ContentValues values = new ContentValues();
            values.put(HydrobiologieTaxEntry.KEY_TAXON, hydrobiologieTax.getTaxon());
            values.put(HydrobiologieTaxEntry.KEY_RESULTAT, hydrobiologieTax.getResultat());
            values.put(HydrobiologieTaxEntry.KEY_DATE_PROD_RESULTAT, DateFormat.dateToString(hydrobiologieTax.getDtprodresultatbiologique(), "y-M-d"));
            values.put(HydrobiologieTaxEntry.KEY_PRELEVEUR, hydrobiologieTax.getPreleveur());
            values.put(HydrobiologieTaxEntry.KEY_STATION_FK, stationID);
            values.put(HydrobiologieTaxEntry.KEY_PARK_FK, parkName);

            hydrobiologieID = db.insert(HydrobiologieTaxEntry.TABLE_HYDROBIOLOGIE_TAX, null, values);
        }
        c.close();
        return hydrobiologieID;
    }

    /**
     * Select one object from his ID
     * @param id requested ID
     * @return object corresponding to the ID
     */
    public HydrobiologieTax select(long id) {
        String selectFromId = "SELECT * FROM " + HydrobiologieTaxEntry.TABLE_HYDROBIOLOGIE_TAX + ", "
                + StationContract.StationEntry.TABLE_STATION + ", "
                + PositionContract.PositionEntry.TABLE_POSITION
                + " WHERE " + HydrobiologieTaxEntry.TABLE_HYDROBIOLOGIE_TAX + "." + HydrobiologieTaxEntry.KEY_STATION_FK
                + " = " + StationContract.StationEntry.TABLE_STATION + "." + StationContract.StationEntry.KEY_ID
                + " AND " + StationContract.StationEntry.TABLE_STATION + "." + StationContract.StationEntry.KEY_POSITION_FK
                + " = " + PositionContract.PositionEntry.TABLE_POSITION + "." + PositionContract.PositionEntry.KEY_POSITION_ID
                + " AND " + HydrobiologieTaxEntry.TABLE_HYDROBIOLOGIE_TAX + "." + HydrobiologieTaxEntry.KEY_ID + " = " + String.valueOf(id);

        Cursor cursor = db.rawQuery(selectFromId, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            return createHydrobilogieTaxFromCursor(cursor);
        }

        cursor.close();
        return null;
    }

    /**
     * Select all hydrobiologieTax between renderDistance and ringDistance from the point in parameter
     * @param searchPosition position of search
     * @param renderDistance search distance
     * @param ringDistance size of the empty zone before getting data
     * @return return a list of hydrobiologieTax objects at requested distance
     * @throws IllegalArgumentException if the render distance is smaller than the ring distance
     */
    public List<HydrobiologieTax> getAllHydrobiologieAtDistance(Position searchPosition, int renderDistance, int ringDistance) throws IllegalArgumentException {
        if(renderDistance < ringDistance) {
            throw new IllegalArgumentException("ring cannot be larger than render distance");
        }

        // list for store and return matching objects
        List<HydrobiologieTax> macroList = new ArrayList<>();

        // Select All Query
        Cursor cursor = db.rawQuery(HydrobiologieTaxContract.SELECT_HYDROBIOLOGIETAX_STATION_POSITION, null);

        //if cursor is empty, return an empty list
        if(cursor.getCount() == 0) {
            return macroList;
        }

        // looping through all rows and adding to list if they are in render distance
        if (cursor.moveToFirst()) {
            do {
                double objectLongitude =  Double.parseDouble(cursor.getString(cursor.getColumnIndex(PositionContract.PositionEntry.KEY_POSITION_LON)));
                double objectLatitude =  Double.parseDouble(cursor.getString(cursor.getColumnIndex(PositionContract.PositionEntry.KEY_POSITION_LAT)));

                // create position of the db object and calculates distance from user position
                Position np = new Position(objectLatitude, objectLongitude);
                float d = searchPosition.getDistanceTo(np);

                // if object is too far, continue
                if (d > renderDistance || d < ringDistance) {
                    continue;
                }

                // Adding to list
                macroList.add(createHydrobilogieTaxFromCursor(cursor));
            } while (cursor.moveToNext());
        }

        cursor.close();
        return macroList;
    }

    /**
     * Select all hydrobiologieTax at a certain distance from the point in parameter
     * @param searchPosition position of search
     * @param renderDistance search distance
     * @return return a list of hydrobiologieTax objects at requested distance
     */
    public List<HydrobiologieTax> getAllHydrobiologieAtDistance(Position searchPosition, int renderDistance) {
        return getAllHydrobiologieAtDistance(searchPosition, renderDistance, 0);
    }

    /**
     * Creates an HydrobiologieTax object from the content of a cursor
     * @param cursor SQLiteDatabase cursor containing datas of an HydrobiologieTax object
     * @return the created object
     */
    private HydrobiologieTax createHydrobilogieTaxFromCursor(Cursor cursor) {
        Map<String, String> paramsMap = new HashMap<>();
        paramsMap.put("taxon",                      cursor.getString(cursor.getColumnIndex(HydrobiologieTaxEntry.KEY_TAXON)));
        paramsMap.put("resultat",                   cursor.getString(cursor.getColumnIndex(HydrobiologieTaxEntry.KEY_RESULTAT)));
        paramsMap.put("dtprodresultatbiologique",   cursor.getString(cursor.getColumnIndex(HydrobiologieTaxEntry.KEY_DATE_PROD_RESULTAT)));
        paramsMap.put("preleveur",                  cursor.getString(cursor.getColumnIndex(HydrobiologieTaxEntry.KEY_PRELEVEUR)));
        paramsMap.put("nom_station",                cursor.getString(cursor.getColumnIndex(StationContract.StationEntry.KEY_NAME)));
        paramsMap.put("longitude",                  cursor.getString(cursor.getColumnIndex(PositionContract.PositionEntry.KEY_POSITION_LON)));
        paramsMap.put("latitude",                   cursor.getString(cursor.getColumnIndex(PositionContract.PositionEntry.KEY_POSITION_LAT)));

        return new HydrobiologieTax(paramsMap);
    }
}
