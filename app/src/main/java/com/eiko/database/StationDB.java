package com.eiko.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.eiko.DataObject.Station;
import com.eiko.geolocalization.Position;
import com.eiko.database.contracts.PositionContract;
import com.eiko.database.contracts.StationContract;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created by axelheine on 09/02/2018.
 */

public class StationDB {
    private Database dbInstance;
    private SQLiteDatabase db;
    private Context context;

    public StationDB(Context context) {
        this.context = Objects.requireNonNull(context);
        this.dbInstance = Database.getInstance(context);
        this.db = dbInstance.getWritableDatabase();
    }

    public void close() {
        db.close();
    }

    /**
     * Insert station entry in database
     * @param position position of the station
     * @param name name of the station
     * @return the id of inserted row in database
     */
    public long insert(Position position, String name) {
        return insert(position, name, "");
    }

    /**
     * Insert station entry in database
     * @param position position of the station
     * @param name name of the station
     * @param parkName park name to save
     * @return the id of inserted row in database
     */
    public long insert(Position position, String name, String parkName) {
        PositionDB positionDB = new PositionDB(context);
        long positionID = positionDB.insert(position);
        long stationID;

        // On essaue de récupérer la station pour savoir si elle existe
        Cursor c = db.query(
                StationContract.StationEntry.TABLE_STATION,
                new String[] {StationContract.StationEntry.KEY_ID},
                StationContract.StationEntry.KEY_NAME + " = ? AND " + StationContract.StationEntry.KEY_POSITION_FK + " = ? AND " + StationContract.StationEntry.KEY_PARK_FK + " = ?",
                new String[] {name, String.valueOf(positionID), parkName},
                null, null, null
        );

        if (c.moveToFirst() && c.getCount() > 0) {
            stationID = c.getLong(c.getColumnIndex(PositionContract.PositionEntry.KEY_POSITION_ID));
        } else {
            ContentValues values = new ContentValues();
            values.put(StationContract.StationEntry.KEY_NAME, name);
            values.put(StationContract.StationEntry.KEY_POSITION_FK, positionID);
            values.put(StationContract.StationEntry.KEY_PARK_FK, parkName);

            stationID = db.insert(StationContract.StationEntry.TABLE_STATION, null, values);
        }
        c.close();
        return stationID;
    }

    /**
     * Select all macroinvertebre at a certain distance from the point in parameter
     * @param searchPosition position of search
     * @param renderDistance search distance
     * @return return a list of macroinvertebre objects at requested distance
     */
    public List<Station> getAllStationsAtDistance(Position searchPosition, int renderDistance) {
        //Liste pour le retour des objets
        List<Station> macroList = new ArrayList<>();

        // Select All Query
        Cursor cursor = db.rawQuery(StationContract.SELECT_STATION_POSITION, null);

        //Si le curseur est vide, pas de résultat
        if(cursor.getCount() == 0) {
            return macroList;
        }

        // looping through all rows and adding to list if they are in render distance
        if (cursor.moveToFirst()) {
            do {
                double objectLongitude =  Double.parseDouble(cursor.getString(cursor.getColumnIndex(PositionContract.PositionEntry.KEY_POSITION_LON)));
                double objectLatitude =  Double.parseDouble(cursor.getString(cursor.getColumnIndex(PositionContract.PositionEntry.KEY_POSITION_LAT)));

                // create position of the db object and calculates distance from user position
                Position np = new Position(objectLatitude, objectLongitude);
                float d = searchPosition.getDistanceTo(np);

                // if object is too far, continue
                if (d > renderDistance) {
                    continue;
                }

                // Adding to list
                macroList.add(createStationFromCursor(cursor));
            } while (cursor.moveToNext());
        }

        cursor.close();
        return macroList;
    }

    private Station createStationFromCursor(Cursor cursor) {
        Map<String, String> mapParams = new HashMap<>();
        mapParams.put("station_nom", cursor.getString(cursor.getColumnIndex(StationContract.StationEntry.KEY_NAME)));
        mapParams.put("longitude",      cursor.getString(cursor.getColumnIndex(PositionContract.PositionEntry.KEY_POSITION_LON)));
        mapParams.put("latitude",       cursor.getString(cursor.getColumnIndex(PositionContract.PositionEntry.KEY_POSITION_LAT)));

        return new Station(mapParams);
    }


}
