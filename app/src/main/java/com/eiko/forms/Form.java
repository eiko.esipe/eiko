package com.eiko.forms;

import com.eiko.geolocalization.Position;

import org.json.JSONException;
import org.json.JSONObject;

public class Form {
    private final String userMail;
    private final String password;
    private final String comment;
    private final String picture;
    private final Position gps;
    private final String station;

    private Form(String userMail, String password, String comment, String picture, Position gps, String station) {
        this.userMail = userMail;
        this.password = password;
        this.comment = comment;
        this.picture = picture;
        this.gps = gps;
        this.station = station;
    }

    /**
     * Generate a Form object from UI elements of an Android activity
     * @param user User corresponding to the Eiko+ technician
     * @param password Password to reach the form server
     * @param comment Comment of the form
     * @param imageString String corresponding to the picture
     * @param position Current position
     * @param station StationName near the position
     * @return Form corresponding to all these parameters
     */
    public static Form generateFormFromUI(String user, String password, String comment, String imageString, Position position, String station) {
        return new Form(user, password, comment, imageString, position, station);
    }

    /**
     * Generate a Form object from a JSONObject
     * @param json JSON containing all form parameters
     * @return Form corresponding to the JSON
     * @throws JSONException If the JSON is not correct
     */
    public static Form generateFormFromJSON(JSONObject json) throws JSONException {
        String user = json.getString("user");
        String password = json.getString("password");
        String comment = json.getString("comment");
        String picture = json.getString("picture");
        Position gps = new Position(json.getJSONArray("gps").getDouble(1), json.getJSONArray("gps").getDouble(0));
        String station = json.getString("station");
        return new Form(user, password, comment, picture, gps, station);
    }

    /**
     * Create a string corresponding to the Form object in JSON format
     * @return String ready to be sent to the form server
     */
    public String toJsonString() {
        String json = "{\"user\":\"" + userMail + "\"," +
                "\"password\":\"" + password + "\"," +
                "\"comment\":\"" + comment + "\"," +
                "\"gps\":[" + gps.getLongitude() + "," + gps.getLatitude() + "]," +
                "\"station\":\"" + station + "\"," +
                "\"picture\":\"" + picture + "\"}";
        return json.replace("\n", "").replace("\r", "");
    }

    /**
     * Getter for the user of the form
     * @return User mail
     */
    public String getUserMail() {
        return userMail;
    }

    /**
     * Getter for the password of the form
     * @return Password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Getter for the comment of the form
     * @return Comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * Getter for the picture of the form
     * @return Picture as a string
     */
    public String getPicture() {
        return picture;
    }

    /**
     * Getter for the position of the form
     * @return Position object
     */
    public Position getPosition() {
        return gps;
    }

    /**
     * Getter for the station of the form
     * @return Station name
     */
    public String getStation() {
        return station;
    }
}
