package com.eiko.forms;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

public class FormServer {
    private final static String formServerConfFile = "formServerConfig.json";
    private String url;
    private String password;

    public FormServer(Context mContext) throws IOException, JSONException {
        JSONObject jsonFormServerConfig = FormServer.readJSONConfFile(mContext);
        url = jsonFormServerConfig.getJSONObject("formServerConfig").getString("url");
        password = jsonFormServerConfig.getJSONObject("formServerConfig").getString("password");
    }

    /**
     * Read the JSON form server configuration file to takeForDisplay the address and port
     * @param context Context of activity
     * @return JSON object of the form server configuration
     * @throws IOException If the file can't be read
     * @throws JSONException If the JSON is not correct
     */
    private static JSONObject readJSONConfFile(Context context) throws IOException, JSONException {
        InputStream is = context.getAssets().open(formServerConfFile);
        int size = is.available();
        byte[] buffer = new byte[size];
        is.read(buffer);
        is.close();
        return (JSONObject) new JSONArray(new String(buffer, "UTF-8")).get(0);
    }

    /**
     * Getter for the URL of the form server
     * @return Full URL
     */
    public String getUrl() {
        return url;
    }

    /**
     * Getter for the password of the form server
     * @return Password
     */
    public String getPassword() {
        return password;
    }
}
