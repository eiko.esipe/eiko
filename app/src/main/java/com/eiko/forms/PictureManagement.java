package com.eiko.forms;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.NonNull;
import android.util.Base64;
import android.view.View.MeasureSpec;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;
import java.util.Objects;

/**
 * Give static methods for picture conversions that are needed in FormActivity
 */
public class PictureManagement {
    public static final String REAL_PICTURE = "real";
    public static final String DUMMY_PICTURE = "dummy";

    /**
     * Method to use to convert the ImageView in the form into a String with Base64
     * This method filters
     * @param picture ImageView from FormActivity
     * @return Empty string if the image has the DUMMY_PICTURE tag, and the string representation
     *         of the image if the tag is REAL_PICTURE
     */
    public static String getImageString(ImageView picture) {
        String pictureTag = picture.getTag().toString();
        String imageString = "";
        if (PictureManagement.isPictureTaggedReal(pictureTag)) {
            Bitmap bitmap = ((BitmapDrawable) picture.getDrawable()).getBitmap();
            imageString = PictureManagement.bitmapToString(bitmap);
        }
        return imageString;
    }

    /**
     * A picture tagged with REAL_PICTURE is considered as taken
     * A picture tagged with DUMMY_PICTURE or something else is considered as a dummy picture
     * @param tag Tag of the picture
     * @return true if tag is REAL_PICTURE, false if not
     */
    public static boolean isPictureTaggedReal(String tag) {
        return REAL_PICTURE.equals(tag);
    }

    /**
     * Convert an ImageView into a Bitmap
     * @param iv ImageView that needs to be converted
     * @return Bitmap object corresponding
     */
    public static Bitmap imageViewToBitmap(@NonNull ImageView iv) {
        Objects.requireNonNull(iv);
        iv.setDrawingCacheEnabled(true);
        iv.measure(MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED),
                MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
        iv.layout(0, 0, iv.getMeasuredWidth(), iv.getMeasuredHeight());
        iv.buildDrawingCache(true);
        Bitmap b = Bitmap.createBitmap(iv.getDrawingCache());
        iv.setDrawingCacheEnabled(false);
        return b;
    }

    /**
     * Convert a picture/image into a Base 64 string
     * @param bitmap Bitmap of the image
     * @return String corresponding
     */
    public static String bitmapToString(@NonNull Bitmap bitmap) {
        Objects.requireNonNull(bitmap);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100, baos);
        byte[] b = baos.toByteArray();
        return Base64.encodeToString(b, Base64.DEFAULT);
    }
}
