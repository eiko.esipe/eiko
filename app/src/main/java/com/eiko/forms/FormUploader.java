package com.eiko.forms;

import org.json.JSONException;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

/**
 * Contains necessary methods to send a Form through an HTTP connection
 */
public class FormUploader {
    private final String host;

    public FormUploader(String host) {
        this.host = host;
    }

    /**
     * Send a form to the formServer
     * @param form Form to send
     * @return Http response code
     * @throws IOException If the connection with server is lost
     * @throws JSONException From Form class if the JSON is not correct
     * @throws NullPointerException If form is null
     */
    public int sendFormThroughHttp(Form form) throws IOException, JSONException {
        String json = form.toJsonString();
        URLConnection urlConnection = getUrlConnection();
        sendJSON(json, urlConnection);
        return ((HttpURLConnection)urlConnection).getResponseCode();
    }

    /**
     * Create a HTTP connection to an URL
     * @return urlConnection ready to send data
     * @throws IOException If the connection is lost
     */
    public URLConnection getUrlConnection() throws IOException {
        URL url = new URL(host);
        URLConnection urlConnection = url.openConnection();
        urlConnection.setDoOutput(true);
        return urlConnection;
    }

    /**
     * Send the JSON object through the HTTP connection
     * @param json JSON to send
     * @param urlConnection HTTP connection to the form server
     * @throws IOException If the connection is lost
     */
    public static void sendJSON(String json, URLConnection urlConnection) throws IOException {
        OutputStream out = urlConnection.getOutputStream();
        OutputStreamWriter outStream = new OutputStreamWriter(out);
        outStream.write(json);
        outStream.flush();
        outStream.close();
    }
}
