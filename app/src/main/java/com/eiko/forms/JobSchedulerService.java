package com.eiko.forms;

import android.app.job.JobParameters;
import android.app.job.JobService;

import org.json.JSONException;

import java.io.IOException;

/**
 * Service that runs the FormAsyncTask AsyncTask
 */
public class JobSchedulerService extends JobService {

    @Override
    public boolean onStartJob(JobParameters jobParameters) {
        try {
            FormServer formServer = new FormServer(getApplicationContext());
            FormAsyncTask formAsyncTask = new FormAsyncTask(getApplicationContext(), formServer);
            formAsyncTask.execute();
        } catch (IOException | JSONException e) {
            jobFinished(jobParameters, true);
        }
        jobFinished(jobParameters, false);
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        return false;
    }
}
