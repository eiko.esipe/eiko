package com.eiko.forms;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.eiko.database.FormJsonDB;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.ref.WeakReference;

/**
 * AsyncTask that sends all forms saved in database to the form server
 */
public class FormAsyncTask extends AsyncTask<Void, Void, Integer> {
    private final WeakReference<Context> weakContext;
    private FormServer formServer;

    FormAsyncTask(Context context, FormServer server) {
        weakContext = new WeakReference<>(context);
        formServer = server;
    }

    /**
     * Send all forms saved in database to the form server
     * @param params No param
     * @return null because onProgressUpdate is not used
     */
    @Override
    protected Integer doInBackground(Void... params) {
        Log.d("Eiko+", "Start FormAsyncTask");
        FormUploader formUploader = new FormUploader(formServer.getUrl());
        FormJsonDB formDB = new FormJsonDB(weakContext.get());
        String jsonString;
        int count = 0;
        while ((jsonString = formDB.getFirstJSON()) != null) {
            Log.d("Eiko+", "Sending form: " + jsonString);
            try {
                Form form = Form.generateFormFromJSON(new JSONObject(jsonString));
                int responseCode = formUploader.sendFormThroughHttp(form);
                if (responseCode == 200) {
                    Log.d("Eiko+", "Form: received 200 OK");
                    formDB.deleteFirstJSON();
                    count++;
                } else if (responseCode == 400) {
                    Log.d("Eiko+", "Form: received 400 Bad Request");
                    formDB.deleteFirstJSON();
                } else if (responseCode == 401) {
                    Log.d("Eiko+", "Form: received 401 Unauthorized");
                    formDB.deleteFirstJSON();
                } else {
                    Log.d("Eiko+", "Form: received " + responseCode);
                }
            } catch (IOException e) {
                Log.d("Eiko+", "Form: Can't reach the server");
                break;
            } catch (JSONException e) {
                Log.d("Eiko+", "Form: JSON form not good");
                formDB.deleteFirstJSON();
            }
        }
        return count;
    }

    @Override
    protected void onPostExecute(Integer count) {
        if (count > 0) {
            Toast.makeText(weakContext.get(), "Formulaires envoyés (" + count + ")", Toast.LENGTH_SHORT).show();
            Log.d("Eiko+", count + " forms sent");
        }
    }
}
