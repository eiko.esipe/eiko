package com.eiko.matrix;

import android.util.Log;

import com.google.ar.core.Anchor;
import com.google.ar.core.Pose;
import com.google.ar.core.Session;
import com.google.ar.core.exceptions.FatalException;

import java.util.Objects;

/**
 * Created by eikouser on 15/02/18.
 */

public class ARPosition {
    private Anchor anchor;

    /**
     * Create an Anchor for the ARCore Session with the Pose given in argument
     * @param session
     * @param pose
     */
    private ARPosition(Session session, Pose pose){
        try {
            anchor = session.createAnchor(pose);
        } catch (FatalException e){
            Log.e("Eiko","Pose generating an error Fatal exception : " + pose);
        }
    }

    /**
     * This method return the anchor for this ARPosition
     * @return The anchor.
     */
    public Anchor getAnchor() {
        return anchor;
    }

    /**
     * Create a new position in AR and add it an anchor to track it. Doesn't handle the rotation.
     * @param session The ARCore session running
     * @param translation The translation from the camera
     * @return
     */
    public static ARPosition createNewPositionInAR(Session session, float[] translation, Pose cam){
        Objects.requireNonNull(session);
        if (translation.length<3){
            return null;
        }
        float[] f = {0,0,0,cam.qw()};
        Pose p = new Pose(translation,f);
        return new ARPosition(session,p);
    }
}
