package com.eiko.matrix;

import com.eiko.DataObject.DisplayableData;
import com.eiko.geolocalization.Position;

/**
 * Created by yslh on 14/02/18.
 */

public class ARObjectFactory {

    public ARObjectFactory() {
    }

    /* *************** */
    /* *************** */

    /**
     * @param devicePosition : Smartphone position
     * @param angleNorthToDevice : Angle between North and smartphone (in degrees)
     * @param latitudeData : Data latitude
     * @param longitudeData : Data longitude
     * @return ARObject
     */
    public static ARObject createARObject(Position devicePosition, double angleNorthToDevice, double latitudeData, double longitudeData) {
        double latitudeDevice = devicePosition.getLatitude();
        double longitudeDevice = devicePosition.getLongitude();

        // Delta Lat/Long
        double longDelta = longitudeDevice - longitudeData;
        double latDelta = latitudeDevice - latitudeData;

        // Ortho coordinates
        double x = longDelta * (-7) * 10000;
        double y = latDelta * (-13) * 10000;

        // AR coordinates
        double distance = Math.sqrt(Math.pow(x,2) + Math.pow(y,2));
        double angle = Math.toDegrees(Math.atan2(y, x)) - angleNorthToDevice;

        while (angle < 0) {
            angle += 360;
        }
        angle = angle % 360;

        return new ARObject(distance, (float) angle);
    }

    /**
     * @param devicePosition : Smartphone position
     * @param angleNorthToDevice : Angle between North and smartphone (in degrees)
     * @param latitudeData : Data latitude
     * @param longitudeData : Data longitude
     * @param data : Content of the data to insert in the ARobject
     * @return ARObject
     */
    public static ARObject createARObject(Position devicePosition, double angleNorthToDevice, double latitudeData, double longitudeData, DisplayableData data) {
        double latitudeDevice = devicePosition.getLatitude();
        double longitudeDevice = devicePosition.getLongitude();

        // Delta Lat/Long
        double longDelta = longitudeDevice - longitudeData;
        double latDelta = latitudeDevice - latitudeData;

        // Ortho coordinates
        double x = longDelta * (-7) * 10000;
        double y = latDelta * (-13) * 10000;

        // AR coordinates
        double distance = Math.sqrt(Math.pow(x,2) + Math.pow(y,2));
        double angle = Math.toDegrees(Math.atan2(y, x)) - angleNorthToDevice;

        while (angle < 0) {
            angle += 360;
        }
        angle = angle % 360;

        return new ARObject(data, distance, (float) angle);
    }
}
