package com.eiko.matrix;

import com.eiko.DataObject.DisplayableData;
import com.eiko.ar.rendering.LayRenderer;
import com.google.ar.core.Pose;
import com.google.ar.core.Session;

/**
 * Created by yslh on 06/02/18, for project eiko
 */

public class ARObject {
    private DisplayableData data;
    private double distance;
    private double x;
    private double y;
    private double z;
    private ARPosition arPosition;
    private LayRenderer mLayRenderer;


    public ARObject(DisplayableData currentData, double distance, float angle) {
        this.data = currentData;
        this.distance = distance;
        this.x = distance * Math.cos(Math.toRadians(angle));
        this.y = 0;
        this.z = (-1) * distance * Math.sin(Math.toRadians(angle));

        System.out.println("-ARitem-\nX:  "+ x + "\nZ:  " + z);
    }

    public ARObject(double distance, float angle) {
        this.distance = distance;
        this.x = distance * Math.cos(Math.toRadians(angle));
        this.y = 0;
        this.z = (-1) * distance * Math.sin(Math.toRadians(angle));

        System.out.println("-ARitem-\nX:  "+ x + "\nZ:  " + z);
    }

    public void setARObjectAnchor(Session session, Pose cam){
        this.x = (x + ((-1)*cam.tx()));
        this.y = cam.ty(); // to avoid flags being too high
        this.z = (z + ((-1)*cam.tz()));
        float[] translation = {(float)x,(float)y,(float)z};
        arPosition = ARPosition.createNewPositionInAR(session,translation, cam);
        mLayRenderer.init = true;
    }

    // GETTERS
    public DisplayableData getData() {
        return data;
    }

    public ARPosition getArPosition() {
        return arPosition;
    }

    public double getDistance() { return distance; }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public LayRenderer getLayRenderer() { return mLayRenderer; }

    // SETTERS
    public void setX(double val) {
        this.x = val;
    }

    public void setY(double val) {
        this.y = val;
    }

    public void setZ(double val) {
        this.z = val;
    }

    public void setData(DisplayableData data) {
        this.data = data;
    }

    public void setLayRenderer(LayRenderer layRenderer) { this.mLayRenderer = layRenderer; }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof ARObject))
            return false;
        ARObject ar = (ARObject) o;
        return !(this.x != ar.getX()) && !(this.y != ar.getY()) && !(this.z != ar.getZ()) && this.arPosition == ar.getArPosition() && this.data == ar.getData();
    }

    @Override
    public String toString() {
        return data.toString() + " x: " + x + " y: " + y + " z: " + z;
    }
}