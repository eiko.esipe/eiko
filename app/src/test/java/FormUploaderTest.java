import com.eiko.forms.Form;
import com.eiko.forms.FormUploader;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URLConnection;
import java.util.Random;

import static org.junit.Assert.assertEquals;

/**
 * Unit test the real form server to check its response according to what is sent
 */
public class FormUploaderTest {

    /**
     * Test sending of a basic Form
     * @throws JSONException If the JSON is incorrect
     * @throws IOException If the connection is lost
     */
    @Test
    public void testSendForm_nominal() throws JSONException, IOException {
        String host = "http://5.149.188.204/form_eiko/index.php";
        FormUploader formUploader = new FormUploader(host);

        String jsonString = "{\"user\":\"userMail\"," +
                "\"password\":\"OnemaDcie94300\"," +
                "\"comment\":\"comment\"," +
                "\"gps\":[4.0,2.0]," +
                "\"station\":\"station\"," +
                "\"picture\":\"pictureCode\"}";
        JSONObject json = new JSONObject(jsonString);
        Form form = Form.generateFormFromJSON(json);

        int responseCode = formUploader.sendFormThroughHttp(form);
        assertEquals(200, responseCode);
    }

    /**
     * Test sending of a basic Form with an extra attribute
     * @throws JSONException If the JSON is incorrect
     * @throws IOException If the connection is lost
     */
    @Test
    public void testSendForm_withExtra() throws JSONException, IOException {
        String host = "http://5.149.188.204/form_eiko/index.php";
        String password = "OnemaDcie94300";
        FormUploader formUploader = new FormUploader(host);

        String jsonString = "{\"user\":\"userMail\"," +
                "\"password\":\"" + password + "\"," +
                "\"comment\":\"comment\"," +
                "\"gps\":[4.0,2.0]," +
                "\"station\":\"station\"," +
                "\"picture\":\"pictureCode\"," +
                "\"extra\":\"forTest\"}";
        JSONObject json = new JSONObject(jsonString);
        Form form = Form.generateFormFromJSON(json);

        int responseCode = formUploader.sendFormThroughHttp(form);
        assertEquals(200, responseCode);
    }

    /**
     * Test sending a form without picture
     * @throws JSONException If the JSON is incorrect
     * @throws IOException If the connection is lost
     */
    @Test
    public void testSendForm_noPicture() throws JSONException, IOException {
        String host = "http://5.149.188.204/form_eiko/index.php";
        String password = "OnemaDcie94300";
        FormUploader formUploader = new FormUploader(host);

        String jsonString = "{\"user\":\"userMail\"," +
                "\"password\":\"" + password + "\"," +
                "\"comment\":\"comment\"," +
                "\"gps\":[4.0,2.0]," +
                "\"station\":\"station\"," +
                "\"picture\":\"\"}";
        JSONObject json = new JSONObject(jsonString);
        Form form = Form.generateFormFromJSON(json);

        int responseCode = formUploader.sendFormThroughHttp(form);
        assertEquals(400, responseCode);
    }

    /**
     * Test sending a form with a picture containing special chars
     * @throws JSONException If the JSON is incorrect
     * @throws IOException If the connection is lost
     */
    @Test
    public void testSendForm_specialChars() throws JSONException, IOException {
        String host = "http://5.149.188.204/form_eiko/index.php";
        String password = "OnemaDcie94300";
        FormUploader formUploader = new FormUploader(host);

        String jsonString = "{\"user\":\"userMail\"," +
                "\"password\":\"" + password + "\"," +
                "\"comment\":\"comment\"," +
                "\"gps\":[4.0,2.0]," +
                "\"station\":\"station\"," +
                "\"picture\":\"String_with_special//chars\"}";
        JSONObject json = new JSONObject(jsonString);
        Form form = Form.generateFormFromJSON(json);

        int responseCode = formUploader.sendFormThroughHttp(form);
        assertEquals(200, responseCode);
    }

    /**
     * Test sending a form with a 100k chars picture
     * @throws JSONException If the JSON is incorrect
     * @throws IOException If the connection is lost
     */
    @Test
    public void testSendForm_bigPicture() throws JSONException, IOException {
        String host = "http://5.149.188.204/form_eiko/index.php";
        String password = "OnemaDcie94300";
        FormUploader formUploader = new FormUploader(host);

        Integer length = 100000 ;
        StringBuilder outputBuffer = new StringBuilder(length);
        for (int i = 0; i < length; i++){
            Random rnd = new Random();
            outputBuffer.append((char)(rnd.nextInt(26) + 'a'));
        }
        String ns =  outputBuffer.toString();

        String jsonString = "{\"user\":\"userMail\"," +
                "\"password\":\"" + password + "\"," +
                "\"comment\":\"comment\"," +
                "\"gps\":[4.0,2.0]," +
                "\"station\":\"station\"," +
                "\"picture\":\"" + ns + "\"}";
        JSONObject json = new JSONObject(jsonString);
        Form form = Form.generateFormFromJSON(json);

        int responseCode = formUploader.sendFormThroughHttp(form);
        assertEquals(200, responseCode);
    }

    /**
     * Test sending of an incorrect JSON
     * @throws IOException If the connection is lost
     */
    @Test
    public void testSendForm_incorrectJSON() throws IOException {
        String host = "http://5.149.188.204/form_eiko/index.php";
        FormUploader formUploader = new FormUploader(host);
        URLConnection conn = formUploader.getUrlConnection();
        String json = "incorrectContents";
        FormUploader.sendJSON(json, conn);
        assertEquals(400, ((HttpURLConnection) conn).getResponseCode());
    }


    /**
     * Test sending of an incorrect JSON
     * @throws IOException If the connection is lost
     */
    @Test
    public void testSendForm_uncompleteJSON() throws IOException {
        String host = "http://5.149.188.204/form_eiko/index.php";
        FormUploader formUploader = new FormUploader(host);
        URLConnection conn = formUploader.getUrlConnection();
        String jsonString = "{\"user\":\"userMail\"," +
                "\"password\":\"OnemaDcie94300\"," +
                "\"comment\":\"comment\"," +
                "\"gps\":[4.0,2.0]," +
                "\"station\":\"station\"}";
        FormUploader.sendJSON(jsonString, conn);
        assertEquals(400, ((HttpURLConnection) conn).getResponseCode());
    }

    /**
     * Test sending of a wrong password
     * @throws JSONException If the JSON is incorrect
     * @throws IOException If the connection is lost
     */
    @Test
    public void testSendForm_wrongPassword() throws JSONException, IOException {
        String host = "http://5.149.188.204/form_eiko/index.php";
        String password = "wrongPassword";
        FormUploader formUploader = new FormUploader(host);

        String jsonString = "{\"user\":\"userMail\"," +
                "\"password\":\"" + password + "\"," +
                "\"comment\":\"comment\"," +
                "\"gps\":[4.0,2.0]," +
                "\"station\":\"station\"," +
                "\"picture\":\"pictureCode\"}";
        JSONObject json = new JSONObject(jsonString);
        Form form = Form.generateFormFromJSON(json);

        int responseCode = formUploader.sendFormThroughHttp(form);
        assertEquals(401, responseCode);
    }
}
