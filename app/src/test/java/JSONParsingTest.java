import com.eiko.DataObject.*;
import com.eiko.downloads.DataDownloadedParser;
import com.eiko.downloads.DataDownloader;

import org.json.JSONArray;
import org.json.JSONException;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by eikouser on 13/02/18.
 */

public class JSONParsingTest {

    @Test
    public void testParseAmphibianFile(){
        ClassLoader classLoader = this.getClass().getClassLoader();
        URL resource = classLoader.getResource("amphibiens.json");
        File file = new File(resource.getPath());
        DataDownloader dl = DataDownloader.getDataDownloader();

        List<Amphibien> list = (List<Amphibien>) dl.downloadsSectorsFromFile(file);
        List<String> sList = new ArrayList<>();

        List<String> testList = Arrays.asList("R. ridibunda", "Bufo bufo", "R. esculenta","", "R. temporaria");

        for(int i = 0; i < list.size();i++){
            sList.add(list.get(i).getTaxon());
        }
        Assert.assertEquals(testList,sList);
    }

    @Test
    public void testParseInvertebrateFile(){
        ClassLoader classLoader = this.getClass().getClassLoader();
        URL resource = classLoader.getResource("invertebres.json");
        File file = new File(resource.getPath());
        DataDownloader dl = DataDownloader.getDataDownloader();

        List<Invertebre> list = (List<Invertebre>) dl.downloadsSectorsFromFile(file);
        List<String> sList = new ArrayList<>();

        List<String> testList = Arrays.asList("Chalcolestes viridis", "Caleopteryx virgo", "Sympecma fusca", "Ishnura elegans", "Sympetrum striolatum", "Caleopteryx splendens", "Brachytron pratense", "Crocothemis erythraea", "Orthetrum cancellatum", "Coenagrion pulchellum", "Orthetrum brunneum", "Sympetrum vulgatum", "Enallagma cyathigerum", "Platycnemis pennipes", "Coenagrion puella", "Pyrrhosoma nymphula", "Aeshna mixta");

        for(int i = 0; i < list.size();i++){
            sList.add(list.get(i).getTaxon());
        }
        Assert.assertEquals(testList,sList);
    }

    @Test
    public void testParseFloraFile(){
        ClassLoader classLoader = this.getClass().getClassLoader();
        URL resource = classLoader.getResource("flore_cbn.json");
        File file = new File(resource.getPath());
        DataDownloader dl = DataDownloader.getDataDownloader();

        List<Flore> list = (List<Flore>) dl.downloadsSectorsFromFile(file);
        List<String> sList = new ArrayList<>();

        List<String> testList = Arrays.asList("96519.0", "99922.0", "96519.0", "96519.0", "99922.0", "99922.0", "96519.0", "99922.0", "99922.0", "99922.0", "99922.0", "99922.0", "96465.0", "99922.0", "99922.0", "96465.0", "96519.0", "99922.0", "96465.0", "99922.0", "96465.0", "99922.0", "99922.0", "96465.0", "96519.0", "99922.0", "99922.0", "96465.0", "96465.0", "99922.0", "99922.0", "99922.0", "96519.0", "99922.0", "99922.0", "99922.0", "99922.0", "96519.0", "92497.0", "92497.0", "92497.0", "92497.0", "92497.0", "96465.0", "99922.0", "96465.0", "99922.0", "99922.0", "96465.0", "96519.0", "99922.0", "96465.0", "99922.0", "96465.0", "96465.0", "99922.0", "96465.0", "99922.0", "96465.0", "99922.0", "96519.0", "99922.0", "99922.0", "99922.0", "99922.0", "96465.0", "96519.0", "99922.0", "92497.0", "92497.0", "92497.0", "92497.0", "92497.0");
        for(int i = 0; i < list.size();i++){
            sList.add(list.get(i).getCd_sp());
        }
        Assert.assertEquals(testList,sList);
    }

    @Test
    public void testParseHydrobiologyTaxFile(){
        ClassLoader classLoader = this.getClass().getClassLoader();
        URL resource = classLoader.getResource("hydrobiologie.json");
        File file = new File(resource.getPath());
        DataDownloader dl = DataDownloader.getDataDownloader();

        List<HydrobiologieTax> list = (List<HydrobiologieTax>) dl.downloadsSectorsFromFile(file);
        List<String> sList = new ArrayList<>();

        List<String> testList = Arrays.asList("3 64320003525", "10 64320003526", "5 64320003527", "12 64320003522", "2 64320003463", "7 47870000059", "9 64320003528", "72 47870000124", "4 47870000130", "14 64320003523", "3 47870000054", "6 47870000049", "34 64320003548", "6 64320003532");
        for(int i = 0; i < list.size();i++){
            sList.add(list.get(i).getResultat()+" "+list.get(i).getCode_operation());
        }
        Assert.assertEquals(testList,sList);
    }

    @Test
    public void testParseHydrobiologyParFile(){
        ClassLoader classLoader = this.getClass().getClassLoader();
        URL resource = classLoader.getResource("hydrobiologie-parametre.json");
        File file = new File(resource.getPath());
        DataDownloader dl = DataDownloader.getDataDownloader();

        List<HydrobiologiePar> list = (List<HydrobiologiePar>) dl.downloadsSectorsFromFile(file);
        List<String> sList = new ArrayList<>();

        List<String> testList = Arrays.asList("0.31 10090002710");
        for(int i = 0; i < list.size();i++){
            sList.add(list.get(i).getResultat()+" "+list.get(i).getCode_operation());
        }
        Assert.assertEquals(testList,sList);
    }

    @Test
    public void testParseMacroInvertebrateFile(){
        ClassLoader classLoader = this.getClass().getClassLoader();
        URL resource = classLoader.getResource("macroinvertebres.json");
        File file = new File(resource.getPath());
        DataDownloader dl = DataDownloader.getDataDownloader();

        List<MacroInvertebre> list = (List<MacroInvertebre>) dl.downloadsSectorsFromFile(file);
        List<String> sList = new ArrayList<>();

        List<String> testList = Arrays.asList("Baetidae", "Gammaridae", "Elmidae", "Simulidae", "Neretidae", "Glossiphoridae", "Elmidae", "Ephemeridae", "Gammaridae", "Hydropsychidae", "Sphaeridae", "Sericostomatidae", "Sphaeridae", "Ephemeridae", "Gammaridae", "Elmidae", "Oligochètes", "Heptagemidae", "Gammaridae", "Hydropsychidae", "Sericostomatidae", "Sphaeridae", "Oligochètes", "Calopterygidae", "Gammaridae", "Sphaeridae", "Elmidae", "Sphaeridae", "Gammaridae", "Elmidae", "Gammaridae", "Glossiphoridae", "Sphaeridae", "Oligochètes", "Sericostomatidae", "Ephemeridae", "Gammaridae", "Ephemeridae", "Sphaeridae");
        for(int i = 0; i < list.size();i++){
            sList.add(list.get(i).getTaxon());
        }
        Assert.assertEquals(testList,sList);
    }

    @Test
    public void testParsePhysicalChemistryFile(){
        ClassLoader classLoader = this.getClass().getClassLoader();
        URL resource = classLoader.getResource("physicochimie.json");
        File file = new File(resource.getPath());
        DataDownloader dl = DataDownloader.getDataDownloader();

        List<Physicochimie> list = (List<Physicochimie>)dl.downloadsSectorsFromFile(file);
        List<String> sList = new ArrayList<>();

        List<String> testList = Arrays.asList("0.2", "2.4", "0.87", "1.8", "0.2", "0.001", "0.494", "2.6", "0.0079", "15.7", "15.7", "0.0079", "2.4", "0.2", "0.87", "1.8", "0.2", "0.494", "2.6", "0.001");
        for(int i = 0; i < list.size();i++){
            sList.add(list.get(i).getRsana());
        }
        Assert.assertEquals(testList,sList);
    }

    @Test
    public void testErrParse() {
        ClassLoader classLoader = this.getClass().getClassLoader();
        URL resource = classLoader.getResource("testErrParsing.json");
        File file = new File(resource.getPath());
        DataDownloader dl = DataDownloader.getDataDownloader();

        List<? extends DisplayableData> list = dl.downloadsSectorsFromFile(file);
        List<? extends DisplayableData> testList = new ArrayList<>();
        //Affiche "Error while parsing amphibian json"
        Assert.assertEquals(testList, list);
    }

    @Test
    public void testErrCommonData() {
        ClassLoader classLoader = this.getClass().getClassLoader();
        URL resource = classLoader.getResource("testErrCommonData.json");
        File file = new File(resource.getPath());
        DataDownloader dl = DataDownloader.getDataDownloader();

        List<? extends DisplayableData> list = dl.downloadsSectorsFromFile(file);
        List<? extends DisplayableData> testList = new ArrayList<>();
        //Affiche "Error while getting amphibian's common data"
        Assert.assertEquals(testList, list);
    }

    @Test
    public void testErrAllAnalysis() {
        ClassLoader classLoader = this.getClass().getClassLoader();
        URL resource = classLoader.getResource("testErrAllAnalysis.json");
        File file = new File(resource.getPath());
        DataDownloader dl = DataDownloader.getDataDownloader();

        List<? extends DisplayableData> list = dl.downloadsSectorsFromFile(file);
        List<? extends DisplayableData> testList = new ArrayList<>();
        //Affiche "Error while getting all amphibian's analyzes"
        Assert.assertEquals(testList, list);
    }

    @Test
    public void testCenteredPosition(){
        ClassLoader classLoader = this.getClass().getClassLoader();
        URL resource = classLoader.getResource("amphibiens.json");
        File file = new File(resource.getPath());
        DataDownloader dl = DataDownloader.getDataDownloader();

        List<? extends DisplayableData> list = dl.downloadsSectorsFromFile(file);

        double testLongitude = (2.39299112080979 +2.39338729033225+2.39355993098004+2.39376031269732)/4;
        double testLatitude = (49.8193784786106+49.8193102391978+49.8191991615009+49.8192549457049)/4;
        Amphibien amp = (Amphibien) list.get(0);

        Assert.assertEquals(testLatitude,amp.getGeom().getLatitude(),0.0000000000001);
        Assert.assertEquals(testLongitude,amp.getGeom().getLongitude(),0.0000000000001);
    }

    @Test
    public void testMultipleObject(){
        ClassLoader classLoader = this.getClass().getClassLoader();
        URL resource = classLoader.getResource("eiko_amphibiens.json");
        File file = new File(resource.getPath());
        DataDownloader dl = DataDownloader.getDataDownloader();

        List<? extends DisplayableData> list = dl.downloadsSectorsFromFile(file);
        Amphibien amp1 = (Amphibien) list.get(0);
        Amphibien amp2 = (Amphibien) list.get(1);

        Assert.assertEquals("2b11",amp1.getStationCode());
        Assert.assertEquals("2b11bis",amp2.getStationCode());

        Assert.assertEquals("amphibien1",amp1.getTaxon());
        Assert.assertEquals("amphibien2",amp2.getTaxon());

        Assert.assertEquals(2,amp1.getTotalIndividu());
        Assert.assertEquals(3,amp2.getTotalIndividu());
    }

    @Test
    public void testDumpFlore(){

        ClassLoader classLoader = this.getClass().getClassLoader();
        URL resource = classLoader.getResource("dump_flore.json");
        File file = new File(resource.getPath());
        try {
            JSONArray jsonObject = DataDownloadedParser.getJsonFile(file);
            List<Flore> list = Flore.fromJSON(jsonObject);

            List<String> testList = Arrays.asList("109.2", "0.0", "0.0", "140.2", "140.2", "140.2" );
            List<String> sList = new ArrayList<>();

            for(int i = 0; i < list.size();i++){
                sList.add(String.valueOf(list.get(i).getGeom().getAltitude()));
            }
            Assert.assertEquals(testList,sList);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
