import com.eiko.DataObject.Perimeter;
import com.eiko.downloads.DataDownloadedParser;
import com.eiko.downloads.DataDownloader;

import junit.framework.Assert;

import org.json.JSONArray;
import org.json.JSONException;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rinkui on 16/02/2018.
 */


public class DataDownloadedParserTest {
    @Test
    public void parseNullTest(){
        DataDownloadedParser.readGeoDataJSON(null);
        DataDownloadedParser.readMetadataJSON(null);
        DataDownloadedParser.readPerimetersJSON(null, null);
        DataDownloadedParser.readTypePerimetersJSON(null);
        DataDownloadedParser.readTileJSON(null);
        DataDownloadedParser.readPerimetersJSON(null, null);
    }

    @Test
    public void parseEmptyFieldTest(){
        DataDownloadedParser.readPerimetersJSON(null, "");
        DataDownloadedParser.readPerimetersJSON(new JSONArray(), null);
        DataDownloadedParser.readPerimetersJSON(new JSONArray(), "");
        DataDownloadedParser.readGeoDataJSON(new JSONArray());
        DataDownloadedParser.readMetadataJSON(new JSONArray());
        DataDownloadedParser.readTypePerimetersJSON(new JSONArray());
        DataDownloadedParser.readTileJSON(new JSONArray());
        DataDownloadedParser.readPerimetersJSON(new JSONArray(), null);
        DataDownloadedParser.readPerimetersJSON(null, "");
        DataDownloadedParser.readPerimetersJSON(new JSONArray(), "");
    }

    @Test
    public void parseTypePerimetersTest(){
        String typePerimeters = "[[{\"type_perimeter\": \"parc_national_adhesion\", \"source_perimeter\": \"Parcs nationaux via data.gouv.fr, 2017\"}, {\"type_perimeter\": \"reserve_naturelle\", \"source_perimeter\": \"INPN 2017\"}, {\"type_perimeter\": \"departement\", \"source_perimeter\": \"Generated for Eiko\"}, {\"type_perimeter\": \"commune\", \"source_perimeter\": \"cog - 2017\"}, {\"type_perimeter\": \"parc_national_coeur\", \"source_perimeter\": \"Parcs nationaux via data.gouv.fr, 2017\"}]]";
        try {
            JSONArray perimetersArray = new JSONArray(typePerimeters);
            List<String> types = DataDownloadedParser.readTypePerimetersJSON(perimetersArray);
            ArrayList<String> toCompare = new ArrayList<>();
            toCompare.add("parc_national_coeur");
            toCompare.add( "reserve_naturelle");
            Assert.assertTrue(types.containsAll(toCompare));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void parsNamesPerimetersTest(){
        String namesPerimeters = "[[{\"query_code\": \"Vanoise\", \"type_perimeter\": \"parc_national_coeur\", \"source_perimeter\": \"Parcs nationaux via data.gouv.fr, 2017\"}, {\"query_code\": \"C\\u00e9vennes\", \"type_perimeter\": \"parc_national_coeur\", \"source_perimeter\": \"Parcs nationaux via data.gouv.fr, 2017\"}, {\"query_code\": \"Mercantour\", \"type_perimeter\": \"parc_national_coeur\", \"source_perimeter\": \"Parcs nationaux via data.gouv.fr, 2017\"}, {\"query_code\": \"Coeur du Parc national de Port-Cros\", \"type_perimeter\": \"parc_national_coeur\", \"source_perimeter\": \"Parcs nationaux via data.gouv.fr, 2017\"}, {\"query_code\": \"Calanques\", \"type_perimeter\": \"parc_national_coeur\", \"source_perimeter\": \"Parcs nationaux via data.gouv.fr, 2017\"}, {\"query_code\": \"Pyr\\u00e9n\\u00e9es\", \"type_perimeter\": \"parc_national_coeur\", \"source_perimeter\": \"Parcs nationaux via data.gouv.fr, 2017\"}, {\"query_code\": \"Ecrins\", \"type_perimeter\": \"parc_national_coeur\", \"source_perimeter\": \"Parcs nationaux via data.gouv.fr, 2017\"}]]";
        try {
            JSONArray namesArray = new JSONArray(namesPerimeters);
            List<Perimeter> types = DataDownloadedParser.readPerimetersJSON(namesArray, "parc_national_coeur");
            Perimeter perim = new Perimeter("parc_national_coeur","Vanoise", null);
            Perimeter perim2 = new Perimeter("parc_national_coeur", "Ecrins", null);
            Assert.assertTrue(types.contains(perim));
            Assert.assertTrue(types.contains(perim2));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void parseTileTest(){
        String tiles = "[[{\"query_code\": \"65013\", \"geom\": \"{\\\"type\\\":\\\"Polygon\\\",\\\"coordinates\\\":[[[\"0.095\",\"43.425\"],[\"0.095\",\"43.435\"],[\"0.105\",\"43.435\"],[\"0.105\",\"43.425\"],[\"0.095\",\"43.425\"]]]}\", \"type_perimeter\": \"commune\", \"source_perimeter\": \"cog - 2017\"}]]";
        tiles = tiles.replace("\"{", "{");
        tiles = tiles.replace("}\"", "}");
        tiles = tiles.replace("\\\"","\"");
        JSONArray namesArray = null;
        try {
            namesArray = new JSONArray(tiles);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        List<List<Double>> tile = DataDownloadedParser.readTileJSON(namesArray);
        List<Double> compareTo = new ArrayList<>();
        compareTo.add(0.095);
        compareTo.add(43.425);
        compareTo.add(0.095);
        compareTo.add(43.435);
        compareTo.add(0.105);
        compareTo.add(43.435);
        compareTo.add(0.105);
        compareTo.add(43.425);
        compareTo.add(0.095);
        compareTo.add(43.425);
        Assert.assertTrue(tile.contains(compareTo));
    }

    @Test
    public void parsePerimetersTest(){
        ClassLoader classLoader = this.getClass().getClassLoader();
        URL resource = classLoader.getResource("perimeters.json");
        File file = new File(resource.getPath());
        try {
            JSONArray jsonArray = DataDownloadedParser.getJsonFile(file);
            List<Perimeter> perimeters = DataDownloadedParser.readPerimetersJSON(jsonArray, "parc_national_coeur");
            Assert.assertTrue(perimeters.contains(new Perimeter("parc_national_coeur", "Vanoise", null)));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
