import com.eiko.downloads.DataDownloader;
import com.eiko.downloads.DataDownloadedParser;

import org.junit.Test;

import java.io.File;
import java.net.URL;

/**
 * Created by Rinkui on 09/02/2018.
 */

public class DownloaderAndDataFactoryTest {

    // le test doit passer sans problème. Même si le fichier n'est pas conforme ça ne doit pas faire crash l'appli
    @Test
    public void testUnsupportedMetaDataFile(){
        ClassLoader classLoader = this.getClass().getClassLoader();
        URL resource = classLoader.getResource("testUnsupportedMetaDataFile.json");
        File file = new File(resource.getPath());
        DataDownloader.getDataDownloader().downloadsSectorsFromFile(file);
    }

    @Test
    public void testEmptyFile(){
        ClassLoader classLoader = this.getClass().getClassLoader();
        URL resource = classLoader.getResource("testEmptyFile.json");
        File file = new File(resource.getPath());
        DataDownloader.getDataDownloader().downloadsSectorsFromFile(file);
    }

    @Test
    public void testNonJSONFile(){
        ClassLoader classLoader = this.getClass().getClassLoader();
        URL resource = classLoader.getResource("testNonJSONFile.json");
        File file = new File(resource.getPath());
        DataDownloader.getDataDownloader().downloadsSectorsFromFile(file);
    }

    @Test
    public void testEmptyJSONFile(){
        ClassLoader classLoader = this.getClass().getClassLoader();
        URL resource = classLoader.getResource("testEmptyJSONFile.json");
        File file = new File(resource.getPath());
        DataDownloader.getDataDownloader().downloadsSectorsFromFile(file);
    }

    @Test
    public void testUnformattedJSONFile(){
        ClassLoader classLoader = this.getClass().getClassLoader();
        URL resource = classLoader.getResource("testUnformattedJSONFile.json");
        File file = new File(resource.getPath());
        DataDownloader.getDataDownloader().downloadsSectorsFromFile(file);
    }


    @Test(expected = NullPointerException.class)
    public void testFile(){
        DataDownloader.getDataDownloader().downloadsSectorsFromFile(null);
    }
}