import com.eiko.DataObject.*;
import com.eiko.downloads.DataDownloader;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by eikouser on 19/02/26.
 */

public class ExteriorFilesTest {

    @Test
    public void testAmphibianFile() {

        ClassLoader classLoader = this.getClass().getClassLoader();
        URL resource = classLoader.getResource("test_amphibiens.json");
        File file = new File(resource.getPath());
        DataDownloader dl = DataDownloader.getDataDownloader();

        List<Amphibien> list = (List<Amphibien>) dl.downloadsSectorsFromFile(file);
        List<String> sList = new ArrayList<>();

        List<String> testList = Arrays.asList("amphibien1", "amphibien2", "amphibien3");

        for(int i = 0; i < list.size();i++){
            sList.add(list.get(i).getName());
        }
        Assert.assertEquals(testList,sList);
    }

    @Test
    public void testFloraFile(){
        ClassLoader classLoader = this.getClass().getClassLoader();
        URL resource = classLoader.getResource("test_flore_cbn.json");
        File file = new File(resource.getPath());
        DataDownloader dl = DataDownloader.getDataDownloader();

        List<Flore> list = (List<Flore>) dl.downloadsSectorsFromFile(file);
        List<String> sList = new ArrayList<>();

        List<String> testList = Arrays.asList("Fleur1", "Fleur4", "Fleur7");

        for(int i = 0; i < list.size();i++){
            sList.add(list.get(i).getName());
        }
        Assert.assertEquals(testList,sList);
    }

    @Test
    public void testHydrobiologyTaxFile(){
        ClassLoader classLoader = this.getClass().getClassLoader();
        URL resource = classLoader.getResource("test_hydrobiologieTax.json");
        File file = new File(resource.getPath());
        DataDownloader dl = DataDownloader.getDataDownloader();

        List<HydrobiologieTax> list = (List<HydrobiologieTax>) dl.downloadsSectorsFromFile(file);
        List<String> sList = new ArrayList<>();

        List<String> testList = Arrays.asList("Poisson1", "Poisson2", "Poisson3");

        for(int i = 0; i < list.size();i++){
            sList.add(list.get(i).getName());
        }
        Assert.assertEquals(testList,sList);
    }

    @Test
    public void testHydrobiologyParFile(){
        ClassLoader classLoader = this.getClass().getClassLoader();
        URL resource = classLoader.getResource("test_hydrobiologiePar.json");
        File file = new File(resource.getPath());
        DataDownloader dl = DataDownloader.getDataDownloader();

        List<HydrobiologiePar> list = (List<HydrobiologiePar>) dl.downloadsSectorsFromFile(file);
        List<String> sList = new ArrayList<>();

        List<String> testList = Arrays.asList("parametre 1", "parametre 2", "parametre 3");

        for(int i = 0; i < list.size();i++){
            sList.add(list.get(i).getName());
        }
        Assert.assertEquals(testList,sList);
    }

    @Test
    public void testInvertebrateFile(){
        ClassLoader classLoader = this.getClass().getClassLoader();
        URL resource = classLoader.getResource("test_invertebres.json");
        File file = new File(resource.getPath());
        DataDownloader dl = DataDownloader.getDataDownloader();

        List<Invertebre> list = (List<Invertebre>) dl.downloadsSectorsFromFile(file);
        List<String> sList = new ArrayList<>();

        List<String> testList = Arrays.asList("invertebre1", "invertebre2", "invertebre3");

        for(int i = 0; i < list.size();i++){
            sList.add(list.get(i).getName());
        }
        Assert.assertEquals(testList,sList);
    }

    @Test
    public void testMacroInvertebrateFile(){
        ClassLoader classLoader = this.getClass().getClassLoader();
        URL resource = classLoader.getResource("test_macroinvertebres.json");
        File file = new File(resource.getPath());
        DataDownloader dl = DataDownloader.getDataDownloader();

        List<MacroInvertebre> list = (List<MacroInvertebre>) dl.downloadsSectorsFromFile(file);
        List<String> sList = new ArrayList<>();

        List<String> testList = Arrays.asList("macroInvertebre1", "macroInvertebre2", "macroInvertebre3", "macroInvertebre4");

        for(int i = 0; i < list.size();i++){
            sList.add(list.get(i).getName());
        }
        Assert.assertEquals(testList,sList);
    }

    @Test
    public void testPhysicalChemistryFile(){
        ClassLoader classLoader = this.getClass().getClassLoader();
        URL resource = classLoader.getResource("test_physicochimie.json");
        File file = new File(resource.getPath());
        DataDownloader dl = DataDownloader.getDataDownloader();

        List<Physicochimie> list = (List<Physicochimie>)dl.downloadsSectorsFromFile(file);
        List<String> sList = new ArrayList<>();

        List<String> testList = Arrays.asList("physicochimie1", "physicochimie2", "physicochimie3");

        for(int i = 0; i < list.size();i++){
            sList.add(list.get(i).getName());
        }
        Assert.assertEquals(testList,sList);
    }
}
