import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.widget.ImageView;

import com.eiko.forms.PictureManagement;

import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
public class PictureManagementTest {
    private Context context;

    @Before
    public void onInit() {
        this.context = InstrumentationRegistry.getContext();
    }

    /**
     * Test isPictureTaken method with tag PictureManagement.REAL_PICTURE
     */
    @Test
    public void isPictureTaken_true() throws IOException, JSONException {
        ImageView iv = new ImageView(context);
        iv.setTag(PictureManagement.REAL_PICTURE);
        assertTrue(PictureManagement.isPictureTaggedReal(iv.getTag().toString()));
    }

    /**
     * Test isPictureTaken method with tag PictureManagement.DUMMY_PICTURE
     */
    @Test
    public void isPictureTaken_false() throws IOException, JSONException {
        ImageView iv = new ImageView(context);
        iv.setTag(PictureManagement.DUMMY_PICTURE);
        assertFalse(PictureManagement.isPictureTaggedReal(iv.getTag().toString()));
    }

    /**
     * Test isPictureTaken method with null argument
     */
    @Test
    public void isPictureTaken_null() throws IOException, JSONException {
      //  PictureManagement.isPictureTaggedReal(null);
    }

    /**
     * Test imageViewToBitmap method
     */
    @Test(expected=NullPointerException.class)
    public void imageViewToBitmap() throws IOException, JSONException {
        PictureManagement.imageViewToBitmap(null);
        ImageView picture = new ImageView(context);
        InputStream ims = context.getAssets().open("androidLogo.jpg");
        Drawable d = Drawable.createFromStream(ims, null);
        picture.setImageDrawable(d);
        picture.setTag(PictureManagement.REAL_PICTURE);
        Bitmap bitmap = PictureManagement.imageViewToBitmap(picture);
        assertNotNull(bitmap);
    }

    /**
     * Test imageViewToBitmap method with null argument
     */
    @Test(expected=NullPointerException.class)
    public void imageViewToBitmap_null() throws IOException, JSONException {
        PictureManagement.imageViewToBitmap(null);
    }

    /**
     * Test bitmapToString and stringToBitmap methods (reverse)
     */
    @Test
    public void bitmapToString_nominal() throws IOException {
        ImageView picture = new ImageView(context);
        InputStream ims = context.getAssets().open("androidLogo.png");
        Drawable d = Drawable.createFromStream(ims, null);
        picture.setImageDrawable(d);
        picture.setTag(PictureManagement.REAL_PICTURE);
        Bitmap bitmap = PictureManagement.imageViewToBitmap(picture);
        String imageString = PictureManagement.bitmapToString(bitmap);
        assertNotNull(imageString);
    }

    /**
     * Test bitmapToString method with null argument
     */
    @Test(expected=NullPointerException.class)
    public void bitmapToString_null() throws IOException, JSONException {
        PictureManagement.bitmapToString(null);
    }
}