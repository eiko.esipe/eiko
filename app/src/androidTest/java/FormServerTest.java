import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.eiko.forms.FormServer;

import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
public class FormServerTest {
    private Context context;

    @Before
    public void onInit() {
        this.context = InstrumentationRegistry.getTargetContext();
    }

    /**
     * Test readJSONConfFile method
     */
    @Test
    public void readJSONConfFile() throws IOException, JSONException {
        FormServer formServer = new FormServer(context);
        assertEquals("http://5.149.188.204/form_eiko/index.php", formServer.getUrl());
        assertEquals("OnemaDcie94300", formServer.getPassword());
    }
}