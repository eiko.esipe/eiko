import android.content.Context;
import android.support.test.InstrumentationRegistry;

import com.eiko.ARObjectManager;
import com.eiko.DataObject.*;
import com.eiko.database.*;
import com.eiko.geolocalization.PositionManager;
import com.eiko.matrix.ARObject;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.eiko.database.Database.importObjectsToDatabase;

/**
 * Created by eikouser on 19/02/18.
 */

public class ARObjectManagerTest {

    private Context context;


    @Before
    public void onInit() {

        this.context = InstrumentationRegistry.getTargetContext();

        //Amphibien
        Map<String, String> mAmp = new HashMap<>();
        mAmp.put("taxon", "amp");
        mAmp.put("latitude", "2.4193485920");
        mAmp.put("longitude", "42.194529682459");
        mAmp.put("station_code", "statest");
        mAmp.put("date_de_sortie", "12-12-2017");
        mAmp.put("total_ind", "42");

        //Flore
        Map<String, String> mFlo = new HashMap<>();
        mFlo.put("date_debut", "12-12-2017");
        mFlo.put("latitude", "2.4193485920");
        mFlo.put("longitude", "42.194529682459");
        mFlo.put("date_fin", "12-12-2017");
        mFlo.put("observation_rmq", "a pu");
        Map<String, List<String>> mlFlo = new HashMap<>();
        List<String> lFlo = new ArrayList<>();
        lFlo.add("flo");
        mlFlo.put("names_sp", lFlo);

        //HydrobiologiePar
        Map<String, String> mHyP = new HashMap<>();
        mHyP.put("altitude", "too high");
        mHyP.put("latitude", "2.4193485920");
        mHyP.put("longitude", "42.194529682459");
        mHyP.put("nomstation", "statest");
        mHyP.put("resultat", "maybe");
        mHyP.put("dtprodresultatbiologique", "12-12-2017");
        mHyP.put("preleveur", "Clement");
        mHyP.put("parametre", "no param");

        //HydrobiologieTax
        Map<String, String> mHyT = new HashMap<>();
        mHyT.put("altitude", "too low");
        mHyT.put("latitude", "2.4193485920");
        mHyT.put("longitude", "42.194529682459");
        mHyT.put("nomstation", "statest");
        mHyT.put("resultat", "watercooled");
        mHyT.put("dtprodresultatbiologique", "12-12-2017");
        mHyT.put("preleveur", "Clement");
        mHyT.put("taxon", "hyt poisson");

        //Invertebre
        Map<String, String> mInv = new HashMap<>();
        mInv.put("taxon", "inv");
        mInv.put("latitude", "2.4193485920");
        mInv.put("longitude", "42.194529682459");
        mInv.put("station_code", "statest");
        mInv.put("date_de_sortie", "12-12-2017");
        mInv.put("total_ind", "77");

        //MacroInvertebre
        Map<String, String> mMac = new HashMap<>();
        mMac.put("taxon", "mac");
        mMac.put("latitude", "0.0");
        mMac.put("longitude", "0.0");
        mMac.put("station_code", "statest");
        mMac.put("date_de_sortie", "12-12-2017");
        mMac.put("total_ind", "99");

        //Physicochimie
        Map<String, String> mPhy = new HashMap<>();
        mPhy.put("parametre", "phy");
        mPhy.put("latitude", "2.4193485920");
        mPhy.put("longitude", "42.194529682459");
        mPhy.put("station_nom", "statest");
        mPhy.put("lsana", "1.11");
        mPhy.put("ldana", "1.12");

        mPhy.put("rsana", "1.1");
        mPhy.put("laboratoire", "labo");
        mPhy.put("code_remarque", "rmq");
        mPhy.put("methode", "meth");
        mPhy.put("producteur", "prod");
        mPhy.put("methode", "meth");
        mPhy.put("lqana", "1.1");

        mPhy.put("dateprel", "12-12-2017");

        Amphibien amphibien = new Amphibien(mAmp);
        Flore flore = new Flore(mFlo, mlFlo);
        HydrobiologiePar hydroPar = new HydrobiologiePar(mHyP);
        HydrobiologieTax hydroTax = new HydrobiologieTax(mHyT);
        Invertebre invertebre = new Invertebre(mInv);
        MacroInvertebre macroinvertebre = new MacroInvertebre(mMac);
        Physicochimie physicochimie = new Physicochimie(mPhy);

        List<DisplayableData> list = new ArrayList<>();
        list.add(amphibien);
        list.add(flore);
        list.add(hydroPar);
        list.add(hydroTax);
        list.add(invertebre);
        list.add(macroinvertebre);
        list.add(physicochimie);
        importObjectsToDatabase(list, context);
    }

    @Test
    public void testUpdate() {
        PositionManager.initPositionManager(context);
        UserParamDB upd = new UserParamDB(context);
        PositionManager pm = PositionManager.getInstance();
        ARObjectManager arom = new ARObjectManager(pm, context);

        //arom.updateAR(0);
        Map<String,List<ARObject>> map = arom.getDisplayableMap();
        int taille=0;
        for (List<ARObject> list: map.values()) {
            taille+=list.size();
        }

        Assert.assertEquals(6, taille);
    }

    @Test
    public void testData(){
        PositionManager.initPositionManager(context);
        UserParamDB upd = new UserParamDB(context);
        PositionManager pm = PositionManager.getInstance();
        ARObjectManager arom = new ARObjectManager(pm, context);

        //arom.updateAR(0);
        Map<String,List<ARObject>> map = arom.getDisplayableMap();
        ARObject aro = map.get("flore").get(0);
        Flore flo = (Flore)aro.getData();

        Assert.assertEquals("a pu", flo.getObservation_rmq());
        Assert.assertEquals("flo", flo.getName());
    }

}
