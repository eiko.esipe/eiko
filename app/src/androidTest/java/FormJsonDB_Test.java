import android.content.Context;
import android.support.test.InstrumentationRegistry;

import com.eiko.database.FormJsonDB;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class FormJsonDB_Test {
    private FormJsonDB formDB;


    @Before
    public void onInit() {
        Context context = InstrumentationRegistry.getTargetContext();
        this.formDB = new FormJsonDB(context);
    }

    @Test
    public void insertJson() {
        String json = "{\"key1\":\"value1\"}";
        formDB.addJSON(json);
        assertEquals(json, formDB.getFirstJSON());
        formDB.deleteFirstJSON();
    }

    @Test
    public void doubleInsertNotSame() {
        String json = "{\"key\":\"value\"}";
        long id = formDB.addJSON(json);
        String json2 = "{\"key2\":\"value2\"}";
        long id2 = formDB.addJSON(json2);
        assertNotSame(id, id2);
        formDB.deleteFirstJSON();
        formDB.deleteFirstJSON();
    }

    @Test
    public void doubleInsertSameJson() {
        String json = "{\"key\":\"value\"}";
        long id = formDB.addJSON(json);
        long id2 = formDB.addJSON(json);
        assertNotEquals(id, id2);
        formDB.deleteFirstJSON();
        formDB.deleteFirstJSON();
    }

    @Test
    public void getFirstJson() {
        String json = "{\"key\":\"value\"}";
        formDB.addJSON(json);
        String getJson = formDB.getFirstJSON();
        assertEquals(json, getJson);
        formDB.deleteFirstJSON();
    }

    @Test
    public void getFirstJson_null() {
        assertEquals(null, formDB.getFirstJSON());
    }

    @Test
    public void deleteFirstJson() {
        String json = "{\"key\":\"value\"}";
        formDB.addJSON(json);
        formDB.deleteFirstJSON();
        assertEquals(null, formDB.getFirstJSON());
    }
}
