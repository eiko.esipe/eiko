import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.util.Log;

import com.eiko.ARObjectProvider;
import com.eiko.DataObject.*;
import com.eiko.database.*;
import com.eiko.geolocalization.Position;
import com.eiko.matrix.ARObject;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.eiko.database.Database.importObjectsToDatabase;


/**
 * Created by eikouser on 20/02/18.
 */

public class ARObjectProviderTest {
    private Context context;

    @Before
    public void onInit() {

        this.context = InstrumentationRegistry.getTargetContext();

        //Amphibien
        Map<String, String> mAmp = new HashMap<>();
        mAmp.put("taxon", "amp");
        mAmp.put("latitude", "2.4193485920");
        mAmp.put("longitude", "42.194529682459");
        mAmp.put("station_code", "statest");
        mAmp.put("date_de_sortie", "12-12-2017");
        mAmp.put("total_ind", "42");

        //Flore
        Map<String, String> mFlo = new HashMap<>();
        mFlo.put("date_debut", "12-12-2017");
        mFlo.put("latitude", "2.4193485920");
        mFlo.put("longitude", "42.194529682459");
        mFlo.put("date_fin", "12-12-2017");
        mFlo.put("observation_rmq", "a pu");
        Map<String, List<String>> mlFlo = new HashMap<>();
        List<String> lFlo = new ArrayList<>();
        lFlo.add("flo");
        mlFlo.put("names_sp", lFlo);

        //HydrobiologiePar
        Map<String, String> mHyP = new HashMap<>();
        mHyP.put("altitude", "too high");
        mHyP.put("latitude", "2.4193485920");
        mHyP.put("longitude", "42.194529682459");
        mHyP.put("nomstation", "statest");
        mHyP.put("resultat", "maybe");
        mHyP.put("dtprodresultatbiologique", "12-12-2017");
        mHyP.put("preleveur", "Clement");
        mHyP.put("parametre", "no param");

        //HydrobiologieTax
        Map<String, String> mHyT = new HashMap<>();
        mHyT.put("altitude", "too low");
        mHyT.put("latitude", "1.4193485920");
        mHyT.put("longitude", "42.194529682459");
        mHyT.put("nomstation", "statest");
        mHyT.put("resultat", "watercooled");
        mHyT.put("dtprodresultatbiologique", "12-12-2017");
        mHyT.put("preleveur", "Clement");
        mHyT.put("taxon", "hyt poisson");

        //Invertebre
        Map<String, String> mInv = new HashMap<>();
        mInv.put("taxon", "inv");
        mInv.put("latitude", "2.4193485920");
        mInv.put("longitude", "42.194529682459");
        mInv.put("station_code", "statest");
        mInv.put("date_de_sortie", "12-12-2017");
        mInv.put("total_ind", "77");

        //MacroInvertebre
        Map<String, String> mMac = new HashMap<>();
        mMac.put("taxon", "mac");
        mMac.put("latitude", "2.4193485920");
        mMac.put("longitude", "42.194529682459");
        mMac.put("station_code", "statest");
        mMac.put("date_de_sortie", "12-12-2017");
        mMac.put("total_ind", "99");

        //Physicochimie
        Map<String, String> mPhy = new HashMap<>();
        mPhy.put("parametre", "phy");
        mPhy.put("latitude", "3.4193485920");
        mPhy.put("longitude", "42.194529682459");
        mPhy.put("station_nom", "statest");
        mPhy.put("lsana", "1.11");
        mPhy.put("ldana", "1.12");

        mPhy.put("rsana", "1.1");
        mPhy.put("laboratoire", "labo");
        mPhy.put("code_remarque", "rmq");
        mPhy.put("methode", "meth");
        mPhy.put("producteur", "prod");
        mPhy.put("lqana", "1.1");

        mPhy.put("dateprel", "2017-12-12");

        Amphibien amphibien = new Amphibien(mAmp);
        Flore flore = new Flore(mFlo, mlFlo);
        HydrobiologiePar hydroPar = new HydrobiologiePar(mHyP);
        HydrobiologieTax hydroTax = new HydrobiologieTax(mHyT);
        Invertebre invertebre = new Invertebre(mInv);
        MacroInvertebre macroinvertebre = new MacroInvertebre(mMac);
        Physicochimie physicochimie = new Physicochimie(mPhy);

        List<DisplayableData> list = new ArrayList<>();
        list.add(amphibien);
        list.add(flore);
        list.add(hydroPar);
        list.add(hydroTax);
        list.add(invertebre);
        list.add(macroinvertebre);
        list.add(physicochimie);
        importObjectsToDatabase(list, context);
    }

    @Test
    public void testGetFromDatabase() {
        ARObjectProvider arop = new ARObjectProvider();
        Position position = new Position(2.4193485920, 42.194529682459);

        Map<String, List<? extends DisplayableData>> datas = arop.getFromDatabase(position, 100000000, context);
        int totalObject = 0;

        for (Map.Entry<String, List<? extends DisplayableData>> entry: datas.entrySet()) {
            Log.e("MAP", entry.getValue().toString());
        }

        for (String type : datas.keySet()) {
            totalObject += datas.get(type).size();
        }

        Assert.assertEquals(7, totalObject);
    }

    @Test
    public void testGetNotAllFromDatabase() {
        ARObjectProvider arop = new ARObjectProvider();
        Position position = new Position(2.4193485920, 42.194529682459);

        Map<String, List<? extends DisplayableData>> datas = arop.getFromDatabase(position, 10, context);
        int totalObject = 0;
        for (String type : datas.keySet()) {
            totalObject += datas.get(type).size();
        }

        Assert.assertEquals(7, totalObject);
    }


    public boolean testDataToARObject() {
        ARObjectProvider arop = new ARObjectProvider();
        Position position = new Position(2.4193485920, 42.194529682459);
        int bearing = 0;

        Map<String, List<? extends DisplayableData>> datas = arop.getFromDatabase(position, 100000000, context);
        Position pos = new Position(0, 0);
        Map<String, List<ARObject>> ARList = arop.dataToARObject(datas, bearing, position);
        BitSet bs = new BitSet(7);
        List<Integer> tab = new ArrayList<>(7);//7

        for (String type : ARList.keySet()) {
            for (int i = 0; i < ARList.get(type).size(); i++) {
                Log.e("Toast", ARList.get(type).get(i).getData().toString());
                switch (ARList.get(type).get(i).getData().getClass().getName()) {
                    case "com.eiko.DataObject.Amphibien":
                        if (bs.get(0)) {
                            Log.e("PROVIDER", "bs Amphibien");
                            return false;
                        }
                        bs.set(0);
                        break;
                    case "com.eiko.DataObject.Invertebre":
                        if (bs.get(1)) {
                            Log.e("PROVIDER", "bs Invertebre");
                            return false;
                        }
                        bs.set(1);
                        break;
                    case "com.eiko.DataObject.MacroInvertebre":
                        if (bs.get(2)) {
                            Log.e("PROVIDER", "bs MacroInvertebre");
                            return false;
                        }
                        bs.set(2);
                        break;
                    case "com.eiko.DataObject.Flore":
                        if (bs.get(3)) {
                            Log.e("PROVIDER", "bs Flore");
                            return false;
                        }
                        bs.set(3);
                        break;
                    case "com.eiko.DataObject.HydrobiologiePar":
                        if (bs.get(4)) {
                            Log.e("PROVIDER", "bs HydrobiologiePar");
                            return false;
                        }
                        bs.set(4);
                        break;
                    case "com.eiko.DataObject.HydrobiologieTax":
                        if (bs.get(5)) {
                            Log.e("PROVIDER", "bs HydrobiologieTax");
                            return false;
                        }
                        bs.set(5);
                        break;
                    case "com.eiko.DataObject.Physicochimie":
                        if (bs.get(6)) {
                            Log.e("PROVIDER", "bs Physicochimie");
                            return false;
                        }
                        bs.set(6);
                        break;
                }
            }
        }

        Log.e("PROVIDER", "Cardinality" + bs.cardinality());
        if (bs.cardinality() != 7)
            return false;
        return true;
    }

    @Test
    public void DataToARObject() {
        Assert.assertEquals(true, testDataToARObject());
    }
}
