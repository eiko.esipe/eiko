import android.content.Context;
import android.support.test.InstrumentationRegistry;

import com.eiko.geolocalization.Position;
import com.eiko.database.PositionDB;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by axelheine on 13/02/2018.
 */

public class PositionDB_Test {
    private Context context;
    private PositionDB pdb;


    @Before
    public void onInit() {
        this.context = InstrumentationRegistry.getTargetContext();
        this.pdb = new PositionDB(context);
    }

    @After
    public void onClose() {
        pdb.close();
    }

    @Test
    public void test_InsertObject() {
        Position position = new Position(2.2343624562, 42.2462457356, 100);

        long id = pdb.insert(position);

        Assert.assertTrue("ID sup 0", id > 0);
    }

    @Test
    public void test_DoubleInsertNotSame() {
        Position position = new Position(2.2343624562, 42.2462457356, 100);
        Position position2 = new Position(42.2462457356, 2.2343624562, 105);

        long id = pdb.insert(position);

        long id2 = pdb.insert(position2);

        Assert.assertNotSame(id, id2);
    }

    @Test
    public void test_DoubleInsertSameObject() {
        Position position = new Position(2.234362456342, 42.2462454567356, 110);

        long id = pdb.insert(position);

        long id2 = pdb.insert(position);

        Assert.assertEquals(id, id2);
    }

    @Test
    public void test_GetObject() {
        Position position = new Position(2.234362456342, 42.2462454567356, 100);

        long id = pdb.insert(position);

        Position gettedPosition = pdb.select(id);

        Assert.assertTrue(position.getLatitude() == gettedPosition.getLatitude()
                                && position.getLongitude() == gettedPosition.getLongitude()
                                && position.getAltitude() == gettedPosition.getAltitude());
    }
}
