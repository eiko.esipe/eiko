import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.eiko.database.UserParamDB;

import org.json.JSONException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class UserParamDB_Test {
    private UserParamDB userParamDB;

    @Before
    public void onInit() {
        Context context = InstrumentationRegistry.getTargetContext();
        this.userParamDB = new UserParamDB(context);
    }

    @After
    public void onClose() {
        userParamDB.close();
    }

    /**
     * Test toggle method for Hydrobiologie
     */
    @Test
    public void toggleHydrobiologie() throws IOException, JSONException {
        boolean initValue = userParamDB.getHydrobiologie();
        boolean newValue = userParamDB.toggleHydrobiologie();
        assertNotEquals(initValue, newValue);
        boolean newValue2 = userParamDB.toggleHydrobiologie();
        assertNotEquals(newValue, newValue2);
        assertEquals(initValue, newValue2);
    }

    /**
     * Test toggle method for Flore
     */
    @Test
    public void toggleFlore() throws IOException, JSONException {
        boolean initValue = userParamDB.getFlore();
        boolean newValue = userParamDB.toggleFlore();
        assertNotEquals(initValue, newValue);
        boolean newValue2 = userParamDB.toggleFlore();
        assertNotEquals(newValue, newValue2);
        assertEquals(initValue, newValue2);
    }

    /**
     * Test toggle method for Biologie
     */
    @Test
    public void toggleBiologie() throws IOException, JSONException {
        boolean initValue = userParamDB.getBiologie();
        boolean newValue = userParamDB.toggleBiologie();
        assertNotEquals(initValue, newValue);
        boolean newValue2 = userParamDB.toggleBiologie();
        assertNotEquals(newValue, newValue2);
        assertEquals(initValue, newValue2);
    }

    /**
     * Test toggle method for Physicochimie
     */
    @Test
    public void togglePhysicochimie() throws IOException, JSONException {
        boolean initValue = userParamDB.getPhysicochimie();
        boolean newValue = userParamDB.togglePhysicochimie();
        assertNotEquals(initValue, newValue);
        boolean newValue2 = userParamDB.togglePhysicochimie();
        assertNotEquals(newValue, newValue2);
        assertEquals(initValue, newValue2);
    }

    /**
     * Test setUserMail method
     */
    @Test
    public void setUserMail() throws IOException, JSONException {
        String email = "abc@gmail.com";
        userParamDB.setUserMail(email);
        assertEquals(email, userParamDB.getUserMail());
        assertTrue(userParamDB.isUserLogged());
        userParamDB.deleteUserMail();
        assertEquals(null, userParamDB.getUserMail());
    }

    /**
     * Test setUserMail method with null
     */
    @Test
    public void setUserMail_null() throws IOException, JSONException {
        userParamDB.setUserMail(null);
        assertEquals(null, userParamDB.getUserMail());
        assertFalse(userParamDB.isUserLogged());
        userParamDB.deleteUserMail();
        assertEquals(null, userParamDB.getUserMail());
    }

    /**
     * Test setUserMail method with empty mail
     */
    @Test
    public void setUserMail_empty() throws IOException, JSONException {
        userParamDB.setUserMail("");
        assertEquals(null, userParamDB.getUserMail());
        assertFalse(userParamDB.isUserLogged());
        userParamDB.deleteUserMail();
        assertEquals(null, userParamDB.getUserMail());
    }

    /**
     * Test setUserMail method with incorrect mail
     */
    @Test
    public void setUserMail_incorrectMail() throws IOException, JSONException {
        userParamDB.setUserMail("incorrect");
        assertEquals(null, userParamDB.getUserMail());
        assertFalse(userParamDB.isUserLogged());
        userParamDB.deleteUserMail();
        assertEquals(null, userParamDB.getUserMail());
    }

    /**
     * Test setUserMail method with another incorrect mail
     */
    @Test
    public void setUserMail_incorrectMail2() throws IOException, JSONException {
        userParamDB.setUserMail("incorrect@");
        assertEquals(null, userParamDB.getUserMail());
        assertFalse(userParamDB.isUserLogged());
        userParamDB.deleteUserMail();
        assertEquals(null, userParamDB.getUserMail());
    }
}