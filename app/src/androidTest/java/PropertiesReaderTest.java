import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.eiko.PropertiesReader;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
/**
 * Created by Rinkui on 12/02/2018.
 *
 * Test la classe PropertiesReader
 */

@RunWith(AndroidJUnit4.class)
public class PropertiesReaderTest {
    @Test
    public void testExistingProperty(){
        Context mContext = InstrumentationRegistry.getContext();
        PropertiesReader.getProReader(mContext);
        Assert.assertEquals("eikoplus", PropertiesReader.getProReader().getProperty(PropertiesReader.SERVER_APPNAME));
    }

    @Test
    public void testInexistingProperty(){
        Context mContext = InstrumentationRegistry.getContext();
        PropertiesReader.getProReader(mContext);
        Assert.assertEquals(null, PropertiesReader.getProReader().getProperty("toto"));
    }

    @Test
    public void testNullContext(){
        PropertiesReader.resetPropertiesReader();
        Context mContext = InstrumentationRegistry.getContext();
        Assert.assertEquals(null, PropertiesReader.getProReader(null));
    }

    @Test
    public void testNoneContext(){
        PropertiesReader.resetPropertiesReader();
        Context mContext = InstrumentationRegistry.getContext();
        Assert.assertEquals(null, PropertiesReader.getProReader());
    }
}
