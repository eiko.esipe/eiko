import android.content.Context;
import android.support.test.InstrumentationRegistry;

import com.eiko.DataObject.Invertebre;
import com.eiko.geolocalization.Position;
import com.eiko.database.InvertebreDB;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by axelheine on 13/02/2018.
 */

public class InvertebreDB_test {
    private Context context;
    private InvertebreDB idb;


    @Before
    public void onInit() {
        this.context = InstrumentationRegistry.getTargetContext();
        this.idb = new InvertebreDB(context);
    }

    @After
    public void onClose() {
        idb.close();
    }

    @Test
    public void test_InsertObject() {
        Map<String, String> mapParamsInvertebre = new HashMap<>();
        mapParamsInvertebre.put("taxon", "test");
        mapParamsInvertebre.put("latitude", "2.4193485920");
        mapParamsInvertebre.put("longitude", "42.194529682459");
        mapParamsInvertebre.put("station_code", "statest");
        mapParamsInvertebre.put("date_de_sortie", "12-12-2017");
        mapParamsInvertebre.put("total_ind", "42");

        Invertebre invertebre = new Invertebre(mapParamsInvertebre);

        long id = idb.insert(invertebre);

        Assert.assertTrue("ID sup 0", id > 0);
    }

    @Test
    public void test_DoubleInsertNotSame() {
        Map<String, String> mapParamsInvertebre = new HashMap<>();
        mapParamsInvertebre.put("taxon", "test");
        mapParamsInvertebre.put("latitude", "2.4193485920");
        mapParamsInvertebre.put("longitude", "42.194529682459");
        mapParamsInvertebre.put("station_code", "statest");
        mapParamsInvertebre.put("date_de_sortie", "12-12-2017");
        mapParamsInvertebre.put("total_ind", "42");

        Map<String, String> mapParamsInvertebre2 = new HashMap<>();
        mapParamsInvertebre2.put("taxon", "test2");
        mapParamsInvertebre2.put("latitude", "2.4193485920");
        mapParamsInvertebre2.put("longitude", "42.194529682459");
        mapParamsInvertebre2.put("station_code", "statest");
        mapParamsInvertebre2.put("date_de_sortie", "12-12-2017");
        mapParamsInvertebre2.put("total_ind", "43");

        Invertebre invertebre = new Invertebre(mapParamsInvertebre);
        Invertebre invertebre2 = new Invertebre(mapParamsInvertebre2);

        long id = idb.insert(invertebre);

        long id2 = idb.insert(invertebre2);

        System.out.println(id + " " + id2);

        Assert.assertNotSame(id, id2);
    }

    @Test
    public void test_DoubleInsertSameObject() {
        Map<String, String> mapParamsInvertebre = new HashMap<>();
        mapParamsInvertebre.put("taxon", "test");
        mapParamsInvertebre.put("latitude", "2.4193485920");
        mapParamsInvertebre.put("longitude", "42.194529682459");
        mapParamsInvertebre.put("station_code", "statest");
        mapParamsInvertebre.put("date_de_sortie", "12-12-2017");
        mapParamsInvertebre.put("total_ind", "42");

        Invertebre invertebre = new Invertebre(mapParamsInvertebre);

        long id = idb.insert(invertebre);

        long id2 = idb.insert(invertebre);

        Assert.assertEquals(id, id2);
    }

    @Test
    public void test_GetObject() {
        Map<String, String> mapParamsInvertebre = new HashMap<>();
        mapParamsInvertebre.put("taxon", "test3");
        mapParamsInvertebre.put("latitude", "2.4193485920");
        mapParamsInvertebre.put("longitude", "42.194529682459");
        mapParamsInvertebre.put("station_code", "statest");
        mapParamsInvertebre.put("date_de_sortie", "12-12-2017");
        mapParamsInvertebre.put("total_ind", "42");

        Invertebre invertebre = new Invertebre(mapParamsInvertebre);

        long id = idb.insert(invertebre);

        Invertebre gettedInvertebre = idb.select(id);

        Assert.assertTrue(invertebre.getTaxon().equals(gettedInvertebre.getTaxon()));
    }

    @Test
    public void test_GetAllAtDistance() {
        //Creation de deux objets avec positions differentes
        Map<String, String> mapParamsInvertebre = new HashMap<>();
        mapParamsInvertebre.put("taxon", "testDistanceProche");
        mapParamsInvertebre.put("latitude", "9.4193485920");
        mapParamsInvertebre.put("longitude", "42.194529682459");
        mapParamsInvertebre.put("station_code", "statest");
        mapParamsInvertebre.put("date_de_sortie", "12-12-2017");
        mapParamsInvertebre.put("total_ind", "42");

        Invertebre invertebre = new Invertebre(mapParamsInvertebre);
        idb.insert(invertebre);

        Map<String, String> mapParamsInvertebreLoin = new HashMap<>();
        mapParamsInvertebreLoin.put("taxon", "testDistanceLoin");
        mapParamsInvertebreLoin.put("latitude", "2.4193485920");
        mapParamsInvertebreLoin.put("longitude", "42.194529682459");
        mapParamsInvertebreLoin.put("station_code", "statest");
        mapParamsInvertebreLoin.put("date_de_sortie", "12-12-2017");
        mapParamsInvertebreLoin.put("total_ind", "42");

        Invertebre invertebreLoin = new Invertebre(mapParamsInvertebreLoin);
        idb.insert(invertebreLoin);

        Position searchPosition = new Position(9.4193485920, 42.194529682459);
        List<Invertebre> list = idb.getAllInvertebreAtDistance(searchPosition, 100);

        for(Invertebre i: list) {
            Assert.assertEquals(i.getTaxon(), invertebre.getTaxon());
        }

    }
}
