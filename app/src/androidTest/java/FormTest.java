import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.widget.ImageView;

import com.eiko.forms.PictureManagement;
import com.eiko.forms.Form;
import com.eiko.geolocalization.Position;

import org.hamcrest.CoreMatchers;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertThat;

@RunWith(AndroidJUnit4.class)
public class FormTest {
    private Context context;

    @Before
    public void onInit() {
        this.context = InstrumentationRegistry.getContext();
    }

    /**
     * Test generateFormFromUI method when the picture is default one
     */
    @Test
    public void generateFormFromUI_dummyPicture() throws IOException {
        String userMail = "userMail";
        String password = "password";
        String comment = "comment";

        ImageView picture = new ImageView(context);
        InputStream ims = context.getAssets().open("androidLogo.png");
        Drawable d = Drawable.createFromStream(ims, null);
        picture.setImageDrawable(d);
        picture.setTag(PictureManagement.DUMMY_PICTURE);
        String imageString = PictureManagement.getImageString(picture);

        Position position = new Position(2.0, 4.0);
        String station = "station";
        Form form = Form.generateFormFromUI(userMail, password, comment, imageString, position, station);

        assertEquals("userMail", form.getUserMail());
        assertEquals("password", form.getPassword());
        assertEquals("comment", form.getComment());
        assertEquals("", form.getPicture());
        assertEquals(2.0, form.getPosition().getLatitude(), 0);
        assertEquals(4.0, form.getPosition().getLongitude(), 0);
        assertEquals("station", form.getStation());
    }

    /**
     * Test generateFormFromUI method for a real picture
     */
    @Test
    public void generateFormFromUI_realPicture() throws IOException {
        String userMail = "userMail";
        String password = "password";
        String comment = "comment";

        ImageView picture = new ImageView(context);
        InputStream ims = context.getAssets().open("androidLogo.png");
        Drawable d = Drawable.createFromStream(ims, null);
        picture.setImageDrawable(d);
        picture.setTag(PictureManagement.REAL_PICTURE);
        String imageString = PictureManagement.getImageString(picture);

        Position position = new Position(2.0, 4.0);
        String station = "station";
        Form form = Form.generateFormFromUI(userMail, password, comment, imageString, position, station);

        assertEquals("userMail", form.getUserMail());
        assertEquals("password", form.getPassword());
        assertEquals("comment", form.getComment());
        assertNotEquals("", form.getPicture());
        assertEquals(2.0, form.getPosition().getLatitude(), 0);
        assertEquals(4.0, form.getPosition().getLongitude(), 0);
        assertEquals("station", form.getStation());
    }

    /**
     * Test generateFormFromJSON method when the picture is default one
     */
    @Test
    public void generateFormFromJSON_dummyPicture() throws JSONException {
        String jsonString = "{\"user\":\"userMail\"," +
                "\"password\":\"password\"," +
                "\"comment\":\"comment\"," +
                "\"gps\":[4.0,2.0]," +
                "\"station\":\"station\"," +
                "\"picture\":\"\"}";
        JSONObject json = new JSONObject(jsonString);
        Form form = Form.generateFormFromJSON(json);

        assertEquals("userMail", form.getUserMail());
        assertEquals("password", form.getPassword());
        assertEquals("comment", form.getComment());
        assertEquals("", form.getPicture());
        assertEquals(2.0, form.getPosition().getLatitude(), 0);
        assertEquals(4.0, form.getPosition().getLongitude(), 0);
        assertEquals("station", form.getStation());
    }

    /**
     * Test generateFormFromJSON method for a real picture
     */
    @Test
    public void generateFormFromJSON_realPicture() throws JSONException {
        String jsonString = "{\"user\":\"userMail\"," +
                "\"password\":\"password\"," +
                "\"comment\":\"comment\"," +
                "\"gps\":[4.0,2.0]," +
                "\"station\":\"station\"," +
                "\"picture\":\"pictureCode\"}";
        JSONObject json = new JSONObject(jsonString);
        Form form = Form.generateFormFromJSON(json);

        assertEquals("userMail", form.getUserMail());
        assertEquals("password", form.getPassword());
        assertEquals("comment", form.getComment());
        assertNotEquals("", form.getPicture());
        assertEquals(2.0, form.getPosition().getLatitude(), 0);
        assertEquals(4.0, form.getPosition().getLongitude(), 0);
        assertEquals("station", form.getStation());
    }

    /**
     * Test generateFormFromJSON method with incorrect JSON
     */
    @Test(expected = JSONException.class)
    public void generateFormFromJSON_wrongJSON() throws JSONException {
        String jsonString = "{\"user\":\"userMail\"," +
                "\"password\":\"password\"," +
                "\"comment\":\"comment\"," +
                "\"gps\":[4.0,2.0]," +
                "\"station\":\"station\"," +
                "\"picture\"}";
        JSONObject json = new JSONObject(jsonString);
        Form.generateFormFromJSON(json);
    }

    /**
     * Test toString method for a dummy picture
     */
    @Test
    public void toString_dummyPicture() throws IOException {
        String user = "userMail";
        String password = "password";
        String comment = "comment";

        ImageView picture = new ImageView(context);
        InputStream ims = context.getAssets().open("androidLogo.png");
        Drawable d = Drawable.createFromStream(ims, null);
        picture.setImageDrawable(d);
        picture.setTag(PictureManagement.DUMMY_PICTURE);
        String imageString = PictureManagement.getImageString(picture);

        Position position = new Position(2.0, 4.0);
        String station = "station";
        Form form = Form.generateFormFromUI(user, password, comment, imageString, position, station);

        String expected = "{\"user\":\"userMail\"," +
                "\"password\":\"password\"," +
                "\"comment\":\"comment\"," +
                "\"gps\":[4.0,2.0]," +
                "\"station\":\"station\"," +
                "\"picture\":\"\"}";
        assertEquals(expected, form.toJsonString());
    }

    /**
     * Test toString method for a real picture
     */
    @Test
    public void toString_realPicture() throws IOException {
        String user = "userMail";
        String password = "password";
        String comment = "comment";

        ImageView picture = new ImageView(context);
        InputStream ims = context.getAssets().open("androidLogo.png");
        Drawable d = Drawable.createFromStream(ims, null);
        picture.setImageDrawable(d);
        picture.setTag(PictureManagement.REAL_PICTURE);
        String imageString = PictureManagement.getImageString(picture);

        Position position = new Position(2.0, 4.0);
        String station = "station";
        Form form = Form.generateFormFromUI(user, password, comment, imageString, position, station);

        String expectedStart = "{\"user\":\"userMail\"," +
                "\"password\":\"password\"," +
                "\"comment\":\"comment\"," +
                "\"gps\":[4.0,2.0]," +
                "\"station\":\"station\"," +
                "\"picture\":\"";
        assertThat(form.toJsonString(), CoreMatchers.containsString(expectedStart));
    }

    /**
     * Test toJSON method for a dummy picture
     * @throws JSONException if problem in the JSON
     */
    @Test
    public void toJSON_dummyPicture() throws JSONException, IOException {
        String user = "userMail";
        String password = "password";
        String comment = "comment";

        ImageView picture = new ImageView(context);
        InputStream ims = context.getAssets().open("androidLogo.png");
        Drawable d = Drawable.createFromStream(ims, null);
        picture.setImageDrawable(d);
        picture.setTag(PictureManagement.DUMMY_PICTURE);
        String imageString = PictureManagement.getImageString(picture);

        Position position = new Position(2.0, 4.0);
        String station = "station";
        Form form = Form.generateFormFromUI(user, password, comment, imageString, position, station);

        String expected = "{\"user\":\"userMail\"," +
                "\"password\":\"password\"," +
                "\"comment\":\"comment\"," +
                "\"gps\":[4.0,2.0]," +
                "\"station\":\"station\"," +
                "\"picture\":\"\"}";
        assertEquals(expected, form.toJsonString());
    }

    /**
     * Test toJSON method for a real picture
     * @throws JSONException if problem in the JSON
     */
    @Test
    public void toJSON_realPicture() throws JSONException, IOException {
        String user = "userMail";
        String password = "password";
        String comment = "comment";

        ImageView picture = new ImageView(context);
        InputStream ims = context.getAssets().open("androidLogo.png");
        Drawable d = Drawable.createFromStream(ims, null);
        picture.setImageDrawable(d);
        picture.setTag(PictureManagement.REAL_PICTURE);
        String imageString = PictureManagement.getImageString(picture);

        Position position = new Position(2.0, 4.0);
        String station = "station";
        Form form = Form.generateFormFromUI(user, password, comment, imageString, position, station);

        String expectedStart = "{\"user\":\"userMail\"," +
                "\"password\":\"password\"," +
                "\"comment\":\"comment\"," +
                "\"gps\":[4.0,2.0]," +
                "\"station\":\"station\"," +
                "\"picture\":\"";
        assertThat(form.toJsonString(), CoreMatchers.containsString(expectedStart));
    }
}