import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.eiko.DataObject.Amphibien;
import com.eiko.DataObject.DisplayableData;
import com.eiko.database.Database;
import com.eiko.geolocalization.Position;
import com.eiko.database.AmphibienDB;
import com.eiko.geolocalization.Position;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by axelheine on 12/02/2018.
 */
@RunWith(AndroidJUnit4.class)
public class AmphibienDB_Test {
    private Context context;
    private AmphibienDB adb;


    @Before
    public void onInit() {
        this.context = InstrumentationRegistry.getTargetContext();
        this.adb = new AmphibienDB(context);
    }

    //@After
    //public void onClose() {
    //    adb.close();
    //}

    @Test
    public void test_InsertObject() {
        Map<String, String> mapParamsAmphibien = new HashMap<>();
        mapParamsAmphibien.put("taxon", "test");
        mapParamsAmphibien.put("latitude", "2.4193485920");
        mapParamsAmphibien.put("longitude", "42.194529682459");
        mapParamsAmphibien.put("station_code", "statest");
        mapParamsAmphibien.put("date_de_sortie", "12-12-2017");
        mapParamsAmphibien.put("total_ind", "42");

        Amphibien amphibien = new Amphibien(mapParamsAmphibien);

        long id = adb.insert(amphibien);

        Assert.assertTrue("ID sup 0", id > 0);
    }

    @Test
    public void test_DoubleInsertNotSame() {
        Map<String, String> mapParamsAmphibien = new HashMap<>();
        mapParamsAmphibien.put("taxon", "test");
        mapParamsAmphibien.put("latitude", "2.4193485920");
        mapParamsAmphibien.put("longitude", "42.194529682459");
        mapParamsAmphibien.put("station_code", "statest");
        mapParamsAmphibien.put("date_de_sortie", "12-12-2017");
        mapParamsAmphibien.put("total_ind", "42");

        Map<String, String> mapParamsAmphibien2 = new HashMap<>();
        mapParamsAmphibien2.put("taxon", "test2");
        mapParamsAmphibien2.put("latitude", "2.4193485920");
        mapParamsAmphibien2.put("longitude", "42.194529682459");
        mapParamsAmphibien2.put("station_code", "statest");
        mapParamsAmphibien2.put("date_de_sortie", "12-12-2017");
        mapParamsAmphibien2.put("total_ind", "43");

        Amphibien amphibien = new Amphibien(mapParamsAmphibien);
        Amphibien amphibien2 = new Amphibien(mapParamsAmphibien2);

        long id = adb.insert(amphibien);

        long id2 = adb.insert(amphibien2);

        System.out.println(id + " " + id2);

        Assert.assertNotSame(id, id2);
    }

    @Test
    public void test_DoubleInsertSameObject() {
        Map<String, String> mapParamsAmphibien = new HashMap<>();
        mapParamsAmphibien.put("taxon", "test");
        mapParamsAmphibien.put("latitude", "2.4193485920");
        mapParamsAmphibien.put("longitude", "42.194529682459");
        mapParamsAmphibien.put("station_code", "statest");
        mapParamsAmphibien.put("date_de_sortie", "12-12-2017");
        mapParamsAmphibien.put("total_ind", "42");

        Amphibien amphibien = new Amphibien(mapParamsAmphibien);

        long id = adb.insert(amphibien);

        long id2 = adb.insert(amphibien);

        Assert.assertEquals(id, id2);
    }

    @Test
    public void test_GetObject() {
        Map<String, String> mapParamsAmphibien = new HashMap<>();
        mapParamsAmphibien.put("taxon", "test3");
        mapParamsAmphibien.put("latitude", "2.4193485920");
        mapParamsAmphibien.put("longitude", "42.194529682459");
        mapParamsAmphibien.put("station_code", "statest");
        mapParamsAmphibien.put("date_de_sortie", "12-12-2017");
        mapParamsAmphibien.put("total_ind", "42");

        Amphibien amphibien = new Amphibien(mapParamsAmphibien);

        long id = adb.insert(amphibien);

        Amphibien gettedAmphibien = adb.select(id);

        Assert.assertTrue(amphibien.getTaxon().equals(gettedAmphibien.getTaxon()));
    }

    @Test
    public void test_GetAllAtDistance() {
        Map<String, String> mapParamsAmphibien = new HashMap<>();
        mapParamsAmphibien.put("taxon", "testDistance");
        mapParamsAmphibien.put("latitude", "2.4193485920");
        mapParamsAmphibien.put("longitude", "42.194529682459");
        mapParamsAmphibien.put("station_code", "statest");
        mapParamsAmphibien.put("date_de_sortie", "12-12-2017");
        mapParamsAmphibien.put("total_ind", "42");

        Amphibien amphibien = new Amphibien(mapParamsAmphibien);
        adb.insert(amphibien);

        Map<String, String> mapParamsAmphibienLoin = new HashMap<>();
        mapParamsAmphibienLoin.put("taxon", "testDistance");
        mapParamsAmphibienLoin.put("latitude", "9.4193485920");
        mapParamsAmphibienLoin.put("longitude", "42.194529682459");
        mapParamsAmphibienLoin.put("station_code", "statest");
        mapParamsAmphibienLoin.put("date_de_sortie", "12-12-2017");
        mapParamsAmphibienLoin.put("total_ind", "42");

        Amphibien amphibienLoin = new Amphibien(mapParamsAmphibienLoin);
        adb.insert(amphibienLoin);

        Position searchPosition = new Position(9.4193485920, 42.194529682459);
        List<Amphibien> la = adb.getAllAmphibiensAtDistance(searchPosition, 100);
        printMap(Database.getAllObjectsAtDistanceByType(searchPosition,100, context));

        Assert.assertEquals(la.get(0).getTaxon(), amphibienLoin.getTaxon());
    }

    public static void printMap(Map<String, List<? extends DisplayableData>> objsFromDB){
        Log.e("ARTest", "Entering printMap");
        for (String type : objsFromDB.keySet()){
            for(DisplayableData data : objsFromDB.get(type)){
                System.out.println((data.toString()));
            }
        }
    }
}
