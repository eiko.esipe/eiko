import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.eiko.DataObject.Amphibien;
import com.eiko.DataObject.DisplayableData;
import com.eiko.DataObject.Flore;
import com.eiko.DataObject.HydrobiologiePar;
import com.eiko.DataObject.HydrobiologieTax;
import com.eiko.DataObject.Invertebre;
import com.eiko.DataObject.MacroInvertebre;
import com.eiko.DataObject.Physicochimie;
import com.eiko.database.AmphibienDB;
import com.eiko.database.Database;
import com.eiko.database.PhysicochimieDB;
import com.eiko.database.UserParamDB;
import com.eiko.geolocalization.Position;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by axelheine on 21/02/2018.
 */

@RunWith(AndroidJUnit4.class)
public class DatabaseTest {
    private Context context;
    private SQLiteDatabase db;

    @Before
    public void onInit() {
        this.context = InstrumentationRegistry.getTargetContext();
        this.db = Database.getInstance(context).getWritableDatabase();
    }

    @Test
    public void addObjectWithParkName() {
        Map<String, String> mapParamsAmphibien = new HashMap<>();
        mapParamsAmphibien.put("taxon", "amphibien proche");
        mapParamsAmphibien.put("latitude", "3.435");
        mapParamsAmphibien.put("longitude", "48.423452");
        mapParamsAmphibien.put("station_code", "statest");
        mapParamsAmphibien.put("date_de_sortie", "12-12-2017");
        mapParamsAmphibien.put("total_ind", "42");

        Amphibien amphibien = new Amphibien(mapParamsAmphibien);

        List<DisplayableData> list = new ArrayList<>();
        list.add(amphibien);

        Database.importObjectsToDatabase(list, context);

        Position pr = new Position(3.435,48.423452);
        Assert.assertEquals(1, countMap(Database.getAllObjectsAtDistanceByType(pr, 1, context)));
    }

    @Test
    public void addObjectAnotherPosition() {
        Database.deleteAllInPark("PARKTEST", context);
        Database.deleteData(context);

        Map<String, String> mapParamsAmphibien = new HashMap<>();
        mapParamsAmphibien.put("taxon", "amphibien proche");
        mapParamsAmphibien.put("latitude", "2.4193485920");
        mapParamsAmphibien.put("longitude", "42.194529682459");
        mapParamsAmphibien.put("station_code", "statest");
        mapParamsAmphibien.put("date_de_sortie", "12-12-2017");
        mapParamsAmphibien.put("total_ind", "42");

        Amphibien amphibien = new Amphibien(mapParamsAmphibien);

        Map<String, String> mapParamsAmphibienLoin = new HashMap<>();
        mapParamsAmphibienLoin.put("taxon", "amphibien loin");
        mapParamsAmphibienLoin.put("latitude", "3.4193485920");
        mapParamsAmphibienLoin.put("longitude", "42.194529682459");
        mapParamsAmphibienLoin.put("station_code", "statest");
        mapParamsAmphibienLoin.put("date_de_sortie", "12-12-2017");
        mapParamsAmphibienLoin.put("total_ind", "42");

        Amphibien amphibienLoin = new Amphibien(mapParamsAmphibienLoin);

        Map<String, String> mapParamsInvertebre = new HashMap<>();
        mapParamsInvertebre.put("taxon", "invertebre");
        mapParamsInvertebre.put("latitude", "2.4193485920");
        mapParamsInvertebre.put("longitude", "42.194529682459");
        mapParamsInvertebre.put("station_code", "statest");
        mapParamsInvertebre.put("date_de_sortie", "12-12-2017");
        mapParamsInvertebre.put("total_ind", "42");

        Invertebre inv = new Invertebre(mapParamsInvertebre);

        Map<String, String> mapFlore = new HashMap<>();
        mapFlore.put("date_debut", "21-12-2017");
        mapFlore.put("date_fin", "22-12-2017");
        mapFlore.put("observation_rmq", "obs");
        mapFlore.put("longitude", "42.194529682459");
        mapFlore.put("latitude", "2.4193485920");

        Map<String, List<String>> mapFloreEspeces = new HashMap<>();
        List<String> nameList = new ArrayList<>();
        nameList.add("fleur1");
        nameList.add("fleur2");
        mapFloreEspeces.put("names_sp", nameList);

        Flore flore = new Flore(mapFlore, mapFloreEspeces);

        Map<String, String> mapHydroBioTax = new HashMap<>();
        mapHydroBioTax.put("taxon", "poisson");
        mapHydroBioTax.put("resultat", "21");
        mapHydroBioTax.put("dtprodresultatbiologique", "2017-12-22");
        mapHydroBioTax.put("preleveur", "obs");
        mapHydroBioTax.put("nomstation", "hydrobio_station");
        mapHydroBioTax.put("longitude", "42.194529682459");
        mapHydroBioTax.put("latitude", "2.4193485920");

        HydrobiologieTax hydrobiologieTax = new HydrobiologieTax(mapHydroBioTax);

        Map<String, String> mapHydroBioPar = new HashMap<>();
        mapHydroBioPar.put("parametre", "poiscaille");
        mapHydroBioPar.put("resultat", "22");
        mapHydroBioPar.put("dtprodresultatbiologique", "2017-12-22");
        mapHydroBioPar.put("preleveur", "obs");
        mapHydroBioPar.put("nomstation", "hydrobio_station");
        mapHydroBioPar.put("longitude", "42.194529682459");
        mapHydroBioPar.put("latitude", "2.4193485920");

        HydrobiologiePar hydrobiologiePar = new HydrobiologiePar(mapHydroBioPar);

        Map<String, String> mapPhysicochimie = new HashMap<>();
        mapPhysicochimie.put("parametre", "physico");
        mapPhysicochimie.put("latitude", "2.4193485920");
        mapPhysicochimie.put("longitude", "42.194529682459");
        mapPhysicochimie.put("lsana", "0.342");
        mapPhysicochimie.put("ldana", "0.342");
        mapPhysicochimie.put("rsana", "0.342");
        mapPhysicochimie.put("laboratoire", "Labo");
        mapPhysicochimie.put("code_remarque", "Remarque");
        mapPhysicochimie.put("methode", "Methode");
        mapPhysicochimie.put("producteur", "JeanMi");
        mapPhysicochimie.put("lqana", "0.342");
        mapPhysicochimie.put("dateprel", "2018-02-17");
        mapPhysicochimie.put("station_nom", "station");

        Physicochimie physicochimie = new Physicochimie(mapPhysicochimie);

        Map<String, String> mMac = new HashMap<>();
        mMac.put("taxon", "mac");
        mMac.put("latitude", "2.4193485920");
        mMac.put("longitude", "42.194529682459");
        mMac.put("station_code", "statest");
        mMac.put("date_de_sortie", "12-12-2017");
        mMac.put("total_ind", "99");

        MacroInvertebre macroinv = new MacroInvertebre(mMac);

        List<DisplayableData> dd = new ArrayList<>();
        dd.add(physicochimie);
        dd.add(amphibien);
        dd.add(amphibienLoin); // This one is to far to be catched by the Database.getAllObjectsAtDistanceByType method
        dd.add(inv);
        dd.add(macroinv);
        dd.add(flore);
        dd.add(hydrobiologieTax);
        dd.add(hydrobiologiePar);


        Database.importObjectsToDatabase(dd, context);

        Position searchPosition = new Position(2.4193485920, 42.194529682459);
        Map<String, List<? extends DisplayableData>> map = Database.getAllObjectsAtDistanceByType(searchPosition, 100, context);

        for (Map.Entry<String, List<? extends DisplayableData>> entry: map.entrySet()) {
            Log.e("MAP", entry.getValue().toString());
        }

        Assert.assertEquals(7, countMap(map));
    }

    @Test
    public void deleteAll() {
        Database.deleteAllInPark("PARKTEST", context);

        Map<String, String> mapHydroBioTax = new HashMap<>();
        mapHydroBioTax.put("taxon", "poisson");
        mapHydroBioTax.put("resultat", "21");
        mapHydroBioTax.put("dtprodresultatbiologique", "2017-12-22");
        mapHydroBioTax.put("preleveur", "obs");
        mapHydroBioTax.put("nomstation", "hydrobio_station");
        mapHydroBioTax.put("longitude", "42.194529682459");
        mapHydroBioTax.put("latitude", "2.4193485920");

        HydrobiologieTax hydrobiologieTax = new HydrobiologieTax(mapHydroBioTax);

        Map<String, String> mapHydroBioPar = new HashMap<>();
        mapHydroBioPar.put("parametre", "poiscaille");
        mapHydroBioPar.put("resultat", "22");
        mapHydroBioPar.put("dtprodresultatbiologique", "2017-12-22");
        mapHydroBioPar.put("preleveur", "obs");
        mapHydroBioPar.put("nomstation", "hydrobio_station");
        mapHydroBioPar.put("longitude", "42.194529682459");
        mapHydroBioPar.put("latitude", "2.4193485920");

        HydrobiologiePar hydrobiologiePar = new HydrobiologiePar(mapHydroBioPar);

        List<DisplayableData> dd = new ArrayList<>();
        dd.add(hydrobiologieTax);
        dd.add(hydrobiologiePar);

        Database.importObjectsToDatabase(dd, context);

        Position searchPosition = new Position(2.4193485920, 42.194529682459);
        Map<String, List<? extends DisplayableData>> map = Database.getAllObjectsAtDistanceByType(searchPosition, 100, context);
        Assert.assertEquals(2, countMap(map));

        //Ajout de donnees hors du park

        Database.deleteData(context);
        Map<String, List<? extends DisplayableData>> mapEmpty = Database.getAllObjectsAtDistanceByType(searchPosition, 100, context);
        Assert.assertEquals(0, countMap(mapEmpty));

    }

    @Test
    public void insertInPark() {
        Database.deleteData(context);
        Database.deleteAllInPark("PARKTEST", context);

        Map<String, String> mapHydroBioTax = new HashMap<>();
        mapHydroBioTax.put("taxon", "poisson");
        mapHydroBioTax.put("resultat", "21");
        mapHydroBioTax.put("dtprodresultatbiologique", "2017-12-22");
        mapHydroBioTax.put("preleveur", "obs");
        mapHydroBioTax.put("nomstation", "hydrobio_station");
        mapHydroBioTax.put("longitude", "42.194529682459");
        mapHydroBioTax.put("latitude", "2.4193485920");

        HydrobiologieTax hydrobiologieTax = new HydrobiologieTax(mapHydroBioTax);

        Map<String, String> mapHydroBioPar = new HashMap<>();
        mapHydroBioPar.put("parametre", "poiscaille");
        mapHydroBioPar.put("resultat", "22");
        mapHydroBioPar.put("dtprodresultatbiologique", "2017-12-22");
        mapHydroBioPar.put("preleveur", "obs");
        mapHydroBioPar.put("nomstation", "hydrobio_station");
        mapHydroBioPar.put("longitude", "42.194529682459");
        mapHydroBioPar.put("latitude", "2.4193485920");

        HydrobiologiePar hydrobiologiePar = new HydrobiologiePar(mapHydroBioPar);

        List<DisplayableData> dd = new ArrayList<>();
        dd.add(hydrobiologieTax);
        dd.add(hydrobiologiePar);

        Database.addDataInPark(dd, "PARKTEST", context);

        Position searchPosition = new Position(2.4193485920, 42.194529682459);
        Map<String, List<? extends DisplayableData>> map = Database.getAllObjectsAtDistanceByType(searchPosition, 100, context);
        Assert.assertEquals(2, countMap(map));

        //Ajout de donnees hors du park

        Database.deleteAllInPark("PARKTEST", context);
        Map<String, List<? extends DisplayableData>> mapEmpty = Database.getAllObjectsAtDistanceByType(searchPosition, 100, context);
        Assert.assertEquals(0, countMap(mapEmpty));

    }

    @Test
    public void deleteWhenParkSet() {
        Database.deleteAllInPark("PARKTEST", context);

        Map<String, String> mapParamsAmphibien = new HashMap<>();
        mapParamsAmphibien.put("taxon", "amphibien proche");
        mapParamsAmphibien.put("latitude", "2.4193485920");
        mapParamsAmphibien.put("longitude", "42.194529682459");
        mapParamsAmphibien.put("station_code", "statest");
        mapParamsAmphibien.put("date_de_sortie", "12-12-2017");
        mapParamsAmphibien.put("total_ind", "42");

        Amphibien amphibien = new Amphibien(mapParamsAmphibien);

        Map<String, String> mapParamsInvertebre = new HashMap<>();
        mapParamsInvertebre.put("taxon", "invertebre");
        mapParamsInvertebre.put("latitude", "2.4193485920");
        mapParamsInvertebre.put("longitude", "42.194529682459");
        mapParamsInvertebre.put("station_code", "statest");
        mapParamsInvertebre.put("date_de_sortie", "12-12-2017");
        mapParamsInvertebre.put("total_ind", "42");

        Invertebre inv = new Invertebre(mapParamsInvertebre);

        List<DisplayableData> dd = new ArrayList<>();
        dd.add(amphibien);
        dd.add(inv);


        Map<String, String> mapFlore = new HashMap<>();
        mapFlore.put("date_debut", "21-12-2017");
        mapFlore.put("date_fin", "22-12-2017");
        mapFlore.put("observation_rmq", "obs");
        mapFlore.put("longitude", "42.194529682459");
        mapFlore.put("latitude", "2.4193485920");

        Map<String, List<String>> mapFloreEspeces = new HashMap<>();
        List<String> nameList = new ArrayList<>();
        nameList.add("fleur1");
        nameList.add("fleur2");
        mapFloreEspeces.put("names_sp", nameList);

        Flore flore = new Flore(mapFlore, mapFloreEspeces);
        List<DisplayableData> ddpark = new ArrayList<>();
        ddpark.add(flore);

        Database.importObjectsToDatabase(dd, context);

        Database.addDataInPark(ddpark, "PARKTEST", context);

        Position searchPosition = new Position(2.4193485920, 42.194529682459);
        Map<String, List<? extends DisplayableData>> map = Database.getAllObjectsAtDistanceByType(searchPosition, 100, context);
        Assert.assertEquals(3, countMap(map));

        Database.deleteData(context);

        Map<String, List<? extends DisplayableData>> mapPark = Database.getAllObjectsAtDistanceByType(searchPosition, 100, context);
        Assert.assertEquals(1, countMap(mapPark));

    }

    @Test
    public void doubleInsertInPark() {
        Database.deleteAllInPark("PARKTEST", context);
        Database.deleteData(context);

        Map<String, String> mapParamsInvertebre = new HashMap<>();
        mapParamsInvertebre.put("taxon", "invertebre");
        mapParamsInvertebre.put("latitude", "2.4193485920");
        mapParamsInvertebre.put("longitude", "42.194529682459");
        mapParamsInvertebre.put("station_code", "statest");
        mapParamsInvertebre.put("date_de_sortie", "12-12-2017");
        mapParamsInvertebre.put("total_ind", "42");

        Invertebre inv = new Invertebre(mapParamsInvertebre);

        List<DisplayableData> dd = new ArrayList<>();
        dd.add(inv);
        dd.add(inv);

        Database.addDataInPark(dd, "PARKTEST", context);

        Cursor c = Database.getInstance(context).getWritableDatabase().rawQuery("SELECT * FROM invertebre WHERE park_fk LIKE '%PARKTEST%';", null);

        Assert.assertEquals(1, c.getCount());
    }

    @Test
    public void doubleInsert() {
        Database.deleteAllInPark("PARKTEST", context);
        Database.deleteData(context);

        Map<String, String> mapParamsInvertebre = new HashMap<>();
        mapParamsInvertebre.put("taxon", "invertebre");
        mapParamsInvertebre.put("latitude", "2.4193485920");
        mapParamsInvertebre.put("longitude", "42.194529682459");
        mapParamsInvertebre.put("station_code", "statest");
        mapParamsInvertebre.put("date_de_sortie", "12-12-2017");
        mapParamsInvertebre.put("total_ind", "42");

        Invertebre inv = new Invertebre(mapParamsInvertebre);

        List<DisplayableData> dd = new ArrayList<>();
        dd.add(inv);
        dd.add(inv);

        Database.importObjectsToDatabase(dd, context);

        Cursor c = Database.getInstance(context).getWritableDatabase().rawQuery("SELECT * FROM invertebre WHERE park_fk LIKE '';", null);

        Assert.assertEquals(1, c.getCount());
    }

    private int countMap(Map<String, List<? extends  DisplayableData>> map) {
        int total = 0;
        total += map.get("hydrobiologie").size();
        total += map.get("flore").size();
        total += map.get("biologie").size();
        total += map.get("physicochimie").size();
        return total;
    }

    @Test
    public void addAndGetParksNames() {
        Database.deleteAllInPark("PARKTEST", context);
        Database.deleteData(context);

        Map<String, String> mapParamsInvertebre = new HashMap<>();
        mapParamsInvertebre.put("taxon", "invertebre");
        mapParamsInvertebre.put("latitude", "2.4193485920");
        mapParamsInvertebre.put("longitude", "42.194529682459");
        mapParamsInvertebre.put("station_code", "statest");
        mapParamsInvertebre.put("date_de_sortie", "12-12-2017");
        mapParamsInvertebre.put("total_ind", "42");

        Invertebre inv = new Invertebre(mapParamsInvertebre);

        List<DisplayableData> dd = new ArrayList<>();
        dd.add(inv);

        Database.addDataInPark(dd, "PARKTEST", context);

        List<String> list = Database.getSavedParks(context);

        Assert.assertEquals(1, list.size());
    }

    @Test
    public void deleteAndGetParksNames() {
        Database.deleteAllInPark("PARKTEST", context);
        Database.deleteAllInPark("PARKTEST2", context);
        Database.deleteData(context);

        Map<String, String> mapParamsInvertebre = new HashMap<>();
        mapParamsInvertebre.put("taxon", "invertebre");
        mapParamsInvertebre.put("latitude", "2.4193485920");
        mapParamsInvertebre.put("longitude", "42.194529682459");
        mapParamsInvertebre.put("station_code", "statest");
        mapParamsInvertebre.put("date_de_sortie", "12-12-2017");
        mapParamsInvertebre.put("total_ind", "42");

        Invertebre inv = new Invertebre(mapParamsInvertebre);

        List<DisplayableData> dd = new ArrayList<>();
        dd.add(inv);

        Database.addDataInPark(dd, "PARKTEST", context);

        Map<String, String> mapParamsInvertebre2 = new HashMap<>();
        mapParamsInvertebre2.put("taxon", "invertebre2");
        mapParamsInvertebre2.put("latitude", "2.4193485920");
        mapParamsInvertebre2.put("longitude", "42.194529682459");
        mapParamsInvertebre2.put("station_code", "statest");
        mapParamsInvertebre2.put("date_de_sortie", "12-12-2017");
        mapParamsInvertebre2.put("total_ind", "42");

        Invertebre inv2 = new Invertebre(mapParamsInvertebre2);

        List<DisplayableData> dd2 = new ArrayList<>();
        dd2.add(inv2);

        Database.addDataInPark(dd2, "PARKTEST2", context);

        List<String> list = Database.getSavedParks(context);

        Assert.assertEquals(2, list.size());

        Database.deleteAllInPark("PARKTEST2", context);

        list = Database.getSavedParks(context);

        Assert.assertEquals(1, list.size());
    }

    /**
     * WARN THESE TESTS NEEDS A FRESHLY INSTALLED APPLICATION TO RUN SUCCESSFULLY!
     */
    @Test
    public void testToggle() {
        UserParamDB udb = new UserParamDB(context);
        boolean newValue = udb.toggleHydrobiologie();
        Assert.assertEquals(false, newValue);
        udb.close();
    }

    @Test
    public void deleteAndGetUserMail() {
        UserParamDB udb = new UserParamDB(context);
        udb.deleteUserMail();
        String mail = udb.getUserMail();
        Assert.assertEquals(null, mail);
        udb.close();
    }

    @Test
    public void setAndGetUSerMail() {
        UserParamDB udb = new UserParamDB(context);
        String mail = "toto@eiko.fr";
        udb.setUserMail(mail);
        String mailGet = udb.getUserMail();
        Assert.assertEquals(mail, mailGet);
        udb.close();
    }

    @Test
    public void deleteUserMailAndUserLogged() {
        UserParamDB udb = new UserParamDB(context);
        udb.deleteUserMail();
        boolean userLog = udb.isUserLogged();
        Assert.assertEquals(false, userLog);
        udb.close();
    }

    @Test
    public void isUserLogged() {
        UserParamDB udb = new UserParamDB(context);
        String mail = "toto@eiko.fr";
        udb.setUserMail(mail);
        boolean userLog = udb.isUserLogged();
        Assert.assertEquals(true, userLog);
        udb.close();
    }

    @Test
    public void setAndGetRenderDistance() {
        UserParamDB udb = new UserParamDB(context);
        int setRender = 150;
        udb.setRenderDistance(setRender);
        int renderDistance = udb.getRenderDistance();
        Assert.assertEquals(setRender, renderDistance);
        udb.close();
    }
}
