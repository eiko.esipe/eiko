import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.eiko.PropertiesReader;
import com.eiko.downloads.AsyncTasks.MetaAndDataDownloaderAsyncTask;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.ExecutionException;

/**
 * Created by Rinkui on 14/02/2018.
 */

@RunWith(AndroidJUnit4.class)
public class DataDownloaderAsyncTaskTest {
    @Test
    public void doInBackgroundWrongArgument(){
        Context mContext = InstrumentationRegistry.getContext();
        PropertiesReader.getProReader(mContext);
        MetaAndDataDownloaderAsyncTask ddat = new MetaAndDataDownloaderAsyncTask();
        ddat.execute("toto");
        try {
            ddat.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void doInBackgroundNone(){
        Context mContext = InstrumentationRegistry.getContext();
        PropertiesReader.getProReader(mContext);
        MetaAndDataDownloaderAsyncTask ddat = new MetaAndDataDownloaderAsyncTask();
        ddat.execute();
        try {
            ddat.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void doInBackgroundNull(){
        Context mContext = InstrumentationRegistry.getTargetContext();
        PropertiesReader.getProReader(mContext);
        MetaAndDataDownloaderAsyncTask ddat = new MetaAndDataDownloaderAsyncTask();
        String s = null;
        ddat.execute(s);
        try {
            ddat.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}