import android.content.Context;
import android.provider.ContactsContract;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.eiko.DataObject.Amphibien;
import com.eiko.DataObject.DisplayableData;
import com.eiko.DataObject.Flore;
import com.eiko.DataObject.Physicochimie;
import com.eiko.PropertiesReader;
import com.eiko.database.Database;
import com.eiko.downloads.DataDownloader;
import com.eiko.geolocalization.Position;

import org.json.JSONArray;
import org.json.JSONException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.Map;

/**
 * Created by Rinkui on 14/02/2018.
 */

@RunWith(AndroidJUnit4.class)
public class DataDownloaderAPITest {
    @Test
    public void downloadsSectorNull(){
        Context mContext = InstrumentationRegistry.getContext();
        PropertiesReader.getProReader(mContext);
        DataDownloader.getDataDownloader().downloadsSectors(null);
    }

    @Test
    public void downloadsSectorFakePosition(){
        Context mContext = InstrumentationRegistry.getContext();
        PropertiesReader.getProReader(mContext);
        DataDownloader.getDataDownloader().downloadsSectors(new Position(0,0));
    }

    @Test
    public void insertInDatabase(){
        Context mContext = InstrumentationRegistry.getTargetContext();
        String s = "[\n" +
                "   {\n" +
                "      \"data\":[\n" +
                "         {\n" +
                "         \"parametre\": \"1,2,3,4,6,7,8,9-Octachlorodibenzo-p-dioxine\",\n" +
                "         \"station_geom\": {\n" +
                "            \"type\": \"Point\",\n" +
                "            \"coordinates\": [\n" +
                "               -61.217148,\n" +
                "               14.830727\n" +
                "            ]\n" +
                "         },\n" +
                "         \"station_nom\": \"Amont Prise canal habitation Céron\",\n" +
                "         \"nb\": 4,\n" +
                "         \"station_code\": \"08014101\",\n" +
                "         \"geom\": {\n" +
                "            \"type\": \"Point\",\n" +
                "            \"coordinates\": [\n" +
                "               -61.220753,\n" +
                "               14.829391\n" +
                "            ]\n" +
                "         },\n" +
                "         \"point_prelevement\": \"Non renseigné\",\n" +
                "         \"analyses\": [\n" +
                "            {\n" +
                "               \"resultats\": [\n" +
                "                  {\n" +
                "                     \"lsana\": \"\",\n" +
                "                     \"ldana\": \"\",\n" +
                "                     \"rsana\": \"0.4\",\n" +
                "                     \"laboratoire\": \"Laboratoire Départemental d'Analyses de la Drôme\",\n" +
                "                     \"code_remarque\": \"Domaine de validité\",\n" +
                "                     \"methode\": \"Méthode inconnue\",\n" +
                "                     \"producteur\": \"Office de l'Eau de la Martinique\",\n" +
                "                     \"lqana\": \"\",\n" +
                "                     \"dateprel\": \"2011-05-18\"\n" +
                "                  },\n" +
                "                  {\n" +
                "                     \"lsana\": \"\",\n" +
                "                     \"ldana\": \"\",\n" +
                "                     \"rsana\": \"0.2\",\n" +
                "                     \"laboratoire\": \"Laboratoire Départemental d'Analyses de la Drôme\",\n" +
                "                     \"code_remarque\": \"< seuil de détection\",\n" +
                "                     \"methode\": \"Méthode inconnue\",\n" +
                "                     \"producteur\": \"Office de l'Eau de la Martinique\",\n" +
                "                     \"lqana\": \"\",\n" +
                "                     \"dateprel\": \"2012-05-16\"\n" +
                "                  }\n" +
                "               ],\n" +
                "               \"type_analyse\": {\n" +
                "                  \"fraction_analysee\": \"Particule < 2 mm de sédiments\",\n" +
                "                  \"nb\": 2,\n" +
                "                  \"unite\": \"µg/kg\"\n" +
                "               }\n" +
                "            }\n" +
                "         ],\n" +
                "         \"cdparametre\": \"2566\",\n" +
                "         \"altitude\": null\n" +
                "      }\n" +
                "      ],\n" +
                "      \"metadata\":{\n" +
                "         \"y\":49.8183316109004,\n" +
                "         \"x\":2.39369319605958,\n" +
                "         \"radius\":50,\n" +
                "         \"group\":\"eiko_physicochimie\",\n" +
                "         \"class\":\"12365\"\n" +
                "      }\n" +
                "   }\n" +
                "]";
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(s);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        List<? extends DisplayableData> list = Physicochimie.fromJSON(jsonArray);
        Database.importObjectsToDatabase(list, mContext);
        Map<String, List<? extends DisplayableData>> map = Database.getAllObjectsAtDistanceByType( new Position(14.830, -61.229),2500, mContext);
        Assert.assertTrue(map.containsValue(list));
    }
}
